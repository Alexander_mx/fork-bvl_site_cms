export interface IBusiness {
  business: string;
}

export interface IDocumentType {
  code: string;
  description: string;
}

export interface ISearchRequest {
  filter: string;
}

export interface ISearchResponse {
  path: string;
  title: string;
  metadata: ISearchMetadataResponse;
}

export interface ISearchMetadataResponse {
  title: string;
  description: string;
  autor: string;
  subject: string;
  language: string;
}

export interface IKeysConfiguration {
  googleMaps: string;
  gtm: string;
}
