export interface IContactRequest {
  fullName: string;
  subject: string;
  phone: string;
  message: string;
  email: string;
}

export interface IContactResponse {
  id: string;
  fullName: string;
  subject: string;
  phone: string;
  message: string;
  email: string;
  createdDate: string;
}
