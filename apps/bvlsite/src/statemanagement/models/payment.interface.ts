export interface ICancelSubscriptionRequest {
  idUserCognito: string;
  idSubscription: string;
}

export interface ISubscriptionResponse {
  metadata: {};
  cancel_at_period_end: boolean;
  trial_end: number;
  creation_date: number;
  total_period: number;
  current_period_end: number;
  current_period: number;
  charges: Array<any>;
  trial_start: number;
  id: string;
  plan: IPlanResponse;
  current_period_start: number;
  ended_at: number;
  card: any;
  object: string;
  status: string;
  cancel_at: any;
  next_billing_date: number;
}

export interface IPlanResponse {
  interval_count: number;
  amount: number;
  metadata: {};
  name: string;
  limit: number;
  interval: string;
  id: string;
  creation_date: number;
  trial_days: number;
  total_subscriptions: number;
  currency_code: string;
  object: string;
}

export interface ICustomer {
  firstName: string;
  lastName: string;
  email: string;
  address: string;
  addressCity: string;
  countryCode: string;
  phoneNumber: string;
  invoice: boolean;
  ruc: string;
  companyName: string;
}

export interface ICheckoutRequest {
  idCognito: string;
  customer: ICustomer;
  idToken: string;
  idPlan: string;
}
