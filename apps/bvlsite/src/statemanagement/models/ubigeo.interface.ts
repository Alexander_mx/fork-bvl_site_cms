export interface IUbigeo {
  id: string;
  name: string;
}

export interface IRegionResponse extends IUbigeo { }

export interface IProvinceResponse extends IRegionResponse {
  idRegion: string;
}

export interface IDistrictResponse extends IProvinceResponse {
  idProvince: string;
}
