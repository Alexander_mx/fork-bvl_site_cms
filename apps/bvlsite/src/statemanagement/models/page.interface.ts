import { ISocialNetworking } from '@bvl-admin/statemanagement/models/configuration.interface';

export interface IMetadata {
  title?: string;
  description?: string;
  robots?: string;
  nositelinksearchbox?: string;
  nosnippet?: string;
  autor?: string;
  subject?: string;
  language?: string;
  revisitafter?: string;
  titleSnippet?: string;
  ogType?: string;
  ogUrl?: string;
  ogTitle?: string;
  ogDescription?: string;
  ogImage?: string;
  ogSitename?: string;
  twitterSite?: string;
  twitterCard?: string;
  twitterTitle?: string;
  twitterDescription?: string;
  twitterImage?: string;
  canonicalLink?: string;
  noindex?: string;
  nofollow?: string;
  avancedRobots?: Array<string>;
}

export interface IPathPage {
  language: string;
  path: string;
}

export interface IPageContentLanguage {
  language: string;
  canonical: string;
  path: string;
  pagePath: string;
  parentPath: string;
  metadata: IMetadata;
  title: string;
  category: string;
  tags: Array<string>;
  content: IContent;
  name?: string;
  data?: any;
}

export interface IContent {
  html: string;
  refComponents: Array<string>;
  data?: any;
}

export interface IConfiguration {
  icon?: string;
  socialNetworking?: ISocialNetworking;
}

export interface IPageResponse {
  name: string;
  pageType: number;
  type: number;
  id?: string;
  languages: Array<IPageContentLanguage>;
  paths: Array<IPathPage>;
  configuration?: IConfiguration;
}
