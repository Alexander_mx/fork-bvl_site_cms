import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import {
  BvlFormAlertModule,
  BvlFormCheckboxModule,
  BvlFormInputModule,
  BvlFormRadioModule,
  BvlFormSelectModule,
  SectionHeaderModule
} from '@bvl/library';
import { TermsConditionsModule } from '../modals/terms-conditions/terms-conditions.module';
import { SignUpFormComponent } from './sign-up-form.component';

@NgModule({
  declarations: [SignUpFormComponent],
  imports: [
    CommonModule,
    SectionHeaderModule,
    BvlFormAlertModule,
    BvlFormRadioModule,
    BvlFormInputModule,
    BvlFormSelectModule,
    ReactiveFormsModule,
    ControlErrorModule,
    BvlFormCheckboxModule,
    TermsConditionsModule,
    ModalModule
  ],
  exports: [SignUpFormComponent],
  entryComponents: [SignUpFormComponent]
})
export class SignUpFormModule { }
