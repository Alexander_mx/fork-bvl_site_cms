import { Component, ElementRef, OnInit, Optional } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { COGNITO_ERROR_CODE, ICognitoError } from '@bvl-core/shared/helpers/aws-amplfy';
import { ActiveModal, IModalConfig, IPayloadModalComponent, ModalService } from '@bvl-core/shared/helpers/modal';
import { ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { TermsConditionsComponent } from '@bvl-site/components/modals/terms-conditions/terms-conditions.component';
import { AuthService, ConfigurationService, GeneralService } from '@bvl-site/services';
import { GA_SIGN_UP_BUTTON, GA_SIGN_UP_TERMS_COND } from '@bvl-site/settings/constants/ga.constant';
import { DOCUMENT_TYPE, FORM_TYPE, PERSON_TYPE } from '@bvl-site/settings/constants/general.constant';
import { ISignUpRequest, IUserAttributes } from '@bvl-site/statemanagement/models/auth.interface';
import { IBusiness, IDocumentType } from '@bvl-site/statemanagement/models/general.interface';
import { GaUnsubscribeBase } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss']
})
export class SignUpFormComponent extends GaUnsubscribeBase implements OnInit, IPayloadModalComponent {

  modalPayload: IUserAttributes;

  FORM_TYPE = FORM_TYPE;
  PERSON_TYPE = PERSON_TYPE;
  DOCUMENT_TYPE = DOCUMENT_TYPE;

  frmSignUp: FormGroup;
  mPersonType: AbstractControl;
  /** Natural */
  mNames: AbstractControl;
  mFathersSurname: AbstractControl;
  mMothersSurname: AbstractControl;
  mDocumentType: AbstractControl;
  mDocumentNumber: AbstractControl;
  /** Jurídica */
  mLegalName: AbstractControl;
  mCoreBusiness: AbstractControl;
  mRepresentative: AbstractControl;
  mRucNumber: AbstractControl;
  /** */
  mEmail: AbstractControl;
  mPassword: AbstractControl;
  mConfirmPassword: AbstractControl;
  mTerms: AbstractControl;

  errorMessage: string;
  businessCoreList: Array<IBusiness>;

  constructor(
    protected _element: ElementRef,
    private _formBuilder: FormBuilder,
    private _generalService: GeneralService,
    private _router: Router,
    @Optional() private _activeModal: ActiveModal,
    private _configurationService: ConfigurationService,
    private _authService: AuthService,
    private _modalService: ModalService
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this._createForm();
    this._businessCoreList();
    if (this.modalPayload) { this._confirm(); }
  }

  private _createForm(): void {
    this.frmSignUp = this._formBuilder.group({
      mPersonType: [this.PERSON_TYPE.natural, Validators.required],
      mNames: ['', [Validators.required, ValidatorUtil.onlyLetter()]],
      mFathersSurname: ['', [Validators.required, ValidatorUtil.onlyLetter()]],
      mMothersSurname: ['', [ValidatorUtil.onlyLetter()]],
      mDocumentType: ['', Validators.required],
      mDocumentNumber: ['', Validators.required],
      mLegalName: ['', Validators.required],
      mCoreBusiness: ['', Validators.required],
      mRepresentative: ['', [Validators.required, ValidatorUtil.onlyLetter()]],
      mRucNumber: ['', [Validators.required, ValidatorUtil.onlyNumber(), Validators.minLength(11), Validators.maxLength(11)]],
      mEmail: [''],
      mPassword: [''],
      mConfirmPassword: [''],
      mTerms: [false]
    });

    this.mPersonType = this.frmSignUp.get('mPersonType');
    this.mNames = this.frmSignUp.get('mNames');
    this.mFathersSurname = this.frmSignUp.get('mFathersSurname');
    this.mMothersSurname = this.frmSignUp.get('mMothersSurname');
    this.mDocumentType = this.frmSignUp.get('mDocumentType');
    this.mDocumentNumber = this.frmSignUp.get('mDocumentNumber');
    this.mLegalName = this.frmSignUp.get('mLegalName');
    this.mCoreBusiness = this.frmSignUp.get('mCoreBusiness');
    this.mRepresentative = this.frmSignUp.get('mRepresentative');
    this.mRucNumber = this.frmSignUp.get('mRucNumber');
    this.mEmail = this.frmSignUp.get('mEmail');
    this.mPassword = this.frmSignUp.get('mPassword');
    this.mConfirmPassword = this.frmSignUp.get('mConfirmPassword');
    this.mTerms = this.frmSignUp.get('mTerms');

    if (!this.modalPayload) {
      this.mEmail.setValidators([Validators.required, Validators.email]);
      this.mPassword.setValidators(
        [ Validators.required, ValidatorUtil.passwordPolicy(), ValidatorUtil.confirmPassword(this.mConfirmPassword, true)]
      );
      this.mConfirmPassword.setValidators([Validators.required, ValidatorUtil.confirmPassword(this.mPassword)]);
      this.mTerms.setValidators(Validators.requiredTrue);
      const ctrls = ['mEmail', 'mPassword', 'mConfirmPassword', 'mTerms', 'frmSignUp'];
      for (const ctrl of ctrls) {
        this[ctrl].updateValueAndValidity();
      }
    } else {
      this._setControlsValue(this.modalPayload);
    }
  }

  private _setControlsValue(userAttributes: IUserAttributes): void {
    this.mPersonType.setValue(userAttributes['custom:personType']);
    this.mNames.setValue(userAttributes['custom:names'] || '');
    this.mLegalName.setValue(userAttributes['custom:names'] || '');
    this.mFathersSurname.setValue(userAttributes['custom:paternalSurname'] || '');
    this.mMothersSurname.setValue(userAttributes['custom:maternalSurname'] || '');
    this.mDocumentType.setValue(
      (userAttributes['custom:documentType'])
        ? { code: userAttributes['custom:documentType'], description: userAttributes['custom:documentType'] }
        : '');
    this.mDocumentNumber.setValue(userAttributes['custom:documentNumber']);
    this.mRucNumber.setValue(userAttributes['custom:documentNumber']);
    this.mCoreBusiness.setValue((userAttributes['custom:coreBusiness'])
                                  ? { business: userAttributes['custom:coreBusiness'] }
                                  : '');
    this.mRepresentative.setValue(userAttributes['custom:representative'] || '');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmSignUp, controlName);
  }

  changeDocumentType(documentType: IDocumentType): void {
    const validatorsDocumentNumber = [Validators.required].concat(ValidatorUtil.validatorsByDocumentType(documentType.code));
    this.mDocumentNumber.setValidators(validatorsDocumentNumber);
    this.mDocumentNumber.updateValueAndValidity();

    const validatorsMothersSurname = [ValidatorUtil.onlyLetter()].concat((documentType.code === 'DNI') ? Validators.required : []);
    this.mMothersSurname.setValidators(validatorsMothersSurname);
    this.mMothersSurname.updateValueAndValidity();
  }

  private _businessCoreList(): void {
    this._generalService.businessCoreList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IBusiness>) => {
        this.businessCoreList = response;
      });
  }

  private _setValidateByPersonType(personType: string): void {
    const controlsErrorNull = (personType === this.PERSON_TYPE.natural)
                                ? ['mLegalName', 'mCoreBusiness', 'mRepresentative', 'mRucNumber']
                                : ['mNames', 'mFathersSurname', 'mMothersSurname', 'mDocumentType', 'mDocumentNumber'];

    for (const control in this.frmSignUp.controls) {
      if (controlsErrorNull.includes(control)) {
        this.frmSignUp.controls[control].setErrors(null);
      } else {
        this.frmSignUp.controls[control].updateValueAndValidity();
      }
    }
  }

  private _validateForm(): boolean {
    this._setValidateByPersonType(this.mPersonType.value);
    ValidatorUtil.validateForm(this.frmSignUp);

    return this.frmSignUp.valid;
  }

  private _paramsAttributes(): IUserAttributes {
    const getControlValue = (personType: string, controlValue: any, optionControlValue: any = '') => {
      return (this.mPersonType.value === personType)
        ? controlValue || ''
        : optionControlValue || '';
    };

    return {
      'custom:personType': this.mPersonType.value,
      'custom:names': getControlValue(this.PERSON_TYPE.natural, this.mNames.value, this.mLegalName.value),
      'custom:paternalSurname': getControlValue(this.PERSON_TYPE.natural, this.mFathersSurname.value),
      'custom:maternalSurname': getControlValue(this.PERSON_TYPE.natural, this.mMothersSurname.value),
      'custom:documentType': getControlValue(this.PERSON_TYPE.natural, this.mDocumentType.value.code, 'RUC'),
      'custom:documentNumber': getControlValue(this.PERSON_TYPE.natural, this.mDocumentNumber.value, this.mRucNumber.value),
      'custom:coreBusiness': getControlValue(this.PERSON_TYPE.legal, this.mCoreBusiness.value.business),
      'custom:representative': getControlValue(this.PERSON_TYPE.legal, this.mRepresentative.value)
    } as IUserAttributes;
  }

  private _paramsSignUp(): ISignUpRequest {
    return {
      username: this.mEmail.value,
      password: this.mPassword.value,
      attributes: this._paramsAttributes()
    } as ISignUpRequest;
  }

  private _singUp(): void {
    const params = this._paramsSignUp();
    this._authService.signUp(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._eventSignUp(true);
        this._router.navigate([], { queryParams: { confirmacion: true } });
      }, (error: ICognitoError) => {
        this._eventSignUp(false);
        this.errorMessage = COGNITO_ERROR_CODE[error.code] || COGNITO_ERROR_CODE.GeneralException;
      });
  }

  private _eventSignUp(success: boolean): void {
    const data = {
      state: `${(success) ? 'Exitoso' : 'Fallido'}`,
      tipoRegistro: this.mPersonType.value,
      tipoDocumento: this.mDocumentType.value.description
    };
    this.addEvent(GA_SIGN_UP_BUTTON, data);
  }

  private _edit(): void {
    const params = this._paramsAttributes();
    this._authService.updateUserAttributes(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        const userProfile = this._configurationService.getUserProfile();
        userProfile.attributes = { ...params };
        this._configurationService.setUserProfile(userProfile);
        this._activeModal.close(params);
      }, (error: ICognitoError) => {
        this.errorMessage = COGNITO_ERROR_CODE[error.code] || COGNITO_ERROR_CODE.GeneralException;
      });
  }

  buttonAction(): void {
    this.errorMessage = '';
    const validateForm = this._validateForm();
    if (validateForm) {
      if (!this.modalPayload) {
        this._singUp();
      } else {
        this._edit();
      }
    }
  }

  private _confirm(): void {
    this._activeModal.confirm()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this.buttonAction();
      });
  }

  termsConditions(): void {
    this.addEvent(GA_SIGN_UP_TERMS_COND);
    const modalConfig = {
      titleText: 'Términos y Condiciones',
      showFooter: false
    } as IModalConfig;
    this._modalService.open(TermsConditionsComponent, modalConfig).result
      .then(() => {
        return true;
      }, () => {
        return false;
      });
  }

  setEventsGA(): void { }

}
