import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IModalConfig, ModalService } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { TermsConditionsComponent } from '@bvl-site/components/modals/terms-conditions/terms-conditions.component';
import { environment_client } from '@bvl-site/environments/environment';
import { ContactService } from '@bvl-site/services/contact.service';
import { IContactRequest, IContactResponse } from '@bvl-site/statemanagement/models/contact.interface';
import { LANGUAGES } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-contact-us-form',
  templateUrl: './contact-us-form.component.html',
  styleUrls: ['./contact-us-form.component.scss']
})
export class ContactUsFormComponent extends UnsubscribeOnDestroy implements OnInit {

  frmContactUs: FormGroup;
  mName: AbstractControl;
  mPhone: AbstractControl;
  mEmail: AbstractControl;
  mSubject: AbstractControl;
  mMessage: AbstractControl;
  recaptchaKey: string;
  notification: any;
  responseMessage: string;
  messageType: string;
  mTerms: AbstractControl;

  TEXT: any;

  language: string;
  constructor(
    private _formBuilder: FormBuilder,
    private _contactService: ContactService,
    private _router: ActivatedRoute,
    private _modalService: ModalService
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
    this.TEXT = LANGUAGES[this._router.snapshot.data.lang];

  }

  private _createForm(): void {
    this.frmContactUs = this._formBuilder.group({
      mName: [null, [Validators.required, ValidatorUtil.onlyLetter()]],
      mPhone: [null, [Validators.required, ValidatorUtil.phoneNumber()]],
      mEmail: [null, [Validators.required, Validators.email]],
      mSubject: [null, Validators.required],
      mMessage: [null, Validators.required],
      mRecaptcha: [null, Validators.required],
      mTerms: [false, Validators.requiredTrue]
    });
    this.mName = this.frmContactUs.get('mName');
    this.mPhone = this.frmContactUs.get('mPhone');
    this.mEmail = this.frmContactUs.get('mEmail');
    this.mSubject = this.frmContactUs.get('mSubject');
    this.mMessage = this.frmContactUs.get('mMessage');
    this.mTerms = this.frmContactUs.get('mTerms');
    this.recaptchaKey = environment_client.RECAPTCHA_KEY;
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmContactUs, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmContactUs);

    return this.frmContactUs.valid;
  }

  send(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._paramsContact();
      this._contactService.saveContact(params)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe((response: IContactResponse) => {
          if (response.id) {
            this.responseMessage = this.TEXT.contactus.message.success;
            this.messageType = 'success';
            this.frmContactUs.reset();
          } else {
            this.messageType = 'error';
            this.responseMessage = this.TEXT.contactus.message.error;
          }
        });
    }
  }

  private _paramsContact(): IContactRequest {
    return {
      fullName: this.mName.value,
      subject: this.mSubject.value,
      phone: this.mPhone.value,
      message: this.mMessage.value,
      email: this.mEmail.value
    } as IContactRequest;
  }

  termsConditions(): void {
    const modalConfig = {
      titleText: 'Términos y Condiciones',
      showFooter: false
    } as IModalConfig;
    this._modalService.open(TermsConditionsComponent, modalConfig).result
      .then(() => {
        return true;
      }, () => {
        return false;
      });
  }

}
