import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlFormAlertModule, BvlFormCheckboxModule, BvlFormInputModule, BvlFormTextareaModule, SectionHeaderModule } from '@bvl/library';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { TermsConditionsModule } from '../modals/terms-conditions/terms-conditions.module';
import { ContactUsFormComponent } from './contact-us-form.component';

@NgModule({
  declarations: [ContactUsFormComponent],
  imports: [
    BvlFormAlertModule,
    BvlFormCheckboxModule,
    BvlFormInputModule,
    BvlFormTextareaModule,
    CommonModule,
    ControlErrorModule,
    ReactiveFormsModule,
    RecaptchaFormsModule,
    RecaptchaModule,
    SectionHeaderModule,
    TermsConditionsModule,
    ModalModule
  ],
  exports: [ContactUsFormComponent]
})
export class ContactUsFormModule { }
