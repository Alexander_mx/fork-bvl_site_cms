import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { BvlFormCheckboxModule, BvlFormInputModule, BvlFormSelectModule, SectionHeaderModule } from '@bvl/library';
import { CardPaymentFormComponent } from './card-payment-form.component';

@NgModule({
  declarations: [CardPaymentFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SectionHeaderModule,
    BvlFormInputModule,
    BvlFormSelectModule,
    BvlFormCheckboxModule,
    ControlErrorModule
  ],
  exports: [CardPaymentFormComponent]
})
export class CardPaymentFormModule { }
