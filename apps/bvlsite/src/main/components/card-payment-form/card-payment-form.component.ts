import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { ConfigurationService, UbigeoService } from '@bvl-site/services';
import { IDistrictResponse, IProvinceResponse, IRegionResponse } from '@bvl-site/statemanagement/models/ubigeo.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-card-payment-form',
  templateUrl: './card-payment-form.component.html',
  styleUrls: ['./card-payment-form.component.scss']
})
export class CardPaymentFormComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild('submitButton') submitButton: ElementRef<HTMLButtonElement>;

  frmCardPayment: FormGroup;
  mCardNumber: AbstractControl;
  mMM: AbstractControl;
  mYYYY: AbstractControl;
  mCVV: AbstractControl;
  mEmail: AbstractControl;
  mPhone: AbstractControl;
  mInvoice: AbstractControl;
  mRuc: AbstractControl;
  mLegalName: AbstractControl;
  mRegion: AbstractControl;
  mProvince: AbstractControl;
  mDistrict: AbstractControl;
  mAddress: AbstractControl;
  regionsList: Array<IRegionResponse>;
  provincesList: Array<IProvinceResponse>;
  districtsList: Array<IDistrictResponse>;

  constructor(
    private _formBuilder: FormBuilder,
    private _configurationService: ConfigurationService,
    private _ubigeoService: UbigeoService
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    const userProfile = this._configurationService.getUserProfile();
    this.frmCardPayment = this._formBuilder.group({
      mCardNumber: [null, [
        Validators.required,
        Validators.minLength(16),
        Validators.maxLength(16),
        ValidatorUtil.onlyNumber()]
      ],
      mMM: [null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(2),
        ValidatorUtil.onlyNumber()]
      ],
      mYYYY: [null, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(4),
        ValidatorUtil.onlyNumber()]
      ],
      mCVV: [null, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(3),
        ValidatorUtil.onlyNumber()]
      ],
      mEmail: [userProfile.attributes.email, [
        Validators.required,
        Validators.email]
      ],
      mPhone: [null, Validators.required],
      mInvoice: [false],
      mRuc: [null],
      mLegalName: [null],
      mRegion: [null, Validators.required],
      mProvince: [null, Validators.required],
      mDistrict: [null, Validators.required],
      mAddress: [null, Validators.required]
    });

    this.mCardNumber = this.frmCardPayment.get('mCardNumber');
    this.mMM = this.frmCardPayment.get('mMM');
    this.mYYYY = this.frmCardPayment.get('mYYYY');
    this.mCVV = this.frmCardPayment.get('mCVV');
    this.mEmail = this.frmCardPayment.get('mEmail');
    this.mPhone = this.frmCardPayment.get('mPhone');
    this.mInvoice = this.frmCardPayment.get('mInvoice');
    this.mRuc = this.frmCardPayment.get('mRuc');
    this.mLegalName = this.frmCardPayment.get('mLegalName');
    this.mRegion = this.frmCardPayment.get('mRegion');
    this.mProvince = this.frmCardPayment.get('mProvince');
    this.mDistrict = this.frmCardPayment.get('mDistrict');
    this.mAddress = this.frmCardPayment.get('mAddress');

    this._regionsList();
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmCardPayment, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmCardPayment);

    return this.frmCardPayment.valid;
  }

  private _regionsList(): void {
    this._ubigeoService.regionsList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IRegionResponse>) => {
        this.regionsList = response;
      });
  }

  getProvincesList(): void {
    const idRegion = this.mRegion.value.id;
    this._ubigeoService.provincesList(idRegion)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IProvinceResponse>) => {
        this.provincesList = response;
      });
  }

  getDistrictsList(): void {
    const idRegion = this.mRegion.value.id,
          idProvince = this.mProvince.value.id;
    this._ubigeoService.districtsList(idRegion, idProvince)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IDistrictResponse>) => {
        this.districtsList = response;
      });
  }

  changeInvoice(invoice: boolean): void {
    if (invoice) {
      this.mRuc.setValidators([Validators.required, Validators.minLength(11), Validators.maxLength(11)]);
      this.mLegalName.setValidators(Validators.required);
    } else {
      this.mRuc.setValidators([]);
      this.mLegalName.setValidators([]);
    }
    this.mRuc.updateValueAndValidity();
    this.mLegalName.updateValueAndValidity();
  }

  getForm(): string {
    this.submitButton.nativeElement.click();

    return (this._validateForm())
            ? this.frmCardPayment.getRawValue()
            : null;
  }

}
