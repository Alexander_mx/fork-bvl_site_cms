import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { OrderByPipeModule, SafeUrlPipeModule } from '@bvl-core/shared/helpers/pipes';
import { AccordionModule, DomChangeDirectiveModule, PipesModule } from '@bvl/library';
import { RealTimeModule } from 'modules/real-time/src/app/real-time.module';
import { FooterComponent } from './footer.component';

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    AccordionModule,
    RealTimeModule,
    OrderByPipeModule,
    SafeUrlPipeModule,
    DomChangeDirectiveModule
  ],
  declarations: [FooterComponent],
  exports: [FooterComponent]
})
export class FooterModule { }
