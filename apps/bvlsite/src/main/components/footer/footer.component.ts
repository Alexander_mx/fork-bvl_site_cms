import { Component, ElementRef, HostBinding, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { takeUntil } from 'rxjs/operators';

import { ISiteMenu } from '@bvl-core/shared/helpers/util/menu';
import { PageServices } from '@bvl-site/services';
import { GA_FOOTER_LINK, GA_FOOTER_SOCIAL_NETWORK } from '@bvl-site/settings/constants/ga.constant';
import { GaUnsubscribeBase } from '@bvl/library';

@Component({
  selector: 'bvl-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterComponent extends GaUnsubscribeBase implements OnInit {

  @HostBinding('attr.class') attrClass = 'g-sticky-footer';

  @Input() footer: Array<ISiteMenu>;

  socialNetworks: Array<any>;
  address: string;
  certifications: Array<any>;
  phoneNumbers: Array<string>;

  constructor(
    protected _element: ElementRef,
    private pageServices: PageServices
  ) {
    super(_element);
    this.ga = [
      GA_FOOTER_SOCIAL_NETWORK,
      GA_FOOTER_LINK
    ];

    this.address = 'Pasaje Acuña 106 - Lima 1, Perú';
    this.certifications = [
      {
        url: 'assets/images/certifications/sgs-iso-9001.png',
        alt: 'SGS - ISO 9001'
      },
      {
        url: 'assets/images/certifications/sgs-iso-22301.png',
        alt: 'SGS - ISO 22301'
      },
      {
        url: 'assets/images/certifications/sgs-iso-iec-27001.png',
        alt: 'SGS - ISO/IEC 27001'
      }
    ];
    this.phoneNumbers = ['619-3333', '619-3331'];
  }

  ngOnInit(): void {
    this.pageServices.getPageCurrenSub()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(page => {
        if (page && page.configuration && page.configuration.socialNetworking) {
          const socialNetworking: Array<any> = [];
          Object.keys(page.configuration.socialNetworking)
            .forEach(k => {
              if (page.configuration.socialNetworking[k]) {
                const url = k === 'urlTwitter'
                                  ? `https://twitter.com/${page.configuration.socialNetworking[k]}`
                                  : page.configuration.socialNetworking[k];
                socialNetworking.push({
                  name: k.substr(3),
                  url
                });
              }
            });
          this.socialNetworks = socialNetworking;
        }
      });
  }

}
