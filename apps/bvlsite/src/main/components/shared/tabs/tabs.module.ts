import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabContentComponent } from './tab-content/tab-content.component';
import { TabHeaderComponent } from './tab-header/tab-header.component';

@NgModule({
  declarations: [
    TabHeaderComponent,
    TabContentComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    TabHeaderComponent,
    TabContentComponent
  ]
})
export class TabsModule { }
