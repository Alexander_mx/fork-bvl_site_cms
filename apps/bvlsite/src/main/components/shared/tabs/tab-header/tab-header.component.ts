import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bvl-tab-header',
  templateUrl: './tab-header.component.html',
  styleUrls: ['./tab-header.component.scss']
})
export class TabHeaderComponent implements OnInit {

  @HostBinding('attr.class') attrClass = 'g-tabs';

  @Input() tabHeaderList: Array<any>;

  constructor() { }

  ngOnInit(): void { }

}
