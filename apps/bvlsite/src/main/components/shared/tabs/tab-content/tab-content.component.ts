import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'bvl-tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: ['./tab-content.component.scss']
})
export class TabContentComponent implements OnInit {

  @HostBinding('attr.class') attrClass = 'box g-p--25 g-p-md--35';

  constructor() { }

  ngOnInit(): void { }

}
