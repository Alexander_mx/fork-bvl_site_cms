import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { SectionHeaderModule } from '@bvl/library';
import { ChangePasswordFormModule } from '../change-password-form/change-password-form.module';
import { SignUpFormModule } from '../sign-up-form/sign-up-form.module';
import { PersonalDataCardComponent } from './personal-data-card.component';

@NgModule({
  declarations: [PersonalDataCardComponent],
  imports: [
    CommonModule,
    SectionHeaderModule,
    ModalModule,
    SignUpFormModule,
    ChangePasswordFormModule
  ],
  exports: [PersonalDataCardComponent]
})
export class PersonalDataCardModule { }
