import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalRef, ModalService } from '@bvl-core/shared/helpers/modal';
import { AuthService, ConfigurationService } from '@bvl-site/services';
import {
  GA_CHANGE_PASSWORD_ACEPT_BUTTON,
  GA_CHANGE_PASSWORD_CANCEL_BUTTON,
  GA_CHANGE_PASSWORD_LINK,
  GA_EDIT_CANCEL_BUTTON,
  GA_EDIT_UPDATE_BUTTON,
  GA_EDIT_USER_LINK,
  GA_LOGOUT_BUTTON } from '@bvl-site/settings/constants/ga.constant';
import { PERSON_TYPE } from '@bvl-site/settings/constants/general.constant';
import { IUserAttributes } from '@bvl-site/statemanagement/models/auth.interface';
import { GaUnsubscribeBase } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';
import { ChangePasswordFormComponent } from '../change-password-form/change-password-form.component';
import { SignUpFormComponent } from '../sign-up-form/sign-up-form.component';

@Component({
  selector: 'bvl-personal-data-card',
  templateUrl: './personal-data-card.component.html',
  styleUrls: ['./personal-data-card.component.scss']
})
export class PersonalDataCardComponent extends GaUnsubscribeBase implements OnInit {

  PERSON_TYPE = PERSON_TYPE;

  userAttributes: IUserAttributes;

  constructor(
    protected _element: ElementRef,
    private _authService: AuthService,
    private _configurationService: ConfigurationService,
    private _modalService: ModalService,
    private _router: Router
  ) {
    super(_element);
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    this.userAttributes = this._configurationService.getUserProfile().attributes;
  }

  editData(): void {
    this.addEvent(GA_EDIT_USER_LINK);
    const modalRef: ModalRef = this._modalService.open(SignUpFormComponent, { titleText: 'Edición', confirmButtonText: 'Actualizar' });
    modalRef.setPayload(this.userAttributes);
    modalRef.result
      .then((result: IUserAttributes) => {
        this.addEvent(GA_EDIT_UPDATE_BUTTON, { title: 'Edición', tipoDocumento: result['custom:documentType']});
        this.userAttributes = result;
      })
      .catch(() => {
        this.addEvent(GA_EDIT_CANCEL_BUTTON, { title: 'Edición' });

        return false;
      });
  }

  changePassword(): void {
    this.addEvent(GA_CHANGE_PASSWORD_LINK);
    this._modalService.open(ChangePasswordFormComponent, { titleText: 'Cambiar contraseña', showFooter: false }).result
      .then(() => {
        this.addEvent(GA_CHANGE_PASSWORD_ACEPT_BUTTON, { title: 'Cambiar contraseña' });

        return true;
      })
      .catch(() => {
        this.addEvent(GA_CHANGE_PASSWORD_CANCEL_BUTTON, { title: 'Cambiar contraseña' });

        return false;
      });
  }

  logout(): void {
    this._authService.logout(true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this.addEvent(GA_LOGOUT_BUTTON);
        this._router.navigate(['login']);
      });
  }

}
