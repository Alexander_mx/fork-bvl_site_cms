import { Component, ElementRef, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { COGNITO_ERROR_CODE, ICognitoError } from '@bvl-core/shared/helpers/aws-amplfy';
import { ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { AuthService } from '@bvl-site/services';
import { GA_RECOVER_PASSWORD } from '@bvl-site/settings/constants/ga.constant';
import { GaUnsubscribeBase } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-forgot-password-form',
  templateUrl: './forgot-password-form.component.html',
  styleUrls: ['./forgot-password-form.component.scss']
})
export class ForgotPasswordFormComponent extends GaUnsubscribeBase implements OnInit {

  frmForgotPassword: FormGroup;
  mEmail: AbstractControl;
  errorMessage: string;

  constructor(
    protected _element: ElementRef,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _authService: AuthService
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.frmForgotPassword = this._formBuilder.group({
      mEmail: ['', [Validators.required, Validators.email]]
    });
    this.mEmail = this.frmForgotPassword.get('mEmail');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmForgotPassword, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmForgotPassword);

    return this.frmForgotPassword.valid;
  }

  forgotPassword(): void {
    this.errorMessage = '';
    const validateForm = this._validateForm();
    if (validateForm) {
      const email = this.mEmail.value;
      this._authService.forgotPassword(email, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.addEvent(GA_RECOVER_PASSWORD);
          this._router.navigate([], { queryParams: { confirmacion: true } });
        }, (error: ICognitoError) => {
          this.errorMessage = COGNITO_ERROR_CODE[error.code] || COGNITO_ERROR_CODE.GeneralException;
        });
    }
  }

  setEventsGA(): void { }

}
