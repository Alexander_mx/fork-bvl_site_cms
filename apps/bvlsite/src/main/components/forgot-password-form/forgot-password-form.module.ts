import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { BvlFormAlertModule, BvlFormInputModule, SectionHeaderModule } from '@bvl/library';
import { ForgotPasswordFormComponent } from './forgot-password-form.component';

@NgModule({
  declarations: [ForgotPasswordFormComponent],
  imports: [
    CommonModule,
    SectionHeaderModule,
    ReactiveFormsModule,
    BvlFormAlertModule,
    BvlFormInputModule,
    ControlErrorModule
  ],
  exports: [ForgotPasswordFormComponent]
})
export class ForgotPasswordFormModule { }
