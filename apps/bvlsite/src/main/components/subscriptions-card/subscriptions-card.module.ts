import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SectionHeaderModule } from '@bvl/library';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { SubscriptionsCardComponent } from './subscriptions-card.component';

@NgModule({
  declarations: [SubscriptionsCardComponent],
  imports: [
    CommonModule,
    SectionHeaderModule,
    SweetAlert2Module
  ],
  exports: [SubscriptionsCardComponent]
})
export class SubscriptionsCardModule { }
