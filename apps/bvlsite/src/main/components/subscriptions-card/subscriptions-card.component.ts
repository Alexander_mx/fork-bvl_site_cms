import { Component, OnInit } from '@angular/core';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { ConfigurationService } from '@bvl-site/services';
import { PaymentService } from '@bvl-site/services/payment.service';
import { DATE_FORMAT } from '@bvl-site/settings/constants/general.constant';
import { IUserProfile } from '@bvl-site/statemanagement/models/auth.interface';
import { ICancelSubscriptionRequest, ISubscriptionResponse } from '@bvl-site/statemanagement/models/payment.interface';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'bvl-subscriptions-card',
  templateUrl: './subscriptions-card.component.html',
  styleUrls: ['./subscriptions-card.component.scss']
})
export class SubscriptionsCardComponent extends UnsubscribeOnDestroy implements OnInit {

  DATE_FORMAT = DATE_FORMAT;

  userProfile: IUserProfile;
  subscriptionsList: Array<ISubscriptionResponse>;

  constructor(
    private _paymentService: PaymentService,
    private _configurationService: ConfigurationService,
    private _swalConfirmService: SwalConfirmService
  ) {
    super();
  }

  ngOnInit(): void {
    this.userProfile = this._configurationService.getUserProfile();
    this._subscriptionsList();
  }

  private _subscriptionsList(): void {
    this._paymentService.subscriptionsList(this.userProfile.cognitoUsername)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<ISubscriptionResponse>) => {
        this.subscriptionsList = response;
      });
  }

  cancelSubscriptionSwal(subscription: ISubscriptionResponse): SweetAlertOptions {
    return this._swalConfirmService.warning(`¿Desea cancelar la suscripción ${subscription.plan.name}?`);
  }

  cancelSubscription(subscription: ISubscriptionResponse): void {
    const params = {
      idSubscription: subscription.id,
      idUserCognito: this.userProfile.cognitoUsername
    } as ICancelSubscriptionRequest;
    this._paymentService.cancelSubscription(params)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        const index = this.subscriptionsList.indexOf(subscription);
        this.subscriptionsList.splice(index, 1);
      });
  }

}
