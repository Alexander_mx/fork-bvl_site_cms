import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SectionHeaderModule } from '@bvl/library';
import { MessageFormComponent } from './message-form.component';

@NgModule({
  declarations: [MessageFormComponent],
  imports: [
    CommonModule,
    SectionHeaderModule
  ],
  exports: [MessageFormComponent]
})
export class MessageFormModule { }
