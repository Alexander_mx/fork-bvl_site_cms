import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bvl-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.scss']
})
export class MessageFormComponent implements OnInit {

  @Input() verticalCentered: boolean;
  @Input() title: string;
  @Input() message: string;

  constructor() {
    this.verticalCentered = true;
  }

  ngOnInit(): void { }

}
