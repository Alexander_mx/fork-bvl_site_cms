import { Location } from '@angular/common';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { AngularUtil } from '@bvl-core/shared/helpers/util';
import { ISiteMenu, SITE_MENU_ACTION } from '@bvl-core/shared/helpers/util/menu';
import { ResizeService } from '@bvl-core/shared/helpers/util/resize';
import { HeaderService } from '@bvl-site/services';
import { GaUnsubscribeBase } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-site-menu',
  templateUrl: './site-menu.component.html',
  styleUrls: ['./site-menu.component.scss']
})
export class SiteMenuComponent extends GaUnsubscribeBase implements OnInit {

  @ViewChildren('subMenu') subMenu: QueryList<ElementRef<HTMLDivElement>>;

  @Input() language: string;
  private _menu: Array<ISiteMenu>;
  @Input()
  get menu(): Array<ISiteMenu> {
    return this._menu;
  }
  set menu(value: Array<ISiteMenu>) {
    this._menu = this.formatMenu(value);
    this._setHeaderHeight();
  }

  @Input() findSabLink: string;
  @Input() findSabText: string;

  @Output() changeLanguage: EventEmitter<string>;

  SITE_MENU_ACTION = SITE_MENU_ACTION;

  path: string;

  constructor(
    private _headerService: HeaderService,
    private _location: Location,
    private _resizeService: ResizeService,
    protected _elementRef: ElementRef<HTMLDivElement>
  ) {
    super(_elementRef);
    this.changeLanguage = new EventEmitter<string>();
    this.path = this.getPath(this._location.path() || '');
  }

  setEventsGA(): void { }

  ngOnInit(): void { }

  getPath(locationPath: string): string {
    const KEY_REGEX_PATH = /^(?:\/es|\/en){0,1}\/([^\/]*).?([^\/]*)/i;

    return (locationPath.length > 0)
            ? locationPath.match(KEY_REGEX_PATH)[0]
              .substring(1)
            : '/';
  }

  formatMenu(menu: Array<ISiteMenu>): Array<ISiteMenu> {
    const path = this.getPath(this._location.path() || '');

    return (menu || []).map(parent => {
      parent.active = parent.path === path;
      parent.children.forEach(child => {
        child.active = child.path === path;
        if (child.active) { parent.active = child.active; }
      });

      return parent;
    });
  }

  private _setHeaderHeight(): void {
    this._resizeService.getResizeEvent()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((w: number) => {
        setTimeout(() => {
          const header = this._elementRef.nativeElement.parentElement.clientHeight;
          const subMenuRef = this.subMenu && this.subMenu.toArray()
                              .find((item: ElementRef<HTMLDivElement>) => {
                                return AngularUtil.hasClassName(item.nativeElement, 'active');
                              });
          const subMenu = (subMenuRef && subMenuRef.nativeElement.firstElementChild.clientHeight) || 0;
          const headerHeight = (w > 768)
                                ? (header + subMenu)
                                : header;
          this._headerService.setHeaderHeight(headerHeight);
        });
    });
  }

  private _resetActiveMenu(menu: Array<ISiteMenu>): void {
    menu.forEach(item => {
      item.active = false;
      this._resetActiveMenu(item.children);
    });
  }

  goMenu(item: ISiteMenu): void {
    this._resetActiveMenu(this.menu);
    item.active = true;
    this._setHeaderHeight();
  }

}
