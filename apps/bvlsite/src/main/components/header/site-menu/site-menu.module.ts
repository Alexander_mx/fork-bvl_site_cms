import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { OrderByPipeModule } from '@bvl-core/shared/helpers/pipes';
import { DomChangeDirectiveModule } from '@bvl/library';
import { SwitchLanguageModule } from '../switch-language/switch-language.module';
import { SiteMenuComponent } from './site-menu.component';

@NgModule({
  declarations: [SiteMenuComponent],
  imports: [
    CommonModule,
    OrderByPipeModule,
    SwitchLanguageModule,
    FormsModule,
    DomChangeDirectiveModule
  ],
  exports: [SiteMenuComponent]
})
export class SiteMenuModule { }
