import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { DomChangeDirectiveModule } from '@bvl/library';
import { HeaderComponent } from './header.component';
import { SiteMenuModule } from './site-menu/site-menu.module';
import { SwitchLanguageModule } from './switch-language/switch-language.module';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    FormsModule,
    SwitchLanguageModule,
    SiteMenuModule,
    DomChangeDirectiveModule
  ],
  exports: [HeaderComponent]
})
export class HeaderModule { }
