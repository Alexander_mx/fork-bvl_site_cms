import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LANGUAGE_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { PageServices } from '@bvl-site/services';
import { SEOService } from '@bvl-site/services/seo.service';
import { GA_CHANGE_LANG } from '@bvl-site/settings/constants/ga.constant';
import { IPathPage } from '@bvl-site/statemanagement/models/page.interface';
import { GaUnsubscribeBase } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-switch-language',
  templateUrl: './switch-language.component.html',
  styleUrls: ['./switch-language.component.scss']
})
export class SwitchLanguageComponent extends GaUnsubscribeBase implements OnInit {

  @Input() name: string;

  paths: Array<IPathPage> = [];
  currentLang = 'es';

  LANGUAGE_TYPES = LANGUAGE_TYPES;

  constructor(
    protected _element: ElementRef,
    private _seoService: SEOService,
    private _pageService: PageServices,
    private _router: ActivatedRoute
  ) {
    super(_element);
    this.name = 'lang';

    this.ga = [
      GA_CHANGE_LANG
    ];
  }

  ngOnInit(): void {
    const data = this._router.snapshot.firstChild.data;
    if (data.isInternal) {
      this.currentLang = data.lang;
      this._pageService.setLanguageLocal(this.currentLang);
      this.paths = this._pageService.getPaths(data.code);
    } else {
      this._pageService.getPageCurrenSub()
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => {
          this.currentLang = this._pageService.getCurrentLang();
          this.paths = this._pageService.getPaths();
        });
    }
  }

  isLanguageCurrent(lang: string): boolean {
    return this.currentLang === lang.substring(0, 2);
  }

  getUrlLanguage(lang: string): string {
    const pathLang = this.paths && this.paths.find(l => l.language === lang.substring(0, 2));

    return this._seoService.getUrlAll(pathLang && pathLang.path || '');
  }

}
