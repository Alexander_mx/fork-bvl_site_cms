import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { DomChangeDirectiveModule } from '@bvl/library';
import { SwitchLanguageComponent } from './switch-language.component';

@NgModule({
  declarations: [SwitchLanguageComponent],
  imports: [
    CommonModule,
    FormsModule,
    DomChangeDirectiveModule
  ],
  exports: [SwitchLanguageComponent]
})
export class SwitchLanguageModule { }
