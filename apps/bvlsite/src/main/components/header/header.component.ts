import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, Input, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getTimeStamp } from '@bvl-core/shared/helpers/util/general';
import { ISiteMenu } from '@bvl-core/shared/helpers/util/menu';
import { LANGUAGE_TYPES_CODE } from '@bvl-core/shared/settings/constants/general.constants';
import { OnboardingService, PageServices } from '@bvl-site/services';
import {
  GA_HEADER_CONTACT_US_ICON,
  GA_HEADER_LOGIN_ICON,
  GA_HEADER_LOGO,
  GA_HEADER_SAB_LINK,
  GA_HEADER_SEARCH_ICON,
  GA_MENU,
  GA_SUB_MENU
} from '@bvl-site/settings/constants/ga.constant';
import { STATIC_PAGES } from '@bvl-site/settings/constants/general.constant';
import { STORAGE_REAL_TIME } from '@bvl-site/settings/constants/storage-key.constant';
import { GaUnsubscribeBase, LANGUAGES } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';
import { OnboardingIntroComponent } from '../modals/onboarding-intro/onboarding-intro.component';

@Component({
  selector: 'bvl-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends GaUnsubscribeBase implements OnInit {

  @Input() language: string;
  @Input() menu: Array<ISiteMenu>;

  hasOnboarding: boolean;

  get showlRealTime(): boolean {
    const value = (sessionStorage.getItem(STORAGE_REAL_TIME))
      ? sessionStorage.getItem(STORAGE_REAL_TIME)
      : 'true';

    return JSON.parse(value) as boolean;
  }
  set showlRealTime(value: boolean) {
    sessionStorage.setItem(STORAGE_REAL_TIME, value.toString());
  }

  private _toggleMenu: boolean;
  get toggleMenu(): boolean {
    return !!this._toggleMenu;
  }
  set toggleMenu(value: boolean) {
    const fn = (value)
      ? 'addClass'
      : 'removeClass';
    this._renderer2[fn](this._document.body, 'overflow-hidden');
    this._toggleMenu = value;
  }

  onboardingActive: boolean;

  TEXT: any;
  LANGUAGE_TYPES_CODE = LANGUAGE_TYPES_CODE;
  STATIC_PAGES = STATIC_PAGES;

  constructor(
    protected _element: ElementRef,
    @Inject(DOCUMENT) private _document: Document,
    private _onboardingService: OnboardingService,
    private _pageService: PageServices,
    private _renderer2: Renderer2,
    private _router: ActivatedRoute
  ) {
    super(_element);
    this._element.nativeElement.setAttribute('keyc', getTimeStamp());
    this.ga = [
      GA_HEADER_LOGO,
      GA_HEADER_SAB_LINK,
      GA_HEADER_SEARCH_ICON,
      GA_HEADER_CONTACT_US_ICON,
      GA_HEADER_LOGIN_ICON,
      GA_MENU,
      GA_SUB_MENU
    ];
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    this.TEXT = LANGUAGES[this.language];

    if (this._router.snapshot.firstChild.data.isInternal) {
      this.language = this._router.snapshot.firstChild.data.lang;
      this.setLangText();
    } else {
      this._pageService.getPageCurrenSub()
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => {
          this.language = this._pageService.getCurrentLang();
          this.setLangText();
        });
    }

    this.hasOnboarding = this._onboardingService.hasOnboarding();

    if (!this.hasOnboarding) {
      return void 0;
    }

    this._onboardingService.getActive()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: boolean) => {
        this.onboardingActive = response;
      });
  }

  setLangText(): void {
    this.TEXT = LANGUAGES[this.language];
  }

  getPathStaticPage(page: string): string {
    return this.STATIC_PAGES[this.language] && this.STATIC_PAGES[this.language][page] || '';
  }

  changeOnboarding(): void {
    if (this._onboardingService.hasIntro() && !this.onboardingActive) {
      this._onboardingService.showIntro(OnboardingIntroComponent);

      return void 0;
    }

    this._onboardingService.setActive(!this.onboardingActive);
  }

}
