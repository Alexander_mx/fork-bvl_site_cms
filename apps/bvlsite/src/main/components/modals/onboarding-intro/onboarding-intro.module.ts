import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { OnboardingIntroComponent } from './onboarding-intro.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [OnboardingIntroComponent],
  exports: [OnboardingIntroComponent],
  entryComponents: [OnboardingIntroComponent]
})
export class OnboardingIntroModule { }
