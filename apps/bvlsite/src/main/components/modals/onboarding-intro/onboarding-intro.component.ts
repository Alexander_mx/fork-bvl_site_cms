import { Component } from '@angular/core';

import { ActiveModal } from '@bvl-core/shared/helpers/modal';

@Component({
  selector: 'bvl-onboarding-intro',
  templateUrl: './onboarding-intro.component.html',
  styleUrls: ['./onboarding-intro.component.scss']
})
export class OnboardingIntroComponent {

  constructor(
    private _activeModal: ActiveModal
  ) { }

  start(): void {
    this._activeModal.close();
  }

  close(): void {
    this._activeModal.dismiss();
  }
}
