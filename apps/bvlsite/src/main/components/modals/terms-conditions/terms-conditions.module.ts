import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TermsConditionsComponent } from './terms-conditions.component';

@NgModule({
  declarations: [TermsConditionsComponent],
  imports: [
    CommonModule
  ],
  exports: [TermsConditionsComponent],
  entryComponents: [TermsConditionsComponent]
})
export class TermsConditionsModule { }
