import { Component, ElementRef, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { COGNITO_ERROR_CODE, ICognitoError } from '@bvl-core/shared/helpers/aws-amplfy';
import { ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { AuthService, ConfigurationService } from '@bvl-site/services';
import { GA_LOGIN } from '@bvl-site/settings/constants/ga.constant';
import { ILoginRequest, IUserProfile } from '@bvl-site/statemanagement/models/auth.interface';
import { GaUnsubscribeBase } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';
import { Md5 } from 'ts-md5/dist/md5';

@Component({
  selector: 'bvl-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent extends GaUnsubscribeBase implements OnInit {

  frmLogin: FormGroup;
  mEmail: AbstractControl;
  mPassword: AbstractControl;
  errorMessage: string;

  constructor(
    protected _element: ElementRef,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _configurationService: ConfigurationService,
    private _authService: AuthService
  ) {
    super(_element);
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.frmLogin = this._formBuilder.group({
      mEmail: ['', [Validators.required, Validators.email]],
      mPassword: ['', [Validators.required, Validators.minLength(8)]]
    });
    this.mEmail = this.frmLogin.get('mEmail');
    this.mPassword = this.frmLogin.get('mPassword');
  }

  private _params(): ILoginRequest {
    return {
      username: this.mEmail.value,
      password: this.mPassword.value
    } as ILoginRequest;
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmLogin, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmLogin);

    return this.frmLogin.valid;
  }

  signIn(): void {
    this.errorMessage = '';
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._params();
      this._authService.signIn(params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: any) => {
          const md5 = new Md5();
          const userProfile: IUserProfile = {
            cognitoUsername: response.username,
            username: params.username,
            usernameMD5: md5.appendStr(params.username)
            .end()
            .toString(),
            attributes: response.attributes
          };
          this._configurationService.setUserProfile(userProfile);
          this._eventSignIn(true);
          this._router.navigate(['sitio-privado']);
        }, (error: ICognitoError) => {
          this._eventSignIn(false);
          this.errorMessage = COGNITO_ERROR_CODE[error.code] || COGNITO_ERROR_CODE.GeneralException;
        });
    }
  }

  private _eventSignIn(success: boolean): void {
    this.addEvent(GA_LOGIN.signIn, { state: (success) ? 'Exitoso' : 'Fallido' });
  }

  goTo(path: string, ga: string): void {
    this.addEvent(GA_LOGIN[ga]);
    this._router.navigate([path]);
  }

}
