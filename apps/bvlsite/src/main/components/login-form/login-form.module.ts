import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { BvlFormAlertModule, BvlFormInputModule, SectionHeaderModule } from '@bvl/library';
import { LoginFormComponent } from './login-form.component';

@NgModule({
  declarations: [LoginFormComponent],
  imports: [
    CommonModule,
    SectionHeaderModule,
    ReactiveFormsModule,
    BvlFormAlertModule,
    BvlFormInputModule,
    RouterModule,
    ControlErrorModule
  ],
  exports: [LoginFormComponent]
})
export class LoginFormModule { }
