import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { COGNITO_ERROR_CODE, ICognitoError } from '@bvl-core/shared/helpers/aws-amplfy';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { AuthService } from '@bvl-site/services';
import { INewPasswordRequest } from '@bvl-site/statemanagement/models/auth.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-new-password-form',
  templateUrl: './new-password-form.component.html',
  styleUrls: ['./new-password-form.component.scss']
})
export class NewPasswordFormComponent extends UnsubscribeOnDestroy implements OnInit {

  frmNewPassword: FormGroup;
  mEmail: AbstractControl;
  mNewPassword: AbstractControl;
  code: string;
  errorMessage: string;

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _authService: AuthService
  ) {
    super();
  }

  ngOnInit(): void {
    this.code = (this._activatedRoute.snapshot.queryParams && this._activatedRoute.snapshot.queryParams.code) || '';
    this._createForm();
  }

  private _createForm(): void {
    this.frmNewPassword = this._formBuilder.group({
      mEmail: ['', [Validators.required, Validators.email]],
      mNewPassword: ['', [Validators.required, Validators.minLength(8)]]
    });
    this.mEmail = this.frmNewPassword.get('mEmail');
    this.mNewPassword = this.frmNewPassword.get('mNewPassword');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmNewPassword, controlName);
  }

  private _params(): INewPasswordRequest {
    return {
      username: this.mEmail.value,
      password: this.mNewPassword.value,
      code: this.code
    } as INewPasswordRequest;
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmNewPassword);

    return this.frmNewPassword.valid;
  }

  newPassword(): void {
    this.errorMessage = '';
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._params();
      this._authService.forgotPasswordSubmit(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._router.navigate([], { queryParams: { confirmacion: true } });
      }, (error: ICognitoError) => {
        this.errorMessage = COGNITO_ERROR_CODE[error.code] || COGNITO_ERROR_CODE.GeneralException;
      });
    }
  }

}
