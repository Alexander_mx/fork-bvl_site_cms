import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { BvlFormAlertModule, BvlFormInputModule, SectionHeaderModule } from '@bvl/library';
import { NewPasswordFormComponent } from './new-password-form.component';

@NgModule({
  declarations: [NewPasswordFormComponent],
  imports: [
    CommonModule,
    SectionHeaderModule,
    ReactiveFormsModule,
    BvlFormAlertModule,
    BvlFormInputModule,
    ControlErrorModule
  ],
  exports: [NewPasswordFormComponent]
})
export class NewPasswordFormModule { }
