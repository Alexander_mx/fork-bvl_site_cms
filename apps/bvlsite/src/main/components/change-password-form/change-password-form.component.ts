import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { COGNITO_ERROR_CODE, ICognitoError } from '@bvl-core/shared/helpers/aws-amplfy';
import { ActiveModal } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { AuthService } from '@bvl-site/services';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-change-password-form',
  templateUrl: './change-password-form.component.html',
  styleUrls: ['./change-password-form.component.scss']
})
export class ChangePasswordFormComponent extends UnsubscribeOnDestroy implements OnInit {

  frmChangePassword: FormGroup;
  mPassword: AbstractControl;
  mNewPassword: AbstractControl;
  errorMessage: string;
  isMessageForm: boolean;
  verticalCentered: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private _activeModal: ActiveModal,
    private _authService: AuthService
  ) {
    super();
    this.verticalCentered = false;
  }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.frmChangePassword = this._formBuilder.group({
      mPassword: ['', [Validators.required, Validators.minLength(8)]],
      mNewPassword: ['', [Validators.required, Validators.minLength(8)]]
    });

    this.mPassword = this.frmChangePassword.get('mPassword');
    this.mNewPassword = this.frmChangePassword.get('mNewPassword');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmChangePassword, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmChangePassword);

    return this.frmChangePassword.valid;
  }

  changePassword(): void {
    this.errorMessage = '';
    const validateForm = this._validateForm();
    if (validateForm) {
      this._authService.changePassword(this.mPassword.value, this.mNewPassword.value, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.isMessageForm = true;
          setTimeout(() => {
            this._activeModal.close();
          }, 1500);
        }, (error: ICognitoError) => {
          this.errorMessage = COGNITO_ERROR_CODE[error.code] || COGNITO_ERROR_CODE.GeneralException;
        });
    }
  }

  cancel(): void {
    this._activeModal.dismiss();
  }

}
