import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { BvlFormAlertModule, BvlFormInputModule, SectionHeaderModule } from '@bvl/library';
import { MessageFormModule } from '../message-form/message-form.module';
import { ChangePasswordFormComponent } from './change-password-form.component';

@NgModule({
  declarations: [ChangePasswordFormComponent],
  imports: [
    CommonModule,
    SectionHeaderModule,
    ReactiveFormsModule,
    BvlFormAlertModule,
    BvlFormInputModule,
    MessageFormModule,
    ControlErrorModule
  ],
  exports: [ChangePasswordFormComponent],
  entryComponents: [ChangePasswordFormComponent]
})
export class ChangePasswordFormModule { }
