import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, HostBinding, Inject, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { ISiteMenu, menuTree, SITE_MENU_TYPE } from '@bvl-core/shared/helpers/util/menu';
import { OnboardingService, PageServices } from '@bvl-site/services';
import { HeaderService } from '@bvl-site/services/header.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'bvl-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent extends UnsubscribeOnDestroy implements OnInit, AfterViewInit {

  @HostBinding('attr.class') attrClass = 'g-sticky';

  language: string;
  menu: Array<ISiteMenu>;
  footer: Array<ISiteMenu>;

  guideProcess: any;
  currentGuide: any;

  constructor(
    @Inject(DOCUMENT) private _document: Document,
    private _headerService: HeaderService,
    private _onboardingService: OnboardingService,
    private _pageService: PageServices,
    private _renderer2: Renderer2,
    private _router: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this.language = 'es';
    if (this._router.snapshot.firstChild.data.isInternal) {
      this.language = this._router.snapshot.firstChild.data.lang;
      this._getMenu();
    } else {
      this._pageService.getPageCurrenSub()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        const currentLang = this._pageService.getCurrentLang()
        .substring(0, 2);
        if (currentLang) {
          this.language = currentLang;
          this._getMenu();
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this._getHeaderHeight();
    setTimeout(() => {
      this._onboarding();
    }, 1000);
  }

  private _onboarding(): void {
    this._onboardingService.listenOnboarding()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        if (response) {
          this._onboardingService.showOnboarding(response);
        }
      });
  }

  private _getMenu(): void {
    this._pageService.getMenu(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<ISiteMenu>) => {
        this.menu = menuTree(response.filter(menu => menu.type === SITE_MENU_TYPE.HEADER));
        this.footer = menuTree(response.filter(menu => menu.type === SITE_MENU_TYPE.FOOTER));
      });
  }

  private _getHeaderHeight(): void {
    this._headerService.getHeaderHeight()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(headerHeight => {
        this._renderer2.setStyle(this._document.body, 'margin-top', `${headerHeight}px`);
    });
  }

}
