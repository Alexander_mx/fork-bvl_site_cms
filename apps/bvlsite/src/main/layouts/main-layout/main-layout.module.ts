import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooterModule } from '@bvl-site/components/footer/footer.module';
import { HeaderModule } from '@bvl-site/components/header/header.module';
import { OnboardingIntroModule } from '@bvl-site/components/modals/onboarding-intro/onboarding-intro.module';
import { OnboardingService } from '@bvl-site/services';
import { GuideMessageModule } from '@bvl/library/guide-message';
import { MainLayoutComponent } from './main-layout.component';

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    HeaderModule,
    FooterModule,
    GuideMessageModule,
    OnboardingIntroModule
  ],
  providers: [OnboardingService]
})
export class MainLayoutModule { }
