import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { LANGUAGES } from '@bvl/library';
import { takeUntil } from 'rxjs/operators';
import { GeneralService } from '../../../providers/services/general.service';
import { ISearchRequest, ISearchResponse } from '../../../statemanagement/models/general.interface';
@Component({
  selector: 'bvl-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent extends UnsubscribeOnDestroy implements OnInit {

  frmSearch: FormGroup;
  mSearch: AbstractControl;
  searchPlaceholder: string;
  searchs: Array<ISearchResponse>;
  title: string;
  titleAlign: string;
  errorMessage: string;

  resultSearch: string;
  countResult: number;

  TEXT: any;

  constructor(
    private _formBuilder: FormBuilder,
    private _generalService: GeneralService,
    private _router: ActivatedRoute) {
    super();
   }

  ngOnInit(): void {
    this._createForm();
    this.searchs = [];
    this.mSearch = this._formBuilder.control('');
    this.TEXT = LANGUAGES[this._router.snapshot.data.lang];

    this.title =  this.TEXT.searchPageSite.title;
    this.titleAlign = 'center';
    this.resultSearch = this.TEXT.searchPageSite.results.withoutResponse;
  }

  private _createForm(): void {
    this.frmSearch = this._formBuilder.group({
      mSearch: ['mSearch']
    });
    this.mSearch = this.frmSearch.get('mSearch');
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmSearch);

    return this.frmSearch.valid;
  }

  private _params(): ISearchRequest {
    return {
      filter: this.mSearch.value
    } as ISearchRequest;
  }

  search(): void {

    this.errorMessage = '';
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._params();
      this._generalService.searchPagesSite(params)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<ISearchResponse>) => {
        this.resultSearch = response.length > 0 ?
        this.TEXT.searchPageSite.results.withResponse :
        this.TEXT.searchPageSite.results.withoutResponse;

        this.searchs = response;
        this.countResult = this.searchs.length;
      }, (error: any) => {
        this.errorMessage = error;
      });
    }
  }

}
