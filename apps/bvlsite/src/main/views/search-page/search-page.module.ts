import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BvlFormAlertModule, BvlFormInputModule, SectionHeaderModule } from '@bvl/library';
import { SearchPageRoutingModule } from './search-page-routing.module';
import { SearchPageComponent } from './search-page.component';

@NgModule({
  declarations: [SearchPageComponent],
  imports: [
    BvlFormInputModule,
    CommonModule,
    ReactiveFormsModule,
    BvlFormAlertModule,
    SearchPageRoutingModule,
    SectionHeaderModule
  ],
  exports: [SearchPageComponent]
})
export class SearchPageModule { }
