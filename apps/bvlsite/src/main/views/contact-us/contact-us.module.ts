import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ContactUsFormModule } from '@bvl-site/components/contact-us-form/contact-us-form.module';
import { ContactUsRoutingModule } from './contact-us-routing.module';
import { ContactUsComponent } from './contact-us.component';

@NgModule({
  declarations: [ContactUsComponent],
  imports: [
    CommonModule,
    ContactUsRoutingModule,
    ContactUsFormModule
  ]
})
export class ContactUsModule { }
