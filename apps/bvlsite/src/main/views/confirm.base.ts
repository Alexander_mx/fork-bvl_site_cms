import { OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

export abstract class ConfirmBase extends UnsubscribeOnDestroy implements OnInit {

  confirmation: boolean;

  constructor(
    protected _activatedRoute: ActivatedRoute
  ) {
    super();
    this.confirmation = false;
  }

  ngOnInit(): void {
    this._activatedRoute.queryParams
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: any) => {
        this.confirmation = response && response.confirmacion && response.confirmacion === 'true';
      });
  }

}
