import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfirmBase } from '../confirm.base';

@Component({
  selector: 'bvl-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent extends ConfirmBase implements OnInit {

  constructor(
    protected _activatedRoute: ActivatedRoute
  ) {
    super(_activatedRoute);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

}
