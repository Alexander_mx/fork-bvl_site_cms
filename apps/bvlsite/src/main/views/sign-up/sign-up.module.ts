import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MessageFormModule } from '@bvl-site/components/message-form/message-form.module';
import { SignUpFormModule } from '@bvl-site/components/sign-up-form/sign-up-form.module';
import { SignUpRoutingModule } from './sign-up-routing.module';
import { SignUpComponent } from './sign-up.component';

@NgModule({
  declarations: [SignUpComponent],
  imports: [
    CommonModule,
    SignUpRoutingModule,
    SignUpFormModule,
    MessageFormModule
  ]
})
export class SignUpModule { }
