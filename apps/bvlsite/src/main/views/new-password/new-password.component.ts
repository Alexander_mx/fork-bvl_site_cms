import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfirmBase } from '../confirm.base';

@Component({
  selector: 'bvl-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent extends ConfirmBase implements OnInit {

  constructor(
    protected _activatedRoute: ActivatedRoute
  ) {
    super(_activatedRoute);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

}
