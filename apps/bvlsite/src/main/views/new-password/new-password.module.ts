import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MessageFormModule } from '@bvl-site/components/message-form/message-form.module';
import { NewPasswordFormModule } from '@bvl-site/components/new-password-form/new-password-form.module';
import { NewPasswordRoutingModule } from './new-password-routing.module';
import { NewPasswordComponent } from './new-password.component';

@NgModule({
  declarations: [NewPasswordComponent],
  imports: [
    CommonModule,
    NewPasswordRoutingModule,
    NewPasswordFormModule,
    MessageFormModule
  ]
})
export class NewPasswordModule { }
