import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentTabComponent } from './payment-tab/payment-tab.component';
import { PlanPaymentComponent } from './plan-payment.component';

const routes: Routes = [
  {
    path: '',
    component: PlanPaymentComponent,
    children: [
      {
        path: '',
        component: PaymentTabComponent,
        children: [
          {
            path: 'tarjeta',
            loadChildren: './payment-tab/card-payment/card-payment.module#CardPaymentModule'
          },
          {
            path: 'deposito',
            loadChildren: './payment-tab/deposit-payment/deposit-payment.module#DepositPaymentModule'
          }
        ]
      },
      {
        path: 'pago-confirmado',
        loadChildren: './payment-confirm/payment-confirm.module#PaymentConfirmModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanPaymentRoutingModule { }
