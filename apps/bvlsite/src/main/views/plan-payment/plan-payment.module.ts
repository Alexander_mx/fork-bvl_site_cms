import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PaymentTabModule } from './payment-tab/payment-tab.module';
import { PlanPaymentRoutingModule } from './plan-payment-routing.module';
import { PlanPaymentComponent } from './plan-payment.component';

@NgModule({
  declarations: [PlanPaymentComponent],
  imports: [
    CommonModule,
    PlanPaymentRoutingModule,
    PaymentTabModule
  ]
})
export class PlanPaymentModule { }
