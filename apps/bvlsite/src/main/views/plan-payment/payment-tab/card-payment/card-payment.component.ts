import { Component, OnInit, ViewChild } from '@angular/core';
import { CardPaymentFormComponent } from '@bvl-site/components/card-payment-form/card-payment-form.component';

@Component({
  selector: 'bvl-card-payment',
  templateUrl: './card-payment.component.html',
  styleUrls: ['./card-payment.component.scss']
})
export class CardPaymentComponent implements OnInit {

  @ViewChild(CardPaymentFormComponent) private _cardPaymentComponent: CardPaymentComponent;

  constructor() { }

  ngOnInit(): void { }

  getForm(): any {
    return this._cardPaymentComponent.getForm();
  }

}
