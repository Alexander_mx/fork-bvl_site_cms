import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardPaymentFormModule } from '@bvl-site/components/card-payment-form/card-payment-form.module';
import { CardPaymentRoutingModule } from './card-payment-routing.module';
import { CardPaymentComponent } from './card-payment.component';

@NgModule({
  declarations: [CardPaymentComponent],
  imports: [
    CommonModule,
    CardPaymentRoutingModule,
    CardPaymentFormModule
  ]
})
export class CardPaymentModule { }
