import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationEnd, PRIMARY_OUTLET, Router } from '@angular/router';
import { IModalConfig, ModalService } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { TermsConditionsComponent } from '@bvl-site/components/modals/terms-conditions/terms-conditions.component';
import { ConfigurationService } from '@bvl-site/services';
import { PaymentService } from '@bvl-site/services/payment.service';
import { CULQI } from '@bvl-site/settings/constants/culqi.constant';
import { COUNTRY_CODE, PERSON_TYPE } from '@bvl-site/settings/constants/general.constant';
import { SRC_PAYMENT_CARDS } from '@bvl-site/settings/constants/image.constant';
import { TAB_PLAN_PLAYMENT } from '@bvl-site/settings/constants/tab.constant';
import { ICheckoutRequest, IPlanResponse } from '@bvl-site/statemanagement/models/payment.interface';
import { filter, takeUntil } from 'rxjs/operators';

/**
 * Se declara Culqi para utilizar su libreria
 */
declare let Culqi: any;

@Component({
  selector: 'bvl-payment-tab',
  templateUrl: './payment-tab.component.html',
  styleUrls: ['./payment-tab.component.scss']
})
export class PaymentTabComponent extends UnsubscribeOnDestroy implements OnInit {

  TAB_PLAN_PLAYMENT = TAB_PLAN_PLAYMENT;
  SRC_PAYMENT_CARDS = SRC_PAYMENT_CARDS;

  currentRoute: { path: string, plan: string };
  cardPaymentForm: any;
  frmCulqi: FormGroup;
  mTerms: AbstractControl;
  plan: IPlanResponse;
  paramsCheckout: ICheckoutRequest;
  errorMessage: string;

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _configurationService: ConfigurationService,
    private _paymentService: PaymentService,
    private _modalService: ModalService
  ) {
    super();
  }

  ngOnInit(): void {
    this._currentRoute();
    this._plan();
    this._initCulqi();
  }

  componentAdded(componentRef: any): void {
    this.cardPaymentForm = componentRef;
  }

  private _getCurrentRoute(url: string): { path: string, plan: string} {
    const urlTree = this._router.parseUrl(url),
          urlSegments = urlTree.root.children[PRIMARY_OUTLET].segments;

    return {
      path: urlSegments[1].path,
      plan: urlTree.queryParams.plan
    };
  }

  private _currentRoute(): void {
    this.currentRoute = this._getCurrentRoute(this._router.url);
    this._router.events
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe((event: NavigationEnd) => {
        this.currentRoute = this._getCurrentRoute(event.url);
      });
  }

  private _plan(): void {
    this._paymentService.planById(this.currentRoute.plan)
    .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IPlanResponse) => {
        response.interval = `${response.interval_count} ${response.interval}`;
        response.amount = (response.amount / 100);
        this.plan = response;
      });
  }

  private _formCulqi(): void {
    this.frmCulqi = this._formBuilder.group({
      mEmail: [null],
      mCardNumber: [null],
      mCVV: [null],
      mMM: [null],
      mYYYY: [null],
      mTerms: [false, Validators.requiredTrue]
    });

    this.mTerms = this.frmCulqi.get('mTerms');
  }

  private _culqi(): void {
    (window as any).culqi = (() => {
      if (Culqi.token && Culqi.token.id) {
        this.paramsCheckout.idToken = Culqi.token.id;
        this._paymentService.checkout(this.paramsCheckout, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(response => {
            const queryParams = {
              suscripcion: response.id
            };
            this._router.navigate(['pago-plan/pago-confirmado'], { queryParams });
          });
      } else {
        this.errorMessage = 'No se puede realizar la operación, vuelva a intentarlo en unos minutos';
      }
    });
  }

  private _initCulqi(): void {
    Culqi.publicKey = CULQI.publicKey;
    Culqi.init();
    this._formCulqi();
    this._culqi();
  }

  private _paramsCheckout(form: any): ICheckoutRequest {
    const userProfile = this._configurationService.getUserProfile();

    return {
      idCognito: userProfile.cognitoUsername,
      customer: {
        firstName: userProfile.attributes['custom:names'],
        lastName: (userProfile.attributes['custom:personType'] === PERSON_TYPE.natural)
                    ? `${userProfile.attributes['custom:paternalSurname']} ${userProfile.attributes['custom:maternalSurname']}`
                    : userProfile.attributes['custom:names'],
        email: form.mEmail || userProfile.attributes.email,
        address: form.mAddress,
        addressCity: form.mDistrict.name,
        countryCode: COUNTRY_CODE.peru,
        phoneNumber: form.mPhone,
        invoice: form.mInvoice,
        ruc: form.mRuc || '',
        companyName: form.mLegalName || ''
      },
      idToken: '',
      idPlan: this.currentRoute.plan
    } as ICheckoutRequest;
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmCulqi, controlName);
  }

  private _validateFormCulqi(): boolean {
    ValidatorUtil.validateForm(this.frmCulqi);

    return this.frmCulqi.valid;
  }

  pay(): void {
    this.errorMessage = '';

    const form = this.cardPaymentForm.getForm(),
          isValideFrmCulqi = this._validateFormCulqi();

    if (form && isValideFrmCulqi) {
      this.frmCulqi.patchValue(form);
      this.paramsCheckout = this._paramsCheckout(form);
      Culqi.createToken();
    }
  }

  termsConditions(): void {
    const modalConfig = {
      titleText: 'Términos y Condiciones',
      showFooter: false
    } as IModalConfig;
    this._modalService.open(TermsConditionsComponent, modalConfig).result
      .then(() => {
        return true;
      }, () => {
        return false;
      });
  }

}
