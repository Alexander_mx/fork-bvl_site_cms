import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepositPaymentComponent } from './deposit-payment.component';

const routes: Routes = [
  {
    path: '',
    component: DepositPaymentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepositPaymentRoutingModule { }
