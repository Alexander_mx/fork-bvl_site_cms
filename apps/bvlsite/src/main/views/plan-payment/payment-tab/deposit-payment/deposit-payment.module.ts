import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SectionHeaderModule } from '@bvl/library';
import { DepositPaymentRoutingModule } from './deposit-payment-routing.module';
import { DepositPaymentComponent } from './deposit-payment.component';

@NgModule({
  declarations: [DepositPaymentComponent],
  imports: [
    CommonModule,
    DepositPaymentRoutingModule,
    SectionHeaderModule
  ]
})
export class DepositPaymentModule { }
