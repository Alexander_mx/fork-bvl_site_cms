import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { TermsConditionsModule } from '@bvl-site/components/modals/terms-conditions/terms-conditions.module';
import { TabsModule } from '@bvl-site/components/shared/tabs/tabs.module';
import { BvlFormAlertModule, BvlFormCheckboxModule, SectionHeaderModule } from '@bvl/library';
import { PaymentTabComponent } from './payment-tab.component';

@NgModule({
  declarations: [PaymentTabComponent],
  imports: [
    CommonModule,
    RouterModule,
    TabsModule,
    SectionHeaderModule,
    ReactiveFormsModule,
    BvlFormCheckboxModule,
    BvlFormAlertModule,
    ModalModule,
    TermsConditionsModule
  ],
  exports: [PaymentTabComponent]
})
export class PaymentTabModule { }
