import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PaymentConfirmRoutingModule } from './payment-confirm-routing.module';
import { PaymentConfirmComponent } from './payment-confirm.component';

@NgModule({
  declarations: [PaymentConfirmComponent],
  imports: [
    CommonModule,
    PaymentConfirmRoutingModule
  ]
})
export class PaymentConfirmModule { }
