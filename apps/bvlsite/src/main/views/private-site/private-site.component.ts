import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { addElementToRef, addScriptTag } from '@bvl-core/shared/helpers/util/web-components';
import { PageServices } from '@bvl-site/services';
import { WC_BUNDLE_PATH } from '@bvl-site/settings/constants/general.constant';

@Component({
  selector: 'bvl-private-site',
  templateUrl: './private-site.component.html',
  styleUrls: ['./private-site.component.scss']
})
export class PrivateSiteComponent implements OnInit {

  get tplBannerSlider(): string {
    // tslint:disable:max-line-length
    return `<bvl-banner-slider
      banners='[
        {
          "background-image": [
            {
              "name": "ABI_BannerRectangular3.png",
              "small": {
                "name": "469X115_ABI_BannerRectangular3.png",
                "width": 469,
                "height": 115,
                "imageUri": "https://s3.us-east-1.amazonaws.com/site.images.cdn.prod.bvl.com.pe/469X115_ABI_BannerRectangular3.png"
              },
              "medium": {
                "name": "939X231_ABI_BannerRectangular3.png",
                "width": 939,
                "height": 231,
                "imageUri": "https://s3.us-east-1.amazonaws.com/site.images.cdn.prod.bvl.com.pe/939X231_ABI_BannerRectangular3.png"
              },
              "large": {
                "name": "1878X462_ABI_BannerRectangular3.png",
                "width": 1878,
                "height": 462,
                "imageUri": "https://s3.us-east-1.amazonaws.com/site.images.cdn.prod.bvl.com.pe/1878X462_ABI_BannerRectangular3.png"
              },
              "selected": false
            }
          ],
          "title": "Tome mejores decisiones de inversión con ABI",
          "description": "Analice rápidamente la información financiera del mercado local en tiempo real sin importar el lugar donde se encuentre.",
          "is-button": null,
          "text-button": null,
          "url-button": null,
          "is-link": null,
          "text-link": null,
          "url-link": null
        },
        {
          "background-image": [
            {
              "name": "Como_listar_en_bolsa.png",
              "small": {
                "name": "469X115_Como_listar_en_bolsa.jpg",
                "width": 469,
                "height": 115,
                "imageUri": "https://s3.us-east-1.amazonaws.com/site.images.cdn.prod.bvl.com.pe/469X115_Como_listar_en_bolsa.png"
              },
              "medium": {
                "name": "939X231_Como_listar_en_bolsa.png",
                "width": 939,
                "height": 231,
                "imageUri": "https://s3.us-east-1.amazonaws.com/site.images.cdn.prod.bvl.com.pe/939X231_Como_listar_en_bolsa.png"
              },
              "large": {
                "name": "1878X462_Como_listar_en_bolsa.png",
                "width": 1878,
                "height": 462,
                "imageUri": "https://s3.us-east-1.amazonaws.com/site.images.cdn.prod.bvl.com.pe/1878X462_Como_listar_en_bolsa.png"
              },
              "selected": false
            }
          ],
          "title": "Lleve su empresa a nivel mundial",
          "description": "Listar en la BVL coloca a su empresa en los ojos del Perú y del mundo",
          "is-button": null,
          "text-button": null,
          "url-button": null,
          "is-link": null,
          "text-link": null,
          "url-link": null
        }
      ]'>
    </bvl-banner-slider>`;
  }

  constructor(
    private _router: ActivatedRoute,
    private _pageService: PageServices
  ) {
    this._pageService.setLanguageLocal(this._router.snapshot.data.lang);
  }

  ngOnInit(): void {
    this._setWebComponents();
  }

  private _setWebComponents(): void {
    addElementToRef({
      html:
      `
      <bvl-daily-table private-site=true></bvl-daily-table>
      <br>
      <bvl-index-table private-site=true></bvl-index-table>
      <br>
      <bvl-bag-play private-site='true'></bvl-bag-play>
      `,
      position: 'beforeend',
      parent: 'web-components'
    });
    addScriptTag(['banner-slider', 'daily-table', 'index-table', 'bag-play'], WC_BUNDLE_PATH);
  }
}
