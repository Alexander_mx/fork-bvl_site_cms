import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrivateSiteComponent } from './private-site.component';

const routes: Routes = [
  {
    path: '',
    component: PrivateSiteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateSiteRoutingModule { }
