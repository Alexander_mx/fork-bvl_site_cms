import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PipesModule } from '@bvl-core/shared/helpers/pipes';
import { PersonalDataCardModule } from '@bvl-site/components/personal-data-card/personal-data-card.module';
import { SubscriptionsCardModule } from '@bvl-site/components/subscriptions-card/subscriptions-card.module';
import { SectionHeaderModule } from '@bvl/library';
import { PrivateSiteRoutingModule } from './private-site-routing.module';
import { PrivateSiteComponent } from './private-site.component';

@NgModule({
  declarations: [PrivateSiteComponent],
  imports: [
    CommonModule,
    PrivateSiteRoutingModule,
    SectionHeaderModule,
    PersonalDataCardModule,
    SubscriptionsCardModule,
    PipesModule
  ]
})
export class PrivateSiteModule { }
