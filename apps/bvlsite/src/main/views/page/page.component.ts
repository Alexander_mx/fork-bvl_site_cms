import { isPlatformServer } from '@angular/common';
import { Component, HostBinding, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { from, Observable, Subscription } from 'rxjs';

import { ScriptService } from '@bvl-core/shared/core';
import { addElementToRef } from '@bvl-core/shared/helpers/util/web-components';
import { OnboardingIntroComponent } from '@bvl-site/components/modals/onboarding-intro/onboarding-intro.component';
import { OnboardingService } from '@bvl-site/services';
import { PageServices } from '@bvl-site/services/pages.services';
import { SEOService } from '@bvl-site/services/seo.service';
import { WC_BUNDLE_PATH } from '@bvl-site/settings/constants/general.constant';
import { IPageContentLanguage, IPageResponse } from '@bvl-site/statemanagement/models/page.interface';
import { SCRIPT_TYPE } from '@bvl/library';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'bvl-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit, OnDestroy {

  @HostBinding('attr.class') attrClass = 'g-sticky-content';

  private response: IPageResponse;

  withParent = false;
  private _pageSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private _platformId,
    private _activatedRoute: ActivatedRoute,
    private _onboardingService: OnboardingService,
    private _pagesServices: PageServices,
    private _seoService: SEOService,
    private _scriptService: ScriptService
  ) { }

  ngOnInit(): void {
    const page = this._activatedRoute.snapshot.data[0];
    this._setPage(page);
  }

  ngOnDestroy(): void {
    if (this._pageSubscription) { this._pageSubscription.unsubscribe(); }
  }

  private _setPage(response: IPageResponse): void {
    this.response = response;
    const page: IPageContentLanguage = response.languages && response.languages[0];

    this._addMeta(page);
    if (isPlatformServer(this._platformId)) {

      return void 0;
    }

    this._pagesServices.setPageCurrent(this.response);
    this._seoService.setFavicon(this.response.configuration.icon || 'favicon.ico');
    this._seoService.setLinksLang(this.response.paths);

    addElementToRef({
      html: page.content.html,
      position: 'beforeend',
      parent: 'page-view'
    });
    this._mapWebComponents(response)
      .pipe(delay(200))
      .subscribe(() => {
        this._rmLoader();
        this._handlerOnboarding();
      });

    this.withParent = !['/', null].includes(page.parentPath);
  }

  private _handlerOnboarding(): void {
    if (!this._onboardingService.hasOnboarding()) {
      return void 0;
    }

    if (this._onboardingService.wasLoaded()) {
      return void 0;
    }

    if (this._onboardingService.hasIntro()) {
      this._onboardingService.showIntro(OnboardingIntroComponent);

      return void 0;
    }

    setTimeout(() => {
      this._onboardingService.setActive(true);
    }, 500);
  }

  private _addMeta(page: IPageContentLanguage): void {
    const meta = page.metadata;
    meta.language = page.language;
    this._seoService.setData(page.metadata);
    this._seoService.setHtmlLang(page.language);
  }

  private _rmLoader(): any {
    const loader = document.querySelector('.pre-loader');
    loader.remove();
  }

  private _mapWebComponents(res: IPageResponse): Observable<any> {
    const page: IPageContentLanguage = res.languages && res.languages[0];

    const scripts = page.content.refComponents.map(ref => ({
      src: `${WC_BUNDLE_PATH}/${ref}/main.js`,
      type: SCRIPT_TYPE.JS
    }));

    return from(this._scriptService.load(scripts));
  }

}
