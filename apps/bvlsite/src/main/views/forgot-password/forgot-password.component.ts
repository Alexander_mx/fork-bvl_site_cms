import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfirmBase } from '../confirm.base';

@Component({
  selector: 'bvl-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent extends ConfirmBase implements OnInit {

  constructor(
    protected _activatedRoute: ActivatedRoute
  ) {
    super(_activatedRoute);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

}
