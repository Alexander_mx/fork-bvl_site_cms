import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ForgotPasswordFormModule } from '@bvl-site/components/forgot-password-form/forgot-password-form.module';
import { MessageFormModule } from '@bvl-site/components/message-form/message-form.module';
import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { ForgotPasswordComponent } from './forgot-password.component';

@NgModule({
  declarations: [ForgotPasswordComponent],
  imports: [
    CommonModule,
    ForgotPasswordRoutingModule,
    ForgotPasswordFormModule,
    MessageFormModule
  ]
})
export class ForgotPasswordModule { }
