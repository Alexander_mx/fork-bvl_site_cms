import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment_client } from './environments/environment';
import Amplify from 'aws-amplify';

if (environment_client.production) {
  enableProdMode();
}

document.addEventListener('DOMContentLoaded', () => {
  platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
});

Amplify.configure(environment_client.AWS_AMPLIFY_CONFIG);
