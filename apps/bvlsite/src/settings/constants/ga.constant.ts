export const GA_FOOTER_LINK = {
  control: 'link',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Footer',
  action: 'BVL - {{title}}',
  label: '{{link}}'
};

export const GA_FOOTER_SOCIAL_NETWORK = {
  control: 'socialNetworkLink',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Footer',
  action: 'BVL - Redes Sociales',
  label: '{{socialNetworkLink}}'
};

export const GA_CHANGE_PASSWORD_ACEPT_BUTTON = {
  control: 'aceptButton',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - {{title}}',
  label: 'Aceptar'
};

export const GA_CHANGE_PASSWORD_CANCEL_BUTTON = {
  control: 'cancelButton',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - {{title}}',
  label: 'Cancelar'
};

export const GA_EDIT_UPDATE_BUTTON = {
  control: 'updateButton',
  controlAction: 'custom',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - {{title}}',
  label: 'Actualizar',
  dimentions: 'tipoDocumento:{{tipoDocumento}}'
};

export const GA_EDIT_CANCEL_BUTTON = {
  control: 'cancelButton',
  controlAction: 'custom',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - {{title}}',
  label: 'Cancelar'
};

export const GA_EDIT_USER_LINK = {
  control: 'editDataLink',
  controlAction: 'custom',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - {{qs(H2)}}',
  label: 'Editar datos'
};

export const GA_CHANGE_PASSWORD_LINK = {
  control: 'changePasswordLink',
  controlAction: 'custom',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - {{qs(H2)}}',
  label: 'Cambiar contraseña'
};

export const GA_LOGOUT_BUTTON = {
  control: 'logoutButton',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - {{qs(H2)}}',
  label: 'Cerrar sesión'
};

export const GA_SUB_MENU = {
  control: 'subMenuLink',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Header',
  action: 'BVL - {{menu}} - Submenu',
  label: '{{subMenu}}'
};

export const GA_MENU = {
  control: 'menuLink',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Header',
  action: 'BVL - Menu principal',
  label: '{{menu}}'
};

export const GA_HEADER_LOGIN_ICON = {
  control: 'loginIcon',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Header',
  action: 'BVL - Identificate',
  label: 'Ir a iniciar sesion'
};

export const GA_HEADER_CONTACT_US_ICON = {
  control: 'contactUsIcon',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Header',
  action: 'BVL - Correo',
  label: 'Solicitar informacion'
};

export const GA_HEADER_SEARCH_ICON = {
  control: 'searchIcon',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Header',
  action: 'BVL - Buscador',
  label: 'Buscador'
};

export const GA_HEADER_SAB_LINK = {
  control: 'findYourSabLink',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Header',
  action: 'BVL - Encuentra tu sab',
  label: 'Ir a: Encuentra tu sab'
};

export const GA_HEADER_LOGO = {
  control: 'logoLink',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Header',
  action: 'BVL - Logo',
  label: 'Ir a inicio'
};

export const GA_SIGN_UP_BUTTON = {
  control: 'registerButton',
  controlAction: 'custom',
  event: 'ga_event_login',
  category: 'Login',
  action: 'BVL - Registrarse',
  label: 'Registrar - {{state}}',
  dimentions: 'tipoDocumento:{{tipoDocumento}},tipoRegistro:{{tipoRegistro}}'
};

export const GA_SIGN_UP_TERMS_COND = {
  control: 'termsConditionsLink',
  controlAction: 'custom',
  event: 'ga_event_login',
  category: 'Login',
  action: 'BVL - Registrarse',
  label: 'terminos y condiciones'
};

export const GA_RECOVER_PASSWORD = {
  control: 'sendButton',
  controlAction: 'custom',
  event: 'ga_event_login',
  category: 'Login',
  action: 'BVL - Restablecer contraseña',
  label: 'Enviar'
};

export const GA_LOGIN = {
  signIn: {
    control: 'signInButton',
    controlAction: 'custom',
    event: 'ga_event_login',
    category: 'Login',
    action: 'BVL - Identificate',
    label: 'Ingresar - {{state}}',
    dimentions: 'userId:{{g.userId}}'
  },
  forgotPassword: {
    control: 'forgotPasswordLink',
    controlAction: 'custom',
    event: 'ga_event_login',
    category: 'Login',
    action: 'BVL - Identificate',
    label: '¿Olvidaste tu contraseña?'
  },
  signUpHere: {
    control: 'signUpHereLink',
    controlAction: 'custom',
    event: 'ga_event_login',
    category: 'Login',
    action: 'BVL - Identificate',
    label: 'regístrate aquí'
  }
};

export const GA_CHANGE_LANG = {
  control: 'linkLang',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Header',
  action: 'BVL - Cambiar idioma',
  label: '{{lang}}'
};
