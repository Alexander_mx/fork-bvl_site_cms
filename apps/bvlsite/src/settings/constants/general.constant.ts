import { ISpinnerConfig } from '@bvl-core/shared/helpers/spinner';
import { environment_client } from '@bvl-site/environments/environment';
import { IDocumentType } from '@bvl-site/statemanagement/models/general.interface';

export const WC_BUNDLE_PATH = environment_client.WC_BUNDLE_PATH;

export const DATE_FORMAT = {
  ddMMyyyy: 'dd-MM-yyyy',
  ddMMyyyyHHmm: 'dd-MM-yyyy HH:mm'
};

export const COUNTRY_CODE = {
  peru: 'PE'
};

export const PERSON_TYPE = {
  natural: 'NATURAL',
  legal: 'JURIDICO'
};

export const DOCUMENT_TYPE: Array<IDocumentType> = [
  {
    code: 'DNI',
    description: 'DNI'
  },
  {
    code: 'CE',
    description: 'CE'
  },
  {
    code: 'PAS',
    description: 'PAS'
  }
];

export const FORM_TYPE = {
  signUp: 'SU',
  edition: 'E'
};

export const SPINNER_CONFIG: ISpinnerConfig = {
  spinnerIconClass: 'icon-spinner g-site-color--red1'
};

export const SWAL_CONFIG = {
  customContainerClass: 'g-site-modal-alert',
  customClass: '',
  reverseButtons: true,
  buttonsStyling: false,
  cancelButtonClass: 'btn btn-outline',
  confirmButtonClass: 'btn btn-primary'
};

export const STATIC_PAGES = {
  es: {
    marketAgentsList: 'mercado/agentes/listado',
    marketIndex: 'mercado/indices',
    privateSite: 'sitio-privado',
    contactUs: 'contactenos',
    search: 'busqueda',
    marketDailyMovement: 'mercado/movimientos-diarios'
  },
  en: {
    marketAgentsList: 'en/market/agents/list',
    marketIndex: 'market/index',
    privateSite: 'private-site',
    contactUs: 'contactus',
    search: 'search',
    marketDailyMovement: 'en/market/daily-movement'
  }
};

export const LANGUAGE_AVIABLES = [
  {
    value: 'es-ES',
    label: 'ES'
  },
  {
    value: 'en-EN',
    label: 'EN'
  }
];

export const MAPS_CONFIG_VALUES = {
  zoom: 16
};

export const COMPANY_VALUES = {
  sectorCodeFI: 'FI'
};

export const COMPANY_BENEFICT_TYPE = {
  DE: 'Efe.',
  AL : 'Accs.'
};
