export const IMAGES_PATH = {
  images: 'assets/images/'
};

export const PAYMENT_CARDS_PATH = {
  images: 'assets/images/payment-cards/'
};

export const SRC_PAYMENT_CARDS = [
  'assets/images/payment-cards/americanexpress.svg',
  'assets/images/payment-cards/mastercard.svg',
  'assets/images/payment-cards/visa.svg'
];
