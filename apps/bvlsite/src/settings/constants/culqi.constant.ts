import { environment_client } from '@bvl-site/environments/environment';

export const CULQI = {
  publicKey: environment_client.CULQI_PUBLIC_KEY
};
