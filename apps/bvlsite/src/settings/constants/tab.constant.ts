export const TAB_PLAN_PLAYMENT = [
  {
    name: 'Pago con tarjeta',
    route: './tarjeta',
    queryParamsHandling: 'preserve'
  },
  {
    name: 'Pago con depósito bancario',
    route: './deposito',
    queryParamsHandling: 'preserve'
  }
];
