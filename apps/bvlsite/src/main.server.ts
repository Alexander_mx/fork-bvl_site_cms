import { enableProdMode } from '@angular/core';

import { environment_client } from './environments/environment';

if (environment_client.production) {
  enableProdMode();
}

export { AppServerModule } from './app/app.server.module';
