import { Component, HostBinding, HostListener, OnInit, ViewEncapsulation } from '@angular/core';

import {
  ConfigurationService,
  DOCUMENT_GA_EVENT,
  EventGoogleAnalytics,
  GAService,
  IEventCustomGoogleAnalytics
} from '@bvl/library';

@Component({
  selector: 'bvl-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

  constructor(
    public gaService: GAService,
    private _configurationService: ConfigurationService
  ) {
  }

  @HostBinding('attr.class') attrClass = 'w-100';

  @HostListener(DOCUMENT_GA_EVENT, ['$event']) gaEvent(event: CustomEvent<IEventCustomGoogleAnalytics>): void {
    const ga = event.detail && event.detail.ga;
    if (ga) {
      const userProfile = this._configurationService.getUserProfile();
      const data = (event.detail.data || {});

      const eventGa = new EventGoogleAnalytics({
        event: ga.event,
        category: this._getValues(ga.category, event.detail.ctrl, data, event.detail.comp),
        action: this._getValues(ga.action, event.detail.ctrl, data, event.detail.comp),
        label: this._getValues(ga.label, event.detail.ctrl, data, event.detail.comp),
        value: this._getValues(ga.value, event.detail.ctrl, data, event.detail.comp)
      });
      if (!ga.dimentions && userProfile) {
        ga.dimentions = 'userId:{{g.userId}}';
      }
      if (ga.dimentions && ga.dimentions.indexOf('userId:') === -1 && userProfile) {
        ga.dimentions += ',userId:{{g.userId}}';
      }
      const dimension = this._getDimension(this._getValues(ga.dimentions, event.detail.ctrl, data, event.detail.comp));
      this.gaService.add({ ...eventGa, ...dimension });
    }
  }

  ngOnInit(): void { }

  private _getValues(label: string, element: any, data?: any, comp?: HTMLElement): string {
    let labelNew = label;
    if (label) {
      const existsGlobals = label.match(/{{g\.[a-zA-Z]+}}/gi);
      const existsData = label.match(/{{[a-zA-Z]+}}/gi);
      const existsQuerySelector = label.match(/{{qs\(.+?\)}}/gi);
      const existsQuerySelectorElement = label.match(/{{qse\(.+?\)}}/gi);

      if (existsGlobals) {
        labelNew = this._replaceValues(labelNew, this._getGlobalValues(comp));
      }

      if (existsData) {
        labelNew = this._replaceValues(labelNew, data);
      }

      if (existsQuerySelector) {
        labelNew = this._replaceValueQuerySelector(labelNew, existsQuerySelector, comp);
      }

      if (existsQuerySelectorElement) {
        labelNew = this._replaceValueQuerySelector(labelNew, existsQuerySelectorElement, element);
      }
    }

    return labelNew;
  }

  private _replaceValues(label: string, properties: any): string {
    let labelNew = label;
    Object.keys(properties)
      .forEach(name => {
        labelNew = labelNew.replace(`{{${name}}}`, properties[name]);
      });

    return labelNew;
  }

  private _replaceValueQuerySelector(value: string, existsQuerySelector: Array<string>, element: HTMLElement): string {
    const TAG_PROPERTY = {
      INPUT: 'value',
      OPTION: 'text'
    };

    return existsQuerySelector.reduce((previous, current) => {
      const querySelector = current.match(/([^{{qs?=e(].+)(?=[\)])/gi);
      const selectorElement = element.querySelector((querySelector) ? querySelector[0] : null);

      let replaceValue = '';
      if (selectorElement) {
        const property = TAG_PROPERTY[selectorElement.tagName] || 'textContent';
        replaceValue = (selectorElement[property] || '').trim();
      }
      previous = previous.replace(current, replaceValue);

      return previous;
    }, value);
  }

  private _getDimension(value: string): {} {
    if (!value) { return {}; }

    const values = value.split(',');

    return values.reduce((previous, current) => {
      const currentSplit = current.split(':');
      previous[currentSplit[0]] = currentSplit[1];

      return previous;
    }, {});
  }

  private _getGlobalValues(comp?: HTMLElement): {} {
    const referrer = document.referrer;
    const pathName = location.pathname;
    const urlPage = location.href;
    const KEY_REGEX_PATH = /^(?:\/es|\/en){0,1}\/([^\/]*).?([^\/]*)/i;
    const valuesPath = pathName.match(KEY_REGEX_PATH);
    const componentName = comp ? comp.localName : '';
    const userProfile = this._configurationService.getUserProfile();

    return {
      'g.url': urlPage,
      'g.path': pathName,
      'g.referrer': referrer,
      'g.section': valuesPath[1],
      'g.subsection': valuesPath[2],
      'g.componentname': componentName,
      'g.userId': (userProfile) ? userProfile.usernameMD5 : ''
    };
  }

}
