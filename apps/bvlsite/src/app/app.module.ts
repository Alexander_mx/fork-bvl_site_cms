import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { ApiModule } from '@bvl-core/shared/helpers/api';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { MODAL_CONFIG } from '@bvl-core/shared/settings/constants/general.constants';
import { OnboardingIntroModule } from '@bvl-site/components/modals/onboarding-intro/onboarding-intro.module';
import { MainLayoutModule } from '@bvl-site/layouts/main-layout/main-layout.module';
import { RoutesModule } from '@bvl-site/routes/routes.module';
import { SPINNER_CONFIG, SWAL_CONFIG } from '@bvl-site/settings/constants/general.constant';
import { GoogleModule, I18nEsPeModule, PipesModule } from '@bvl/library';
import { SEOProvider } from 'core/providers/seo.provider';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    ApiModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    FlexLayoutModule,
    GoogleModule,
    I18nEsPeModule,
    MainLayoutModule,
    ModalModule.forRoot(MODAL_CONFIG),
    OnboardingIntroModule,
    PipesModule,
    RoutesModule,
    SpinnerModule.forRoot(SPINNER_CONFIG),
    SweetAlert2Module.forRoot(SWAL_CONFIG),
    TranslateModule.forRoot()
  ],
  providers: [
    SEOProvider
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
