import { NgModule } from '@angular/core';
import { SERVER_TOKEN } from '@angular/flex-layout';
import { ServerModule } from '@angular/platform-server';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';

import { AppComponent } from './app.component';
import { AppModule } from './app.module';

@NgModule({
  imports: [
    AppModule,
    ModuleMapLoaderModule,
    ServerModule
  ],
  providers: [
    { provide: SERVER_TOKEN, useValue: true }
  ],
  bootstrap: [AppComponent]
})
export class AppServerModule { }
