import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { addGAScript } from '@bvl-core/shared/core';
import { GeneralService } from '@bvl-site/services';
import { IKeysConfiguration } from '@bvl-site/statemanagement/models/general.interface';
import { StorageService } from '@bvl/library';

@Injectable({
  providedIn: 'root'
})
export class PageAngularResolve implements Resolve<any> {

  constructor(
    private _generalService: GeneralService,
    private _storageService: StorageService
  ) { }

  resolve(): Observable<any> {
    return this._generalService.keysApi()
      .pipe(
        tap(this._mapKeys.bind(this)),
        tap(this._rmPreLoader.bind(this))
      );
  }

  private _mapKeys(res: IKeysConfiguration): void {
    this._storageService.setItem('keys_site', res);
    if (res.gtm) {
      addGAScript(res.gtm);
    }
  }

  private _rmPreLoader(): void {
    const loader = document.querySelector('.pre-loader');
    if (!loader) {

      return void 0;
    }

    loader.remove();
  }
}
