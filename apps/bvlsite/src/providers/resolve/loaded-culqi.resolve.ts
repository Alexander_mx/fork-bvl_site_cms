import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadedCulqiResolve implements Resolve<any> {

  constructor() { }

  resolve(): Observable<boolean> {
    return new Observable((observer: Observer<boolean>) => {
      const tmpScript = document.getElementById('culqi_script');
      if (!tmpScript) {
        const scriptTag = document.createElement(`script`);
        scriptTag.setAttribute('src', 'https://checkout.culqi.com/v2');
        scriptTag.setAttribute('type', 'text/javascript');
        scriptTag.setAttribute('id', `${tmpScript}`);
        scriptTag.onload = () => {
          observer.next(true);
          observer.complete();
        };
        scriptTag.onerror = (error: any) => {
          observer.error(false);
        };
        document.body.appendChild(scriptTag);
      }
    });
  }
}
