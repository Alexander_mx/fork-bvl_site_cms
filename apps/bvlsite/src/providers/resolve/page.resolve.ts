import { PlatformLocation } from '@angular/common';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Resolve } from '@angular/router';
import { addGAScript } from '@bvl-core/shared/core';
import { getPathArgument } from '@bvl-core/shared/helpers/util/path-arguments';
import { GeneralService, PageServices } from '@bvl-site/services';
import { IKeysConfiguration } from '@bvl-site/statemanagement/models/general.interface';
import { IPageResponse } from '@bvl-site/statemanagement/models/page.interface';
import { StorageService } from '@bvl/library';

@Injectable({
  providedIn: 'root'
})
export class PageResolve implements Resolve<any> {

  constructor(
    private _generalService: GeneralService,
    private _pagesServices: PageServices,
    private _platformLocation: PlatformLocation,
    private _storageService: StorageService
  ) { }

  resolve(): Observable<any> {
    return this._generalService.keysApi()
      .pipe(
        map(this._mapKeys.bind(this)),
        switchMap(this._getPage.bind(this))
      );
  }

  private _getPage(): Observable<IPageResponse> {
    const isHistory = this._getPath()
      .indexOf('history');

    return isHistory > -1
      ? this._pagesServices.getHistoryPage(getPathArgument('id'))
      : this._pagesServices.getPages({ path: this._getPath() });
  }

  private _mapKeys(res: IKeysConfiguration): void {
    this._storageService.saveStorage('keys_site', res);
    if (res.gtm) {
      addGAScript(res.gtm);
    }
  }

  private _getPath(): string {
    return this._platformLocation.pathname.length > 0 ? this._platformLocation.pathname.substring(1) : '/';
  }
}
