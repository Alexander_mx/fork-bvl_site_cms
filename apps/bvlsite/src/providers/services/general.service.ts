import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl-core/shared/helpers/api';
import { GeneralEndPoint } from '@bvl-site/endpoints';
import { IBusiness, IKeysConfiguration, ISearchRequest, ISearchResponse } from '@bvl-site/statemanagement/models/general.interface';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  constructor(
    private _apiService: ApiService
  ) { }

  businessCoreList(): Observable<Array<IBusiness>> {
    return this._apiService.get(GeneralEndPoint.businessCoreList);
  }

  searchPagesSite(param: ISearchRequest): Observable<Array<ISearchResponse>> {
    return this._apiService.get(`${GeneralEndPoint.searchPagesSite}${param.filter}`);
  }

  keysApi(): Observable<IKeysConfiguration> {
    return this._apiService.get(GeneralEndPoint.configurationKeys);
  }
}
