import { Injectable } from '@angular/core';
import { ApiService } from '@bvl-core/shared/helpers/api';
import { PaymentEndPoint } from '@bvl-site/endpoints';
import {
  ICancelSubscriptionRequest,
  ICheckoutRequest,
  IPlanResponse,
  ISubscriptionResponse
} from '@bvl-site/statemanagement/models/payment.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(
    private _apiService: ApiService
  ) { }

  planById(idPlan: string, showSpin: boolean = false): Observable<IPlanResponse> {
    const params = { idPlan };

    return this._apiService.get(PaymentEndPoint.planById, { params, preloader: showSpin });
  }

  checkout(params: ICheckoutRequest, showSpin: boolean = false): Observable<any> {
    return this._apiService.post(PaymentEndPoint.checkout, params, { preloader: showSpin });
  }

  subscriptionsList(idCognito: string, showSpin: boolean = false): Observable<Array<ISubscriptionResponse>> {
    const params = { idCognito };

    return this._apiService.get(PaymentEndPoint.subscriptionsList, { params, preloader: showSpin });
  }

  cancelSubscription(params: ICancelSubscriptionRequest, showSpin: boolean = false): Observable<any> {
    return this._apiService.post(PaymentEndPoint.cancelSubscription, params, { preloader: showSpin });
  }

}
