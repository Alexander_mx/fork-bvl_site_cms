import { Injectable } from '@angular/core';
import { ApiService } from '@bvl-core/shared/helpers/api';
import { UbigeoEndPoint } from '@bvl-site/endpoints';
import {
  IDistrictResponse,
  IProvinceResponse,
  IRegionResponse
} from '@bvl-site/statemanagement/models/ubigeo.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UbigeoService {

  constructor(
    private _apiService: ApiService
  ) { }

  regionsList(showSpin: boolean = false): Observable<Array<IRegionResponse>> {
    return this._apiService.get(UbigeoEndPoint.regionsList, { preloader: showSpin });
  }

  provincesList(idRegion: string, showSpin: boolean = false): Observable<Array<IProvinceResponse>> {
    const params = { idRegion };

    return this._apiService.get(UbigeoEndPoint.provincesList, { params, preloader: showSpin });
  }

  districtsList(idRegion: string, idProvince: string, showSpin: boolean = false): Observable<Array<IDistrictResponse>> {
    const params = { idRegion, idProvince };

    return this._apiService.get(UbigeoEndPoint.districtsList, { params, preloader: showSpin });
  }

}
