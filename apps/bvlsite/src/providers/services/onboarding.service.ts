import { ApplicationRef, ComponentFactoryResolver, ComponentRef, Injectable, Injector, Renderer2, RendererFactory2 } from '@angular/core';
import { ModalService } from '@bvl-core/shared/helpers/modal';
import { AngularUtil, PlatformService, ScrollUtil, UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { CookieStorage } from '@bvl-core/shared/helpers/util/storage-manager';
import { STORAGE_ONBOARDING } from '@bvl-site/settings/constants/storage-key.constant';
import { ActiveOnboarding, GuideMessageComponent, GUIDES, IOnboarding, ONBOARDING_ACTION } from '@bvl/library/guide-message';
import { BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class OnboardingService extends UnsubscribeOnDestroy {

  GUIDES = GUIDES;
  ONBOARDING_ACTION = ONBOARDING_ACTION;

  private _renderer2: Renderer2;
  private _headerElement: any;
  private _realtimeElement: any;
  private _onboardingGuide: any;
  private _currentOnboarding: IOnboarding;
  private _currentElement: HTMLElement;
  private _onboardingRef: ComponentRef<GuideMessageComponent>;
  private _activeBehaviorSubject: BehaviorSubject<boolean>;
  private _actionBehaviorSubject: BehaviorSubject<any>;
  private _separation = 26;
  private _shadowFocus = 10;
  private _cookieStorage: CookieStorage;

  constructor(
    private _applicationRef: ApplicationRef,
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _injector: Injector,
    private _platformService: PlatformService,
    private _rendererFactory2: RendererFactory2,
    private _modalService: ModalService
  ) {
    super();

    this._cookieStorage = new CookieStorage();
    this._renderer2 = this._rendererFactory2.createRenderer(null, null);

    this._onboardingGuide = this._getGuide();

    this._activeBehaviorSubject = new BehaviorSubject<boolean>(false);
    this._actionBehaviorSubject = new BehaviorSubject<any>(null);
  }

  wasLoaded(): boolean {
    return !JSON.parse(this._cookieStorage.getItem(`${STORAGE_ONBOARDING}_${location.pathname}`) || 'true');
  }

  hasOnboarding(): boolean {
    return !!this._onboardingGuide;
  }

  hasIntro(): boolean {
    return this._onboardingGuide.showIntro;
  }

  getActive(): BehaviorSubject<boolean> {
    return this._activeBehaviorSubject;
  }

  setActive(value: boolean): void {
    const active = value && !!this._onboardingGuide;

    this._cookieStorage.createCookie(`${STORAGE_ONBOARDING}_${location.pathname}`, active.toString(), 9000);

    if (active) {
      this._actionBehaviorSubject.next(ONBOARDING_ACTION.init);
    }

    this._activeBehaviorSubject.next(active);
  }

  listenOnboarding(): BehaviorSubject<any> {
    return this._actionBehaviorSubject;
  }

  showOnboarding(action: any): any {
    let index = -1;
    switch (action) {
      case ONBOARDING_ACTION.init:
        this._addHeaderStatic();
        this._addRealtimeStatic();
        this._addBodyActive();
        index = 0;
        break;
      case ONBOARDING_ACTION.next:
        index = this._currentOnboarding.index + 1;
        break;
      case ONBOARDING_ACTION.prev:
        index = this._currentOnboarding.index - 1;
        break;
      default:
        break;
    }
    this._removeFocus();
    this._removeOverlay();
    if (index !== -1) {
      this._showOnboarding(index);
    } else {
      this._closeOnboarding();
    }
  }

  showIntro(component: any): void {
    this._modalService.open(component, { showFooter: false })
      .result
      .then(this.setActive.bind(this, true))
      .catch(() => { });
  }

  private _addHeaderStatic(): void {
    this._headerElement = document.querySelector('.g-site-header');
    this._renderer2.addClass(this._headerElement, 'position-absolute');
  }

  private _removeHeaderStatic(): void {
    if (this._headerElement) {
      this._renderer2.removeClass(this._headerElement, 'position-absolute');
    }
  }

  private _addRealtimeStatic(): void {
    this._realtimeElement = document.querySelector('.g-site-realtime');
    this._renderer2.addClass(this._realtimeElement, 'position-absolute');
    this._renderer2.setStyle(this._realtimeElement, 'margin-bottom', '-41px');
  }

  private _removeRealtimeStatic(): void {
    if (this._realtimeElement) {
      this._renderer2.removeClass(this._realtimeElement, 'position-absolute');
      this._renderer2.removeStyle(this._realtimeElement, 'margin-bottom');
    }
  }

  private _addBodyActive(): void {
    this._renderer2.addClass(document.body, 'g-site-onboarding-active');
  }

  private _removeBodyActive(): void {
    this._renderer2.removeClass(document.body, 'g-site-onboarding-active');
  }

  private _addFocus(): void {
    if (this._currentElement) {
      this._renderer2.addClass(this._currentElement, 'g-site-onboarding-focus');
    }
  }

  private _removeFocus(): void {
    if (this._currentElement) {
      this._renderer2.removeClass(this._currentElement, 'g-site-onboarding-focus');
    }
  }

  private _addOverlay(): void {
    const divGuideOverlay = document.createElement('div');
    this._renderer2.addClass(divGuideOverlay, 'g-site-onboarding-overlay');
    this._currentElement.insertAdjacentElement('afterend', divGuideOverlay);
  }

  private _removeOverlay(): void {
    const overlayElement = document.querySelector('.g-site-onboarding-overlay');
    if (overlayElement) { overlayElement.remove(); }
  }

  private _showOnboarding(index): void {
    this._currentOnboarding = this._onboardingGuide.process[index];
    this._currentOnboarding.index = index;
    this._currentOnboarding.totalGuide = this._onboardingGuide.process.length;

    this._currentElement = document.querySelector<HTMLElement>(this._currentOnboarding.target);

    if (!this._currentElement) {
      this._actionBehaviorSubject.next(ONBOARDING_ACTION.end);

      return void 0;
    }

    setTimeout(() => {
      this._addFocus();
      this._addOverlay();

      if (!this._onboardingRef) {
        const activeOnboarding = new ActiveOnboarding();
        // Create a component reference from the component
        const onboardingFactory = this._componentFactoryResolver.resolveComponentFactory(GuideMessageComponent);
        const newContentInjector = Injector.create({
          providers: [
            {
              provide: ActiveOnboarding,
              useValue: activeOnboarding
            }
          ],
          parent: this._injector
        });
        this._onboardingRef = onboardingFactory.create(newContentInjector);
        // Attach component to the appRef so that it's inside the ng component tree
        this._applicationRef.attachView(this._onboardingRef.hostView);
        // Asign setAction to model
        activeOnboarding.setAction = (action: any) => { this._actionBehaviorSubject.next(action); };
        // Get DOM element from component
        const onboardingElement = this._onboardingRef.location.nativeElement;
        // Append DOM element to the body
        const body = document.querySelector('body');
        body.appendChild(onboardingElement);
      }
      // Instance configuration
      this._onboardingRef.instance.process = this._currentOnboarding;
      this._onboardingRef.changeDetectorRef.detectChanges();
      this._onboardingRef.instance.position = this._getPosition(this._currentElement);
      this._onboardingRef.changeDetectorRef.detectChanges();

      if (this._currentOnboarding.click) {
        this._clickTrigger(this._currentOnboarding.click);
      }

      const onboardingElementRef: HTMLElement = this._onboardingRef.location.nativeElement.firstElementChild;

      const offsetElement = AngularUtil.offset(this._currentElement);

      const jointHeight = this._currentElement.clientHeight + onboardingElementRef.clientHeight;
      const remainderHeight = window.innerHeight - jointHeight;
      const top = window.innerHeight < jointHeight && this._platformService.isMobile
        ? (offsetElement.top - (remainderHeight - 60))
        : offsetElement.top;

      ScrollUtil.goTo(top - 20)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => { });
    }, 0);
  }

  private _closeOnboarding(): void {
    this._removeHeaderStatic();
    this._removeRealtimeStatic();
    this._removeBodyActive();
    // remove DOM onboardingElement from body
    const body = document.querySelector('body');
    body.removeChild(this._onboardingRef.location.nativeElement);
    // detach onboardingRef inside the ng component tree
    this._applicationRef.detachView(this._onboardingRef.hostView);
    // destroy onboardingRef
    this._onboardingRef.destroy();
    this._onboardingRef = null;
    this.setActive(false);
    ScrollUtil.goTo(0)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => { });
  }

  private _clickTrigger(element): void {
    const findElement = document.querySelectorAll(element);
    if (findElement) {
      findElement[0].click(true);
    }
  }

  private _getPosition(target): any {
    const offsetElement = AngularUtil.offset(target);
    const offsetOnboardingElement = AngularUtil.offset(this._onboardingRef.location.nativeElement.firstElementChild);

    const isMobile = this._platformService.isMobile;

    if (isMobile) {
      const top = `${offsetElement.top + (offsetElement.height + this._separation)}px`;

      return { top, left: '0', right: '0' };
    }

    const screenWidth = document.querySelector('body').clientWidth;
    const position = { top: '0px', left: '0px', right: 'inherit' };

    switch (this._currentOnboarding.position) {
      case 'right':
        position.left = `${offsetElement.left + offsetElement.width + this._separation}px`;
        position.top = `${offsetElement.top - this._shadowFocus}px`;
        break;
      case 'left':
        position.left = `inherit`;
        position.right = `${screenWidth - offsetElement.left + this._separation}px`;
        position.top = `${offsetElement.top - this._shadowFocus}px`;
        break;
      case 'bottom':
        position.left = `${offsetElement.left - (offsetOnboardingElement.width / 2 - (offsetElement.width / 2))}px`;
        position.top = `${offsetElement.top + (offsetElement.height + this._separation)}px`;
        break;
      case 'top':
        position.left = `${offsetElement.left - (offsetOnboardingElement.width / 2 - (offsetElement.width / 2))}px`;
        position.top = `${offsetElement.top - (offsetOnboardingElement.height + this._separation)}px`;
        break;
      default:
        break;
    }

    return position;
  }

  private _getGuide(): any {
    let guide = GUIDES.find(item => item.path === location.pathname);

    if (!guide) {
      return undefined;
    }

    guide = {
      ...guide,
      process: this._platformService.isMobile
        ? guide.process.filter(step => !step.hideMobile)
        : guide.process.filter(step => !step.hideDesktop)
    };

    if (!guide.process.length) {
      return undefined;
    }

    return guide;
  }

}
