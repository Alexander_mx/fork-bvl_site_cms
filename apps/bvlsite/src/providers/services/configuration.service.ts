import { Injectable } from '@angular/core';
import { STORAGE_TYPE, StorageService } from '@bvl-core/shared/helpers/util';
import { STORAGE_KEY } from '@bvl-site/settings/constants/storage-key.constant';
import { IUserProfile } from '@bvl-site/statemanagement/models/auth.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  userProfileBehaviorSubject: BehaviorSubject<IUserProfile>;

  constructor(
    private _storageService: StorageService
  ) {
    this._storageService.setConfig({ storageType: STORAGE_TYPE.local });
    this.userProfileBehaviorSubject = new BehaviorSubject<IUserProfile>(null);
  }

  setUserProfile(userProfile: IUserProfile): void {
    this._storageService.setItem(STORAGE_KEY.userProfile, userProfile);
    this.userProfileBehaviorSubject.next(userProfile);
  }

  getUserProfile(): IUserProfile {
    return this._storageService.getItemObject(STORAGE_KEY.userProfile);
  }

}
