import { Injectable } from '@angular/core';
import { ApiService } from '@bvl-core/shared/helpers/api';
import { LANGUAGE_TYPES_CODE } from '@bvl-core/shared/settings/constants/general.constants';
import { PageEndPoints } from '@bvl-site/endpoints/page.endpoint';
import { STATIC_PAGES } from '@bvl-site/settings/constants/general.constant';
import { IPageResponse, IPathPage } from '@bvl-site/statemanagement/models/page.interface';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageServices {

  pageCurrent: IPageResponse;
  pageCurrentSub: BehaviorSubject<IPageResponse> = new BehaviorSubject<IPageResponse>(null);
  languageSub: BehaviorSubject<LANGUAGE_TYPES_CODE> = new BehaviorSubject<LANGUAGE_TYPES_CODE>(null);

  constructor(
    private _apiService: ApiService
  ) { }

  getPages(path: { path: string }): Observable<IPageResponse> {
    return this._apiService.post(PageEndPoints.page, path);
  }

  getHistoryPage(id: string): Observable<any> {
    return this._apiService.get(PageEndPoints.history, { params: { id } });
  }

  getMenu(language: string): Observable<any> {
    return this._apiService.get(PageEndPoints.menu, { params: { language } });
  }

  setPageCurrent(page: IPageResponse): void {
    this.pageCurrent = page;
    this.setLanguageLocal(this.getCurrentLang());
    this.pageCurrentSub.next(this.pageCurrent);
  }

  setLanguageLocal(lang: string): void {
    localStorage.setItem('language', lang);
  }

  getPageCurrent(): IPageResponse {
    return this.pageCurrent;
  }

  getPageCurrenSub(): BehaviorSubject<IPageResponse> {
    return this.pageCurrentSub;
  }

  getCurrentLang(): string {
    return this.pageCurrent && this.pageCurrent.languages && this.pageCurrent.languages[0].language || '';
  }

  getPaths(code?: string): Array<IPathPage> {
    return (code)
            ?  Object.keys(STATIC_PAGES)
                .map(lang => {
                  return {
                    language: lang,
                    path: STATIC_PAGES[lang][code]
                  };
                })
            : this.pageCurrent && this.pageCurrent.paths;
  }

}
