import { DOCUMENT } from '@angular/common';
import {
  Inject,
  Injectable,
  RendererFactory2,
  ViewEncapsulation
} from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

import { YES_NOT_CODE } from '@bvl-core/shared/settings/constants/general.constants';
import { IMetadata, IPathPage } from '../../statemanagement/models/page.interface';

export declare type LinkDefinition = {
  charset?: string;
  crossorigin?: string;
  href?: string;
  hreflang?: string;
  media?: string;
  rel?: string;
  rev?: string;
  sizes?: string;
  target?: string;
  type?: string;
} & {
  [prop: string]: string;
};

@Injectable({
  providedIn: 'root'
})
export class SEOService {
  constructor(
    @Inject(DOCUMENT) private _document,
    private _metaService: Meta,
    private _rendererFactory: RendererFactory2,
    private _titleService: Title
  ) { }

  setData(metaData: IMetadata): void {
    if (!metaData) {
      return void 0;
    }

    // Setear Titulo
    this._addPageTitle(metaData.title);

    const metaRobots = this._getMetaRobots(metaData);
    const metaGeneral = this._getMetaGeneral(metaData);
    const metaOPG = this._getMetaOpg(metaData);
    const metaTwitter = this._getMetaTwitter(metaData);

    // Set Metadata
    this._addMetaTags([...metaRobots, ...metaGeneral, ...metaOPG, ...metaTwitter]);

    // Set Links
    this._addTag({ rel: 'canonical', href: this._getCanonical(metaData) }, true);
  }

  setLinksLang(paths: Array<IPathPage>): void {
    (paths || []).forEach(path => {
      this._addTag(
        {
          rel: 'alternate',
          href: `${this._getDomain()}/${path.path}`,
          hreflang: path.language
        },
        true
      );
    });
  }

  setFavicon(favicon: string): void {
    this._addTag(
      {
        rel: 'icon',
        href: favicon,
        type: 'image/png'
      },
      true
    );
  }

  setHtmlLang(language: string): void {
    this._document.documentElement.lang = language;
  }

  getUrlWithDomain(path: string): string {
    return `${this._getDomain()}/${path}`;
  }

  getUrlAll(path: string): string {
    return `${this._getDomain()}/${path}${location.search}${location.hash}`;
  }

  private _getMetaOpg(metaData: IMetadata): Array<any> {
    const metas = [];

    metas.push({ property: 'og:title', content: this._getOgTitle(metaData) });
    metas.push({ property: 'og:locale', content: metaData.language });
    metas.push({ property: 'og:type', content: metaData.ogType });
    metas.push({ property: 'og:url', content: this._getOgUrl(metaData) });
    metas.push({ property: 'og:image', content: this._getOgImage(metaData) });
    metas.push({ property: 'og:description', content: this._getOgDescription(metaData) });
    metas.push({ property: 'og:site_name', content: metaData.ogSitename });

    return metas;
  }

  private _getMetaTwitter(metaData: IMetadata): Array<any> {
    const metas = [];

    metas.push({ name: 'twitter:card', content: metaData.twitterCard });
    metas.push({ name: 'twitter:site', content: this._getTwitteSite(metaData) });
    metas.push({ name: 'twitter:creator', content: this._getTwitteSite(metaData) });
    metas.push({ name: 'twitter:title', content: this._getTwitterTitle(metaData) });
    metas.push({ name: 'twitter:description', content: this._getTwitterDescription(metaData) });
    metas.push({ name: 'twitter:image', content: this._getTwitterImage(metaData) });

    return metas;
  }

  private _getMetaGeneral(metaData: IMetadata): Array<any> {
    const metas = [];

    metas.push({ name: 'description', content: metaData.description });

    return metas;
  }

  private _getMetaRobots(metaData: IMetadata): Array<any> {
    metaData.avancedRobots = metaData.avancedRobots || [];

    const hasMeta = metaData.noindex === YES_NOT_CODE.YES && metaData.nofollow === YES_NOT_CODE.YES && ![...metaData.avancedRobots].length;
    if (hasMeta) {
      return [];
    }

    if (metaData.noindex === YES_NOT_CODE.NO && metaData.nofollow === YES_NOT_CODE.NO) {
      return [{ name: 'robots', content: 'noindex,nofollow' }];
    }

    const metas = [];

    metas.push(`${metaData.noindex === YES_NOT_CODE.NO ? 'no' : ''}index`);
    metas.push(`${metaData.nofollow === YES_NOT_CODE.NO ? 'no' : ''}follow`);

    const content = [...metas, ...metaData.avancedRobots].join(',');

    return [{ name: 'robots', content }];
  }

  private _getDomain(): string {
    return `${location.protocol}//${location.hostname}${
      location.port ? `:${location.port}` : ''
      }`;
  }

  private _getCanonical(metaData: IMetadata): string {
    return metaData.canonicalLink || this._document.URL;
  }

  private _getOgTitle(metaData: IMetadata): string {
    return metaData.ogTitle || metaData.title;
  }

  private _getOgUrl(metaData: IMetadata): string {
    return metaData.ogUrl || this._document.URL;
  }

  private _getOgDescription(metaData: IMetadata): string {
    return metaData.ogDescription || metaData.description;
  }

  private _getOgImage(metaData: IMetadata): string {
    return metaData.ogImage;
  }

  private _getTwitterTitle(metaData: IMetadata): string {
    return metaData.twitterTitle || metaData.ogTitle || metaData.title;
  }

  private _getTwitterDescription(metaData: IMetadata): string {
    return (
      metaData.twitterDescription ||
      metaData.ogDescription ||
      metaData.description
    );
  }

  private _getTwitterImage(metaData: IMetadata): string {
    return metaData.twitterImage || metaData.ogImage;
  }

  private _getTwitteSite(metaData: IMetadata): string {
    const twitterSite = (metaData.twitterSite || '');

    return twitterSite.charAt(0) === '@' ? twitterSite : `@${twitterSite}`;
  }

  private _addMetaTags(
    metas: Array<{ name?: string; property?: string; content: string }>
  ): void {
    metas
      .filter(item => !!item.content)
      .forEach(item => {
        if (item.name) {
          this._removeTag(`name='${item.name}'`);
          this._metaService.addTag(
            { name: item.name, content: item.content },
            true
          );

          return void 0;
        }

        this._removeTag(`property='${item.property}'`);
        this._metaService.addTag(
          { property: item.property, content: item.content },
          true
        );

      });
  }

  private _addPageTitle(title: string): void {
    this._titleService.setTitle(title);
  }

  private _addTag(tag: LinkDefinition, forceCreation?: boolean): void {
    try {
      const renderer = this._rendererFactory.createRenderer(this._document, {
        id: '-1',
        encapsulation: ViewEncapsulation.None,
        styles: [],
        data: {}
      });

      const link = renderer.createElement('link');
      const head = this._document.head;
      // const selector = this._parseSelector(tag);

      if (head === null) {
        throw new Error('<head> not found within DOCUMENT.');
      }

      Object.keys(tag)
        .forEach((prop: string) => {
          return renderer.setAttribute(link, prop, tag[prop]);
        });

      // [TODO]: get them to update the existing one (if it exists) ?
      renderer.appendChild(head, link);
    } catch (e) {
      console.error('Error within linkService : ', e);
    }
  }

  private _removeTag(tag: string): void {
    try {
      this._metaService.removeTag(tag);
    } catch { }
  }
}
