import { Injectable } from '@angular/core';
import { ApiService } from '@bvl-core/shared/helpers/api';
import { STORAGE_TYPE, StorageService } from '@bvl-core/shared/helpers/util';
import { STORAGE_KEY } from '@bvl-site/settings/constants/storage-key.constant';
import { ILoginRequest, INewPasswordRequest, ISignUpRequest, IUserAttributes } from '@bvl-site/statemanagement/models/auth.interface';
import { Auth } from 'aws-amplify';
import { from, Observable } from 'rxjs';
import { finalize, flatMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private _storageService: StorageService,
    private _apiService: ApiService
  ) {
    this._storageService.setConfig({ storageType: STORAGE_TYPE.local });
  }

  isAuthenticated(): boolean {
    return !!this._storageService.getItemObject(STORAGE_KEY.userProfile);
  }

  private _getPipesDefault(showSpin: boolean): Array<any> {
    const pipes = [];
    pipes.push(finalize(() => this._apiService.afterRequest(showSpin)));

    return pipes;
  }

  signIn(params: ILoginRequest, showSpin: boolean = false): Observable<any> {
    this._apiService.beforeRequest(showSpin);
    const pipes = this._getPipesDefault(showSpin),
          observable = from(Auth.signIn(params));

    return this._apiService.formatObservablePipe(observable, pipes);
  }

  forgotPassword(email: string, showSpin: boolean = false): Observable<any> {
    this._apiService.beforeRequest(showSpin);
    const pipes = this._getPipesDefault(showSpin),
          observable = from(Auth.forgotPassword(email));

    return this._apiService.formatObservablePipe(observable, pipes);
  }

  forgotPasswordSubmit(params: INewPasswordRequest, showSpin: boolean = false): Observable<any> {
    this._apiService.beforeRequest(showSpin);
    const pipes = this._getPipesDefault(showSpin),
          observable = from(Auth.forgotPasswordSubmit(params.username, params.code, params.password));

    return this._apiService.formatObservablePipe(observable, pipes);
  }

  signUp(params: ISignUpRequest, showSpin: boolean = false): Observable<any> {
    this._apiService.beforeRequest(showSpin);
    const pipes = this._getPipesDefault(showSpin),
          observable = from(Auth.signUp(params));

    return this._apiService.formatObservablePipe(observable, pipes);
  }

  updateUserAttributes(params: IUserAttributes, showSpin: boolean = false): Observable<any> {
    this._apiService.beforeRequest(showSpin);
    const pipes = this._getPipesDefault(showSpin),
          observable = from(Auth.currentAuthenticatedUser())
                        .pipe(
                          flatMap((resUser: any) => {
                            return from(Auth.updateUserAttributes(resUser, params));
                          })
                        );

    return this._apiService.formatObservablePipe(observable, pipes);
  }

  changePassword(password: string, newPassword: string, showSpin: boolean = false): Observable<any> {
    this._apiService.beforeRequest(showSpin);
    const pipes = this._getPipesDefault(showSpin),
          observable = from(Auth.currentAuthenticatedUser())
                        .pipe(
                          flatMap((resUser: any) => {
                            return from(Auth.changePassword(resUser, password, newPassword));
                          })
                        );

    return this._apiService.formatObservablePipe(observable, pipes);
  }

  logout(showSpin: boolean = false): Observable<any> {
    this._apiService.beforeRequest(showSpin);
    const pipes = this._getPipesDefault(showSpin),
          observable = from(Auth.signOut())
                        .pipe(
                          tap(() => {
                            const storageTypes = [STORAGE_TYPE.cookie, STORAGE_TYPE.local, STORAGE_TYPE.session];
                            for (const storage of storageTypes) {
                              this._storageService.setConfig({ storageType: storage });
                              this._storageService.clear();
                            }
                          })
                        );

    return this._apiService.formatObservablePipe(observable, pipes);
  }

}
