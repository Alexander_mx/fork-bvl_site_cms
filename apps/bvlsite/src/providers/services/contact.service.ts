import { Injectable } from '@angular/core';
import { ApiService } from '@bvl-core/shared/helpers/api';
import { ContactEndPoint } from '@bvl-site/endpoints/contact.endpoint';
import { IContactRequest, IContactResponse } from '@bvl-site/statemanagement/models/contact.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(
    private _apiService: ApiService
  ) { }

  saveContact(contactRequest: IContactRequest, preloader: boolean = true): Observable<IContactResponse> {
    return this._apiService.post(ContactEndPoint.saveContact, contactRequest, { preloader });
  }

}
