import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  private _headerHeight: BehaviorSubject<number>;

  constructor() {
    this._headerHeight = new BehaviorSubject<number>(0);
  }

  getHeaderHeight(): BehaviorSubject<number> {
    return this._headerHeight;
  }

  setHeaderHeight(height): void {
    this._headerHeight.next(height);
  }

}
