export { UbigeoEndPoint } from './ubigeo.endpoint';
export { PaymentEndPoint } from './payment.endpoint';
export { GeneralEndPoint } from './general.endpoint';
