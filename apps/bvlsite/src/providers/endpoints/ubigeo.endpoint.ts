import { environment_client } from '../../environments/environment';

export class UbigeoEndPoint {
  public static regionsList = `${environment_client.API_URL}/auth/ubigeo/regions`;
  public static provincesList = `${environment_client.API_URL}/auth/ubigeo/provinces`;
  public static districtsList = `${environment_client.API_URL}/auth/ubigeo/districts`;
}
