import { environment_client } from '../../environments/environment';

export class ContactEndPoint {
  public static saveContact = `${environment_client.API_URL}/contacts`;
}
