import { environment_client } from '../../environments/environment';

export class PageEndPoints {
  public static page = `${environment_client.API_URL}/site/getPage`;
  public static menu = `${environment_client.API_URL}/site/menus`;
  public static history = `${environment_client.API_URL}/page/history/restore/{id}`;
}
