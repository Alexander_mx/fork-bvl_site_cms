import { environment_client } from '../../environments/environment';

export class GeneralEndPoint {
  public static businessCoreList = `${environment_client.API_URL}/businessCore`;
  public static searchPagesSite = `${environment_client.API_URL}/site/pages/search?value=`;
  public static configurationKeys = `${environment_client.API_URL}/site/configurations/keys`;
}
