import { environment_client } from '../../environments/environment';

export class PaymentEndPoint {

  public static planById = `${environment_client.API_URL}/auth/culqi/plans/{idPlan}`;
  public static checkout = `${environment_client.API_URL}/auth/pasarela/checkout`;
  public static subscriptionsList = `${environment_client.API_URL}/auth/subscriptions/{idCognito}`;
  public static cancelSubscription = `${environment_client.API_URL}/auth/subscriptions`;

}
