import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@bvl-site/guards';
import { MainLayoutComponent } from '@bvl-site/layouts/main-layout/main-layout.component';
import { PageAngularResolve, PageResolve } from '@bvl-site/resolve';
import { LoadedCulqiResolve } from '@bvl-site/resolve/loaded-culqi.resolve';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'contactenos',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'es',
          code: 'contactUs',
          isInternal: true
        },
        loadChildren: '../main/views/contact-us/contact-us.module#ContactUsModule'
      },
      {
        path: 'contactus',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'en',
          code: 'contactUs',
          isInternal: true
        },
        loadChildren: '../main/views/contact-us/contact-us.module#ContactUsModule'
      },
      {
        path: 'login',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'es',
          isInternal: true
        },
        loadChildren: '../main/views/login/login.module#LoginModule'
      },
      {
        path: 'restablecer-contrasena',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'es',
          isInternal: true
        },
        loadChildren: '../main/views/forgot-password/forgot-password.module#ForgotPasswordModule'
      },
      {
        path: 'nueva-contrasena',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'es',
          isInternal: true
        },
        loadChildren: '../main/views/new-password/new-password.module#NewPasswordModule'
      },
      {
        path: 'registrate',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'es',
          isInternal: true
        },
        loadChildren: '../main/views/sign-up/sign-up.module#SignUpModule'
      },
      {
        path: 'sitio-privado',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'es',
          code: 'privateSite',
          isInternal: true
        },
        canActivate: [AuthGuard],
        loadChildren: '../main/views/private-site/private-site.module#PrivateSiteModule'
      },
      {
        path: 'private-site',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'en',
          code: 'privateSite',
          isInternal: true
        },
        canActivate: [AuthGuard],
        loadChildren: '../main/views/private-site/private-site.module#PrivateSiteModule'
      },
      {
        path: 'pago-plan',
        resolve: {
          loadedCulqiResolve: LoadedCulqiResolve,
          keys: PageAngularResolve
        },
        canActivate: [AuthGuard],
        loadChildren: '../main/views/plan-payment/plan-payment.module#PlanPaymentModule'
      },
      {
        path: 'busqueda',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'es',
          code: 'search',
          isInternal: true
        },
        loadChildren: '../main/views/search-page/search-page.module#SearchPageModule'
      },
      {
        path: 'search',
        resolve: {
          keys: PageAngularResolve
        },
        data: {
          lang: 'en',
          code: 'search',
          isInternal: true
        },
        loadChildren: '../main/views/search-page/search-page.module#SearchPageModule'
      },
      {
        path: '**',
        resolve: [PageResolve],
        loadChildren: '../main/views/page/page.module#PageModule',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      paramsInheritanceStrategy: 'always'
    })
  ],
  exports: [
    RouterModule
  ]
})
export class RoutesModule { }
