import { Application } from 'express';
import * as express from 'express';
import dotenv from 'dotenv';
import { join } from 'path';
import { SiteRoutes } from './src/routes/site-routes';
import { UniversalEngine } from './src/engine/universal-engine';
import { SeoRoutes } from './src/routes/seo-routes';
import { Logger } from './src/util/logger';
import { RedirectMiddleware } from './src/middleware/redirect-middleware';

dotenv.config();

const app: Application = express();
const PORT = process.env.PORT || 4000;
const ENV = process.env.ENV || 'dev';
const DIST_SITE_FOLDER = join(process.cwd(), 'dist/bvlsite');

Logger.init(app);
RedirectMiddleware.init(app, ENV);
UniversalEngine.init(app, DIST_SITE_FOLDER);
SeoRoutes.init(app, ENV);
SiteRoutes.init(app, DIST_SITE_FOLDER);

// Start up the Node server
app.listen(PORT, () => {
  console.log(
    `Node Express server listening on http://localhost:${PORT} => mode: ${ENV}`
  );
});
