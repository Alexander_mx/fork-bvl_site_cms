import { Application } from 'express';
import { Request, Response } from 'express';
import { ExpressUtil } from '../util/express';
import { ROBOTS_SITEMAP, ENDPOINT_SEO } from '../settings/seo.constant';
import { ENVIRONMENT } from '../../../../core/shared/src/helpers/environment';

export class SeoRoutes {
  public static init(app: Application, env: string): void {
    Object.keys(ROBOTS_SITEMAP)
      .forEach(key => {
        app.get(`/${key}`, (req: Request, res: Response) => {
          ExpressUtil.getRequest(`${ENVIRONMENT[env].API_URL}/${ENDPOINT_SEO}/${key}`)
            .then(response => {
              res.writeHead(200, {
                'Content-Type': ROBOTS_SITEMAP[key].contentType,
                'Content-Length': response.length
              });
              res.end(response, 'binary');
            })
            .catch(error => {
              res.send(error);
            });
        });
      });
  }
}
