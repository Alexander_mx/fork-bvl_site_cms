import { Application } from 'express';
import * as express from 'express';
import { Request, Response } from 'express';
import { ExpressUtil } from '../util/express';

export class SiteRoutes {

  public static init(app: Application, distSiteFolder: string): void {
    // Server static files from 'distSiteFolder'
    app.get('*.*', express.static(distSiteFolder));

    // All regular routes use the Universal engine
    app.get('*', (req: Request, res: Response) => {

      if (ExpressUtil.isBrowser(req)) {
        return res.sendFile(`${distSiteFolder}/index.html`);
      }

      return res.render('index', { req });
    });
  }

}
