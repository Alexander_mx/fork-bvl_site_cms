import chalk from 'chalk';
import morgan from 'morgan';

import { ExpressUtil } from './express';

morgan.token('host', req => req.hostname);
morgan.token('device', req =>
  ExpressUtil.isBrowser(req) ? 'Browser' : 'No Browser'
);

export class Logger {
  static init(app: any): void {
    const morganMiddleware = morgan(
      (tokens, req, res) => {
        return [
          '\n',
          '🍄  Morgan --> ',
          tokens.method(req, res),
          tokens.status(req, res),
          tokens.url(req, res),
          chalk.green(tokens['response-time'](req, res) + ' ms'),
          chalk.green('@ ' + tokens.date(req, res, 'iso')),
          'from ',
          chalk.cyan(tokens.host(req, res)),
          chalk.yellow(tokens.device(req, res))
        ].join(' ');
      },
      {
        skip: req => ExpressUtil.isFile(req)
      }
    );

    app.use(morganMiddleware);
  }
}
