import { Request } from 'express';
import * as https from 'https';
import { ALLOWED_EXTENSION } from '../settings/request.constant';

export class ExpressUtil {
  public static isBrowser(req: Request): boolean {
    const userAgent = req.header('user-agent') || '';

    return userAgent.match(
      /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
    );
  }

  public static getRequest(url: string): Promise<Buffer> {
    return new Promise((resolve, reject) => {
      let data = [];
      const req = https.get(url, response => {
        response.on('data', chunk => {
          data.push(chunk);
        });
        response.on('end', () => {
          const bufferData = Buffer.concat(data);
          resolve(bufferData);
        });
      });

      req.on('error', error => {
        reject(error);
      });
    });
  }

  static isFile(req): boolean {
    return !!ALLOWED_EXTENSION.find(ext => req.url && req.url.match(ext));
  }
}
