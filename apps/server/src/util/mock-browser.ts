import * as mockBrowser from 'mock-browser';
const mock = new mockBrowser.mocks.MockBrowser();

// Work around until @angular/angular fixes Node undefined bug
// Fix for element no defined
const domino = require('domino');
Object.assign(global, domino.impl);
(global as any)['KeyboardEvent'] = domino.impl.Event;

global['document'] = mock.getDocument();
global['navigator'] = mock.getNavigator();
global['window'] = mock.getWindow();
global['sessionStorage'] = mock.getSessionStorage();
global['localStorage'] = mock.getLocalStorage();
global['location'] = mock.getLocation();
global['ErrorEvent'] = null;
