import { Application, Request, Response } from 'express';
import { CronJob } from 'cron';

import { ENVIRONMENT } from '../../../../core/shared/src/helpers/environment';
import { ExpressUtil } from '../util/express';

export class RedirectMiddleware {
  private static rules = [];

  public static init(app: Application, env: string) {
    this._initCron(env);
    app.use(this.redirect.bind(this));
  }

  public static redirect(req: Request, res: Response, next: any) {
    const rule = this.rules.find(rule => rule.oldUrl === req.originalUrl);
    if (!rule) {
      return next();
    }

    return res.redirect(301, rule.newUrl);
  }

  private static _initCron(env: string): void {
    this._getRules(env);
    new CronJob('0 0 * * * *', this._getRules.bind(this, env), null, true, 'America/Lima');
  }

  private static _getRules(env: string): void {
    ExpressUtil.getRequest(`${ENVIRONMENT[env].API_URL}/site/redirects`)
      .then(res => {
        try {
          this.rules = JSON.parse(res.toString());
        } catch { }
      });
  }
}
