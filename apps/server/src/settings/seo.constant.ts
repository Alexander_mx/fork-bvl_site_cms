export const ENDPOINT_SEO = 'configurations/seo/download'
export const ROBOTS_SITEMAP = {
  'robots.txt': {
    contentType: 'text/plain'
  },
  'sitemap.xml': {
    contentType: 'application/xml'
  },
  'post_es_sitemap.xml': {
    contentType: 'application/xml'
  },
  'post_en_sitemap.xml': {
    contentType: 'application/xml'
  },
  'main-sitemap.xsl': {
    contentType: 'text/xml'
  }
};
