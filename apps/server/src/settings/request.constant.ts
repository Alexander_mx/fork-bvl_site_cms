export const ALLOWED_EXTENSION = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.jpeg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',
  'eot'
];
