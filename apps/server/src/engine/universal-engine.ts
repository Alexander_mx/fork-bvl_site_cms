import 'zone.js/dist/zone-node';
import './../util/mock-browser';

import { enableProdMode } from '@angular/core';
// Express Engine
import { ngExpressEngine } from '@nguniversal/express-engine';
// Import module map for lazy loading
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
// * NOTE :: leave this as require() since this file is built Dynamically from webpack
import { AppServerModuleNgFactory, LAZY_MODULE_MAP } from './../../../../dist/server/main';

import { Application } from 'express';

export class UniversalEngine {

  public static init(app: Application, distSiteFolder: string): void {
    // Faster server renders w/ Prod mode (dev mode never needed)
    enableProdMode();
    // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
    app.engine('html', ngExpressEngine({
      bootstrap: AppServerModuleNgFactory,
      providers: [
        provideModuleMap(LAZY_MODULE_MAP)
      ]
    }));

    app.set('view engine', 'html');
    app.set('views', distSiteFolder);
  }

}
