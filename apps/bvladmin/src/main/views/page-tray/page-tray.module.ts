import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SearchFormModule } from '@bvl-admin/components/search-form/search-form.module';
import { ItemPageModule } from '@bvl-admin/components/trays-item/item-page/item-page.module';
import { BvlButtonModule, BvlCardModule, BvlLinkModule, BvlPaginatorModule } from '@bvl-core/ui/lib/components';
import { WithoutResultsModule } from '@bvl-core/ui/lib/directives';
import { PageTrayRoutingModule } from './page-tray-routing.module';
import { PageTrayComponent } from './page-tray.component';

@NgModule({
  declarations: [PageTrayComponent],
  imports: [
    CommonModule,
    PageTrayRoutingModule,
    BvlCardModule,
    ItemPageModule,
    SearchFormModule,
    WithoutResultsModule,
    BvlPaginatorModule,
    BvlLinkModule,
    BvlButtonModule
  ]
})
export class PageTrayModule { }
