import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { SearchFormComponent } from '@bvl-admin/components/search-form/search-form.component';
import { PageService } from '@bvl-admin/services';
import { PageLang } from '@bvl-admin/settings/lang';
import { ISearchFormView } from '@bvl-admin/statemanagement/models/general.interface';
import { IPagePaginationRequest, IPagePaginationResponse } from '@bvl-admin/statemanagement/models/page.interface';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { BvlPaginator } from '@bvl-core/ui/lib/components';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-page-tray',
  templateUrl: './page-tray.component.html',
  styleUrls: ['./page-tray.component.scss']
})
export class PageTrayComponent extends UnsubscribeOnDestroy implements OnInit, AfterViewInit {

  @ViewChild(SearchFormComponent) searchFormComponent: SearchFormComponent;

  PAGE_LANG = PageLang;

  searchForm: ISearchFormView;
  pagesList: IPagePaginationResponse;
  withoutResults: boolean;
  pagination: BvlPaginator;
  paginationState = false;
  pageState: number;

  constructor(
    private _pageService: PageService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {
    super();
    this.pagination = new BvlPaginator();
  }

  ngOnInit(): void {
    this.pageState = parseInt(this._activatedRoute.snapshot.queryParams.state, 10) || 0;

    this._router.events
    .pipe(
      filter(event => event instanceof NavigationEnd),
      takeUntil(this.unsubscribeDestroy$)
    )
    .subscribe(event => {
      if (!this.paginationState) {
        this._pagesList();
      } else {
        this.paginationState = false;
      }
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.pageState) { this.searchFormComponent.mStates.setValue([{ codeParameterSystem: this.pageState }]); }
      this._pagesList();
    }, 0);
  }

  private _getDefaultFilter(): ISearchFormView {
    const params = this._activatedRoute.snapshot.queryParams;

    this.pagination.pageSize = Number(params.size || this.pagination.pageSize);
    this.pagination.page = Number(params.page || this.pagination.page);

    const valueUser = [].concat(params.userList || []);
    const valueStates = [].concat(params.status || []);

    return {
      search: params.namePage || '',
      users: valueUser,
      statePages: valueStates,
      startDate: params.startDate,
      endDate: params.endDate
    } as unknown as ISearchFormView;
  }

  private _paramPagesList(searchForm?: ISearchFormView): IPagePaginationRequest {
    this.searchForm = searchForm || this._getDefaultFilter();

    return {
      namePage: this.searchForm.search || '',
      status: (this.pageState && !this.searchForm.statePages)
        ? [this.pageState]
        : this.searchForm.statePages || [],
      userList: this.searchForm.users || [],
      startDate: this.searchForm.startDate || '',
      endDate: this.searchForm.endDate || '',
      size: this.pagination.pageSize,
      page: this.pagination.page
    } as IPagePaginationRequest;
  }

  private _pagesList(searchForm?: ISearchFormView): void {
    const params = this._paramPagesList(searchForm);
    this._updateUrlParams(params);
    this._pageService.pagesList(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IPagePaginationResponse) => {
        this.withoutResults = !response.content.length;
        this.pagesList = response;
      }, () => {
        this.withoutResults = true;
      });
  }

  private _updateUrlParams(params: any): void {
    const queryParams = {};
    Object.keys(params)
      .forEach(key => {
        const value = params[key];
        const isArray = Array.isArray(value);
        if ((isArray && value.length) || (!isArray && value)) {
          queryParams[key] = value;
        }
      });
    this._router.navigate(
      [],
      {
        relativeTo: this._activatedRoute,
        queryParams,
        queryParamsHandling: '',
        state: params
      });
  }

  search(searchForm: ISearchFormView): void {
    this.pagination.page = 1;
    this._pagesList(searchForm);
  }

  pageChange(event?: any): void {
    this.paginationState = true;
    this._pagesList(this.searchForm);
  }

  goCreatePublication(): void {
    this._router.navigate(['editor/create/publication']);
  }

  deletePage(): void {
    this.pageChange();
  }
}
