import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageTrayComponent } from './page-tray.component';

const routes: Routes = [
  {
    path: '',
    component: PageTrayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageTrayRoutingModule { }
