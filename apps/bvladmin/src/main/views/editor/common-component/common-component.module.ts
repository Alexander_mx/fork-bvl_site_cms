import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormModule } from '@bvl-admin/components/dynamic-form';
import { EditorButtonsHeaderModule } from '@bvl-admin/components/editor-buttons-header/editor-buttons-header.module';
import { PipesModule } from '@bvl-core/shared/helpers/pipes';
import { BvlButtonModule, BvlInputModule, BvlSwitchValueModule } from '@bvl-core/ui/lib/components';
import { CommonComponentRoutingModule } from './common-component-routing.module';
import { CommonComponentComponent } from './common-component.component';

@NgModule({
  declarations: [CommonComponentComponent],
  imports: [
    CommonModule,
    CommonComponentRoutingModule,
    BvlSwitchValueModule,
    BvlButtonModule,
    DynamicFormModule,
    EditorButtonsHeaderModule,
    PipesModule,
    BvlInputModule,
    ReactiveFormsModule
  ]
})
export class CommonComponentModule { }
