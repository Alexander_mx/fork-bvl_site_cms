import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DynamicFormComponent } from '@bvl-admin/components/dynamic-form';
import { EditorButtonsHeaderComponent } from '@bvl-admin/components/editor-buttons-header/editor-buttons-header.component';
import { ComponentService } from '@bvl-admin/services';
import { COMPONENT_TYPES } from '@bvl-admin/settings/constants/component.constant';
import { EDITOR_TYPES } from '@bvl-admin/settings/constants/editor.constant';
import { ACTION_TYPES, TYPES_CODE, WC_BUNDLE_PATH } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import {
  IComponentResponse,
  IComponentValueResponse,
  ILanguage,
  IProperty,
  ISaveComponentValueRequest
} from '@bvl-admin/statemanagement/models/component.interface';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { AngularUtil, UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { addScriptTag } from '@bvl-core/shared/helpers/util/web-components';
import { LANGUAGE_TYPES_CODE } from '@bvl-core/shared/settings/constants/general.constants';
import { flatMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-common-component',
  templateUrl: './common-component.component.html',
  styleUrls: ['./common-component.component.scss']
})
export class CommonComponentComponent extends UnsubscribeOnDestroy implements OnInit, AfterViewInit {

  @ViewChild(EditorButtonsHeaderComponent) editorButtonsHeader: EditorButtonsHeaderComponent;
  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;

  GENERAL_LANG = GeneralLang;

  EDITOR_TYPES = EDITOR_TYPES;
  LANGUAGE_TYPES_CODE = LANGUAGE_TYPES_CODE;
  TYPES_CODE = TYPES_CODE;

  language: string;
  type: number;
  componentCode: string;
  componentValue: IComponentValueResponse;
  previousComponentValue: IComponentValueResponse;
  component: IComponentResponse;
  components: Array<IComponentResponse>;
  template: string;
  frmEdit: FormGroup;
  mComponentName: AbstractControl;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _componentService: ComponentService,
    private _notificationService: NotificationService,
    private _location: Location,
    private _formBuilder: FormBuilder
  ) {
    super();
    this.components = [];
  }

  ngOnInit(): void {
    this.type = this._activatedRoute.snapshot.data.type;
    this.componentCode = this._activatedRoute.snapshot.paramMap.get('componentCode');
    this._createForm();
  }

  ngAfterViewInit(): void {
    this.language = this.editorButtonsHeader.mLanguage.value;
    this._componentForm(this.type, this.componentCode, this.language);
  }

  private _createForm(): void {
    this.frmEdit = this._formBuilder.group({
      mComponentName: [null, (this.type === this.TYPES_CODE.popup) ? Validators.required : []]
    });
    this.mComponentName = this.frmEdit.get('mComponentName');
  }

  validateControl(control: AbstractControl): boolean {
    return ValidatorUtil.validateAbstractControl(control);
  }

  private _componentForm(type: number, componentCode: string, language: string): void {
    let component: IComponentResponse;
    this._componentService.componentValue(type, componentCode, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        flatMap((response: IComponentValueResponse) => {
          this.componentValue = response;
          this.mComponentName.setValue(this.componentValue.name);
          this.previousComponentValue = AngularUtil.clone(this.componentValue);

          return this._componentService.componentByCode(this.componentValue.code);
        })
      )
      .subscribe((response: IComponentResponse) => {
        this.component = response;

        const languageItem = this._getLanguageItem(this.componentValue, language);
        component = (languageItem)
                      ? this._setComponentByLanguageItem(languageItem)
                      : this.component;

        this.components.push(component);
      });
  }

  private _getLanguageItem(componenteValue: IComponentValueResponse, language: string): ILanguage {
    return componenteValue.languages && componenteValue.languages.find(fv => {
      return fv.language === language;
    });
  }

  private _setComponentByLanguageItem(languageItem: ILanguage): IComponentResponse {
    const component = AngularUtil.clone(this.component);

    languageItem.properties.forEach(ffeValue => {
      let value = ffeValue.value;

      const indexProperty = component.properties
                              .map(fmValue => fmValue.code)
                              .indexOf(ffeValue.code);
      const componentProperty = component.properties[indexProperty];

      if (componentProperty.type === COMPONENT_TYPES.array) {
        value = value.map(fmValue => {
          return fmValue.properties.reduce((previous, current) => {
            previous[current.code] = current.value;

            return previous;
          }, {});
        });
      }

      componentProperty.value = value || null;
    });

    return component;
  }

  private _indexLanguageItem(componenteValue: IComponentValueResponse, language: string): number {
    return componenteValue.languages && componenteValue.languages.map(fv => {
      return fv.language;
    })
    .indexOf(language);
  }

  private _generateLanguageItem(formValue: {}, language: string): ILanguage {
    const properties = Object.keys(formValue)
      .map(fmValue => {
        let value = formValue[fmValue];

        const componentProperty = this.component.properties.find(ffValue => fmValue === ffValue.code);

        if (componentProperty.type === COMPONENT_TYPES.array) {
          value = value.map(smValue => {
            return {
              properties: Object.keys(smValue)
                            .map(tmValue => {
                              return {
                                code: tmValue,
                                value: smValue[tmValue]
                              };
                            })
            };
          });
        }

        return {
          code: fmValue,
          value
        };
      }) as Array<IProperty>;

    return {
      language,
      properties
    } as ILanguage;
  }

  changeLanguage(language: string): void {
    this.template = null;

    // Previous
    const previousLanguage = this.language,
          previousIndex = this._indexLanguageItem(this.previousComponentValue, previousLanguage),
          previousFormValue = this.dynamicForm.getFormValue()[this.component.code],
          previousLanguageItem = this._generateLanguageItem(previousFormValue, previousLanguage);

    if (previousIndex > -1) {
      this.previousComponentValue.languages.splice(previousIndex, 1, previousLanguageItem);
    } else {
      this.previousComponentValue.languages.push(previousLanguageItem);
    }

    // Current
    this.language = language;
    let component = this.component;
    const languageItem = this._getLanguageItem(this.previousComponentValue, this.language);
    if (languageItem) {
      component = this._setComponentByLanguageItem(languageItem);
    }

    // CreateForm
    this.components.splice(0, 1, component);
    this.dynamicForm.createForm(this.components);
  }

  buttonAction(action: string): void {
    switch (action) {
      case ACTION_TYPES.back:
        this._location.back();
        break;
      case ACTION_TYPES.save:
        this._save();
        break;
      default:
        break;
    }
  }

  private _save(): void {
    this.dynamicForm.saveForm();
  }

  private _params(formValue: {}, language: string): ISaveComponentValueRequest {
    const componentValue = AngularUtil.clone(this.componentValue),
          previousIndex = this._indexLanguageItem(this.previousComponentValue, language),
          index = this._indexLanguageItem(componentValue, language),
          languageItem = this._generateLanguageItem(formValue, language);

    if (index > -1) {
      this.previousComponentValue.languages.splice(previousIndex, 1, languageItem);
      componentValue.languages.splice(index, 1, languageItem);
    } else {
      this.previousComponentValue.languages.push(languageItem);
      componentValue.languages.push(languageItem);
    }
    componentValue.lastUpdate = new Date();
    componentValue.name = this.mComponentName.value;

    return componentValue as ISaveComponentValueRequest;
  }

  private _createTemplate(component: any): void {
    addScriptTag([component.ref], WC_BUNDLE_PATH);
    this.template = component.template;
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmEdit);

    return this.frmEdit.valid;
  }

  saveForm(data: any): void {
    this.template = null;
    const validateForm = this._validateForm();
    if (data && validateForm) {
      const params = this._params(data.formValue[this.component.code], this.language);
      this._componentService.saveComponentValue(this.type, params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.componentValue = params;
          this._createTemplate(data.component[0]);
        });
    } else {
      this._notificationService.addWarning(this.GENERAL_LANG.notifications.existIncorrectFields);
    }
  }

}
