import { DOCUMENT, Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { isEqual } from 'lodash-es';
import { forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

import { PageHistoryComponent } from '@bvl-admin/components/modals/page-history/page-history.component';
import { CommentService, ConfigurationService, PageService } from '@bvl-admin/services';
import { SEOService } from '@bvl-admin/services/seo.service';
import { EDITOR_TYPES } from '@bvl-admin/settings/constants/editor.constant';
import { ACTION_TYPES, WC_BUNDLE_PATH } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { EditorLang } from '@bvl-admin/settings/lang/editor.lang';
import { ICommentResponse } from '@bvl-admin/statemanagement/models/comment.interface';
import { ICategory, IContent, IPageByLanguage, IPageResponse } from '@bvl-admin/statemanagement/models/page.interface';
import { IModalConfig, ModalRef, ModalService } from '@bvl-core/shared/helpers/modal';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { SpinnerService } from '@bvl-core/shared/helpers/spinner';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { StringUtil, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { addScriptTag } from '@bvl-core/shared/helpers/util/web-components';
import { LANGUAGE_TYPES_CODE } from '@bvl-core/shared/settings/constants/general.constants';
import { BasePublicationComponent } from './base-publication';

@Component({
  selector: 'admin-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.scss']
})
export class PublicationComponent extends BasePublicationComponent implements OnInit, OnDestroy {

  @ViewChild('swalCancel') private _swalCancel: SwalComponent;

  GENERAL_LANG = GeneralLang;
  EDITOR_LANG = EditorLang;

  EDITOR_TYPES = EDITOR_TYPES;

  idPage: string;
  pageState: number;
  pageItemIndexEditor: number;
  language: string;
  template: string;
  refComponents: Array<string>;
  comments: Array<ICommentResponse>;
  /** Necessary for only seePublicaction */
  seePublication: boolean;

  swalCancelOptions: SweetAlertOptions;

  constructor(
    @Inject(DOCUMENT) private _document: Document,
    private _activatedRoute: ActivatedRoute,
    private _commentService: CommentService,
    private _configurationService: ConfigurationService,
    private _formBuilder: FormBuilder,
    private _location: Location,
    private _modalService: ModalService,
    private _notificationService: NotificationService,
    private _router: Router,
    private _seoService: SEOService,
    private _spinnerService: SpinnerService,
    private _swalConfirmService: SwalConfirmService,
    protected _pageService: PageService
  ) {
    super(_pageService);
    this.categoriesList = { es: [], en: [] };
  }

  ngOnInit(): void {
    const snapshot = this._activatedRoute.snapshot;
    this.language = LANGUAGE_TYPES_CODE.es;
    this.idPage = snapshot.params.idPage;
    this.pageState = parseInt(snapshot.queryParams.state, 10);
    this.seePublication = snapshot.data && snapshot.data.seePublication;
    this.swalCancelOptions = this._swalConfirmService.warning(this.GENERAL_LANG.modalConfirm.titleCancelChange);
    this._createForm();
    this._pageById(this.idPage, this.pageState, this.language);
    this._commentsByIdPage(this.idPage);
  }

  private _createForm(): void {
    this.frmEditor = this._formBuilder.group({
      mCategory: [{ value: undefined, disabled: this.seePublication }],
      mPagePath: [{ value: '', disabled: this.seePublication }, Validators.required],
      mParentPath: [{ value: undefined, disabled: this.seePublication }],
      mParentPathType: [{ value: '', disabled: this.seePublication }],
      mTitlePage: [{ value: '', disabled: this.seePublication }, Validators.required]
    });
    this.mCategory = this.frmEditor.get('mCategory');
    this.mPagePath = this.frmEditor.get('mPagePath');
    this.mParentPath = this.frmEditor.get('mParentPath');
    this.mParentPathType = this.frmEditor.get('mParentPathType');
    this.mTitlePage = this.frmEditor.get('mTitlePage');

    this.handlerParentPathType();
    this.handlerAutocomplete();
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmEditor, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmEditor);

    return this.frmEditor.valid;
  }

  changeTitlePage(titlePage: string): void {
    let pagePath = StringUtil.withoutDiacritics(titlePage);
    pagePath = StringUtil.slugify(pagePath);
    this.mPagePath.setValue(pagePath);
    const pageItem = this._setPageEditor();
    this._seoService.setPageTitleSlug(pageItem.title, pageItem.pagePath);
  }

  changePagePath(): void {
    const pageItem = this._setPageEditor();
    this._seoService.setSlug(pageItem.pagePath);
  }

  private _setCategoriesList(response: Array<ICategory>, language: string): void {
    response.forEach(fv => {
      fv.languages.forEach(sv => {
        this.categoriesList[sv.language].push(sv);
      });
    });
    this.categoriesListByLanguage = this.categoriesList[language];
  }

  protected _setSeoInfo(pageItem: IPageByLanguage): void {
    setTimeout(() => {
      const contentHtml = this._document.getElementById('contentHtml');
      this._seoService.setInfo(this.canonicalPartial, pageItem, contentHtml, this.defaultConfiguration);
    }, 0);
  }

  private _setViewByLanguage(newLanguage: string): void {
    // Actualizamos el lenguaje
    this.language = newLanguage;
    // Obtenemos la página editada actualemente
    const pageEditor = this._pageService.getPageEditor();
    // Obtenemos el indice del nuevo lenguaje
    this.pageItemIndexEditor = this._pageItemIndex(pageEditor, newLanguage);
    // Obtenemos la nueva página por lenguaje
    const newPageItem = pageEditor.languages[this.pageItemIndexEditor];
    // Set formulario de la página
    this.mPagePath.setValue(newPageItem.pagePath);
    if (newPageItem.parentPath) {
      this.mParentPath.setValue({ path: newPageItem.parentPath }, { emitEvent: false });
      this.dataListAutocomplete = [{ path: newPageItem.parentPath }];
    } else {
      this.mParentPath.reset();
    }
    this.mTitlePage.setValue(newPageItem.title);
    this.mCategory.setValue({ category: newPageItem.category });
    this.mParentPathType.setValue(newPageItem.parentPathType || this.PARENT_PATH_TYPE.CATEGORY);

    // Set template/refComponentes de la página
    this.template = newPageItem.content.html;
    this.refComponents = newPageItem.content.refComponents;
    // Cargamos los bundles de los webComponents a utilizar
    addScriptTag(newPageItem.content.refComponents, WC_BUNDLE_PATH);
    // Set información del SEO con la nueva página
    this._setSeoInfo(newPageItem);
  }

  private _setPage(page: IPageResponse): void {
    const storagePage = this._pageService.getPage();
    if (!storagePage) {
      this._pageService.setPage(page);
    }
  }

  private _pageById(idPage: string, pageState: number, language: string): void {
    forkJoin([
      this._pageService.pageById(idPage, pageState, true),
      this._configurationService.getConfiguration(),
      this._pageService.categoriesList(true)
    ])
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(response => {
        this._setCategoriesList(response[2], language);
        // Actualizamos el original page en storage (page)
        this._setPage(response[0]);
        // Actualizamos el original page en storage (pageEditor)
        this._pageService.setPageEditor(response[0]);
        // Almacenmos la configuración por defecto
        this.defaultConfiguration = response[1];
        // Actualizamos la vista (página por lenguaje)
        setTimeout(() => {
          this._setViewByLanguage(language);
          this._getSEOConfiguration();
        }, 0);
      });
  }

  private _getSEOConfiguration(): void {
    // Actualizamos la configuración del SEO de la página actual en el storage
    this._seoService.configuration$
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        // Actualizamos el pageEditor en storage
        this._setPageEditor();
      });
  }

  private _commentsByIdPage(idPage: string): void {
    this._commentService.commentsByIdPage(idPage, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<ICommentResponse>) => {
        this.comments = response;
        this._commentService.setCommentsEditor(response);
      });
  }

  private _pageItemIndex(page: IPageResponse, language: string): number {
    return page.languages
      .map(fv => fv.language)
      .indexOf(language);
  }

  protected _setPageEditor(content?: IContent): IPageByLanguage {
    // Obtenemos la paginaEditor del storage
    const pageEditor = this._pageService.getPageEditor();
    // Obtenemos la página por lenguaje actual
    const pageItem = pageEditor.languages[this.pageItemIndexEditor];

    if (content) {
      // Actualizamos el HTML
      pageItem.content.html = content.html;
      // Actualizamos la referencia(bundles) de los webComponents
      pageItem.content.refComponents = content.refComponents;
    } else {
      /*
        Estructura URL
        categoryValue = { category: 'mercado', path: 'mercado' }
        1- category => mercado
        2- pagePath => bolsa-valores
        3- path => mercado/bolsa-valores
        4- canonical => www.bvl.com/mercado/bolsa-valores
      */
      const categoryValue = this.mCategory.value || {};
      pageItem.canonical = `${this.canonicalPartial}${pageItem.pagePath}`;
      pageItem.category = categoryValue.category || '';
      pageItem.pagePath = this.mPagePath.value || '';
      pageItem.parentPath = typeof this.mParentPath.value !== 'string' ? this.mParentPath.value && this.mParentPath.value.path : '';
      pageItem.parentPathType = this.mParentPathType.value;
      pageItem.path = this.getPath(pageItem, categoryValue.path);

      // Actualizamos el titulo de la página
      pageItem.title = this.mTitlePage.value;
      // Actualizamos los metadata con el seoService
      pageItem.metadata = this._seoService.getMetaData();
    }

    // Actualizamos la página por lenguaje actual
    pageEditor.languages.splice(this.pageItemIndexEditor, 1, pageItem);
    // Actualizamos el pageEditor en storage
    this._pageService.setPageEditor(pageEditor);

    // Retornamos la página por lenguaje actual
    return pageItem;
  }

  changeLanguage(newLanguage: string): void {
    this._spinnerService.showSpinner();
    this.template = null;
    this.categoriesListByLanguage = this.categoriesList[newLanguage];
    setTimeout(() => {
      this._setPageEditor();
      this._setViewByLanguage(newLanguage);
      this._spinnerService.hideSpinner();
    }, 0);
  }

  updateTemplate(content: IContent): void {
    this._setPageEditor(content);
  }

  private _save(): void {
    // Obtenemos el pageEditor del storage
    const pageEditor = this._pageService.getPageEditor();
    this._pageService.savePage(pageEditor, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IPageResponse) => {
        // Actualizamos el page en storage
        this._pageService.setPage(response);
        // Actualizamos el pageEditor en storage
        this._pageService.setPageEditor(response);
        this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      });
  }

  private _publish(publicationType: string): void {
    this._pageService.publicationTypePage(publicationType, this.idPage, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._location.back();
        this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      });
  }

  cancelChanges(): void {
    this._pageService.cancelPageChanges(this.idPage, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._location.back();
        this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      });
  }

  private _observe(): void {
    this._pageService.observePage(this.idPage, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._location.back();
        this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      });
  }

  private _revert(): void {
    this._pageService.revertPage(this.idPage, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._location.back();
        this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      });
  }

  private _validateChangesWithoutSaving(): boolean {
    // Obtenemos el pageEditor del storage
    const pageEditor = this._pageService.getPageEditor();
    // Obtenemos el page del storage
    const page = this._pageService.getPage();

    return isEqual(pageEditor, page);
  }

  private _historic(): void {
    const modalConfig: IModalConfig = {
      titleText: 'Historial de publicaciones',
      showConfirmButton: false
    };
    const modal: ModalRef = this._modalService.open(PageHistoryComponent, modalConfig);
    modal.setPayload(this.idPage);
    modal.result.then(() => {
      /** reloadPage */
      setTimeout(() => {
        this._router.navigateByUrl('/', { skipLocationChange: true })
          .then(() => {
            const queryParams = { state: this.pageState };
            this._router.navigate(['editor/edit/publication', this.idPage], { queryParams });
          });
      }, 0);
    }, () => {
      return false;
    });
  }

  buttonAction(action: string): void {
    switch (action) {
      case ACTION_TYPES.back:
        this._location.back();
        break;
      case ACTION_TYPES.save:
        if (this._validateForm()) {
          this._save();
        }
        break;
      case ACTION_TYPES.publish:
      case ACTION_TYPES.requestPublication:
        if (this._validateForm()) {
          if (this._validateChangesWithoutSaving()) {
            this._publish(action);
          } else {
            this._notificationService.addWarning(this.GENERAL_LANG.notifications.thereAreChangesWithoutSaving);
          }
        }
        break;
      case ACTION_TYPES.cancel:
        this._swalCancel.show();
        break;
      case ACTION_TYPES.observe:
        this._observe();
        break;
      case ACTION_TYPES.revert:
        this._revert();
        break;
      case ACTION_TYPES.historic:
        this._historic();
        break;
      default:
        break;
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._pageService.setPage(null);
    this._pageService.setPageEditor(null);
  }

}
