import { AbstractControl, FormGroup } from '@angular/forms';
import { take, takeUntil } from 'rxjs/operators';

import { PageService } from '@bvl-admin/services';
import { PARENT_PATH_TYPE } from '@bvl-admin/settings/constants/page.constant';
import { IConfiguration } from '@bvl-admin/statemanagement/models/configuration.interface';
import {
  ICategoryByLanguage,
  IContent,
  IPageAutocompleteResponse,
  IPageByLanguage
} from '@bvl-admin/statemanagement/models/page.interface';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';

export abstract class BasePublicationComponent extends UnsubscribeOnDestroy {
  categoriesList: { es: Array<ICategoryByLanguage>, en: Array<ICategoryByLanguage> };
  categoriesListByLanguage: Array<ICategoryByLanguage>;

  frmEditor: FormGroup;
  mCategory: AbstractControl;
  mPagePath: AbstractControl;
  mParentPath: AbstractControl;
  mParentPathType: AbstractControl;
  mTitlePage: AbstractControl;

  dataListAutocomplete: Array<any> = [];

  canonicalPartial: string;
  defaultConfiguration: IConfiguration;
  language: string;
  PARENT_PATH_TYPE = PARENT_PATH_TYPE;

  get languagePath(): string {
    return this.defaultConfiguration.defaultLanguage === this.language
      ? ''
      : `${this.language}/`;
  }

  constructor(
    protected _pageService: PageService
  ) {
    super();
  }

  initParentPath(): void {
    switch (this.mParentPathType.value) {
      case PARENT_PATH_TYPE.PAGE:
        const path = this.mParentPath.value && this.mParentPath.value.path;
        this.changeParentPath({ path });
        break;
      case PARENT_PATH_TYPE.CATEGORY:
        const category = this.mCategory.value && this.mCategory.value.category;
        this.changeParentPath({ category });
        break;
      default:
        break;
    }
  }

  changeParentPath(parent?: any): void {
    let parentPath: string;
    switch (this.mParentPathType.value) {
      case PARENT_PATH_TYPE.PAGE:
        parentPath = parent && parent.path && `${parent.path}/` || '';
        this.canonicalPartial = `${this.defaultConfiguration.domain}/${parentPath}`;

        break;
      case PARENT_PATH_TYPE.CATEGORY:
        const category = this.categoriesListByLanguage.find(cat => cat.category === (parent && parent.category));
        parentPath = category && category.path && `${category.path}/` || '';
        this.canonicalPartial = `${this.defaultConfiguration.domain}/${this.languagePath}${parentPath}`;

        break;
      default:
        break;
    }

    const pageItem = this._setPageEditor();
    this._setSeoInfo(pageItem);
  }

  protected getPath(page: IPageByLanguage, categoryPath: string): string {
    switch (this.mParentPathType.value) {
      case PARENT_PATH_TYPE.PAGE:
        return `${page.parentPath ? `${page.parentPath}/` : ''}${page.pagePath}`;
      case PARENT_PATH_TYPE.CATEGORY:
        return `${this.languagePath}${categoryPath ? `${categoryPath}/` : ''}${page.pagePath}`;
      default:
        return page.pagePath;
    }
  }

  protected handlerParentPathType(): void {
    this.mParentPathType.valueChanges
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(this.initParentPath.bind(this));
  }

  protected handlerAutocomplete(): void {
    this.mParentPath.valueChanges
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(this._getDataAutocomplete.bind(this));
  }

  private _getDataAutocomplete(data: any): void {
    if (typeof data !== 'string') {
      this.changeParentPath(data);

      return void 0;
    }

    this._pageService
      .getPagesAutocopleteByPathAndTitle(data)
      .pipe(take(1))
      .subscribe((response: IPageAutocompleteResponse) => {
        this.dataListAutocomplete = response;
      });
  }

  protected abstract _setPageEditor(content?: IContent): IPageByLanguage;
  protected abstract _setSeoInfo(pageItem: IPageByLanguage): void;
}
