import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { ComponentSidebarModule } from '@bvl-admin/components/component-sidebar/component-sidebar.module';
import { EditorButtonsHeaderModule } from '@bvl-admin/components/editor-buttons-header/editor-buttons-header.module';
import { PageHistoryModule } from '@bvl-admin/components/modals/page-history/page-history.module';
import { NewPublicationOptionsModule } from '@bvl-admin/components/new-publication-options/new-publication-options.module';
import { SeoModule } from '@bvl-admin/components/seo/seo.module';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { PipesModule } from '@bvl-core/shared/helpers/pipes';
import { BvlAutocompleteModule, BvlInputModule, BvlLinkModule, BvlRadioButtonModule } from '@bvl-core/ui/lib/components';
import { BvlSelectModule } from '@bvl-core/ui/lib/components/select';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { PublicationRoutingModule } from './publication-routing.module';
import { PublicationComponent } from './publication.component';

@NgModule({
  declarations: [PublicationComponent],
  imports: [
    BvlAutocompleteModule,
    BvlInputModule,
    BvlLinkModule,
    BvlRadioButtonModule,
    BvlSelectModule,
    CommonModule,
    ComponentSidebarModule,
    EditorButtonsHeaderModule,
    ModalModule,
    NewPublicationOptionsModule,
    PageHistoryModule,
    PipesModule,
    PublicationRoutingModule,
    ReactiveFormsModule,
    SeoModule,
    SweetAlert2Module
  ]
})
export class PublicationModule { }
