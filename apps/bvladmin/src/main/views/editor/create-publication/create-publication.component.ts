import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageService } from '@bvl-admin/services';
import { GeneralLang, PageLang } from '@bvl-admin/settings/lang';
import { ITemplateResponse } from '@bvl-admin/statemanagement/models/template.interface';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-create-publication',
  templateUrl: './create-publication.component.html',
  styleUrls: ['./create-publication.component.scss']
})
export class CreatePublicationComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;
  PAGE_LANG = PageLang;

  templateList: Array<ITemplateResponse>;

  constructor(
    private _router: Router,
    private _pageService: PageService
  ) {
    super();
  }

  ngOnInit(): void {
    this._templateList();
  }

  private _templateList(): void {
    this._pageService.templateList(true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<ITemplateResponse>) => {
        this.templateList = response;
      });
  }

  goBack(): void {
    this._router.navigate(['page-tray']);
  }

}
