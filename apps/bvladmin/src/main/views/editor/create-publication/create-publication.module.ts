import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ItemTemplateModule } from '@bvl-admin/components/item-template/item-template.module';
import { BvlButtonModule } from '@bvl-core/ui/lib/components';
import { CreatePublicationRoutingModule } from './create-publication-routing.module';
import { CreatePublicationComponent } from './create-publication.component';

@NgModule({
  declarations: [CreatePublicationComponent],
  imports: [
    CommonModule,
    CreatePublicationRoutingModule,
    BvlButtonModule,
    ItemTemplateModule
  ]
})
export class CreatePublicationModule { }
