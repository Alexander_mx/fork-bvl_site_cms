import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApprovalTrayComponent } from './approval-tray.component';

const routes: Routes = [
  {
    path: '',
    component: ApprovalTrayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalTrayRoutingModule { }
