import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { SearchFormModule } from '@bvl-admin/components/search-form/search-form.module';
import { ItemPageModule } from '@bvl-admin/components/trays-item/item-page/item-page.module';
import { BvlCardModule, BvlPaginatorModule } from '@bvl-core/ui/lib/components';
import { WithoutResultsModule } from '@bvl-core/ui/lib/directives';
import { ApprovalTrayRoutingModule } from './approval-tray-routing.module';
import { ApprovalTrayComponent } from './approval-tray.component';

@NgModule({
  declarations: [ApprovalTrayComponent],
  imports: [
    CommonModule,
    ApprovalTrayRoutingModule,
    BvlCardModule,
    ItemPageModule,
    ReactiveFormsModule,
    SearchFormModule,
    WithoutResultsModule,
    BvlPaginatorModule
  ]
})
export class ApprovalTrayModule { }
