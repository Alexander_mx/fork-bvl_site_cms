import { Component, OnInit } from '@angular/core';
import { PageService } from '@bvl-admin/services';
import { ISearchFormView } from '@bvl-admin/statemanagement/models/general.interface';
import { IPageApprovalPaginationRequest, IPagePaginationResponse } from '@bvl-admin/statemanagement/models/page.interface';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { BvlPaginator } from '@bvl-core/ui/lib/components';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-approval-tray',
  templateUrl: './approval-tray.component.html',
  styleUrls: ['./approval-tray.component.scss']
})
export class ApprovalTrayComponent extends UnsubscribeOnDestroy implements OnInit {

  searchForm: ISearchFormView;
  approvalsList: IPagePaginationResponse;
  withoutResults: boolean;
  pagination: BvlPaginator;

  constructor(
    private _pageService: PageService
  ) {
    super();
    this.pagination = new BvlPaginator();
  }

  ngOnInit(): void {
    this._approvalsList();
  }

  private _paramsApprovalsList(searchForm?: ISearchFormView): IPageApprovalPaginationRequest {
    this.searchForm = searchForm || {} as ISearchFormView;

    return {
      namePage: this.searchForm.search || '',
      userList:  this.searchForm.users || [],
      size: this.pagination.pageSize,
      page: this.pagination.page
    } as IPageApprovalPaginationRequest;
  }

  private _approvalsList(searchForm?: ISearchFormView): void {
    const params = this._paramsApprovalsList(searchForm);
    this._pageService.approvalsList(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IPagePaginationResponse) => {
        this.withoutResults = !response.content.length;
        this.approvalsList = response;
      }, () => {
        this.withoutResults = true;
      });
  }

  search(searchForm: ISearchFormView): void {
    this.pagination.page = 1;
    this._approvalsList(searchForm);
  }

  pageChange(event: any): void {
    this._approvalsList(this.searchForm);
  }

}
