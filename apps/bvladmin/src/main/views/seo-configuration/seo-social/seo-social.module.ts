import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BvlButtonModule, BvlCardModule, BvlInputModule } from '@bvl-core/ui/lib/components';
import { SeoSocialRoutingModule } from './seo-social-routing.module';
import { SeoSocialComponent } from './seo-social.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SeoSocialRoutingModule,
    BvlCardModule,
    BvlButtonModule,
    BvlInputModule
  ],
  declarations: [SeoSocialComponent]
})
export class SeoSocialModule { }
