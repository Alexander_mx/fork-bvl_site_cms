import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

import { ConfigurationService } from '@bvl-admin/services';
import { REGEX } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IConfiguration, ISocialNetworking } from '@bvl-admin/statemanagement/models/configuration.interface';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';

@Component({
  selector: 'admin-seo-social',
  templateUrl: './seo-social.component.html'
})
export class SeoSocialComponent extends UnsubscribeOnDestroy implements OnInit {

  btnSaveLabel = GeneralLang.buttons.save;
  notifications = GeneralLang.notifications;

  socialNetworking: ISocialNetworking;

  frmNetworking: FormGroup;
  urlFacebook: AbstractControl;
  urlTwitter: AbstractControl;
  urlInstagram: AbstractControl;
  urlLinkedin: AbstractControl;
  urlPinterest: AbstractControl;
  urlYoutube: AbstractControl;
  urlFlickr: AbstractControl;

  constructor(
    private fb: FormBuilder,
    protected configurationService: ConfigurationService,
    protected notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.getConfiguration();
  }

  private createForm(): void {
    this.frmNetworking = this.fb.group({
      urlFacebook: [''],
      urlTwitter: ['', Validators.pattern(new RegExp(REGEX.twitter))],
      urlInstagram: [''],
      urlLinkedin: [''],
      urlPinterest: [''],
      urlYoutube: [''],
      urlFlickr: ['']
    });
    this.urlFacebook = this.frmNetworking.get('urlFacebook');
    this.urlTwitter = this.frmNetworking.get('urlTwitter');
    this.urlInstagram = this.frmNetworking.get('urlInstagram');
    this.urlLinkedin = this.frmNetworking.get('urlLinkedin');
    this.urlPinterest = this.frmNetworking.get('urlPinterest');
    this.urlYoutube = this.frmNetworking.get('urlYoutube');
    this.urlFlickr = this.frmNetworking.get('urlFlickr');
  }

  saveNetworking(): void {
    const validateForm = this.validateForm(this.frmNetworking);
    if (validateForm) {
      const params = this.frmNetworking.value as ISocialNetworking;
      this.configurationService.saveSocialNetworking(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IConfiguration) => {
          this.socialNetworking = response.socialNetworking;
          this.notificationService.addSuccess(this.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.notifications.completeFields);
    }
  }

  cancelNetworking($event: boolean): void {
    this.setDataNetworking();
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmNetworking, controlName);
  }

  private getConfiguration(): void {
    this.configurationService.getConfiguration(true, true)
      .subscribe((response: IConfiguration) => {
        this.configurationService.setConfiguration(response);
        this.socialNetworking = response.socialNetworking || {} as ISocialNetworking;
        this.setDataNetworking();
      });
  }

  private validateForm(formGroup: FormGroup): boolean {
    ValidatorUtil.validateForm(formGroup);

    return formGroup.valid;
  }

  private setDataNetworking(): void {
    this.urlFacebook.setValue(this.socialNetworking.urlFacebook);
    this.urlTwitter.setValue(this.socialNetworking.urlTwitter);
    this.urlInstagram.setValue(this.socialNetworking.urlInstagram);
    this.urlLinkedin.setValue(this.socialNetworking.urlLinkedin);
    this.urlPinterest.setValue(this.socialNetworking.urlPinterest);
    this.urlYoutube.setValue(this.socialNetworking.urlYoutube);
    this.urlFlickr.setValue(this.socialNetworking.urlFlickr);
  }

}
