import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeoSocialComponent } from './seo-social.component';

const routes: Routes = [
  {
    path: '',
    component: SeoSocialComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoSocialRoutingModule { }
