import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BvlTabsRouterModule } from '@bvl-core/ui/lib/components';
import { SeoConfigurationRoutingModule } from './seo-configuration-routing.module';
import { SeoConfigurationComponent } from './seo-configuration.component';

@NgModule({
  imports: [
    CommonModule,
    SeoConfigurationRoutingModule,
    BvlTabsRouterModule
  ],
  declarations: [SeoConfigurationComponent]
})
export class SeoConfigurationModule { }
