import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DevConfigurationComponent } from './dev-configuration.component';

const routes: Routes = [
  {
    path: '',
    component: DevConfigurationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevConfigurationRoutingModule { }
