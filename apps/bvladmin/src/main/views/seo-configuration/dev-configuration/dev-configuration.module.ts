import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { BvlButtonModule, BvlCardModule, BvlInputModule } from '@bvl-core/ui/lib/components';
import { DevConfigurationRoutingModule } from './dev-configuration-routing.module';
import { DevConfigurationComponent } from './dev-configuration.component';

@NgModule({
  imports: [
    BvlButtonModule,
    BvlCardModule,
    BvlInputModule,
    CommonModule,
    DevConfigurationRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [DevConfigurationComponent]
})
export class DevConfigurationModule { }
