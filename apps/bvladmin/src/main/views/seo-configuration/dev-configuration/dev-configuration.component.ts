import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

import { ConfigurationService } from '@bvl-admin/services';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';

@Component({
  selector: 'admin-dev-configuration',
  templateUrl: './dev-configuration.component.html'
})
export class DevConfigurationComponent extends UnsubscribeOnDestroy implements OnInit {

  frmKeys: FormGroup;

  btnSaveLabel = GeneralLang.buttons.save;
  notifications = GeneralLang.notifications;

  constructor(
    private _fb: FormBuilder,
    private _configurationService: ConfigurationService,
    private _notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.frmKeys = this._fb.group({
      googleMaps: undefined,
      gtm: undefined
    });

    this._configurationService.getConfiguration()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.frmKeys.patchValue(res.keys);
      });
  }

  saveKey(): void {
    this._configurationService.saveKeys(this.frmKeys.value)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this._notificationService.addSuccess(this.notifications.success);
      });
  }

}
