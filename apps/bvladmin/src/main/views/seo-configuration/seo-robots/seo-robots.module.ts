import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RobotsFormModule } from '@bvl-admin/components/robots-form/robots-form.module';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlButtonModule, BvlCardModule } from '@bvl-core/ui/lib/components';
import { SeoRobotsRoutingModule } from './seo-robots-routing.module';
import { SeoRobotsComponent } from './seo-robots.component';

@NgModule({
  imports: [
    CommonModule,
    SeoRobotsRoutingModule,
    RobotsFormModule,
    ModalModule,
    BvlButtonModule,
    BvlCardModule
  ],
  declarations: [SeoRobotsComponent]
})
export class SeoRobotsModule { }
