import { Component, OnInit } from '@angular/core';
import { RobotsFormComponent } from '@bvl-admin/components/robots-form/robots-form.component';
import { RobotsService } from '@bvl-admin/services/robots.service';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IListRobots, IListUserAgent, IRobots } from '@bvl-admin/statemanagement/models/robots.interface';
import { IModalConfig, ModalService } from '@bvl-core/shared/helpers/modal';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-seo-robots',
  templateUrl: './seo-robots.component.html'
})
export class SeoRobotsComponent extends UnsubscribeOnDestroy implements OnInit {
  notifications = GeneralLang.notifications;
  btnSaveLang = GeneralLang.buttons.save;
  listUserAgent: IListUserAgent = [];
  robots: IListRobots = [];

  constructor(
    private robotsService: RobotsService,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getUserAgent();
    this.getRobots();
  }

  getUserAgent(): void {
    this.robotsService.getUserAgent()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IListUserAgent) => {
        this.listUserAgent = response;
      });
  }

  getRobots(): void {
    this.robotsService.getRobots()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IListRobots) => {
        this.robots = response;
      });
  }

  addRobots(): void {
    const modalConfig: IModalConfig = {
      titleText: 'Agregar Robots',
      confirmButtonText: this.btnSaveLang
    };
    this.modalService.open(RobotsFormComponent, modalConfig)
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.notifications.success);
          this.getRobots();
        }
      })
      .catch(() => { });
  }

  deleteCategory(robots: IRobots): void {
    this.robotsService.deleteRobots(robots.id)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this.notificationService.addSuccess(this.notifications.success);
        this.getRobots();
      });
  }

  existItems(userAgent: string): number {
    return this.robots.filter(r => r.userAgent === userAgent).length;
  }

  existSitemap(): number {
    return this.robots.filter(r => !r.userAgent).length;
  }

  getRotobos(userAgent: string): IListRobots {
    return this.robots.filter((r: IRobots) => r.userAgent === userAgent)
      .sort((a: IRobots, b: IRobots) => a.type > b.type ? 1 : -1);
  }

  getSitemaps(): IListRobots {
    return this.robots.filter((r: IRobots) => !r.userAgent)
      .sort((a: IRobots, b: IRobots) => a.type > b.type ? 1 : -1);
  }

  generateRobots(): void {
    this.robotsService.generateRobots()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this.notificationService.addSuccess('Robots generado satisfactoriamente.');
      });
  }

}
