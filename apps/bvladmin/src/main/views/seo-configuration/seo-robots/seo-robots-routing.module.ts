import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeoRobotsComponent } from './seo-robots.component';

const routes: Routes = [
  {
    path: '',
    component: SeoRobotsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoRobotsRoutingModule { }
