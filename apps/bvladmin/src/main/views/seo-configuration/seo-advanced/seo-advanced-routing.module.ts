import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeoAdvancedComponent } from './seo-advanced.component';

const routes: Routes = [
  {
    path: '',
    component: SeoAdvancedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoAdvancedRoutingModule { }
