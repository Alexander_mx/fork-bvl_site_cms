import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BvlButtonModule, BvlCardModule, BvlInputModule, BvlRadioButtonModule } from '@bvl-core/ui/lib/components';
import { SeoAdvancedRoutingModule } from './seo-advanced-routing.module';
import { SeoAdvancedComponent } from './seo-advanced.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SeoAdvancedRoutingModule,
    BvlCardModule,
    BvlButtonModule,
    BvlInputModule,
    BvlRadioButtonModule
  ],
  declarations: [SeoAdvancedComponent]
})
export class SeoAdvancedModule { }
