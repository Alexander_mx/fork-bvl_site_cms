import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ConfigurationService } from '@bvl-admin/services';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IConfiguration, IMetadata } from '@bvl-admin/statemanagement/models/configuration.interface';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-seo-advanced',
  templateUrl: './seo-advanced.component.html'
})
export class SeoAdvancedComponent extends UnsubscribeOnDestroy implements OnInit {

  btnSaveLabel = GeneralLang.buttons.save;
  notifications = GeneralLang.notifications;

  metaData: IMetadata;

  frmAdvanced: FormGroup;
  titleSnippet: AbstractControl;
  description: AbstractControl;
  noindex: AbstractControl;

  constructor(
    private fb: FormBuilder,
    protected configurationService: ConfigurationService,
    protected notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.getConfiguration();
  }

  saveAdvanced(): void {
    const params = this.frmAdvanced.value as IMetadata;
    this.configurationService.saveMetadata(params)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IConfiguration) => {
        this.metaData = response.metadata;
        this.notificationService.addSuccess(this.notifications.success);
      });
  }

  private createForm(): void {
    this.frmAdvanced = this.fb.group({
      titleSnippet: [''],
      description: [''],
      noindex: ['']
    });
    this.titleSnippet = this.frmAdvanced.get('titleSnippet');
    this.description = this.frmAdvanced.get('description');
    this.noindex = this.frmAdvanced.get('noindex');
  }

  private getConfiguration(): void {
    this.configurationService.getConfiguration(true, true)
      .subscribe((response: IConfiguration) => {
        this.metaData = response.metadata;
        this.setMetadata();
      });
  }

  private setMetadata(): void {
    this.noindex.setValue(this.metaData.noindex);
    this.titleSnippet.setValue(this.metaData.titleSnippet);
  }

}
