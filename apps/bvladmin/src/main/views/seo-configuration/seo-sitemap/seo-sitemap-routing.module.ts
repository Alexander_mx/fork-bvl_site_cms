import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeoSitemapComponent } from './seo-sitemap.component';

const routes: Routes = [
  {
    path: '',
    component: SeoSitemapComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoSitemapRoutingModule { }
