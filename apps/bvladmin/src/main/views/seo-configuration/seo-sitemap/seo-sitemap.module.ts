import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SafeUrlPipeModule } from '@bvl-core/shared/helpers/pipes';
import { BvlButtonModule, BvlCardModule, BvlTabsModule } from '@bvl-core/ui/lib/components';
import { SeoSitemapRoutingModule } from './seo-sitemap-routing.module';
import { SeoSitemapComponent } from './seo-sitemap.component';

@NgModule({
  imports: [
    CommonModule,
    SeoSitemapRoutingModule,
    BvlButtonModule,
    BvlCardModule,
    BvlTabsModule,
    SafeUrlPipeModule
  ],
  declarations: [SeoSitemapComponent]
})
export class SeoSitemapModule { }
