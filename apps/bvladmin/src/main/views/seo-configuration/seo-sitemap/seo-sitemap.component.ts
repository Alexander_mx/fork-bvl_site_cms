import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';

import { ConfigurationService } from '@bvl-admin/services';
import { RobotsService } from '@bvl-admin/services/robots.service';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';

@Component({
  selector: 'admin-seo-sitemap',
  templateUrl: './seo-sitemap.component.html'
})
export class SeoSitemapComponent extends UnsubscribeOnDestroy implements OnInit {

  isVisibleFrame: boolean;

  siteMapLink: string;
  siteMapEsLink: string;
  siteMapENLink: string;

  constructor(
    private _robotsService: RobotsService,
    private _notificationService: NotificationService,
    private _configurationService: ConfigurationService
  ) {
    super();
  }

  ngOnInit(): void {
    this._configurationService.getConfiguration(true, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(condfiguration => {
        this.isVisibleFrame = true;
        this.siteMapLink = `${condfiguration.domain}/sitemap.xml`;
        this.siteMapEsLink = `${condfiguration.domain}/post_es_sitemap.xml`;
        this.siteMapENLink = `${condfiguration.domain}/post_en_sitemap.xml`;
      });
  }

  generateSitemap(): void {
    this.isVisibleFrame = false;
    this._robotsService.generateSitemap()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this._notificationService.addSuccess('Sitemap generado satisfactoriamente.');
        this.isVisibleFrame = true;
      });
  }

}
