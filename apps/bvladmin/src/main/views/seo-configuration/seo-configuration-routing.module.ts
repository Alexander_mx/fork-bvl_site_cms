import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeoConfigurationComponent } from './seo-configuration.component';

const routes: Routes = [
  {
    path: '',
    component: SeoConfigurationComponent,
    children: [
      {
        path: '',
        redirectTo: 'general'
      },
      {
        path: 'general',
        loadChildren: './seo-general/seo-general.module#SeoGeneralModule'
      },
      {
        path: 'social-networks',
        loadChildren: './seo-social/seo-social.module#SeoSocialModule'
      },
      {
        path: 'robots',
        loadChildren: './seo-robots/seo-robots.module#SeoRobotsModule'
      },
      {
        path: 'sitemap',
        loadChildren: './seo-sitemap/seo-sitemap.module#SeoSitemapModule'
      },
      {
        path: 'advanced',
        loadChildren: './seo-advanced/seo-advanced.module#SeoAdvancedModule'
      },
      {
        path: 'developing',
        loadChildren: './dev-configuration/dev-configuration.module#DevConfigurationModule'
      },
      {
        path: 'redirect',
        loadChildren: './seo-redirect/seo-redirect.module#SeoRedirectModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoConfigurationRoutingModule { }
