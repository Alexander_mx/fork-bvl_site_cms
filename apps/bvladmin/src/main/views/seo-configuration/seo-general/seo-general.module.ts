import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GallerySelectModule } from '@bvl-admin/components/gallery-select/gallery-select.module';
import { BvlAutocompleteModule, BvlButtonModule, BvlCardModule, BvlInputModule } from '@bvl-core/ui/lib/components';
import { SeoGeneralRoutingModule } from './seo-general-routing.module';
import { SeoGeneralComponent } from './seo-general.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SeoGeneralRoutingModule,
    GallerySelectModule,
    BvlCardModule,
    BvlButtonModule,
    BvlInputModule,
    BvlAutocompleteModule
  ],
  declarations: [SeoGeneralComponent]
})
export class SeoGeneralModule { }
