import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeoGeneralComponent } from './seo-general.component';

const routes: Routes = [
  {
    path: '',
    component: SeoGeneralComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoGeneralRoutingModule { }
