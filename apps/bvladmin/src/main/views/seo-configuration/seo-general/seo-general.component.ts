import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ConfigurationService, PageService } from '@bvl-admin/services';
import { REGEX } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IConfiguration, ISiteIdentityRequest } from '@bvl-admin/statemanagement/models/configuration.interface';
import { FILE_TYPE } from '@bvl-admin/statemanagement/models/file.interface';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { AngularUtil, UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';

@Component({
  selector: 'admin-seo-general',
  templateUrl: './seo-general.component.html'
})
export class SeoGeneralComponent extends UnsubscribeOnDestroy implements OnInit {

  FILE_TYPE = FILE_TYPE;

  btnSaveLabel = GeneralLang.buttons.save;
  notifications = GeneralLang.notifications;

  configuration: IConfiguration;

  frmGeneral: FormGroup;

  description: AbstractControl;
  domain: AbstractControl;
  iconSite: AbstractControl;
  page: AbstractControl;
  sitename: AbstractControl;

  pagesList: Array<any>;

  constructor(
    private fb: FormBuilder,
    protected configurationService: ConfigurationService,
    protected notificationService: NotificationService,
    private _pageService: PageService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.getConfiguration();
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmGeneral, controlName);
  }

  private createForm(): void {
    this.frmGeneral = this.fb.group({
      iconSite: [null],
      sitename: [null, Validators.required],
      domain: [null, [Validators.required, Validators.pattern(new RegExp(REGEX.domain))]],
      description: [null, Validators.required],
      page: [null, Validators.required]
    });
    this.iconSite = this.frmGeneral.get('iconSite');
    this.sitename = this.frmGeneral.get('sitename');
    this.description = this.frmGeneral.get('description');
    this.domain = this.frmGeneral.get('domain');
    this.page = this.frmGeneral.get('page');
  }

  private _params(): ISiteIdentityRequest {
    const params = AngularUtil.clone(this.frmGeneral.value);
    params.homePageId = params.page.idPage;
    delete params.page;

    return params as ISiteIdentityRequest;
  }

  saveIdentity(): void {
    const validateForm = this.validateForm(this.frmGeneral);
    if (validateForm) {
      const params = this._params();
      this.configurationService.saveSiteIdentity(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IConfiguration) => {
          this.configurationService.setConfiguration(response);
          this.configuration.metadata = response.metadata;
          this.configuration.sitename = response.sitename;
          this.configuration.domain = response.domain;
          this.configuration.icon = response.icon;
          this.configuration.homePageId = response.homePageId;

          this.notificationService.addSuccess(this.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.notifications.completeFields);
    }
  }

  getSitename(): string {
    return this.sitename.value || '';
  }

  private getConfiguration(): void {
    forkJoin([
      this._pageService.getPagesAutocopleteByPath(),
      this.configurationService.getConfiguration(true, true)
    ])
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(response => {
        this.pagesList = response[0];
        this.configuration = response[1];
        this.setDataIdentitfy();
      });
  }

  private validateForm(formGroup: FormGroup): boolean {
    ValidatorUtil.validateForm(formGroup);

    return formGroup.valid;
  }

  private setDataIdentitfy(): void {
    this.sitename.setValue(this.configuration.sitename);
    this.iconSite.setValue(this.configuration.icon);
    this.description.setValue(this.configuration.metadata.description);
    this.domain.setValue(this.configuration.domain);
    const page = this.pagesList.find(p => p.idPage === this.configuration.homePageId);
    this.page.setValue(page || null);
  }

}
