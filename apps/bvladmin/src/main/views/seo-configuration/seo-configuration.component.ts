import { Component, OnInit } from '@angular/core';
import { TAB_CONFIGURATION } from '@bvl-admin/settings/constants/tab.constant';

@Component({
  selector: 'admin-seo-configuration',
  templateUrl: './seo-configuration.component.html'
})
export class SeoConfigurationComponent implements OnInit {

  tabItemsList = TAB_CONFIGURATION;

  constructor() { }

  ngOnInit(): void {
  }

}
