import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

import { RedirectService } from '@bvl-admin/services/redirect.service';
import { EXCEL_MIME_TYPE } from '@bvl-admin/settings/constants/file.constant';
import { REGEX } from '@bvl-admin/settings/constants/general.constant';
import { SEO_REDIRECT_STATES } from '@bvl-admin/settings/constants/seo.constant';
import { IRedirect, IRedirectFilter, IRedirectPagination } from '@bvl-admin/statemanagement/models/redirect.interface';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { DATE_FORMAT } from '@bvl-site/settings/constants/general.constant';

@Component({
  selector: 'admin-seo-redirect',
  templateUrl: './seo-redirect.component.html'
})
export class SeoRedirectComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild('file') input: ElementRef<HTMLInputElement>;

  frm: FormGroup;
  frmFilter: FormGroup;

  SEO_REDIRECT_STATES = SEO_REDIRECT_STATES;
  DATE_FORMAT = DATE_FORMAT;
  pagination: IRedirectPagination;

  constructor(
    private _formBuilder: FormBuilder,
    private _notificationService: NotificationService,
    private _refirectService: RedirectService
  ) {
    super();
  }

  ngOnInit(): void {
    this.frm = this._formBuilder.group({
      oldUrl: [undefined, [Validators.required, Validators.pattern(REGEX.space)]],
      newUrl: [undefined, [Validators.required, Validators.pattern(REGEX.space)]]
    });

    this.frmFilter = this._formBuilder.group({ ...this._defaultFilter });

    this._getData();
  }

  saveRedirect(): void {
    ValidatorUtil.validateForm(this.frm);
    if (!this.frm.valid) {
      return void 0;
    }

    this._refirectService.add(this.frm.value)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this.frm.reset();
        this._getData();
        this._notificationService.addSuccess('Agregado satisfactoriamente.');
      });
  }

  search(): void {
    this.frmFilter.patchValue({ page: 1 });
    this._getData();
  }

  clear(): void {
    this.frmFilter.patchValue({ ...this._defaultFilter });
    this._getData();
  }

  pageChange(page: number): void {
    this.frmFilter.patchValue({ page });
    this._getData();
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frm, controlName);
  }

  selectExcel(): void {
    const input = this.input.nativeElement;
    input.type = '';
    input.value = null;
    // end reset
    input.type = 'file';
    input.multiple = false;
    input.accept = EXCEL_MIME_TYPE.join(',');

    const onchange = () => {
      const files = Array.from(input.files);
      this._importExcel(files[0]);

      input.onchange = null;
    };

    input.onchange = onchange;
    input.click();
  }

  switchStateRedirect(redirect: IRedirect, index: number): void {
    const body = {
      ...redirect,
      state: redirect.state === 1 ? 0 : 1
    };

    this._refirectService.update(redirect.id, body as IRedirect)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(resp => {
        this.pagination.content[index] = resp;
        this._notificationService.addSuccess('Actualizado correctamente.');
      });
  }

  private _importExcel(file: File): void {
    this._refirectService.importExcel(file)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this._getData();
        this._notificationService.addSuccess('Archivo importado satisfactoriamente.');
      });
  }

  private _getData(): void {
    const params = {
      ...this.frmFilter.value,
      state: this.frmFilter.value.state && this.frmFilter.value.state.code
    };

    this._refirectService.filter(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(pagination => {
        this.pagination = pagination;
      });
  }

  private get _defaultFilter(): IRedirectFilter {
    return {
      oldUrl: '',
      newUrl: '',
      state: '',
      size: 10,
      page: 1
    };
  }

}
