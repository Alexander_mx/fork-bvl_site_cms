import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeoRedirectComponent } from './seo-redirect.component';

const routes: Routes = [
  {
    path: '',
    component: SeoRedirectComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoRedirectRoutingModule { }
