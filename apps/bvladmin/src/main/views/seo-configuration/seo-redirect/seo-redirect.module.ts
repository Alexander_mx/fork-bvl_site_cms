import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  BvlButtonModule,
  BvlCardModule,
  BvlInputModule,
  BvlLinkModule,
  BvlPaginatorModule,
  BvlSelectModule
} from '@bvl-core/ui/lib/components';
import { SeoRedirectRoutingModule } from './seo-redirect-routing.module';
import { SeoRedirectComponent } from './seo-redirect.component';

@NgModule({
  imports: [
    BvlButtonModule,
    BvlCardModule,
    BvlInputModule,
    BvlLinkModule,
    BvlPaginatorModule,
    BvlSelectModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SeoRedirectRoutingModule
  ],
  declarations: [SeoRedirectComponent]
})
export class SeoRedirectModule { }
