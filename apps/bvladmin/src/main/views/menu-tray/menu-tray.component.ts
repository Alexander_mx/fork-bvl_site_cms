import { Component, OnInit } from '@angular/core';
import { AddItemMenuComponent } from '@bvl-admin/components/modals/add-item-menu/add-item-menu.component';
import { SiteMenuService } from '@bvl-admin/services';
import { LANGUAGE_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { ModalService } from '@bvl-core/shared/helpers/modal';
import { AngularUtil, UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { ISiteMenu, menuTree, SITE_MENU_TYPE } from '@bvl-core/shared/helpers/util/menu';
import { LANGUAGE_TYPES_CODE } from '@bvl-core/shared/settings/constants/general.constants';
import { forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-menu-tray',
  templateUrl: './menu-tray.component.html',
  styleUrls: ['./menu-tray.component.scss']
})
export class MenuTrayComponent extends UnsubscribeOnDestroy implements OnInit {

  LANGUAGE_TYPES = LANGUAGE_TYPES;
  SITE_MENU_TYPE = SITE_MENU_TYPE;

  activeLanguage: string;

  header: Array<ISiteMenu>;
  footer: Array<ISiteMenu>;

  constructor(
    private _siteMenuService: SiteMenuService,
    private _modalService: ModalService
  ) {
    super();
    this.activeLanguage = LANGUAGE_TYPES_CODE.es;
  }

  ngOnInit(): void {
    this._getMenu(this.activeLanguage);
  }

  private _getMenu(language: string): void {
    this._siteMenuService.getMenu(language)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<ISiteMenu>) => {
        this.header = menuTree(response.filter(fv => fv.type === SITE_MENU_TYPE.HEADER));
        this.footer = menuTree(response.filter(fv => fv.type === SITE_MENU_TYPE.FOOTER));
      });
  }

  selectTabChange(language: string): void {
    this.activeLanguage = language;
    this._getMenu(this.activeLanguage);
  }

  collapsed(item: ISiteMenu, list: Array<ISiteMenu>): void {
    (list || []).forEach(value => {
      value.collapse = (value.id !== item.id)
                          ? true
                          : !item.collapse;
    });
  }

  addItem(type: SITE_MENU_TYPE): void {
    const list = (type ===  SITE_MENU_TYPE.HEADER)
                    ? this.header
                    : this.footer;
    const modal = this._modalService.open(AddItemMenuComponent, { titleText: 'Agregar' });
    modal.setPayload({
      type,
      language: this.activeLanguage,
      order: list.length
    });
    modal.result.then((response: ISiteMenu) => {
      list.push(response);
    })
    .catch(() => false);
  }

  remove(index: number, list: Array<ISiteMenu>): void {
    list.splice(index, 1);
  }

  upChange(index: number, item: ISiteMenu, list: Array<ISiteMenu>): void {
    const dropItem = AngularUtil.clone(list[index - 1]),
          currentItem = AngularUtil.clone(item);

    currentItem.order--;
    dropItem.order++;

    list[index - 1] = currentItem;
    list[index] = dropItem;

    forkJoin(
      this._siteMenuService.updateMenu(currentItem, false),
      this._siteMenuService.updateMenu(dropItem, false)
    )
    .pipe(
      takeUntil(this.unsubscribeDestroy$)
    )
    .subscribe(() => { });
  }

  downChange(index: number, item: ISiteMenu, list: Array<ISiteMenu>): void {
    const dropItem = AngularUtil.clone(list[index + 1]),
          currentItem = AngularUtil.clone(item);

    currentItem.order++;
    dropItem.order--;

    list[index + 1] = currentItem;
    list[index] = dropItem;

    forkJoin(
      this._siteMenuService.updateMenu(currentItem, false),
      this._siteMenuService.updateMenu(dropItem, false)
    )
    .pipe(
      takeUntil(this.unsubscribeDestroy$)
    )
    .subscribe(() => { });
  }

}
