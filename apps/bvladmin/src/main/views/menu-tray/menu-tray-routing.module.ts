import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuTrayComponent } from './menu-tray.component';

const routes: Routes = [
  {
    path: '',
    component: MenuTrayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuTrayRoutingModule { }
