import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AddItemMenuModule } from '@bvl-admin/components/modals/add-item-menu/add-item-menu.module';
import { ItemMenuModule } from '@bvl-admin/components/trays-item/item-menu/item-menu.module';
import { ItemPageModule } from '@bvl-admin/components/trays-item/item-page/item-page.module';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { OrderByPipeModule } from '@bvl-core/shared/helpers/pipes';
import { BvlAccordionModule, BvlButtonModule, BvlCardModule, BvlTabsModule } from '@bvl-core/ui/lib/components';
import { CollapseModule } from '@bvl-core/ui/lib/directives';
import { MenuTrayRoutingModule } from './menu-tray-routing.module';
import { MenuTrayComponent } from './menu-tray.component';

@NgModule({
  declarations: [MenuTrayComponent],
  imports: [
    CommonModule,
    MenuTrayRoutingModule,
    BvlCardModule,
    BvlAccordionModule,
    ItemPageModule,
    BvlTabsModule,
    ItemMenuModule,
    CollapseModule,
    BvlButtonModule,
    AddItemMenuModule,
    ModalModule,
    OrderByPipeModule
  ]
})
export class MenuTrayModule { }
