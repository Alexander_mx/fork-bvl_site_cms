import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentTrayComponent } from './component-tray.component';

const routes: Routes = [
  {
    path: '',
    component: ComponentTrayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentTrayRoutingModule { }
