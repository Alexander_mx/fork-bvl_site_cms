import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PopupTemplatesComponent } from '@bvl-admin/components/modals/popup-templates/popup-templates.component';
import { ComponentService } from '@bvl-admin/services';
import { TYPES_CODE } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IComponentPaginationRequest, IComponentPaginationResponse } from '@bvl-admin/statemanagement/models/component.interface';
import { ISearchFormView } from '@bvl-admin/statemanagement/models/general.interface';
import { IModalConfig, MODAL_BREAKPOINT, ModalService } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { BvlPaginator } from '@bvl-core/ui/lib/components';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-component-tray',
  templateUrl: './component-tray.component.html',
  styleUrls: ['./component-tray.component.scss']
})
export class ComponentTrayComponent extends UnsubscribeOnDestroy implements OnInit {

  type: number;
  searchForm: ISearchFormView;
  componentsList: IComponentPaginationResponse;
  withoutResults: boolean;
  pagination: BvlPaginator;

  TYPES_CODE = TYPES_CODE;
  GENERAL_LANG = GeneralLang;

  constructor(
    private _componentService: ComponentService,
    private _activatedRoute: ActivatedRoute,
    private _modalService: ModalService,
    private _router: Router
  ) {
    super();
    this.pagination = new BvlPaginator();
  }

  ngOnInit(): void {
    this.type = this._activatedRoute.snapshot.data.type;
    this._componentsPagination();
  }

  private _paramComponentsPagination(searchForm?: ISearchFormView): IComponentPaginationRequest {
    this.searchForm = searchForm || {} as ISearchFormView;

    return {
      componentName: this.searchForm.search || '',
      status: this.searchForm.statePages || [],
      userList: this.searchForm.users || [],
      size: this.pagination.pageSize,
      page: this.pagination.page,
      type: this.type
    } as IComponentPaginationRequest;
  }

  private _componentsPagination(searchForm?: ISearchFormView): void {
    const params = this._paramComponentsPagination(searchForm);
    this._componentService.componentsPagination(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IComponentPaginationResponse) => {
        this.withoutResults = !response.content.length;
        this.componentsList = response;
      }, () => {
        this.withoutResults = true;
      });
  }

  search(searchForm: ISearchFormView): void {
    this.pagination.page = 1;
    this._componentsPagination(searchForm);
  }

  pageChange(event: any): void {
    this._componentsPagination(this.searchForm);
  }

  goCreatePopup(): void {
    const modalConfig = {
      titleText: 'Seleccionar popup',
      size: MODAL_BREAKPOINT.md,
      showFooter: false
    } as IModalConfig;
    this._modalService.open(PopupTemplatesComponent, modalConfig).result
      .then((response: string) => {
        this._router.navigate(['editor/edit/popup-component', response]);
      })
      .catch(() => false);
  }

}
