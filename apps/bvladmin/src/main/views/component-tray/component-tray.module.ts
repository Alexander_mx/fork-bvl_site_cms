import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PopupTemplatesModule } from '@bvl-admin/components/modals/popup-templates/popup-templates.module';
import { SearchFormModule } from '@bvl-admin/components/search-form/search-form.module';
import { ItemComponentModule } from '@bvl-admin/components/trays-item/item-component/item-component.module';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlButtonModule, BvlCardModule, BvlPaginatorModule } from '@bvl-core/ui/lib/components';
import { WithoutResultsModule } from '@bvl-core/ui/lib/directives';
import { ComponentTrayRoutingModule } from './component-tray-routing.module';
import { ComponentTrayComponent } from './component-tray.component';

@NgModule({
  declarations: [ComponentTrayComponent],
  imports: [
    CommonModule,
    ComponentTrayRoutingModule,
    BvlCardModule,
    ItemComponentModule,
    SearchFormModule,
    WithoutResultsModule,
    BvlPaginatorModule,
    BvlButtonModule,
    PopupTemplatesModule,
    ModalModule
  ]
})
export class ComponentTrayModule { }
