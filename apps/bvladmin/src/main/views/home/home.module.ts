import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ItemPageModule } from '@bvl-admin/components/trays-item/item-page/item-page.module';
import { BvlCardModule, BvlLinkModule } from '@bvl-core/ui/lib/components';
import { WithoutResultsModule } from '@bvl-core/ui/lib/directives';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    BvlCardModule,
    ItemPageModule,
    BvlLinkModule,
    WithoutResultsModule
  ]
})
export class HomeModule { }
