import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationService, PageService } from '@bvl-admin/services';
import { DASHBOARD } from '@bvl-admin/settings/constants/page.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IPagePaginationResponse } from '@bvl-admin/statemanagement/models/page.interface';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends UnsubscribeOnDestroy implements OnInit {

  welcomeMessage: string;
  dashboard: Array<IPagePaginationResponse>;

  GENERAL_LANG = GeneralLang;
  DASHBOARD = DASHBOARD;

  constructor(
    private _pageService: PageService,
    private _configurationService: ConfigurationService,
    private _router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.welcomeMessage = this._welcomeMessage();
    this._dahsboard();
  }

  private _welcomeMessage(): string {
    return this.GENERAL_LANG.titles.helloUsernameWelcome
            .replace('{{ fullname }}', this._configurationService.getUserPermissions().fullname);
  }

  private _dahsboard(): void {
    this._pageService.dashboard(true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IPagePaginationResponse>) => {
        this.dashboard = response;
      });
  }

  seeAll(stateCode: number): void {
    const queryParams = { state: stateCode };
    this._router.navigate(['/page-tray'], { queryParams });
  }

  deletePage(): void {
    this._dahsboard();
  }

}
