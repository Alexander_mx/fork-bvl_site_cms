import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormModule } from '@bvl-admin/components/dynamic-form';
import { BvlButtonModule } from '@bvl-core/ui/lib/components';
import { ViewEditorRoutingModule } from './view-editor-routing.module';
import { ViewEditorComponent } from './view-editor.component';

@NgModule({
  imports: [
    CommonModule,
    ViewEditorRoutingModule,
    BvlButtonModule,
    DynamicFormModule,
    ReactiveFormsModule
  ],
  declarations: [ViewEditorComponent]
})
export class ViewEditorModule { }
