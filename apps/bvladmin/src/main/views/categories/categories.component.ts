import { Component, OnInit } from '@angular/core';
import { AddCategoryComponent } from '@bvl-admin/components/modals/add-category/add-category.component';
import { CategoriesService } from '@bvl-admin/services/categories.services';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { TRAY_COLLAPSE_BUTTONS } from '@bvl-admin/settings/constants/user.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { ICategories } from '@bvl-admin/statemanagement/models/categories.interface';
import { IModalConfig, ModalRef, ModalService } from '@bvl-core/shared/helpers/modal';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent extends UnsubscribeOnDestroy implements OnInit {

  categories: ICategories;
  GENERAL_LANG = GeneralLang;
  TRAY_COLLAPSE_BUTTONS = TRAY_COLLAPSE_BUTTONS;
  ACTION_TYPES = ACTION_TYPES;
  currentPage = 1;

  constructor(
    private _categoriesService: CategoriesService,
    private _modalService: ModalService,
    private _notificationService: NotificationService
    ) {
    super();
  }

  ngOnInit(): void {
    this._getCategories(1);
  }

  openModal(action, category?): void {
    let modalConfig: IModalConfig;

    if (action === this.ACTION_TYPES.delete) {
      modalConfig = {
        titleText: '¿Está seguro de eliminar esta categoría?',
        confirmButtonText: this.GENERAL_LANG.buttons.acept
      };
    } else {
      modalConfig = {
        titleText: `${action === this.ACTION_TYPES.add ? 'Agregar' : 'Editar' } categoría`,
        confirmButtonText: this.GENERAL_LANG.buttons.save
      };
    }
    const modalRef: ModalRef = this._modalService.open(AddCategoryComponent, modalConfig);
    modalRef.setPayload({ action, category });
    modalRef.result
    .then(() => {
      this._getCategories(this.currentPage);
      this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
    }, () => {
      return false;
    });
  }

  pageChange(page): void {
    this.currentPage = page;
    this._getCategories(page);
  }

  private _getCategories(page): void {
    this._categoriesService.getCategories({
      page,
      size: 10
    })
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe(response => {
      this.categories = response;
    });
  }
}
