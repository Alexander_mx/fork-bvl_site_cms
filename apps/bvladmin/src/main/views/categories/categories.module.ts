import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCategoryModule } from '@bvl-admin/components/modals/add-category/add-category.module';
import { CategoriesService } from '@bvl-admin/services/categories.services';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlButtonModule, BvlCardModule, BvlInputModule, BvlPaginatorModule } from '@bvl-core/ui/lib/components';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';

@NgModule({
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    BvlCardModule,
    BvlPaginatorModule,
    BvlButtonModule,
    ModalModule,
    BvlInputModule,
    FormsModule,
    ReactiveFormsModule,
    AddCategoryModule
  ],
  providers: [ CategoriesService ],
  declarations: [ CategoriesComponent]
})
export class CategoriesModule { }
