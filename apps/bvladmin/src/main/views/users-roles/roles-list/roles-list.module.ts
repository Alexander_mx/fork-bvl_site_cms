import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CreateRoleFormModule } from '@bvl-admin/components/modals/create-role-form/create-role-form.module';
import { CollapseRoleModule } from '@bvl-admin/components/trays-collpase/collapse-role/collapse-role.module';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlCardModule, BvlLinkModule, BvlPaginatorModule } from '@bvl-core/ui/lib/components';
import { RolesListRoutingModule } from './roles-list-routing.module';
import { RolesListComponent } from './roles-list.component';

@NgModule({
  declarations: [RolesListComponent],
  imports: [
    CommonModule,
    RolesListRoutingModule,
    BvlCardModule,
    BvlLinkModule,
    CollapseRoleModule,
    ModalModule,
    CreateRoleFormModule,
    BvlPaginatorModule
  ]
})
export class RolesListModule { }
