import { Component, OnInit } from '@angular/core';
import { CreateRoleFormComponent } from '@bvl-admin/components/modals/create-role-form/create-role-form.component';
import { RoleService } from '@bvl-admin/services';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang, RoleLang } from '@bvl-admin/settings/lang';
import {
  IRolePaginationRequest,
  IRolePaginationResponse,
  IRolePaginationView,
  IRolePermissionsDetailView
} from '@bvl-admin/statemanagement/models/role.interface';
import { ModalService } from '@bvl-core/shared/helpers/modal';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { BvlPaginator } from '@bvl-core/ui/lib/components';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.scss']
})
export class RolesListComponent extends UnsubscribeOnDestroy implements OnInit {

  rolesList: IRolePaginationResponse;
  pagination: BvlPaginator;

  GENERAL_LANG = GeneralLang;
  ROLE_LANG = RoleLang;

  constructor(
    private _roleService: RoleService,
    private _modalService: ModalService,
    private _notificationService: NotificationService
  ) {
    super();
    this.pagination = new BvlPaginator();
  }

  ngOnInit(): void {
    this._rolesPagination();
    this._updateRolesList();
  }

  goCreateRole(): void {
    this._modalService.open(CreateRoleFormComponent).result
      .then((response: string) => {
        if (response === ACTION_TYPES.save) {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.pagination.page = 1;
          this._rolesPagination();
        }
      }, () => {
        return false;
      });
  }

  private _paramsRolesPagination(): IRolePaginationRequest {
    return {
      size: this.pagination.pageSize,
      page: this.pagination.page
    } as IRolePaginationRequest;
  }

  private _rolesPagination(): void {
    const params = this._paramsRolesPagination();
    this._roleService.rolesPagination(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IRolePaginationResponse) => {
        const rolesList: IRolePaginationView = response;
        rolesList.content = rolesList.content.map((role: IRolePermissionsDetailView) => {
          role.disabled = false;

          return role;
        });
        this.rolesList = rolesList;
      });
  }

  pageChange(event: any): void {
    this._rolesPagination();
  }

  private _updateRolesList(): void {
    this._roleService.updateRolesList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IRolePermissionsDetailView) => {
        if (response && this.rolesList) {
          this.rolesList.content = this.rolesList.content.filter((value: IRolePermissionsDetailView) => {
            if (response.actionType === ACTION_TYPES.edit) {
              value.disabled = (response.roleCode !== value.roleCode);
            } else if (response.actionType === ACTION_TYPES.cancel) {
              value.disabled = false;
            } else {
              return response.roleCode !== value.roleCode;
            }

            return value;
          });
        }
      });
  }

}
