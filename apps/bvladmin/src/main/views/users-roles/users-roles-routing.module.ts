import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersRolesComponent } from './users-roles.component';

const routes: Routes = [
  {
    path: '',
    component: UsersRolesComponent,
    children: [
      {
        path: '',
        redirectTo: 'users-list'
      },
      {
        path: 'users-list',
        loadChildren: './users-list/users-list.module#UsersListModule'
      },
      {
        path: 'roles-list',
        loadChildren: './roles-list/roles-list.module#RolesListModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRolesRoutingModule { }
