import { Component, OnInit } from '@angular/core';
import { TAB_USERS_ROLES } from '@bvl-admin/settings/constants/tab.constant';

@Component({
  selector: 'admin-users-roles',
  templateUrl: './users-roles.component.html',
  styleUrls: ['./users-roles.component.scss']
})
export class UsersRolesComponent implements OnInit {

  tabItemsList = TAB_USERS_ROLES;

  constructor() { }

  ngOnInit(): void { }

}
