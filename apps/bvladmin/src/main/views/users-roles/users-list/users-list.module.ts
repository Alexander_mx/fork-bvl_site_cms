import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AddUserFormModule } from '@bvl-admin/components/modals/add-user-form/add-user-form.module';
import { SearchFormModule } from '@bvl-admin/components/search-form/search-form.module';
import { CollapseUserModule } from '@bvl-admin/components/trays-collpase/collapse-user/collapse-user.module';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlButtonModule, BvlCardModule, BvlLinkModule, BvlPaginatorModule } from '@bvl-core/ui/lib/components';
import { WithoutResultsModule } from '@bvl-core/ui/lib/directives';
import { UsersListRoutingModule } from './users-list-routing.module';
import { UsersListComponent } from './users-list.component';

@NgModule({
  declarations: [UsersListComponent],
  imports: [
    CommonModule,
    UsersListRoutingModule,
    BvlCardModule,
    BvlLinkModule,
    CollapseUserModule,
    ModalModule,
    AddUserFormModule,
    SearchFormModule,
    WithoutResultsModule,
    BvlPaginatorModule,
    BvlButtonModule
  ]
})
export class UsersListModule { }
