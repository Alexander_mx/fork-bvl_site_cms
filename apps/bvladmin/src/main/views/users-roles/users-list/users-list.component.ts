import { Component, OnInit } from '@angular/core';
import { AddUserFormComponent } from '@bvl-admin/components/modals/add-user-form/add-user-form.component';
import { UserService } from '@bvl-admin/services';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { UserLang } from '@bvl-admin/settings/lang/user.lang';
import { ISearchFormView } from '@bvl-admin/statemanagement/models/general.interface';
import {
  IUserPaginationRequest,
  IUserPaginationResponse,
  IUserPaginationView,
  IUserView
} from '@bvl-admin/statemanagement/models/user.interface';
import { IModalConfig, ModalService } from '@bvl-core/shared/helpers/modal';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { BvlPaginator } from '@bvl-core/ui/lib/components';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent extends UnsubscribeOnDestroy implements OnInit {

  searchForm: ISearchFormView;
  usersList: IUserPaginationView;
  withoutResults: boolean;
  pagination: BvlPaginator;

  GENERAL_LANG = GeneralLang;
  USER_LANG = UserLang;

  constructor(
    private _userService: UserService,
    private _modalService: ModalService,
    private _notificationService: NotificationService
  ) {
    super();
    this.pagination = new BvlPaginator();
  }

  ngOnInit(): void {
    this._usersPagination();
    this._updateUsersList();
  }

  goAddUser(): void {
    const modalConfig: IModalConfig = {
      titleText: this.USER_LANG.titles.addUser,
      confirmButtonText: this.GENERAL_LANG.buttons.save
    };
    this._modalService.open(AddUserFormComponent, modalConfig).result
      .then((response: string) => {
        if (response === ACTION_TYPES.save) {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.pagination.page = 1;
          this._usersPagination();
        }
      }, () => {
        return false;
      });
  }

  private _paramsUsersPagination(searchForm?: ISearchFormView): IUserPaginationRequest {
    this.searchForm = searchForm || {} as ISearchFormView;

    return {
      fullname: this.searchForm.search || '',
      size: this.pagination.pageSize,
      page: this.pagination.page
    } as IUserPaginationRequest;
  }

  private _usersPagination(searchForm?: ISearchFormView): void {
    const params = this._paramsUsersPagination(searchForm);
    this._userService.usersPagination(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IUserPaginationResponse) => {
        const usersList: IUserPaginationView = response;
        usersList.content = response.content.map((user: IUserView) => {
          user.disabled = false;

          return user;
        });
        this.withoutResults = !response.content.length;
        this.usersList = usersList;
      }, () => {
        this.withoutResults = false;
      });
  }

  search(searchForm: ISearchFormView): void {
    this.pagination.page = 1;
    this._usersPagination(searchForm);
  }

  pageChange(event: any): void {
    this._usersPagination(this.searchForm);
  }

  private _updateUsersList(): void {
    this._userService.updateUsersList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IUserView) => {
        if (response && this.usersList) {
          this.usersList.content = this.usersList.content.filter((value: IUserView) => {
            if (response.actionType === ACTION_TYPES.edit) {
              value.disabled = (value.username !== response.username);
            } else if (response.actionType === ACTION_TYPES.cancel) {
              value.disabled = false;
            } else {
              return value.username !== response.username;
            }

            return value;
          });
        }
      });
  }

}
