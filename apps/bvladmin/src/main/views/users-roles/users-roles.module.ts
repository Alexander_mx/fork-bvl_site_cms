import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BvlTabsRouterModule } from '@bvl-core/ui/lib/components';
import { UsersRolesRoutingModule } from './users-roles-routing.module';
import { UsersRolesComponent } from './users-roles.component';

@NgModule({
  declarations: [UsersRolesComponent],
  imports: [
    CommonModule,
    UsersRolesRoutingModule,
    BvlTabsRouterModule
  ]
})
export class UsersRolesModule { }
