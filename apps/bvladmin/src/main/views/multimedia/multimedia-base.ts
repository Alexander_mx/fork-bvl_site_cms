import { DOCUMENT } from '@angular/common';
import { HttpEventType } from '@angular/common/http';
import { ElementRef, Inject, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileService } from '@bvl-admin/services';
import { FileLang } from '@bvl-admin/settings/lang/file.lang';
import { FILE_TYPE, IMultimediaProperty, IMultimediaResponse, ITmpImage } from '@bvl-admin/statemanagement/models/file.interface';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

export abstract class MultimediaBase extends UnsubscribeOnDestroy {

  @ViewChild('file') input: ElementRef<HTMLInputElement>;

  currentImage: IMultimediaResponse;

  files: Array<IMultimediaResponse> = [];
  lang = FileLang;

  modalPayload: { mimeTypes: Array<string>; maxSize: number; type: FILE_TYPE, defaultFile: IMultimediaResponse };
  openFilter: boolean;
  showLoading: boolean;

  tmpFile: ITmpImage | any;

  form: FormGroup;
  title: AbstractControl;
  alt: AbstractControl;
  description: AbstractControl;

  constructor(
    protected notificationService: NotificationService,
    protected fileService: FileService,
    protected fb: FormBuilder,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super();
  }

  addFile(): void {
    this.openFilter = false;
    const input = this.input.nativeElement;
    input.type = '';
    input.value = null;
    // end reset
    input.type = 'file';
    input.multiple = false;
    input.accept = this.modalPayload.mimeTypes.join(',');

    const onchange = () => {
      const files = Array.from(input.files);
      const valid = this.validateFile(files);

      if (valid) { this.uploadFile(files[0]); }

      input.onchange = null;
    };

    input.onchange = onchange;
    input.click();
  }

  uploadFile(file: File, payload?: any): void {
    const properties = this.form && payload ? this.form.value : {};

    this.getPreview(file, payload);
    this.fileService.uploadFileByType((payload || this.modalPayload).type, file, properties)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        event => {
          if (event.type === HttpEventType.Response) {
            const data = event.body.data;
            this.tmpFile = null;
            this.files.unshift(data);
            this.selected(data, event.body.message);
          }

          if (event.type === HttpEventType.UploadProgress) {
            this.tmpFile.progress = Math.round(event.loaded / event.total * 100);
          }
        },
        err => {
          this.activeError(err.originalError.error.message || this.lang.errors.image.fail);
          if (!payload) {
            this.tmpFile = null;
          }
          this.tmpFile.progress = 0;
        });
  }

  validateFile(files: Array<File> = []): boolean {
    const differentFormat = !!files.find(file => !this.modalPayload.mimeTypes.find(type => type === file.type));
    const errorSize = this.modalPayload.maxSize ? !!files.find(file => (file.size / (1024 * 1024)) > this.modalPayload.maxSize) : false;

    const invalidNames = this.files.map(image => image.name);
    const existFile = !!files.find(file => !!invalidNames.find(name => name === file.name));

    const messageError = differentFormat ? `${this.lang.errors.image.format} (${this.getFormat()}).` :
      errorSize ? `${this.lang.errors.image.size} ${Number(this.modalPayload.maxSize)
        .toFixed(2)} MB.` :
        existFile ? `${this.lang.errors.image.exist}.` : '';

    this.activeError(messageError);

    return !(differentFormat || errorSize || existFile);
  }

  getFormat(): string {
    return this.modalPayload.mimeTypes
      .map(type => {
        const list = type.split('/');

        return list[list.length - 1];
      })
      .join(' , ')
      .toUpperCase();
  }

  activeError(message: string): void {
    if (message) {
      this.notificationService.addError(message);
    }
  }

  getPreview(file: File, payload?: any): void {
    this.tmpFile = {
      name: file.name,
      size: file.size,
      small: {},
      medium: {},
      large: {},
      mimeType: file.type
    };

    switch ((payload || this.modalPayload).type) {
      case FILE_TYPE.document:

        break;
      case FILE_TYPE.image:
        const _URL = window.URL || (window as any).webkitURL;

        this.tmpFile.small.imageUri = _URL.createObjectURL(file);
        this.tmpFile.medium.imageUri = _URL.createObjectURL(file);
        break;

      default:
        this.generateThumbnail(file)
          .then(url => {
            this.tmpFile.small.imageUri = url;
            this.tmpFile.medium.imageUri = url;
          });
        break;
    }
  }

  validateControl(control: AbstractControl): boolean {
    return ValidatorUtil.validateAbstractControl(control);
  }

  createForm(currentImage?: IMultimediaResponse): void {
    this.currentImage = this.currentImage || currentImage;

    const property = this.currentImage && this.currentImage.properties ? this.currentImage.properties : {} as IMultimediaProperty;

    this.form = this.fb.group({
      title: [property.title || ''],
      alt: [property.alt || ''],
      description: [property.description || '']
    });

    this.title = this.form.get('title');
    this.alt = this.form.get('alt');

    if (this.modalPayload.type === FILE_TYPE.image) {
      this.title.setValidators(Validators.required);
      this.alt.setValidators(Validators.required);
    }

    this.description = this.form.get('description');
  }

  private generateThumbnail(videoFile: File): Promise<string> {
    const video: HTMLVideoElement = this.document.createElement('video');
    const canvas: HTMLCanvasElement = this.document.createElement('canvas');
    const context: CanvasRenderingContext2D = canvas.getContext('2d');

    return new Promise<string>((resolve, reject) => {
      canvas.addEventListener('error', reject);
      video.addEventListener('error', reject);
      video.addEventListener('canplay', event => {
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        resolve(canvas.toDataURL());
      });
      if (videoFile.type) {
        video.setAttribute('type', videoFile.type);
      }
      video.preload = 'auto';
      video.src = window.URL.createObjectURL(videoFile);
      video.load();
    });
  }

  abstract selected(file: IMultimediaResponse, message?: string): void;
}
