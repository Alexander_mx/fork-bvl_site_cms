import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BvlCheckboxModule } from '@bvl-core/ui/lib/components';
import { CollapseModule } from '@bvl-core/ui/lib/directives';
import { DropdownCheckboxesComponent } from './dropdown-checkboxes.component';

@NgModule({
  declarations: [DropdownCheckboxesComponent],
  imports: [
    CommonModule,
    BvlCheckboxModule,
    CollapseModule,
    FormsModule
  ],
  exports: [DropdownCheckboxesComponent]
})
export class DropdownCheckboxesModule { }
