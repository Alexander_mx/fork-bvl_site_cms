import { Component, Input, OnInit } from '@angular/core';
import { IComponentResponse } from '@bvl-admin/statemanagement/models/component.interface';

@Component({
  selector: 'admin-component-icon',
  templateUrl: './component-icon.component.html',
  styleUrls: ['./component-icon.component.scss']
})
export class ComponentIconComponent implements OnInit {

  @Input() component: IComponentResponse;

  constructor() { }

  ngOnInit(): void { }

}
