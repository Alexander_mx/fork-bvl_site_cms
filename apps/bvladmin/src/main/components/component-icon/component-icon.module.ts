import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentIconComponent } from './component-icon.component';
import { ComponentIconDirective } from './component-icon.directive';

@NgModule({
  declarations: [
    ComponentIconComponent,
    ComponentIconDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ComponentIconComponent
  ]
})
export class ComponentIconModule { }
