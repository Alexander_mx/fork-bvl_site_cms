import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { IComponentResponse } from '@bvl-admin/statemanagement/models/component.interface';

@Directive({
// tslint:disable-next-line: directive-selector
  selector: '[bvlComponentIcon]'
})
export class ComponentIconDirective implements OnInit {

  @Input() component: IComponentResponse;

  constructor(
    private elementRef: ElementRef<HTMLDivElement>
  ) { }

  ngOnInit(): void {
    this.elementRef.nativeElement.draggable = true;
    this.elementRef.nativeElement.ondragstart = ev => {
      ev.dataTransfer.setData('text', JSON.stringify(this.component));
    };
  }

}
