import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GeneralService, UserService } from '@bvl-admin/services';
import { DATE_FORMAT, VIEW_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { ISearchFormView, IStateResponse } from '@bvl-admin/statemanagement/models/general.interface';
import { IUserResponse } from '@bvl-admin/statemanagement/models/user.interface';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent extends UnsubscribeOnDestroy implements OnInit {

  @Input() type: string;
  @Input() showUsers: boolean;
  @Input() showPageStates: boolean;
  @Input() showCalendar: boolean;

  @Output() search: EventEmitter<any>;

  frmSearch: FormGroup;
  mSearch: AbstractControl;
  mUsers: AbstractControl;
  mStates: AbstractControl;
  mStartDate: AbstractControl;
  mEndDate: AbstractControl;

  usersList: Array<IUserResponse>;
  pageStates: Array<IStateResponse>;

  GENERAL_LANG = GeneralLang;

  constructor(
    private _formBuilder: FormBuilder,
    private _userService: UserService,
    private _generalService: GeneralService,
    private _datePipe: DatePipe,
    private _activatedRoute: ActivatedRoute
  ) {
    super();
    this.type = VIEW_TYPES.user;
    this.search = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this._createForm();
    this._setDefaultFilter();
    if (this.showUsers) { this._usersList(); }
    if (this.showPageStates) { this._pageStates(); }
  }

  private _createForm(): void {
    this.frmSearch = this._formBuilder.group(
      {
        mSearch: [''],
        mUsers: [],
        mStates: [],
        mStartDate: [null],
        mEndDate: [null]
      }
    );
    this.mSearch = this.frmSearch.get('mSearch');
    this.mUsers = this.frmSearch.get('mUsers');
    this.mStates = this.frmSearch.get('mStates');
    this.mStartDate = this.frmSearch.get('mStartDate');
    this.mEndDate = this.frmSearch.get('mEndDate');

    if (this.showCalendar) {
      this.mStartDate.setValidators(ValidatorUtil.date(DATE_FORMAT.ddMMyyyy));
      this.mEndDate.setValidators(ValidatorUtil.date(DATE_FORMAT.ddMMyyyy));
      this.frmSearch.setValidators(ValidatorUtil.rangeDate('mStartDate', 'mEndDate'));
    }
  }

  private _setDefaultFilter(): void {
    const params = this._activatedRoute.snapshot.queryParams;

    const valueUser = [].concat(params.userList)
      .filter(username => username)
      .map(username => ({ username }));

    const valueStates = [].concat(params.status)
      .filter(state => state)
      .map(state => ({ codeParameterSystem: Number(state) }));

    this.mSearch.setValue(params.namePage || '');
    this.mUsers.setValue(valueUser);
    this.mStates.setValue(valueStates);
    this.mStartDate.setValue(params.startDate, {});
    this.mEndDate.setValue(params.endDate, {});
  }

  private _usersList(showSpin: boolean = false): void {
    this._userService.usersList(showSpin)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IUserResponse>) => {
        this.usersList = response;
      });
  }

  private _pageStates(showSpin: boolean = false): void {
    this._generalService.states(showSpin)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IStateResponse>) => {
        this.pageStates = response;
      });
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmSearch);

    return this.frmSearch.valid;
  }

  private _params(): ISearchFormView {
    const getList = (list: Array<any>, field: string) => {
      return list.map(fv => {
        return fv[field];
      }) || [];
    };

    return {
      search: this.mSearch.value,
      users: getList(this.mUsers.value, 'username'),
      statePages: getList(this.mStates.value, 'codeParameterSystem'),
      startDate: this._datePipe.transform(this.mStartDate.value, DATE_FORMAT.ddMMyyyy),
      endDate: this._datePipe.transform(this.mEndDate.value, DATE_FORMAT.ddMMyyyy)
    } as ISearchFormView;
  }

  oSearch(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._params();
      this.search.next(params);
    }
  }

  clear(): void {
    this.frmSearch.reset({
      mUsers: [],
      mStates: []
    });
    const params = this._params();
    this.search.next(params);
  }

  getClassSearch(): string {
    let colClass = 2;
    if (!this.showUsers) { colClass += 2; }
    if (!this.showPageStates) { colClass += 2; }
    if (!this.showCalendar) { colClass += 4; }

    return `col-${colClass}`;
  }

}
