import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownCheckboxesModule } from '@bvl-admin/components/dropdown-checkboxes/dropdown-checkboxes.module';
import { BvlButtonModule, BvlDatepickerModule, BvlInputModule } from '@bvl-core/ui/lib/components';
import { SearchFormComponent } from './search-form.component';

@NgModule({
  declarations: [SearchFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BvlInputModule,
    BvlButtonModule,
    DropdownCheckboxesModule,
    BvlDatepickerModule
  ],
  exports: [SearchFormComponent],
  providers: [
    DatePipe
  ]
})
export class SearchFormModule { }
