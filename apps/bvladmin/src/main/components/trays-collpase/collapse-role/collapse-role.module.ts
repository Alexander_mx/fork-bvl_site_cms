import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BvlButtonModule, BvlCardModule, BvlCheckboxModule, BvlInputModule } from '@bvl-core/ui/lib/components';
import { CollapseModule } from '@bvl-core/ui/lib/directives';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { CollapseRoleComponent } from './collapse-role.component';

@NgModule({
  declarations: [CollapseRoleComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BvlCardModule,
    CollapseModule,
    BvlInputModule,
    BvlCheckboxModule,
    BvlButtonModule,
    SweetAlert2Module
  ],
  exports: [CollapseRoleComponent]
})
export class CollapseRoleModule { }
