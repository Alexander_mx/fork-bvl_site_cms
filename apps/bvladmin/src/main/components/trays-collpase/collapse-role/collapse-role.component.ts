import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PermissionService, RoleService } from '@bvl-admin/services';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { TRAY_COLLAPSE_BUTTONS } from '@bvl-admin/settings/constants/role.constants';
import { GeneralLang, RoleLang } from '@bvl-admin/settings/lang';
import { IPermissionResponse } from '@bvl-admin/statemanagement/models/permission.interface';
import { IDeleteRoleRequest, IRolePermissionsDetailView, IUpdateRoleRequest } from '@bvl-admin/statemanagement/models/role.interface';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'admin-collapse-role',
  templateUrl: './collapse-role.component.html',
  styleUrls: ['./collapse-role.component.scss']
})
export class CollapseRoleComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild('swalDelete') private _swalDelete: SwalComponent;

  @Input() role: IRolePermissionsDetailView;
  @Input() disabled: boolean;

  GENERAL_LANG = GeneralLang;
  ROLE_LANG = RoleLang;
  TRAY_COLLAPSE_BUTTONS = TRAY_COLLAPSE_BUTTONS;

  isCollapsed: boolean;
  frmRole: FormGroup;
  roleText: AbstractControl;
  permissionsCheck: FormArray;
  permissionsList: Array<IPermissionResponse>;
  swalDeleteOptions: SweetAlertOptions;

  constructor(
    private _formBuilder: FormBuilder,
    private _permissionService: PermissionService,
    private _roleService: RoleService,
    private _notificationService: NotificationService,
    private _swalConfirmService: SwalConfirmService
  ) {
    super();
    this.isCollapsed = true;
  }

  ngOnInit(): void {
    const title = this.GENERAL_LANG.modalConfirm.titleDelete.replace('{{ customField }}', `el rol ${this.role.roleName}`);
    this.swalDeleteOptions = this._swalConfirmService.warning(title);
    this._createForm();
  }

  private _createForm(): void {
    this.frmRole = this._formBuilder.group({
      roleText: [this.role.roleName, Validators.required],
      permissionsCheck: this._formBuilder.array([])
    });
    this.roleText = this.frmRole.get('roleText');
    this.permissionsCheck = this.frmRole.get('permissionsCheck') as FormArray;
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmRole, controlName);
  }

  private _toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  edit(): void {
    this._toggleCollapse();
    this._permissionService.permissionsList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IPermissionResponse>) => {
        this.permissionsList = response;
        this._createPermissionsCheckControl();
        this._roleService.setRolesList(ACTION_TYPES.edit, this.role);
      });
  }

  private _createPermissionsCheckControl(): void {
    this.permissionsList.forEach((fv, fk) => {
      const checked = this.role.permissionCodeList.find((sv, sk) => {
        return fv.permissionCode === sv.permissionCode;
      });
      this.permissionsCheck.setControl(fk, this._formBuilder.control(!!checked));
    });
  }

  private _paramsDeleteRole(): IDeleteRoleRequest {
    return {
      roleCode: this.role.roleCode
    } as IDeleteRoleRequest;
  }

  delete(): void {
    const params = this._paramsDeleteRole();
    this._roleService.deleteRole(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this._roleService.setRolesList(ACTION_TYPES.delete, this.role);
      });
  }

  cancel(): void {
    this._toggleCollapse();
    this._roleService.setRolesList(ACTION_TYPES.cancel, this.role);
  }

  private _getPermissionsList(): Array<IPermissionResponse> {
    const permissionsList = [];
    this.permissionsCheck.controls.forEach((fv, fk) => {
      if (fv.value) { permissionsList.push(this.permissionsList[fk]); }
    });

    return permissionsList;
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmRole);

    return this.frmRole.valid;
  }

  private _paramsUpdateRole(): IUpdateRoleRequest {
    const getPermissionsCode = () => {
      return this._getPermissionsList()
              .map((fv, fk) => {
                return fv.permissionCode;
              });
    };

    return {
      roleCode: this.role.roleCode,
      roleName: this.roleText.value,
      permissionCodeList: getPermissionsCode()
    } as IUpdateRoleRequest;
  }

  save(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._paramsUpdateRole();
      this._roleService.updateRole(params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this._toggleCollapse();
          this._setRole();
        });
    } else {
      this._notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  private _setRole(): void {
    this.role.roleName = this.roleText.value;
    this.role.permissionCodeList = this._getPermissionsList();
    this._roleService.setRolesList(ACTION_TYPES.cancel, this.role);
  }

  buttonAction(actionButton: any): void {
    switch (actionButton.actionType) {
      case ACTION_TYPES.edit:
        this.edit();
        break;
      case ACTION_TYPES.delete:
        this._swalDelete.show();
        break;
      case ACTION_TYPES.cancel:
        this.cancel();
        break;
      case ACTION_TYPES.save:
        this.save();
        break;
      default:
        break;
    }
  }

}
