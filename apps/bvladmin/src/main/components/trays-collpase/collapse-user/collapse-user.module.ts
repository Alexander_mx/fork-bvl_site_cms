import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateRoleFormModule } from '@bvl-admin/components/modals/create-role-form/create-role-form.module';
import { BvlButtonModule, BvlCardModule, BvlLinkModule, BvlRadioButtonModule } from '@bvl-core/ui/lib/components';
import { CollapseModule } from '@bvl-core/ui/lib/directives';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { CollapseUserComponent } from './collapse-user.component';

@NgModule({
  declarations: [CollapseUserComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BvlCardModule,
    CollapseModule,
    BvlRadioButtonModule,
    BvlLinkModule,
    BvlButtonModule,
    SweetAlert2Module,
    CreateRoleFormModule
  ],
  exports: [CollapseUserComponent]
})
export class CollapseUserModule { }
