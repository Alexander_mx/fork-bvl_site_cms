import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreateRoleFormComponent } from '@bvl-admin/components/modals/create-role-form/create-role-form.component';
import { RoleService, UserService } from '@bvl-admin/services';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { TRAY_COLLAPSE_BUTTONS } from '@bvl-admin/settings/constants/user.constant';
import { GeneralLang, UserLang } from '@bvl-admin/settings/lang';
import { IRoleResponse } from '@bvl-admin/statemanagement/models/role.interface';
import { IDeleteUserRequest, ISaveUserRequest, IUserView } from '@bvl-admin/statemanagement/models/user.interface';
import { ModalService } from '@bvl-core/shared/helpers/modal';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { takeUntil, tap } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'admin-collapse-user',
  templateUrl: './collapse-user.component.html',
  styleUrls: ['./collapse-user.component.scss']
})
export class CollapseUserComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild('swalDelete') private _swalDelete: SwalComponent;

  @Input() user: IUserView;
  @Input() disabled: boolean;

  GENERAL_LANG = GeneralLang;
  USER_LANG = UserLang;
  TRAY_COLLAPSE_BUTTONS = TRAY_COLLAPSE_BUTTONS;

  isCollapsed: boolean;
  frmUser: FormGroup;
  rolesList: Array<IRoleResponse>;
  radioRoles: AbstractControl;
  swalDeleteOptions: SweetAlertOptions;

  constructor(
    private _formBuilder: FormBuilder,
    private _roleService: RoleService,
    private _userService: UserService,
    private _modalService: ModalService,
    private _notificationService: NotificationService,
    private _swalConfirmService: SwalConfirmService
  ) {
    super();
    this.isCollapsed = true;
  }

  ngOnInit(): void {
    const title = this.GENERAL_LANG.modalConfirm.titleDelete.replace('{{ customField }}', `el usuario ${this.user.username}`);
    this.swalDeleteOptions = this._swalConfirmService.warning(title);
    this._createForm();
  }

  private _createForm(): void {
    this.frmUser = this._formBuilder.group({
      radioRoles: [null, Validators.required]
    });
    this.radioRoles = this.frmUser.get('radioRoles');
  }

  private _toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  edit(): void {
    this._toggleCollapse();
    this._roleService.rolesList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        tap((preResponse: Array<IRoleResponse>) => {
          const role = preResponse.find((value: IRoleResponse) => {
            return value.roleCode === this.user.role.roleCode;
          });
          this.radioRoles.setValue(role);
        })
      )
      .subscribe((response: Array<IRoleResponse>) => {
        this.rolesList = response;
        this._userService.setUsersList(ACTION_TYPES.edit, this.user);
      });
  }

  private _paramsDeleteUser(): IDeleteUserRequest {
    return {
      username: this.user.username
    } as IDeleteUserRequest;
  }

  delete(): void {
    const params = this._paramsDeleteUser();
    this._userService.deleteUser(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this._userService.setUsersList(ACTION_TYPES.delete, this.user);
      });
  }

  cancel(): void {
    this._toggleCollapse();
    this._userService.setUsersList(ACTION_TYPES.cancel, this.user);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmUser);

    return this.frmUser.valid;
  }

  private _paramsSaveUser(): ISaveUserRequest {
    return {
      username: this.user.username,
      fullname: this.user.fullname,
      roleCode: this.radioRoles.value.roleCode
    } as ISaveUserRequest;
  }

  save(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._paramsSaveUser();
      this._userService.updateUser(params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this._toggleCollapse();
          this.user.role = this.radioRoles.value;
          this._userService.setUsersList(ACTION_TYPES.cancel, this.user);
        });
    } else {
      this._notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  goCreateRole(): void {
    this._modalService.open(CreateRoleFormComponent).result
      .then((response: string) => {
        if (response === ACTION_TYPES.save) {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.edit();
        }
      }, () => {
        return false;
      });
  }

  buttonAction(actionButton: any): void {
    switch (actionButton.actionType) {
      case ACTION_TYPES.edit:
        this.edit();
        break;
      case ACTION_TYPES.delete:
        this._swalDelete.show();
        break;
      case ACTION_TYPES.cancel:
        this.cancel();
        break;
      case ACTION_TYPES.save:
        this.save();
        break;
      default:
        break;
    }
  }

}
