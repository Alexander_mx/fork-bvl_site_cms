import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BvlAlertErrorModule, BvlButtonModule, BvlInputModule, BvlSwitchModule } from '@bvl-core/ui/lib/components';
import { LoginFormComponent } from './login-form.component';

@NgModule({
  declarations: [LoginFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BvlInputModule,
    BvlSwitchModule,
    BvlButtonModule,
    BvlAlertErrorModule
  ],
  exports: [LoginFormComponent]
})
export class LoginFormModule { }
