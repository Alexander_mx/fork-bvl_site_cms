import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlButtonModule, BvlInputModule, BvlRadioButtonModule } from '@bvl-core/ui/lib/components';
import { BvlSelectModule } from '@bvl-core/ui/lib/components/select';
import { RobotsFormComponent } from './robots-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ModalModule,
    BvlSelectModule,
    BvlRadioButtonModule,
    BvlInputModule,
    BvlButtonModule
  ],
  declarations: [RobotsFormComponent],
  entryComponents: [RobotsFormComponent]
})
export class RobotsFormModule { }
