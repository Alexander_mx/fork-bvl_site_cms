import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { RobotsService } from '@bvl-admin/services/robots.service';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { TYPE_ROBOT_ALLOW, TYPE_ROBOT_DISALLOW, TYPE_ROBOT_SITEMAP, TYPES_VALUES_ROBOTS } from '@bvl-admin/settings/constants/seo.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IListUserAgent, IRobotsRequest } from '@bvl-admin/statemanagement/models/robots.interface';
import { ActiveModal } from '@bvl-core/shared/helpers/modal';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-robots-form',
  templateUrl: './robots-form.component.html'
})
export class RobotsFormComponent extends UnsubscribeOnDestroy implements OnInit {
  GENERAL_LANG = GeneralLang;
  listUserAgent: IListUserAgent = [];
  listTypes = TYPES_VALUES_ROBOTS;

  frmRobots: FormGroup;
  userAgent: AbstractControl;
  type: AbstractControl;
  value: AbstractControl;

  constructor(
    private fb: FormBuilder,
    private activeModal: ActiveModal,
    private robotsService: RobotsService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getUserAgent();
    this.createForm();
    this.saveConfig();
  }

  getUserAgent(): void {
    this.robotsService.getUserAgent()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IListUserAgent) => {
        this.listUserAgent = response;
      });
  }

  private createForm(): void {
    this.frmRobots = this.fb.group({
      userAgent: [''],
      type: [TYPE_ROBOT_ALLOW],
      value: [null]
    });

    this.userAgent = this.frmRobots.get('userAgent');
    this.type = this.frmRobots.get('type');
    this.value = this.frmRobots.get('value');
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmRobots);

    return this.frmRobots.valid;
  }

  private paramsSaveUser(): IRobotsRequest {
    return {
      userAgent: (this.userAgent.value || {}).code || '',
      type: this.type.value,
      value: this.value.value
    } as IRobotsRequest;
  }

  getPlaceholder(): string {
    let message = '';
    switch (this.type.value) {
      case TYPE_ROBOT_SITEMAP:
        message = 'Ingrese el nombre del mapa del sitio (sitemap.xml)';
        break;
      case TYPE_ROBOT_ALLOW:
        message = 'Ingrese la URL permitida';
        break;
      case TYPE_ROBOT_DISALLOW:
        message = 'Ingrese la URL no permitida';
        break;
      default:
        break;
    }

    return message;
  }

  isSiteMap(): boolean {
    return this.type.value === TYPE_ROBOT_SITEMAP;
  }

  changeType(): void {
    if (this.isSiteMap()) {
      this.userAgent.setValue('');
    }
  }

  save(): void {
    const validateForm = this.type.value === TYPE_ROBOT_SITEMAP ? (!!this.value.value) : (!!this.value.value && !!this.userAgent.value);
    if (validateForm) {
      const params = this.paramsSaveUser();
      this.robotsService.addRobots(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.activeModal.close(ACTION_TYPES.save);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  private saveConfig(): void {
    this.activeModal.confirm()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this.save();
      });
  }
}
