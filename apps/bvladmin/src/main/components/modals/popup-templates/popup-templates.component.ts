import { Component, OnInit } from '@angular/core';
import { ComponentService } from '@bvl-admin/services';
import { LANGUAGE_TYPES, TYPES_CODE } from '@bvl-admin/settings/constants/general.constant';
import { IComponentResponse, IComponentValueResponse } from '@bvl-admin/statemanagement/models/component.interface';
import { ActiveModal } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-popup-templates',
  templateUrl: './popup-templates.component.html',
  styleUrls: ['./popup-templates.component.scss']
})
export class PopupTemplatesComponent extends UnsubscribeOnDestroy implements OnInit {

  components: Array<IComponentResponse>;

  constructor(
    private _componentService: ComponentService,
    private _activeModal: ActiveModal
  ) {
    super();
  }

  ngOnInit(): void {
    this._componentList();
  }

  private _componentList(): void {
    this._componentService.componentListByType(TYPES_CODE.popup, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IComponentResponse>) => {
        this.components = response;
      });
  }

  private _createComponentValue(component: IComponentResponse, showSpin: boolean = false): Observable<any> {
    const params = {
      code: component.code,
      name: '',
      languages: LANGUAGE_TYPES.map(language => {
        return {
          language: language.code,
          properties: []
        };
      }),
      lastUpdate: ''
    };

    return this._componentService.saveComponentValue(TYPES_CODE.popup, params, showSpin);
  }

  componentSelect(component: IComponentResponse): void {
    this._createComponentValue(component, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IComponentValueResponse) => {
        this._activeModal.close(response.id);
      });
  }

}
