import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PopupTemplatesComponent } from './popup-templates.component';

@NgModule({
  declarations: [PopupTemplatesComponent],
  imports: [
    CommonModule
  ],
  exports: [PopupTemplatesComponent],
  entryComponents: [PopupTemplatesComponent]
})
export class PopupTemplatesModule { }
