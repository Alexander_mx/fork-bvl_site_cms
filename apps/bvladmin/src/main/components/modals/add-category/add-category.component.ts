import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoriesService } from '@bvl-admin/services/categories.services';
import { ActiveModal, IPayloadModalComponent } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {

  modalPayload: any;
  formCategory: FormGroup;
  nameMaxLength: number;

  constructor(
    private formBuild: FormBuilder,
    private _activeModal: ActiveModal,
    private _categoriesService: CategoriesService) {
    super();
    this.nameMaxLength = 35;
  }

  ngOnInit(): void {
    this.formCategory = this._setForm();
    if (this.modalPayload.category) {
      this._setCategory(this.modalPayload.category);
    }
    this._submitListenerd();
  }

  private _submitListenerd(): void {
    this._activeModal.confirm()
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe(() => {
      this._actionToCategory();
    });
  }
  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.formCategory);

    return this.formCategory.valid;
  }

  private _actionToCategory(): void {

    if (this.modalPayload.action === 'delete') {
      this._categoriesService.deleteCategory({ id: this.modalPayload.category.id })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => this._activeModal.close(this.modalPayload.delete));
    } else {
      if (this._validateForm()) {
        this._categoriesService.saveCategory({
          id: this.modalPayload.category
            ? this.modalPayload.category.id
            : null,
          languages: [
            {
              category: this.formCategory.value.esName,
              language: 'es',
              path: this.formCategory.value.esPath
            },
            {
              category: this.formCategory.value.enName,
              language: 'en',
              path: this.formCategory.value.enPath
            }
          ]
        })
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(response => {
          this._activeModal.close(this.modalPayload.action);
        });
      }
    }
  }
  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.formCategory, controlName);
  }

  private _setCategory(category): void {
    const findCategory: any = language => {
      return category.languages.find(item => item.language === language);
    };
    this.formCategory.controls.esName.setValue(findCategory('es').category);
    this.formCategory.controls.enName.setValue(findCategory('en').category);
    this.formCategory.controls.esPath.setValue(findCategory('es').path);
    this.formCategory.controls.enPath.setValue(findCategory('en').path);
  }
  private _setForm(): FormGroup {
    return this.formBuild.group({
      enName: [null, [Validators.required, Validators.maxLength(this.nameMaxLength)]],
      esName: [null, [Validators.required, Validators.maxLength(this.nameMaxLength)]],
      enPath: [null],
      esPath: [null]
    });
  }
}
