import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BvlInputModule } from '@bvl-core/ui/lib/components';
import { AddCategoryComponent } from './add-category.component';

@NgModule({
  declarations: [AddCategoryComponent],
  imports: [
    CommonModule,
    BvlInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [ AddCategoryComponent ],
  exports: [AddCategoryComponent]
})
export class AddCategoryModule { }
