import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TemplatePreviewComponent } from './template-preview.component';

@NgModule({
  declarations: [TemplatePreviewComponent],
  imports: [
    CommonModule
  ],
  exports: [TemplatePreviewComponent],
  entryComponents: [TemplatePreviewComponent]
})
export class TemplatePreviewModule { }
