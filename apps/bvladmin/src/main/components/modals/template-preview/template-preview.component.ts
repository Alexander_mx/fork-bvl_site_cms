import { Component, OnInit } from '@angular/core';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { ITemplateResponse } from '@bvl-admin/statemanagement/models/template.interface';
import { ActiveModal, IPayloadModalComponent } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-template-preview',
  templateUrl: './template-preview.component.html',
  styleUrls: ['./template-preview.component.scss']
})
export class TemplatePreviewComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {

  GENERAL_LANG = GeneralLang;
  ACTION_TYPES = ACTION_TYPES;

  modalPayload: ITemplateResponse;

  constructor(
    private _activeModal: ActiveModal
  ) {
    super();
  }

  ngOnInit(): void {
    this._select();
  }

  private _select(): void {
    this._activeModal.confirm()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._activeModal.close();
      });
  }

}
