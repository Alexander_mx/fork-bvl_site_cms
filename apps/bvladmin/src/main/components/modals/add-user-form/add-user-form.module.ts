import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BvlAutocompleteModule, BvlCardModule, BvlRadioButtonModule } from '@bvl-core/ui/lib/components';
import { AddUserFormComponent } from './add-user-form.component';

@NgModule({
  declarations: [AddUserFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BvlAutocompleteModule,
    BvlCardModule,
    BvlRadioButtonModule
  ],
  exports: [AddUserFormComponent],
  entryComponents: [AddUserFormComponent]
})
export class AddUserFormModule { }
