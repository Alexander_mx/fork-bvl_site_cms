import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoleService, UserService } from '@bvl-admin/services';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang, UserLang } from '@bvl-admin/settings/lang';
import { IRolePermissionsDetailResponse } from '@bvl-admin/statemanagement/models/role.interface';
import { IBvlUserResponse, ISaveUserRequest } from '@bvl-admin/statemanagement/models/user.interface';
import { ActiveModal } from '@bvl-core/shared/helpers/modal';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-add-user-form',
  templateUrl: './add-user-form.component.html',
  styleUrls: ['./add-user-form.component.scss']
})
export class AddUserFormComponent extends UnsubscribeOnDestroy implements OnInit {

  USER_LANG = UserLang;
  GENERAL_LANG = GeneralLang;

  frmAddUser: FormGroup;
  user: AbstractControl;
  radioRoles: AbstractControl;
  usersList: Array<IBvlUserResponse>;
  rolesList: Array<IRolePermissionsDetailResponse>;

  constructor(
    private _formBuilder: FormBuilder,
    private _userService: UserService,
    private _roleService: RoleService,
    private _notificationService: NotificationService,
    private _activeModal: ActiveModal
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
    this._usersList();
    this._rolesList();
    this._save();
  }

  private _createForm(): void {
    this.frmAddUser = this._formBuilder.group({
      user: [null, Validators.required],
      radioRoles: [null, Validators.required]
    });
    this.user = this.frmAddUser.get('user');
    this.radioRoles = this.frmAddUser.get('radioRoles');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmAddUser, controlName);
  }

  private _usersList(): void {
    this._userService.bvlUsersList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IBvlUserResponse>) => {
        this.usersList = response;
      });
  }

  private _rolesList(): void {
    this._roleService.rolesListPermissionsDetail()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IRolePermissionsDetailResponse>) => {
        this.rolesList = response;
      });
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmAddUser);

    return this.frmAddUser.valid;
  }

  private _paramsSaveUser(): ISaveUserRequest {
    return {
      username: this.user.value.username,
      fullname: this.user.value.fullname,
      roleCode: this.radioRoles.value.roleCode
    } as ISaveUserRequest;
  }

  save(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._paramsSaveUser();
      this._userService.saveUser(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._activeModal.close(ACTION_TYPES.save);
      });
    } else {
      this._notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  private _save(): void {
    this._activeModal.confirm()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this.save();
      });
  }

}
