import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { BvlLinkModule } from '@bvl-core/ui/lib/components';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { PageHistoryComponent } from './page-history.component';

@NgModule({
  declarations: [PageHistoryComponent],
  imports: [
    CommonModule,
    SweetAlert2Module,
    BvlLinkModule
  ],
  exports: [PageHistoryComponent],
  entryComponents: [PageHistoryComponent],
  providers: [
    DatePipe
  ]
})
export class PageHistoryModule { }
