import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { PageService } from '@bvl-admin/services';
import { BVL, DATE_FORMAT } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IPageResponse } from '@bvl-admin/statemanagement/models/page.interface';
import { ActiveModal, IPayloadModalComponent } from '@bvl-core/shared/helpers/modal';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'admin-page-history',
  templateUrl: './page-history.component.html',
  styleUrls: ['./page-history.component.scss']
})
export class PageHistoryComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {

  DATE_FORMAT = DATE_FORMAT;
  GENERAL_LANG = GeneralLang;

  modalPayload: string;
  pageHisotry: Array<IPageResponse>;

  constructor(
    private _pageService: PageService,
    private _activeModal: ActiveModal,
    private _swalConfirmService: SwalConfirmService,
    private _datePipe: DatePipe
  ) {
    super();
  }

  ngOnInit(): void {
    this._pageHistory(this.modalPayload);
  }

  private _pageHistory(idPage: string): void {
    this._pageService.pageHistory(idPage)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IPageResponse>) => {
        this.pageHisotry = response;
      });
  }

  see(page: IPageResponse): void {
    const pageHistory = `${BVL.urlSite}history?id=${page.id}`;
    window.open(pageHistory, '_blank');
  }

  restoreSwal(page: IPageResponse): SweetAlertOptions {
    const publishingDate = this._datePipe.transform(page.publishingDate, this.DATE_FORMAT.ddMMyyyy);

    return this._swalConfirmService.warning(`¿Desea restaurar la publicación del ${publishingDate} ?`);
  }

  restore(page: IPageResponse): void {
    this._pageService.restorePageHisotry(page.id, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._activeModal.close();
      });
  }

}
