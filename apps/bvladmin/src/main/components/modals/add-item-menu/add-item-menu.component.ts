import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageService, SiteMenuService } from '@bvl-admin/services';
import { SITE_MENU_ACTIONS } from '@bvl-admin/settings/constants/site-menu.constant';
import { IPageAutocomplete } from '@bvl-admin/statemanagement/models/page.interface';
import { ActiveModal, IPayloadModalComponent } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { getItemMenu, ISiteMenu, SITE_MENU_TYPE } from '@bvl-core/shared/helpers/util/menu';
import { LANGUAGE_TYPES_CODE } from '@bvl-core/shared/settings/constants/general.constants';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-add-item-menu',
  templateUrl: './add-item-menu.component.html',
  styleUrls: ['./add-item-menu.component.scss']
})
export class AddItemMenuComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {

  SITE_MENU_ACTIONS = SITE_MENU_ACTIONS;

  modalPayload: {
    type: SITE_MENU_TYPE,
    parent?: ISiteMenu,
    item?: ISiteMenu,
    language: LANGUAGE_TYPES_CODE,
    order: number
  };
  frmAddItemMenu: FormGroup;
  pagesList: Array<IPageAutocomplete>;
  nameMaxLength: number;

  constructor(
    private _formBuilder: FormBuilder,
    private _activeModal: ActiveModal,
    private _pageService: PageService,
    private _siteMenuService: SiteMenuService
  ) {
    super();
    this.nameMaxLength = 35;
  }

  ngOnInit(): void {
    this._createForm();
    this._getPages();
    this._save();
  }

  private _createForm(): void {
    this.frmAddItemMenu = this._formBuilder.group({
      mActions: [(this.modalPayload.item)
                  ? SITE_MENU_ACTIONS.find(action => action.code === this.modalPayload.item.action)
                  : ''],
      mPage:    [(this.modalPayload.item
                  ? { idPage: this.modalPayload.item.idPage, path: this.modalPayload.item.path }
                  : '')],
      mName:    [(this.modalPayload.item
                  ? this.modalPayload.item.name
                  : ''),
                 [Validators.required, Validators.maxLength(this.nameMaxLength)]],
      mVisible: this.modalPayload.item
                  ? this.modalPayload.item.flgVisible
                  : true
    });

    this.frmAddItemMenu.get('mPage')
    .valueChanges
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe(value => {
      if (typeof value === 'string') {
        this._getPages(value);
      }
    });
  }

  private _getPages(value: string = null): void {
    this._pageService.getPagesAutocopleteByPath(value || '')
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IPageAutocomplete>) => {
        this.pagesList = response;
      });
  }

  private _save(): void {
    this._activeModal.confirm()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this.save();
      });
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmAddItemMenu, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmAddItemMenu);

    return this.frmAddItemMenu.valid;
  }

  private _params(): ISiteMenu {
    const formValue = this.frmAddItemMenu.getRawValue();

    return {
      id:         (this.modalPayload.item)
                    ? this.modalPayload.item.id
                    : null,
      idPage:     (formValue.mPage)
                    ? formValue.mPage.idPage
                    : '',
      parentId:   (this.modalPayload.parent)
                    ? this.modalPayload.parent.id
                    : '',
      name:       formValue.mName,
      path:       (formValue.mPage)
                    ? formValue.mPage.path
                    : '',
      language:   this.modalPayload.language,
      type:       this.modalPayload.type,
      action:     (formValue.mActions)
                    ? formValue.mActions.code
                    : '',
      order:      this.modalPayload.order,
      flgVisible: formValue.mVisible
    } as ISiteMenu;
  }

  save(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._params(),
            fn = this.modalPayload.item
                        ? 'updateMenu'
                        : 'addMenu';
      this._siteMenuService[fn](params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: ISiteMenu) => {
          const item = (response)
                        ? getItemMenu(response)
                        : params;
          this._activeModal.close(item);
        });
    }
  }

}
