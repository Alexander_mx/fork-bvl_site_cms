import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BvlAutocompleteModule, BvlCheckboxModule, BvlInputModule, BvlSelectModule } from '@bvl-core/ui/lib/components';
import { AddItemMenuComponent } from './add-item-menu.component';

@NgModule({
  declarations: [AddItemMenuComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BvlAutocompleteModule,
    BvlInputModule,
    BvlSelectModule,
    BvlCheckboxModule
  ],
  exports: [AddItemMenuComponent],
  entryComponents: [AddItemMenuComponent]
})
export class AddItemMenuModule { }
