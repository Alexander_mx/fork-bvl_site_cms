import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { QuillModule } from 'ngx-quill';

import { PipesModule, SafeUrlPipeModule } from '@bvl-core/shared/helpers/pipes';
import { BvlAccordionModule, BvlAutocompleteModule, BvlCheckboxModule, BvlDatepickerModule } from '@bvl-core/ui/lib/components';
import { BvlButtonModule } from '@bvl-core/ui/lib/components/button';
import { BvlInputModule } from '@bvl-core/ui/lib/components/input';
import { GallerySelectModule } from '../gallery-select/gallery-select.module';
import { FormAlignComponent } from './components/form-align/form-align.component';
import { FormAutocompleteComponent } from './components/form-autocomplete/form-autocomplete.component';
import { FormCheckboxComponent } from './components/form-checkbox/form-checkbox.component';
import { FormFileComponent } from './components/form-file/form-file.component';
import { FormImageComponent } from './components/form-image/form-image.component';
import { FormInputFileComponent } from './components/form-input-file/form-input-file.component';
import { FormInputComponent } from './components/form-input/form-input.component';
import { FormRichtextComponent } from './components/form-richtext/form-richtext.component';
import { FormSelectComponent } from './components/form-select/form-select.component';
import { FormStaticComponent } from './components/form-static/form-static.component';
import { FormTableComponent } from './components/form-table/form-table.component';
import { FormTextareaComponent } from './components/form-textarea/form-textarea.component';
import { DynamicFormComponent } from './containers/dynamic-form/dynamic-form.component';
import { DynamicFieldDirective } from './directives/field.directive';
import { DynamicUiDirective } from './directives/ui.directive';
import { ActiveSlidePipe } from './pipes/active-slide.pipe';
import { CustomFormatPipe } from './pipes/custom-format.pipe';

@NgModule({
  imports: [
    BvlAccordionModule,
    BvlAutocompleteModule,
    BvlButtonModule,
    BvlCheckboxModule,
    BvlDatepickerModule,
    BvlInputModule,
    CommonModule,
    DragDropModule,
    GallerySelectModule,
    PipesModule,
    QuillModule.forRoot(),
    ReactiveFormsModule,
    SafeUrlPipeModule,
    SweetAlert2Module
  ],
  declarations: [
    DynamicFieldDirective,
    DynamicUiDirective,
    DynamicFormComponent,

    FormAlignComponent,
    FormAutocompleteComponent,
    FormCheckboxComponent,
    FormFileComponent,
    FormImageComponent,
    FormInputComponent,
    FormInputFileComponent,
    FormRichtextComponent,
    FormSelectComponent,
    FormStaticComponent,
    FormTableComponent,
    FormTextareaComponent,

    ActiveSlidePipe,
    CustomFormatPipe
  ],
  entryComponents: [
    FormAlignComponent,
    FormAutocompleteComponent,
    FormCheckboxComponent,
    FormFileComponent,
    FormImageComponent,
    FormInputComponent,
    FormInputFileComponent,
    FormRichtextComponent,
    FormSelectComponent,
    FormStaticComponent,
    FormTableComponent,
    FormTextareaComponent
  ],
  exports: [DynamicFormComponent]
})
export class DynamicFormModule {}
