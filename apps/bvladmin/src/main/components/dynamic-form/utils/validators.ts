import { ValidatorFn, Validators } from '@angular/forms';
import { IProperty } from '../model';

export class DynamicFormUtils {

  static getValidator(prop: IProperty): Array<ValidatorFn> {
    const validators = [];

    if (prop.required) { validators.push(Validators.required); }
    if (prop.maxLength) { validators.push(Validators.maxLength(prop.maxLength)); }
    if (prop.minLength) { validators.push(Validators.minLength(prop.minLength)); }

    return validators;
  }
}

export const REGEX = {
  // tslint:disable-next-line:max-line-length
  URL: /^(?![^\n]*\.$)(?:https?:\/\/)?(?:(?:[2][1-4]\d|25[1-5]|1\d{2}|[1-9]\d|[1-9])(?:\.(?:[2][1-4]\d|25[1-5]|1\d{2}|[1-9]\d|[0-9])){3}(?::\d{4})?|[a-z\-]+(?:\.[a-z\-]+){2,})$/
};
