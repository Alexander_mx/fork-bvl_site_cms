import { Component } from '@angular/core';
import { take, takeUntil } from 'rxjs/operators';

import { FormBuilder, FormControl } from '@angular/forms';
import { environment } from '@bvl-admin/environments/environment';
import { ApiService } from '@bvl-core/shared/helpers/api';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';
import { IAutocompleteItemResponse } from '../../model';

@Component({
  selector: 'admin-form-autocomplete',
  template: `
    <div class="row align-items-center no-gutters mb-xs-2" [formGroup]="group">
      <div class="col-auto g-w-100">
        <label [for]="config.label" class="g-fw-bold g-fs-15">{{ config.label }}:</label>
      </div>
      <div class="col">
        <bvl-autocomplete
          *ngIf="controlAutocomplete"
          [dataList]="dataList"
          [error]="validateControl()"
          [formControl]="controlAutocomplete"
          [placeholder]="config.placeholder"
          matchField="text"
          textField="text"
        >
        </bvl-autocomplete>
      </div>
    </div>
  `
})
export class FormAutocompleteComponent extends DynamicFormComponentBase {
  dataList: Array<IAutocompleteItemResponse> = [];
  controlAutocomplete: FormControl;
  endpoint: string;
  constructor(private _apiService: ApiService, private _formBuilder: FormBuilder) {
    super();
  }

  bvlOnInit(): void {
    this._initData();
  }

  private _getData(data: any): void {
    if (typeof data !== 'string') {
      this.control.setValue(data && data.value);

      return void 0;
    }

    const params = {
      ...this.config.queryParams,
      name: data
    };
    this._apiService
      .get(this.endpoint, { params })
      .pipe(take(1))
      .subscribe((response: Array<IAutocompleteItemResponse>) => {
        this.dataList = response;
      });
  }

  private _initData(): void {
    this.endpoint = `${environment.API_URL}/${this.config.endpoint}`;
    this.controlAutocomplete = this._formBuilder.control('');
    this.controlAutocomplete.valueChanges.pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(this._getData.bind(this));

    if (!this.control.value) {
      return void 0;
    }

    const params = {
      ...this.config.queryParams,
      name: '',
      id: this.control.value
    };

    this._apiService
      .get(this.endpoint, { params })
      .pipe(take(1))
      .subscribe((response: Array<IAutocompleteItemResponse>) => {
        this.controlAutocomplete.setValue(response && response.length && response[0]);
      });
  }
}
