import { Component } from '@angular/core';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'admin-form-textarea',
  templateUrl: './form-textarea.component.html'
})
export class FormTextareaComponent extends DynamicFormComponentBase { }
