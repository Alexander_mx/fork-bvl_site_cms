import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'admin-form-table',
  templateUrl: './form-table.component.html',
  styleUrls: ['./form-table.component.scss']
})
export class FormTableComponent extends DynamicFormComponentBase {

  private _data: {
    head: Array<string>,
    body: Array<Array<string>>
  };

  form: FormGroup;
  head: FormArray;
  body: FormArray;

  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  bvlOnInit(): void {
    this._createForm();
    this._onChange();
  }

  private _createForm(): void {
    this._data = this.config.value || { head: [], body: [] };

    this._data.head = this._data.head || [];
    this._data.body = this._data.body || [];

    this.form = this.fb.group({
      head: this.fb.array([]),
      body: this.fb.array([])
    });

    this.head = this.form.get('head') as FormArray;
    this.body = this.form.get('body') as FormArray;

    this._data.head.forEach(head => {
      this.head.push(this.fb.control(head));
    });

    this._data.body.forEach(row => {
      row = row || [];
      const array = this.fb.array([]);

      row.forEach(col => {
        array.push(this.fb.control(col));
      });

      this.body.push(array);
    });
  }

  private _onChange(): void {
    this.form.valueChanges
      .subscribe(() => {
        this.control.setValue(this.form.value);
      });
  }

  onDrop(items: Array<any>, event: CdkDragDrop<Array<string>>): void {
    moveItemInArray(items, event.previousIndex, event.currentIndex);
    const tmpPrevious = { ...items[event.previousIndex] };
    const tmpCurrent = { ...items[event.currentIndex] };

    items[event.previousIndex].setValue(tmpPrevious.value);
    items[event.currentIndex].setValue(tmpCurrent.value);
  }

  addItemRow(): void {
    const row = this.fb.array([]);
    this.head.controls.forEach(col => {
      row.push(this.fb.control(''));
    });

    this.body.push(row);
  }

  addItemCol(): void {
    this.head.push(this.fb.control(''));
    this.body.controls.forEach((row: FormArray) => {
      row.push(this.fb.control(''));
    });
  }

  getColName(index: number): string {
    return this.head.controls[index].value || `Col ${index + 1}`;
  }

  removeRow(index: number): void {
    this.body.removeAt(index);
  }

  removeCol(index: number): void {
    this.body.controls.forEach((row: FormArray) => {
      row.removeAt(index);
    });
    this.head.removeAt(index);
  }

}
