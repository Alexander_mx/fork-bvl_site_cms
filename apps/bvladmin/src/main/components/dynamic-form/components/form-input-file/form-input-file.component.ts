import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ValidatorUtil } from '@bvl-core/shared/helpers/util/validator';
import { HTMLInputEvent, IProperty } from './../../model/dynamic-form.interface';

@Component({
  selector: 'admin-form-input-file',
  template: `
    <div
      class="dynamic-field form-input"
      [formGroup]="group">
      <label><span class="g-fw-bold">{{ config.label }}</span> {{ config.description }}</label>
      <div class="row">
        <div class="column" *ngFor="let img of imagesDetailt; let i = index">
          <img
            class="img-card"
            [title]="img.name"
            [src]="img.src | safeUrl"
            [alt]="img.name"
            (load)="getSizeImg($event, i)"/>
          <span class="label">{{ img.name }}</span>
          <p>{{ img.size | fileSize }}</p>
          <p>{{ img.width }} x {{ img.height }} px</p>
        </div>
      </div>
      <bvl-input
        type="file"
        [accept]="acceptsFile"
        [multipled]="multipled"
        [placeholder]="config.placeholder || ''"
        [formControlName]="config.code"
        [error]="validateControl()"
        (onchange)="readURL($event)">
      </bvl-input>
    </div>
  `,
  styles: [`
    img.img-card {
      width: 100px;
      height: 100px;
    }

    span.label {
      padding: 8px;
      display: block;
      overflow: hidden;
      text-overflow: ellipsis;
      text-align: center;
      max-width: 100px;
      font-size: 14px;
    }

    .row {
      overflow-y: overlay !important;
      flex-wrap: initial;
    }

    .column {
      float: left;
      width: auto;
      padding: 10px;
      text-align: center;
      cursor: pointer;
    }

    p {
      font-size: 12px;
    }

  `]
})
export class FormInputFileComponent implements OnInit {

  config: IProperty;
  group: FormGroup;

  acceptsFile: string;
  multipled: boolean;
  imagesDetailt: Array<IFileDetail> = [];

  ngOnInit(): void {
    if (this.config) {
      this.acceptsFile = (this.config.mimeTypes || []).join(', ');
      this.multipled = this.config.max > 1;
    }
  }

  readURL(event: HTMLInputEvent): void {
    this.imagesDetailt = [];
    const _URL = window.URL || (window as any).webkitURL;
    Array.from(event.target.files)
      .forEach((file, index) => {
        this.imagesDetailt[index] = this.imagesDetailt[index] || {} as any;
        this.imagesDetailt[index].name = file.name;
        this.imagesDetailt[index].size = file.size;

        if (file.type === 'application/pdf') {
          this.imagesDetailt[index].src = CImgIcons.PDF;

          return;
        }

        this.imagesDetailt[index].src = _URL.createObjectURL(file);
      });
  }

  getSizeImg(event: any, index: number): void {
    this.imagesDetailt[index].width = event.target.naturalWidth;
    this.imagesDetailt[index].height = event.target.naturalHeight;
  }

  validateControl(): boolean {
    return ValidatorUtil.validateControl(this.group, this.config.code);
  }

}

export const CImgIcons = {
  PDF: 'https://www.zamzar.com/images/filetypes/pdf.png'
};

export interface IFileDetail {
  src: string | ArrayBuffer;
  name: string;
  size: number;
  width: number;
  height: number;
}
