import { Component } from '@angular/core';
import { QuillModules } from 'ngx-quill';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'admin-form-richtext',
  templateUrl: './form-richtext.component.html'
})
export class FormRichtextComponent extends DynamicFormComponentBase {

  modules: QuillModules;

  constructor() {
    super();
    this.loadToolbar();
  }

  loadToolbar(): void {
    this.modules = {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],
        [{ list: 'ordered' }, { list: 'bullet' }],
        [{ header: [1, 2, 3, 4, 5, 6, false] }],
        [{ color: [] }, { background: [] }]
      ]
    };
  }

}
