import { Component } from '@angular/core';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'admin-form-checkbox',
  template: `
  <div class="mb-xs-2" [formGroup]="group">
    <bvl-checkbox [formControlName]="config.code">{{ config.label }}</bvl-checkbox>
  </div>
  `
})
export class FormCheckboxComponent extends DynamicFormComponentBase { }
