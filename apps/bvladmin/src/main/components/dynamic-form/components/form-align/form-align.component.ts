import { Component } from '@angular/core';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'admin-form-align',
  template: `
  <div class="row align-items-center no-gutters mb-xs-2">
    <div class="col-auto g-w-100">
      <label [for]="config.label" class="g-fw-bold g-fs-15">{{ config.label }}:</label>
    </div>
    <div class="col" [formGroup]="group">
      <div class="g-admin-btn-group" [ngClass]="{ 'g-admin-input-error' : validateControl() }">
        <label>
          <input type="radio" value="left" [name]="config.code" [formControlName]="config.code">
          <span class="button">
            <i class="la la-align-left"></i>
          </span>
        </label>
        <label>
          <input type="radio" value="center" [name]="config.code" [formControlName]="config.code">
          <span class="button">
            <i class="la la-align-center"></i>
          </span>
        </label>
        <label>
          <input type="radio" value="right" [name]="config.code" [formControlName]="config.code">
          <span class="button">
            <i class="la la-align-right"></i>
          </span>
        </label>
      </div>
    </div>
  </div>
  `
})
export class FormAlignComponent extends DynamicFormComponentBase { }
