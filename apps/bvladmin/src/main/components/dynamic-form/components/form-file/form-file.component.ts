import { Component } from '@angular/core';
import { AbstractControl, FormBuilder } from '@angular/forms';
import { FILE_MIME_TYPE } from '@bvl-admin/settings/constants/file.constant';
import { StringUtil } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'admin-form-file',
  templateUrl: './form-file.component.html'
})
export class FormFileComponent extends DynamicFormComponentBase {

  formFile: AbstractControl;
  mimeTypes = FILE_MIME_TYPE;

  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  bvlOnInit(): void {
    this.formFile = this.fb.group({file: null});
    this._setFile(this.control);
    this.formFile.get('file')
      .valueChanges
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(val => {
        this.group.get(this.controlName)
          .setValue(val && val.length ? val[0].uri : '');
      });
  }

  private _setFile(control: AbstractControl): void {
    if (control.value && control.value !== '#') {
      this.formFile.get('file')
        .setValue([{
          uri: control.value,
          name: StringUtil.getFileName(control.value, true)
        }]);
    }
  }

}
