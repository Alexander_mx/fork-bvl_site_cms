import { Component } from '@angular/core';

@Component({
  selector: 'admin-form-static',
  template: `
  <div class="g-admin-card g-box-shadow-none">
    <div class="d-flex justify-content-center align-content-center align-items-center p-xs-1">
      <div class="g-admin-circle-icon ">
        <i class="la la-puzzle-piece"></i>
      </div>
      <span class="g-fs-15 text-center ml-xs-1">
        Este elemento es estático, no lo puedes editar.
      </span>
    </div>
  </div>
  `
})
export class FormStaticComponent { }
