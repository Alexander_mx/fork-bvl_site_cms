import { AfterViewInit,  Component } from '@angular/core';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'admin-form-select',
  template: `
  <div class="row align-items-center no-gutters mb-xs-2">
    <div class="col-auto g-w-100">
      <label [for]="config.label" class="g-fw-bold g-fs-15">{{ config.label }}:</label>
    </div>
    <div class="col" [formGroup]="group">
      <div class="d-flex align-items-center">
        <div class="g-admin-select">
          <select [formControlName]="config.code" [ngClass]="{ 'g-admin-input-error' : validateControl() }">
            <option *ngIf="config.placeholder" value="">{{ config.placeholder }}</option>
            <option [value]="item" *ngFor="let item of config.list">
              {{ item }}
            </option>
          </select>
          <span></span>
        </div>
        <div class="ml-3 p-3 g-b-line g-bc-gray3 g-b d-flex justify-content-center align-items-center g-bgc-gray5"
          *ngIf="config.type === 'select-icon' && control.value">
          <i [class]="control.value"></i>
        </div>
      </div>
    </div>
  </div>
  `
})
export class FormSelectComponent extends DynamicFormComponentBase implements AfterViewInit {

  ngAfterViewInit(): void {
    const preValue = this.group.controls[this.config.code].value,
          value = this.config.value
                    ? this.config.value
                    : (this.config.placeholder || '');

    this.group.controls[this.config.code].setValue(preValue || value);
  }

}
