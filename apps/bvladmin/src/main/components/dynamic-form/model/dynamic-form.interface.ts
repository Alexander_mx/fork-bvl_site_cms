export interface IComponent extends IComponentBase {
  code: string;
  description?: string;
  ref?: string;
  language?: Array<ILanguage>;
}

export interface ILanguage {
  language?: string;
  properties?: Array<IProperty>;
}

export interface IComponentBase extends IArrayComponent {
  id?: string;
  name?: string;
  imageUri?: string;
  category?: string;
  status?: number;
  template?: string;
  properties?: Array<IProperty>;
  isFloat?: boolean;
}

export interface IArrayComponent {
  labelItem?: string;
  design?: DESIGN_ARRAY | string;
  size?: number;
  type?: any;
  typeArray?: TYPE_ARRAY | string;
}

export interface ICustomComponent {
  typeDescription?: string;
  types?: Array<string>;
  definitions?: IDefinition;
}

export interface IDefinition {
  [key: string]: {
    properties: Array<IProperty>;
    detail?: {
      label?: string;
      icon?: string;
      image?: string;
    };
  };
}

export interface ISizeComponent {
  minWidth?: string;
  maxWidth?: string;
  height?: number;
  width?: number;
}

export interface ILengthComponent {
  max?: number;
  min?: number;
  maxLength?: number;
  minLength?: number;
}
export interface IFileComponent {
  mimeTypes?: Array<string>;
  imageSize?: IMAGE_SIZE | string;
  weight?: number;
}

export interface ISelectField {
  list?: Array<string>;
}

export interface IActionComponent {
  event?: EVENT_PROPERTY | string;
  request: string;
  dependencies?: Array<string>;
}

export interface IAutocompleteComponent {
  endpoint?: string;
  queryParams?: any;
}

export interface IAutocompleteItemResponse {
  text: string;
  value: string;
}

// tslint:disable-next-line:interface-name
export interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}

export interface IProperty
  extends IComponentBase,
    ISizeComponent,
    ISelectField,
    IFileComponent,
    ILengthComponent,
    ICustomComponent,
    IAutocompleteComponent {
  code?: string;
  label?: string;
  description?: string;
  placeholder?: string;
  value?: any;
  cols?: number;
  required?: boolean;
  typeSelected?: string;
  action?: IActionComponent;
  defaultLanguage?: string;
  parameter?: string;
}

export enum EVENT_PROPERTY {
  change = 'change',
  changeDependence = 'changeDependence',
  load = 'load'
}

export enum DESIGN_ARRAY {
  tabs = 'tabs',
  nothing = 'nothing'
}

export enum TYPE_ARRAY {
  object = 'object',
  string = 'string',
  custom = 'custom'
}

export enum IMAGE_SIZE {
  sm = 'sm',
  md = 'md',
  lg = 'lg'
}

export enum TYPE_VIDEO {
  url = 'url',
  gallery = 'gallery'
}
export interface IActionServiceResponse {
  value: any;
  text: string;
  icon?: string;
}
