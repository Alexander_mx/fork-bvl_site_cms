import { Input, OnInit } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util/unsubscribe-on-destroy';
import { IProperty } from './model';

export class DynamicFormComponentBase extends UnsubscribeOnDestroy implements OnInit {
  @Input() config: IProperty;
  @Input() properties: IProperty;
  @Input() group: FormGroup;
  @Input() code: any;

  control: AbstractControl;
  controlName: any;

  pattern: string;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.controlName = this._getCode();
    this.control = this.group.controls[this.controlName];
    this.bvlOnInit();
  }

  validateControl(): boolean {
    return ValidatorUtil.validateControl(this.group, this.controlName);
  }

  focus(): void {
    this.control.markAsTouched({ onlySelf: true });
  }

  private _getCode(): any {
    // INFO: El code puede ser 0
    return this.code !== undefined ? this.code : this.config.code;
  }

  bvlOnInit(): void {}
}
