import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FormAlignComponent } from '../components/form-align/form-align.component';
import { FormAutocompleteComponent } from '../components/form-autocomplete/form-autocomplete.component';
import { FormCheckboxComponent } from '../components/form-checkbox/form-checkbox.component';
import { FormFileComponent } from '../components/form-file/form-file.component';
import { FormImageComponent } from '../components/form-image/form-image.component';
import { FormInputComponent } from '../components/form-input/form-input.component';
import { FormRichtextComponent } from '../components/form-richtext/form-richtext.component';
import { FormSelectComponent } from '../components/form-select/form-select.component';
import { FormTableComponent } from '../components/form-table/form-table.component';
import { FormTextareaComponent } from '../components/form-textarea/form-textarea.component';
import { IProperty } from '../model';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[dynamicField]'
})
export class DynamicFieldDirective implements OnInit {
  @Input() config: IProperty;
  @Input() group: FormGroup;
  @Input() properties: IProperty;

  component: ComponentRef<any>;

  constructor(private resolver: ComponentFactoryResolver, private container: ViewContainerRef) {}

  ngOnInit(): void {
    const component = components[this.config.type];
    if (component) {
      const factory = this.resolver.resolveComponentFactory<any>(component);
      this.component = this.container.createComponent(factory);
      this.component.instance.config = this.config;
      this.component.instance.properties = this.properties;
      this.component.instance.group = this.group;
    }
  }
}

const components = {
  text: FormInputComponent,
  url: FormInputComponent,
  date: FormInputComponent,
  image: FormImageComponent,
  video: FormImageComponent,
  file: FormFileComponent,
  richtext: FormRichtextComponent,
  select: FormSelectComponent,
  radio: FormAlignComponent,
  checkbox: FormCheckboxComponent,
  textarea: FormTextareaComponent,
  table: FormTableComponent,
  autocomplete: FormAutocompleteComponent,
  'select-icon': FormSelectComponent
};
