import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ApplicationRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { TYPES_CODE } from '@bvl-admin/settings/constants/general.constant';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { AngularUtil } from '@bvl-core/shared/helpers/util';
import { ValidatorUtil } from '@bvl-core/shared/helpers/util/validator';
import { coerceBooleanProp } from '@bvl-core/ui/common/helpers';
import { SweetAlertOptions } from 'sweetalert2';
import { DESIGN_ARRAY, IComponent, IProperty, TYPE_ARRAY } from '../../model';
import { DynamicFormUtils } from './../../utils/validators';

@Component({
  selector: 'admin-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
  styles: [`
    .noselect {
      -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
         -khtml-user-select: none; /* Konqueror HTML */
           -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
                user-select: none; /* Non-prefixed version, currently
                                      supported by Chrome and Opera */
    }
  `]
})
export class DynamicFormComponent implements OnInit {

  @Input() components: Array<IComponent> = [];
  @Input() hideTitleComponent: boolean;

  @Output() submitted: EventEmitter<any> = new EventEmitter<any>();

  form: FormGroup;

  TYPES_CODE = TYPES_CODE;
  TYPE_ARRAY = TYPE_ARRAY;
  TYPE_DESIGN = DESIGN_ARRAY;

  deleteSwal: SweetAlertOptions;
  changeTypeSwal: SweetAlertOptions;

  constructor(
    private fb: FormBuilder,
    private appRef: ApplicationRef,
    private swalConfirmService: SwalConfirmService
  ) { }

  ngOnInit(): void {
    this.createForm(this.components);
    this.deleteSwal = this.swalConfirmService.warning(`¿Seguro de eliminar el elemento?`);
    this.changeTypeSwal = this.swalConfirmService.warning(`¿Seguro de cambiar de tipo?`);
  }

  createForm(components: Array<IComponent>): void {
    // const cmpts = components.filter(component => component.type !== TYPES_CODE.static);
    this.form = this.getControl(components);
  }

  private getControl(properties: Array<IProperty | IComponent>): FormGroup {
    const comp = this.fb.group({});
    properties
      .filter(prop => prop.code)
      .forEach(prop => {
        if (prop.type === 'array') {
          this.getControlArray(prop, comp);
        } else {
          this.getControlDefault(prop, comp);
        }
      });

    return comp;
  }

  private getControlDefault(property: IProperty, form: FormGroup): void {
    if (property.properties) {
      form.addControl(property.code, this.getControl(property.properties));
    } else {
      const validators = DynamicFormUtils.getValidator(property);
      property.value = this.getFormatValue(property.type, property.value);
      form.addControl(property.code, this.fb.control(property.value || null, validators));
    }
  }

  private getFormatValue(type: string, value: any): any {
    switch (type) {
      case 'checkbox':
        return coerceBooleanProp(value);
        break;
      case 'date':
        return value ? value.substr(0, 10) : value;
        break;
      default:
        break;
    }

    return value;
  }

  // MÉTODOS PARA ARRAY
  private getControlArray(property: IProperty, form: FormGroup): void {
    const array = this.fb.array([]);

    if (Array.isArray(property.value) && property.value.length) {
      this.setDefaultDataArray(property, array);
    } else {
      this.setDefaultSizeArray(property, array);
    }

    if (property.design === DESIGN_ARRAY.tabs) {
      (array.controls as Array<FormGroup>).forEach((control, index) => {
        control.addControl('isSelected', this.fb.control(!index));
      });
    }

    form.addControl(property.code, array);
  }

  private setDefaultDataArray(property: IProperty, array: FormArray): void {
    property = AngularUtil.clone(property);
    property.value.forEach(value => {
      let control: FormControl | FormGroup;

      switch (property.typeArray) {
        case TYPE_ARRAY.string:
          const validators = DynamicFormUtils.getValidator(property);
          control = this.fb.control(value, validators);
          break;
        case TYPE_ARRAY.object:
          Object.keys(value)
            .forEach(key => {
              const item = property.properties.find(tmp => tmp.code === key);
              if (item) { item.value = value[key]; }
            });

          control = this.getControl(property.properties);
          break;
        case TYPE_ARRAY.custom:
          if (value.type) {
            const properties = property.definitions[value.type].properties;

            Object.keys(value)
              .forEach(key => {
                const item = properties.find(tmp => tmp.code === key);
                if (item) { item.value = value[key]; }
              });

            control = this.getControl(properties);
            control.addControl('type', this.fb.control(value.type));
          } else {
            control = this.fb.group({ type: [null] });
          }
          break;
        default:
      }

      if (control) {
        array.push(control);
      }
    });
  }

  private setDefaultSizeArray(property: IProperty, array: FormArray): void {
    property = AngularUtil.clone(property);
    const tmp = new Array(property.size || 1);

    for (const _iterator of tmp) {
      let control: FormControl | FormGroup;

      switch (property.typeArray) {
        case TYPE_ARRAY.string:
          const validators = DynamicFormUtils.getValidator(property);
          control = this.fb.control('', validators);
          break;
        case TYPE_ARRAY.object:
          control = this.getControl(property.properties);
          break;
        case TYPE_ARRAY.custom:
          control = this.fb.group({ type: [null] });
          break;
        default:
      }

      if (control) {
        array.push(control);
      }
    }
  }

  addItem(form: FormArray, properties: Array<IProperty>, property: IProperty): void {
    properties = properties ? AngularUtil.clone(properties) : properties;
    property = property ? AngularUtil.clone(property) : property;

    if (this.isIimitItems(form, property)) {
      const validators = DynamicFormUtils.getValidator(property);
      let control;
      switch (property.typeArray) {
        case TYPE_ARRAY.object:
          control = this.getControl(properties);
          break;
        case TYPE_ARRAY.string:
          control = this.fb.control('', validators);
          break;

        case TYPE_ARRAY.custom:
          control = this.fb.group({ type: [null] });
          break;

        default:
      }

      form.push(control);

      if (DESIGN_ARRAY.tabs === property.design) {
        control.addControl('isSelected', this.fb.control(false));
        this.selectedSlide(form, control);
      }
    }
  }

  removeItem(form: FormArray, index: number, isSelected: boolean): void {
    form.removeAt(index);
    this.appRef.tick();
    if (isSelected) {
      const newIndex = index > 1 ? index - 1 : 0;
      (form.controls[newIndex] as FormGroup).controls.isSelected.setValue(true);
    }
  }

  selectedSlide(form: FormArray, control: FormGroup): void {
    form.controls.forEach((ctrl: FormGroup) => {
      ctrl.controls.isSelected.setValue(false);
    });
    control.controls.isSelected.setValue(true);
  }

  isIimitItems(form: FormArray, property: IProperty): boolean {
    return (property && property.max && form.length < property.max) ||
      (!property || (property && !property.max));
  }

  disabledAdd(property: IProperty): boolean {
    return !!property.size;
  }

  generateDynamicControl(control: FormGroup, properties: Array<IProperty>, type: string): void {
    control.controls.type.setValue(type);
    properties.forEach(proper => proper.typeSelected = type);
    const newControl = this.getControl(properties);
    Object.keys(newControl.controls)
      .forEach(key => {
        control.addControl(key, newControl.controls[key]);
      });
  }

  resetCustomArray(control: FormGroup): void {
    Object.keys(control.controls)
      .forEach(key => {
        control.removeControl(key);
      });
    control.addControl('isSelected', this.fb.control(true));
    control.addControl('type', this.fb.control(null));
  }

  // END - MÉTODOS PARA ARRAY

  onDrop(items: Array<FormControl>, event: CdkDragDrop<Array<string>>): void {
    moveItemInArray(items, event.previousIndex, event.currentIndex);
    const tmpPrevious = { ...items[event.previousIndex] };
    const tmpCurrent = { ...items[event.currentIndex] };

    items[event.previousIndex].setValue(tmpPrevious.value);
    items[event.currentIndex].setValue(tmpCurrent.value);
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.form);

    return this.form.valid;
  }

  formatForm(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      this.components = this.getFormatComponent();
      this.submitted.next(this.components);
    }
  }

  saveForm(): void {
    const validateForm = this.validateForm();
    const data = (validateForm)
                   ? { formValue: this.form.getRawValue(), component: this.getFormatComponent() }
                   : null;
    this.submitted.next(data);

    // const formValue = (validateForm)
    //   ? this.form.getRawValue()
    //   : null;
    // this.submitted.next(formValue);
  }

  getFormValue(): {} {
    const formValue = this.form.getRawValue();

    return formValue;
  }

  getFormatComponent(): Array<IComponent> {
    return this.components.map(component => {
      // if (component.type !== TYPES_CODE.static) {
      // }
      const values = this.form.controls[component.code].value;

      const div = document.createElement('div');
      const child = document.createElement(component.code);

      div.appendChild(child);
      if (values && typeof values === TYPE_ARRAY.object) {
        component.properties
          .forEach(prop => {
            const value = values[prop.code];
            if (value) {
              const tmp = typeof value === 'string' ? value : JSON.stringify(value);
              child.setAttribute(prop.code, tmp);
            }
          });
      }
      component.template = div.innerHTML;

      return component;
    });
  }

}
