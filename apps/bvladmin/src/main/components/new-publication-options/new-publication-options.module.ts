import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlButtonModule, BvlLinkModule, BvlTextareaModule } from '@bvl-core/ui/lib/components';
import { DynamicFormModule } from '../dynamic-form';
import { CommentEditComponent } from './components/comment-edit/comment-edit.component';
import { ComponentEditComponent } from './components/component-edit/component-edit.component';
import { NewPublicationPopupComponent } from './components/new-publication-popup/new-publication-popup.component';
import { AddDirectiveDirective } from './directives/add-directives.directive';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    DynamicFormModule,
    BvlLinkModule,
    ReactiveFormsModule,
    BvlTextareaModule,
    BvlButtonModule
  ],
  declarations: [
    AddDirectiveDirective,
    NewPublicationPopupComponent,
    ComponentEditComponent,
    CommentEditComponent
  ],
  entryComponents: [
    NewPublicationPopupComponent,
    ComponentEditComponent,
    CommentEditComponent
  ],
  exports: [
    AddDirectiveDirective
  ]
})
export class NewPublicationOptionsModule { }
