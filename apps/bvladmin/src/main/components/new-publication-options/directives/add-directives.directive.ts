import {
  AfterViewInit,
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  Renderer2,
  ViewContainerRef
} from '@angular/core';
import { CommentService } from '@bvl-admin/services';
import { ComponentService } from '@bvl-admin/services/component.service';
import { WC_BUNDLE_PATH } from '@bvl-admin/settings/constants/general.constant';
import { ICommentResponse } from '@bvl-admin/statemanagement/models/comment.interface';
import { IComponentResponse } from '@bvl-admin/statemanagement/models/component.interface';
import { IContent } from '@bvl-admin/statemanagement/models/page.interface';
import { addScriptTag } from '@bvl-core/shared/helpers/util/web-components';
import { cloneDeepWith } from 'lodash-es';
import { NewPublicationPopupComponent } from '../components/new-publication-popup/new-publication-popup.component';

// INFO: Activa la funcionalidad para agregar multiples componentes en una sola sección
const MULTI_COMPONENTS = false;

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[addDirectives]'
})
export class AddDirectiveDirective implements AfterViewInit {

  @Output() edit: EventEmitter<IContent> = new EventEmitter<IContent>();
  @Input() idPage: string;
  @Input() language: string;
  @Input() hideEdit: boolean;
  @Input() hideDelete: boolean;
  @Input() emptyElement: HTMLDivElement;
  @Input() refComponents: Array<string>;

  private _key = 'keyc';
  private _empty = 'empty';

  private _popupRefs: Array<{ ts: NewPublicationPopupComponent, html: HTMLDivElement }> = [];

  constructor(
    private _elementRef: ElementRef<HTMLDivElement>,
    private _container: ViewContainerRef,
    private _resolver: ComponentFactoryResolver,
    private _renderer: Renderer2,
    private _componentService: ComponentService,
    private _commentService: CommentService
  ) { }

  ngAfterViewInit(): void {
    const elements = this._elementRef.nativeElement.querySelectorAll(`[${this._key}]`);
    const emptyElements = this._elementRef.nativeElement.querySelectorAll(`[${this._empty}]`);

    elements.forEach(element => {
      const componentKey = element.getAttribute(this._key) || this._getTimeStamp();
      element.setAttribute(this._key, componentKey);
      const newElement = this._addOptions(element, componentKey);
      element.replaceWith(newElement);
    });
    emptyElements.forEach(element => {
      const emptyElement = this.emptyElement.cloneNode(true) as HTMLDivElement;
      element.replaceWith(emptyElement);
      this._onDropNewComponent(emptyElement);
    });

    this._formAndEmitEdit();
  }

  private _addOptions(element: Element, componentKey?: string): HTMLDivElement {
    const cloneElementBase = element.cloneNode(true);
    const factory = this._resolver.resolveComponentFactory<NewPublicationPopupComponent>(NewPublicationPopupComponent);

    const referentPopUp = this._container.createComponent(factory);
    const referentPopUpHtml = referentPopUp.location.nativeElement as HTMLDivElement;
    const referentPopUpTs = referentPopUp.instance;

    referentPopUpTs.idPage = this.idPage;
    referentPopUpTs.language = this.language;
    referentPopUpTs.hideEdit = this.hideEdit || false;
    referentPopUpTs.hideDelete = this.hideDelete || false;
    referentPopUpTs.componentCode = (cloneElementBase as HTMLElement).localName;
    referentPopUpTs.componentKey = componentKey;
    referentPopUp.changeDetectorRef.detectChanges();

    this._popupRefs.push({ ts: referentPopUpTs, html: referentPopUpHtml });

    referentPopUpHtml.getElementsByClassName('ng-content')[0]
      .replaceWith(cloneElementBase);

    this._renderer.addClass(referentPopUpHtml, 'g-admin-component-wrapper');

    this._onHover(referentPopUpHtml, referentPopUpTs);
    this._onEdit(referentPopUpHtml, referentPopUpTs);
    this._onRemove(referentPopUpHtml, referentPopUpTs);
    if (MULTI_COMPONENTS) {
      this._onDropAppendComponent(referentPopUpHtml);
    }

    return referentPopUpHtml;
  }

  private _onHover(referentPopUpHtml: HTMLDivElement, referentPopUpTs: NewPublicationPopupComponent): void {
    referentPopUpHtml.onmouseover = () => {
      this._completeStructure();
      const comments = this._getComments(referentPopUpTs.componentKey, referentPopUpTs.language);
      referentPopUpTs.comments = comments;
    };
  }

  private _onEdit(referentPopUpHtml: HTMLDivElement, referentPopUpTs: NewPublicationPopupComponent): void {
    referentPopUpTs.onEdit.subscribe(htmlString => {
      const div = this._getDivWithComponent(htmlString, referentPopUpTs.componentKey);
      referentPopUpHtml.firstElementChild.replaceWith(div);
      this._formAndEmitEdit();
    });
  }

  private _onRemove(referentPopUpHtml: HTMLDivElement, referentPopUpTs: NewPublicationPopupComponent): void {
    referentPopUpTs.onRemove.subscribe(() => {
      this._removeRefComponent(referentPopUpTs);
      const count = referentPopUpHtml.parentElement.childElementCount;
      if (count === 1) {
        const emptyElement = this.emptyElement.cloneNode(true) as HTMLDivElement;
        referentPopUpHtml.replaceWith(emptyElement);
        this._onDropNewComponent(emptyElement);
        this._formAndEmitEdit();
      } else {
        referentPopUpHtml.remove();
      }

    });
  }

  private _onDropNewComponent(emptyElement: HTMLDivElement): void {
    this._onDragOver(emptyElement);
    emptyElement.ondrop = ev => {
      ev.preventDefault();
      const component: IComponentResponse = JSON.parse(ev.dataTransfer.getData('text') || null);
      if (component) {
        const currentTarget = ev.currentTarget as HTMLDivElement;
        const componentElement = this._getDivWithComponent(component.template);
        const newTarget = this._addOptions(componentElement, this._getTimeStamp());
        currentTarget.replaceWith(newTarget);

        this._addNewWebComponent(component.ref);
      }
    };
  }

  private _onDropAppendComponent(referentPopUpHtml: HTMLDivElement): void {
    referentPopUpHtml.ondragover = ev => ev.preventDefault();
    referentPopUpHtml.ondrop = ev => {
      ev.preventDefault();
      const component: IComponentResponse = JSON.parse(ev.dataTransfer.getData('text') || null);
      if (component) {
        const currentTarget = ev.currentTarget as HTMLDivElement;
        const componentElement = this._getDivWithComponent(component.template);
        const newElement = this._addOptions(componentElement, null);
        const parentElement = currentTarget.parentElement;

        const nodes = Array.from(parentElement.childNodes);
        const index = nodes.indexOf(currentTarget);

        parentElement.insertBefore(newElement, nodes[index + 1]);
        this._addNewWebComponent(component.ref);
      }
    };
  }

  /**
   *
   * @param references Example: 'index,lib'
   */
  private _addNewWebComponent(references: string): void {
    const referencesTmp = (references || '').split(',');

    addScriptTag([references], WC_BUNDLE_PATH);
    this._addRefComponent(referencesTmp);
    this._formAndEmitEdit();
  }

  private _onDragOver(element: HTMLDivElement): void {
    element.ondragover = ev => {
      ev.preventDefault();
      this._renderer.addClass(element, 'g-admin-component-wrapper-drag');
    };
    element.ondragleave = ev => {
      ev.preventDefault();
      this._renderer.addClass(element, 'g-admin-component-wrapper-drag');
    };
  }

  private _getDivWithComponent(template: string, componentKey?: string): Element {
    let div: Element = document.createElement('div');
    div.innerHTML = template;
    div = div.firstElementChild;
    div.setAttribute(this._key, componentKey || this._getTimeStamp());

    return div;
  }

  private _formAndEmitEdit(): void {
    const allElements = this._elementRef.nativeElement.cloneNode(true) as HTMLDivElement;
    const elements = allElements.querySelectorAll(`[${this._key}]`);
    const empties = allElements.querySelectorAll(`[${this._empty}]`);
    elements.forEach(element => {
      const parent = element.parentElement;
      while (element.firstChild) {
        element.removeChild(element.firstChild);
      }
      const attrs = element.getAttributeNames()
        .filter(name => name.startsWith('_nghost') || name.startsWith('ng-version'));
      attrs.forEach(attr => element.removeAttribute(attr));
      parent.replaceWith(element);
    });

    empties.forEach(element => {
      while (element.firstChild) {
        element.removeChild(element.firstChild);
      }
      const attrs = element.getAttributeNames()
        .filter(name => name.startsWith('_ngcontent') || name.startsWith('class'));
      attrs.forEach(attr => element.removeAttribute(attr));
    });

    this.edit.next({ html: allElements.innerHTML, refComponents: this.refComponents });
  }

  private _removeRefComponent(referentPopUpTs: NewPublicationPopupComponent): void {
    const currentRefs = referentPopUpTs.component.ref || '';
    const indexRef = this._popupRefs.findIndex(popup => popup.ts === referentPopUpTs);
    this._popupRefs.splice(indexRef, 1);

    currentRefs
      .split(',')
      .forEach(ref => {
        const existsRef = this._popupRefs.find(popup => popup.ts.component.ref === ref);
        if (!existsRef) {
          const index = this.refComponents.indexOf(ref);
          this.refComponents.splice(index, 1);
        }
      });
  }

  private _addRefComponent(references: Array<string>): void {
    (references || []).forEach(reference => {
      const index = this.refComponents.indexOf(reference);
      if (index < 0) { this.refComponents.push(reference); }
    });
  }

  private _completeStructure(): void {
    this._popupRefs.forEach(popup => {
      const component = this._getComponent(popup.ts.componentCode, popup.html.firstElementChild);
      popup.ts.component = component;
    });
  }

  private _getComponent(componentCode: string, element: Element): IComponentResponse {
    const structure = cloneDeepWith(this._getStructure(componentCode));
    const data = this._getValues(element);
    if (structure) {
      structure.properties.forEach(property => {
        const value = data[property.code] || property.value;
        // tslint:disable-next-line:prefer-conditional-expression
        if ((property.type === 'array' ||
          property.type === 'image' ||
          property.type === 'table') &&
          value) {
          property.value = typeof value === 'string' ? JSON.parse(value) : value;
        } else {
          property.value = value;
        }
      });

      return structure;
    }
  }

  private _getStructure(componentCode: string): IComponentResponse {
    return this._componentService.getComponentsEditor()
      .find(component => component.code === componentCode);
  }

  private _getValues(element: Element): any {
    const data = {};
    const keys = Object.keys(element.attributes);
    keys.forEach(key => {
      const attribute = element.attributes[key];
      if (attribute.value) {
        data[attribute.localName] = attribute.value;
      }
    });

    return data;
  }

  private _getTimeStamp(): string {
    const date = new Date();

    return (date.getTime() + Math.floor((Math.random() * 100) + 1)).toString();
  }

  private _getComments(componentKey: string, language: string): Array<ICommentResponse> {
    return this._commentService.getCommentsEditor()
      .filter(fv => fv.idComponent === componentKey && fv.language === language);
  }

}
