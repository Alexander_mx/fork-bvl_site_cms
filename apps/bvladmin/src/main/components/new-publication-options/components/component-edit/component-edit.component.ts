import { Component, OnInit, ViewChild } from '@angular/core';
import { DynamicFormComponent } from '@bvl-admin/components/dynamic-form';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IComponentResponse } from '@bvl-admin/statemanagement/models/component.interface';
import { ActiveModal } from '@bvl-core/shared/helpers/modal';
import { IPayloadModalComponent } from '@bvl-core/shared/helpers/modal/models/modal.interface';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-component-edit',
  templateUrl: './component-edit.component.html'
})
export class ComponentEditComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {

  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;

  GENERAL_LANG = GeneralLang;

  modalPayload: IComponentResponse;

  constructor(
    private _activeModal: ActiveModal
  ) {
    super();
  }

  ngOnInit(): void {
    this._save();
  }

  private _save(): void {
    this._activeModal.confirm()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this.dynamicForm.formatForm();
      });
  }

  formSubmitted(data: Array<IComponentResponse>): void {
    this._activeModal.close(data[0].template);
  }

}
