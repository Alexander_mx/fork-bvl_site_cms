import { Component, ElementRef, EventEmitter, OnInit, Renderer2, ViewChild } from '@angular/core';
import { TYPES_CODE } from '@bvl-admin/settings/constants/general.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { ICommentResponse } from '@bvl-admin/statemanagement/models/comment.interface';
import { IComponentResponse } from '@bvl-admin/statemanagement/models/component.interface';
import { IModalConfig, MODAL_BREAKPOINT, ModalRef, ModalService } from '@bvl-core/shared/helpers/modal';
import { CommentEditComponent } from '../comment-edit/comment-edit.component';
import { ComponentEditComponent } from '../component-edit/component-edit.component';

@Component({
  selector: 'admin-new-publication-popup',
  templateUrl: './new-publication-popup.component.html'
})
export class NewPublicationPopupComponent implements OnInit {

  @ViewChild('btnEdit') btnEdit: ElementRef<HTMLDivElement>;
  @ViewChild('btnDelete') btnDelete: ElementRef<HTMLDivElement>;

  GENERAL_LANG = GeneralLang;

  onEdit: EventEmitter<string> = new EventEmitter<string>();
  onRemove: EventEmitter<void> = new EventEmitter<void>();
  /** IdPage & language necessary for comments */
  idPage: string;
  language: string;
  /** Necessary for seePublication */
  hideEdit: boolean;
  hideDelete: boolean;
  // Example: bvl-admin
  componentCode: string;
  // Referencia al componente en el template, para uso de comentarios
  componentKey: string;

  private _modalConfig: IModalConfig;
  private _modalRef: ModalRef;

  private _component: IComponentResponse;
  set component(component: IComponentResponse) {
    const typesCode = [TYPES_CODE.common];
    if (this.btnEdit && component && (typesCode.includes(component.type) || this.hideEdit)) {
      this._renderer2.removeChild(this._elementRef.nativeElement, this.btnEdit.nativeElement);
    }
    if (this.btnDelete && component && (!component.status || this.hideDelete)) {
      this._renderer2.removeChild(this._elementRef.nativeElement, this.btnDelete.nativeElement);
    }
    this._component = component;
  }
  get component(): IComponentResponse {
    return this._component;
  }

  comments: Array<ICommentResponse>;

  constructor(
    private _elementRef: ElementRef,
    private _modalService: ModalService,
    private _renderer2: Renderer2
  ) { }

  ngOnInit(): void { }

  openModalEdit(): void {
    this._modalConfig = {
      titleText: this.component.name,
      size: MODAL_BREAKPOINT.lg,
      confirmButtonText: this.GENERAL_LANG.buttons.save
    };
    this._modalConfig.titleText = this.component.name;
    this._modalRef = this._modalService.open(ComponentEditComponent, this._modalConfig);
    this._modalRef.setPayload(this.component);
    this._modalRef.result
      .then(result => {
        if (result) { this.onEdit.next(result); }
      }, () => {
        return false;
      });
  }

  openModalComment(): void {
    this._modalConfig = {
      titleText: this.GENERAL_LANG.titles.comments,
      size: MODAL_BREAKPOINT.md,
      showFooter: false
    };
    this._modalRef = this._modalService.open(CommentEditComponent, this._modalConfig);
    const paylod: { idPage: string, language: string, componentKey: string; comments: Array<ICommentResponse> } = {
      idPage: this.idPage,
      language: this.language,
      componentKey: this.componentKey,
      comments: this.comments
    };
    this._modalRef.setPayload(paylod);
  }

  remove(): void {
    this.onRemove.next();
  }

}
