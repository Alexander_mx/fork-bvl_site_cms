import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommentService, ConfigurationService } from '@bvl-admin/services';
import { EditorLang, GeneralLang } from '@bvl-admin/settings/lang';
import { ICommentResponse, ISaveCommentRequest } from '@bvl-admin/statemanagement/models/comment.interface';
import { IUserProfile } from '@bvl-admin/statemanagement/models/configuration.interface';
import { IPayloadModalComponent } from '@bvl-core/shared/helpers/modal';
import { AngularUtil, UnsubscribeOnDestroy, ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.scss']
})
export class CommentEditComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {

  GENERAL_LANG = GeneralLang;
  EDITOR_LANG = EditorLang;

  modalPayload: { idPage: string, language: string, componentKey: string; comments: Array<ICommentResponse> };
  frmComment: FormGroup;
  mComment: AbstractControl;
  userProfile: IUserProfile;
  comments: Array<ICommentResponse>;
  responses: Array<ICommentResponse>;
  isNewComment: boolean;

  constructor(
    private _configurationService: ConfigurationService,
    private _commentService: CommentService,
    private _formBuilder: FormBuilder
  ) {
    super();
  }

  ngOnInit(): void {
    this.userProfile = this._configurationService.getUserProfile();
    this._createForm();
    this._createComments(this.modalPayload.comments);
  }

  private _createForm(): void {
    this.frmComment = this._formBuilder.group({
      mComment: [null, Validators.required]
    });
    this.mComment = this.frmComment.get('mComment');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmComment, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmComment);

    return this.frmComment.valid;
  }

  private _createComments(comments: Array<ICommentResponse>): void {
    this.responses = AngularUtil.clone(comments);
    this.comments = this.responses.splice(0, 1);
    this.isNewComment = !this.comments.length;
  }

  private _paramsSaveComment(): ISaveCommentRequest {
    return {
      idPage: this.modalPayload.idPage,
      idComponent: this.modalPayload.componentKey,
      note: this.mComment.value,
      language: this.modalPayload.language
    } as ISaveCommentRequest;
  }

  linkAction(): void {
    if (this.isNewComment) {
      const validateForm = this._validateForm();
      if (validateForm) {
        const params = this._paramsSaveComment();
        this._commentService.saveComment(params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe((response: ICommentResponse) => {
            this.frmComment.reset();
            this.modalPayload.comments.push(response);
            this._createComments(this.modalPayload.comments);
            this._commentService.getCommentsEditor()
              .push(response);
          });
      }
    } else {
      this.isNewComment = !this.isNewComment;
    }
  }

}
