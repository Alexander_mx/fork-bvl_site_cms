import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TemplatePreviewComponent } from '@bvl-admin/components/modals/template-preview/template-preview.component';
import { PageService } from '@bvl-admin/services';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IPageResponse } from '@bvl-admin/statemanagement/models/page.interface';
import { ITemplateResponse } from '@bvl-admin/statemanagement/models/template.interface';
import { IModalConfig, MODAL_BREAKPOINT, ModalRef, ModalService } from '@bvl-core/shared/helpers/modal';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-item-template',
  templateUrl: './item-template.component.html',
  styleUrls: ['./item-template.component.scss']
})
export class ItemTemplateComponent extends UnsubscribeOnDestroy implements OnInit {

  @Input() template: ITemplateResponse;

  GENERAL_LANG = GeneralLang;

  constructor(
    private _router: Router,
    private _modalService: ModalService,
    private _pageService: PageService
  ) {
    super();
  }

  ngOnInit(): void { }

  selectTemplate(template: ITemplateResponse): void {
    this._pageService.createPage(template, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IPageResponse) => {
        const queryParams = { state: response.status };
        this._router.navigate(['editor/edit/publication', response.idPage], { queryParams });
      });
  }

  templatePreview(template: ITemplateResponse): void {
    const modalConfig = {
      titleText: template.name,
      size: MODAL_BREAKPOINT.lg,
      confirmButtonText: this.GENERAL_LANG.buttons.select
    } as IModalConfig;
    const modal: ModalRef = this._modalService.open(TemplatePreviewComponent, modalConfig);
    modal.setPayload(template);
    modal.result
      .then(() => {
        this.selectTemplate(template);
      }, () => {
        return false;
      });
  }

}
