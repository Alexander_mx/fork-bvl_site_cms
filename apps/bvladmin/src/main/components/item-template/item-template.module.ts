import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TemplatePreviewModule } from '@bvl-admin/components/modals/template-preview/template-preview.module';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlButtonModule } from '@bvl-core/ui/lib/components';
import { ItemTemplateComponent } from './item-template.component';

@NgModule({
  declarations: [ItemTemplateComponent],
  imports: [
    CommonModule,
    BvlButtonModule,
    ModalModule,
    TemplatePreviewModule
  ],
  exports: [ItemTemplateComponent]
})
export class ItemTemplateModule { }
