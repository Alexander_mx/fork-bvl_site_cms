import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ACTION_TYPES, DATE_FORMAT, TYPES_CODE } from '@bvl-admin/settings/constants/general.constant';
import { TRAY_ITEM_BUTTONS } from '@bvl-admin/settings/constants/page.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { IComponentItemResponse } from '@bvl-admin/statemanagement/models/component.interface';
import { ITrayItemButton } from '@bvl-admin/statemanagement/models/page.interface';

@Component({
  selector: 'admin-item-component',
  templateUrl: './item-component.component.html',
  styleUrls: ['./item-component.component.scss']
})
export class ItemComponentComponent implements OnInit {

  @Input() component: IComponentItemResponse;

  actionButtons: Array<ITrayItemButton>;

  GENERAL_LANG = GeneralLang;
  DATE_FORMAT = DATE_FORMAT;

  constructor(
    private _router: Router
  ) { }

  ngOnInit(): void {
    this._createActionButtons();
  }

  private _createActionButtons(): void {
    this.actionButtons = TRAY_ITEM_BUTTONS.filter((fv, fk) => {
      return fv.actionType === ACTION_TYPES.edit;
    });
  }

  buttonAction(actionButton: ITrayItemButton, component: IComponentItemResponse): void {
    this._router.navigate(
      [
        (component.type === TYPES_CODE.common)
          ? 'editor/edit/common-component'
          : 'editor/edit/popup-component',
        (component.type === TYPES_CODE.common)
          ? component.code
          : component.id
      ]);
  }

}
