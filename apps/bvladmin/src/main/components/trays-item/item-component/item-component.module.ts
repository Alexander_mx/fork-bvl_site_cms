import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemLanguageModule } from '@bvl-admin/components/item-language/item-language.module';
import { LabelStateModule } from '@bvl-admin/components/label-state/label-state.module';
import { BvlBreadcrumbModule, BvlButtonModule, BvlCardModule } from '@bvl-core/ui/lib/components';
import { ItemComponentComponent } from './item-component.component';

@NgModule({
  declarations: [ItemComponentComponent],
  imports: [
    CommonModule,
    BvlCardModule,
    BvlBreadcrumbModule,
    LabelStateModule,
    ItemLanguageModule,
    BvlButtonModule
  ],
  exports: [ItemComponentComponent]
})
export class ItemComponentModule { }
