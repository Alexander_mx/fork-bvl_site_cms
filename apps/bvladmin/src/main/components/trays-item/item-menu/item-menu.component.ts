import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AddItemMenuComponent } from '@bvl-admin/components/modals/add-item-menu/add-item-menu.component';
import { SiteMenuService } from '@bvl-admin/services';
import { ACTION_TYPES } from '@bvl-admin/settings/constants/general.constant';
import { ITEM_MENU_BUTTONS } from '@bvl-admin/settings/constants/site-menu.constant';
import { GeneralLang } from '@bvl-admin/settings/lang';
import { ModalService } from '@bvl-core/shared/helpers/modal';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { ISiteMenu } from '@bvl-core/shared/helpers/util/menu';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'admin-item-menu',
  templateUrl: './item-menu.component.html',
  styleUrls: ['./item-menu.component.scss']
})
export class ItemMenuComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild('swalDelete') private _swalDelete: SwalComponent;

  @Input() showUp: boolean;
  @Input() showDown: boolean;
  @Input() item: ISiteMenu;
  @Input() parent: ISiteMenu;

  @Output() upChange: EventEmitter<any>;
  @Output() downChange: EventEmitter<any>;
  @Output() remove: EventEmitter<any>;
  @Output() collapsed: EventEmitter<any>;

  GENERAL_LANG = GeneralLang;
  ITEM_MENU_BUTTONS = ITEM_MENU_BUTTONS;
  ACTION_TYPES = ACTION_TYPES;

  swalDeleteOptions: SweetAlertOptions;

  constructor(
    private _modalService: ModalService,
    private _swalConfirmService: SwalConfirmService,
    private _siteMenuService: SiteMenuService
  ) {
    super();
    this.upChange = new EventEmitter<any>();
    this.downChange = new EventEmitter<any>();
    this.remove = new EventEmitter<any>();
    this.collapsed = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this._setTitleDelete();
  }

  private _setTitleDelete(): void {
    const title = this.GENERAL_LANG.modalConfirm.titleDelete.replace('{{ customField }}', `el item ${this.item.name}`);
    this.swalDeleteOptions = this._swalConfirmService.warning(title);
  }

  up(): void {
    if (this.showUp) {
      this.upChange.emit();
    }
  }

  down(): void {
    if (this.showDown) {
      this.downChange.emit();
    }
  }

  buttonAction(actionType: string): void {
    switch (actionType) {
      case ACTION_TYPES.edit:
        this._edit();
        break;
      case ACTION_TYPES.delete:
        this._swalDelete.show();
        break;
      default:
        this._add();
        break;
    }
  }

  private _edit(): void {
    const modal = this._modalService.open(AddItemMenuComponent, { titleText: 'Editar' });
    modal.setPayload({
      type: this.item.type,
      parent: this.parent,
      item: this.item,
      language: this.item.language,
      order: this.item.order
    });
    modal.result.then((response: ISiteMenu) => {
      this.item = { ...this.item, ...response };
      this._setTitleDelete();
    })
    .catch(() => false);
  }

  private _add(): void {
    const modal = this._modalService.open(AddItemMenuComponent, { titleText: 'Agregar' });
    modal.setPayload({
      type: this.item.type,
      parent: this.item,
      language: this.item.language,
      order: this.item.children.length
    });
    modal.result.then((response: ISiteMenu) => {
      this.item.children.push(response);
    })
    .catch(() => false);
  }

  delete(): void {
    this._siteMenuService.deleteMenu(this.item.id)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this.remove.next();
      });
  }

}
