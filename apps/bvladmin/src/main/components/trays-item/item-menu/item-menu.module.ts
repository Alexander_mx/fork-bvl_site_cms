import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AddItemMenuModule } from '@bvl-admin/components/modals/add-item-menu/add-item-menu.module';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { BvlButtonModule } from '@bvl-core/ui/lib/components';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { ItemMenuComponent } from './item-menu.component';

@NgModule({
  declarations: [ItemMenuComponent],
  imports: [
    CommonModule,
    BvlButtonModule,
    ModalModule,
    AddItemMenuModule,
    SweetAlert2Module
  ],
  exports: [ItemMenuComponent]
})
export class ItemMenuModule { }
