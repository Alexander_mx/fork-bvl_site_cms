import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationService, PageService } from '@bvl-admin/services';
import { ACTION_TYPES, DATE_FORMAT, STATES_CODE, TYPES_CODE } from '@bvl-admin/settings/constants/general.constant';
import { PAGE_TYPES, TRAY_ITEM_BUTTONS } from '@bvl-admin/settings/constants/page.constant';
import { GeneralLang, PageLang } from '@bvl-admin/settings/lang';
import { IPageItemResponse, ITrayItemButton } from '@bvl-admin/statemanagement/models/page.interface';
import { NotificationService } from '@bvl-core/shared/helpers/notification';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'admin-item-page',
  templateUrl: './item-page.component.html',
  styleUrls: ['./item-page.component.scss']
})
export class ItemPageComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild('swalDelete') private _swalDelete: SwalComponent;

  @Input() page: IPageItemResponse;
  @Output() deletePage: EventEmitter<boolean>;

  GENERAL_LANG = GeneralLang;
  PAGE_LANG = PageLang;
  PAGE_TYPES = PAGE_TYPES;
  TYPES_CODE = TYPES_CODE;
  ACTION_TYPES = ACTION_TYPES;
  DATE_FORMAT = DATE_FORMAT;

  actionButtons: Array<ITrayItemButton>;
  swalDeleteOptions: SweetAlertOptions;

  constructor(
    private _configurationService: ConfigurationService,
    private _router: Router,
    private _pageService: PageService,
    private _notificationService: NotificationService,
    private _swalConfirmService: SwalConfirmService
  ) {
    super();
    this.deletePage = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
    this._createActionButtons(this.page);
    const title = this.GENERAL_LANG.modalConfirm.titleDelete.replace('{{ customField }}', `"${this.page.name}"`);
    this.swalDeleteOptions = this._swalConfirmService.warning(title);
  }

  private _createActionButtons(page: IPageItemResponse): void {
    const userPermissions = this._configurationService.getUserPermissions();

    this.actionButtons = TRAY_ITEM_BUTTONS.filter((fv, fk) => {
      let buttons = !!fv.permissions.find((sv, sk) => {
        const roleCode = sv.rolesCode.includes(userPermissions.roleCode),
              pageType = sv.pageTypes.includes(page.pageType),
              type = sv.types.includes(page.type),
              state = sv.statesCode.includes(page.status);

        return roleCode && pageType && type && state;
      });

      if (buttons && fv.actionType === this.ACTION_TYPES.edit && page.status === STATES_CODE.inEdition) {
        buttons = (page.username === userPermissions.username);
      }

      return buttons;
    });
  }

  private _see(): void {
    const queryParams = { state: this.page.status };
    this._router.navigate(['editor/see/publication', this.page.idPage], { queryParams });
  }

  private _edit(): void {
    this._pageService.editPage(this.page.idPage, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        const queryParams = { state: STATES_CODE.inEdition };
        this._router.navigate(['editor/edit/publication', this.page.idPage], { queryParams });
      });
  }

  private _publish(): void {
    this._pageService.publishPage(this.page.idPage, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.page.status = STATES_CODE.published;
        this._createActionButtons(this.page);
      });
  }

  delete(): void {
    this._pageService.deletePage(this.page.idPage, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this.deletePage.emit(true);
      });
  }

  buttonAction(actionButton: ITrayItemButton): void {
    switch (actionButton.actionType) {
      case ACTION_TYPES.see:
        this._see();
        break;
      case ACTION_TYPES.edit:
        this._edit();
        break;
      case ACTION_TYPES.publish:
        this._publish();
        break;
      case ACTION_TYPES.delete:
        this._swalDelete.show();
        break;
      default:
        break;
    }
  }

}
