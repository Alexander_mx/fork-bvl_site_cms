import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemLanguageModule } from '@bvl-admin/components/item-language/item-language.module';
import { LabelStateModule } from '@bvl-admin/components/label-state/label-state.module';
import { BvlBreadcrumbModule, BvlButtonModule, BvlCardModule } from '@bvl-core/ui/lib/components';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { ItemPageComponent } from './item-page.component';

@NgModule({
  declarations: [ItemPageComponent],
  imports: [
    CommonModule,
    BvlCardModule,
    BvlBreadcrumbModule,
    LabelStateModule,
    ItemLanguageModule,
    BvlButtonModule,
    SweetAlert2Module
  ],
  exports: [ItemPageComponent]
})
export class ItemPageModule { }
