import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DebounceInputDirectiveModule } from '@bvl-core/shared/helpers/directives/debounce-input.directive';
import { IconFileDirectiveModule } from '@bvl-core/shared/helpers/directives/icon-file.directive';
import { InfiniteScrollerDirectiveModule } from '@bvl-core/shared/helpers/directives/infinite-scroller.directive';
import { ModalModule } from '@bvl-core/shared/helpers/modal';
import { PipesModule } from '@bvl-core/shared/helpers/pipes';
import { BvlInputModule, BvlNotifyModule, BvlVideoModule } from '@bvl-core/ui/lib/components';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { GalleryFilterDateComponent } from './gallery-filter-date/gallery-filter-date.component';
import { GalleryModalComponent } from './gallery-modal/gallery-modal.component';
import { GalleryPreviewComponent } from './gallery-preview/gallery-preview.component';
import { GallerySelectComponent } from './gallery-select/gallery-select.component';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    FormsModule,
    ReactiveFormsModule,
    BvlNotifyModule,
    PipesModule,
    BvlInputModule,
    BvlVideoModule,
    SweetAlert2Module,
    InfiniteScrollerDirectiveModule,
    DebounceInputDirectiveModule,
    IconFileDirectiveModule
  ],
  declarations: [GalleryModalComponent, GallerySelectComponent, GalleryFilterDateComponent, GalleryPreviewComponent],
  exports: [GalleryModalComponent, GallerySelectComponent, GalleryFilterDateComponent, GalleryPreviewComponent],
  entryComponents: [GalleryModalComponent, GallerySelectComponent, GalleryFilterDateComponent, GalleryPreviewComponent]
})
export class GallerySelectModule { }
