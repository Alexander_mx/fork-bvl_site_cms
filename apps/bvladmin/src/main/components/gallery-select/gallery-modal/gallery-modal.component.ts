import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FileService } from '@bvl-admin/services';
import { FILE_TYPE, IMultimediaProperty, IMultimediaResponse } from '@bvl-admin/statemanagement/models/file.interface';
import { MultimediaBase } from '@bvl-admin/views/multimedia/multimedia-base';
import { ActiveModal, MODAL_BREAKPOINT, ModalService } from '@bvl-core/shared/helpers/modal';
import { IModalConfig, IPayloadModalComponent } from '@bvl-core/shared/helpers/modal/models/modal.interface';
import { NOTIFICATION_TYPES, NotificationService } from '@bvl-core/shared/helpers/notification';
import { SwalConfirmService } from '@bvl-core/shared/helpers/swal';
import { AngularUtil, ValidatorUtil, WindowService } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';
import { GalleryPreviewComponent } from '../gallery-preview/gallery-preview.component';

@Component({
  selector: 'admin-gallery-modal',
  templateUrl: './gallery-modal.component.html'
})
export class GalleryModalComponent extends MultimediaBase implements OnInit, IPayloadModalComponent {

  @ViewChild('file') input: ElementRef<HTMLInputElement>;
  @ViewChild('inputSearch') inputSearch: ElementRef<HTMLInputElement>;

  NOTIFICATION_TYPES = NOTIFICATION_TYPES;
  FILE_TYPE = FILE_TYPE;

  currentSearchText: string;
  currentDate: string;
  searchInput: string;

  typeLabel: string;

  isLastPage: boolean;

  deleteSwal: SweetAlertOptions;
  typeText: string;

  iconNoSelectedFile: string;
  textSelectedFile: string;

  constructor(
    protected notificationService: NotificationService,
    protected swalConfirmService: SwalConfirmService,
    protected modalService: ModalService,
    protected activeModal: ActiveModal,
    protected fileService: FileService,
    protected fb: FormBuilder,
    protected _windowService: WindowService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(notificationService, fileService, fb, document);
  }

  ngOnInit(): void {
    const text = (this.modalPayload.type === FILE_TYPE.document)
                  ? {
                      typeText: 'el documento',
                      textSelectedFile: 'un documento',
                      iconNoSelectedFile: 'la la-file'
                    }
                  : (this.modalPayload.type === FILE_TYPE.video)
                    ? {
                        typeText: 'el video',
                        textSelectedFile: 'un video',
                        iconNoSelectedFile: 'la la-file-movie-o'
                      }
                    : {
                        typeText: 'la imagen',
                        textSelectedFile: 'una imagen',
                        iconNoSelectedFile: 'la la-image'
                      };
    this.typeText = text.typeText;
    this.textSelectedFile = text.textSelectedFile;
    this.iconNoSelectedFile = text.iconNoSelectedFile;

    this.modalPayload.mimeTypes = this.modalPayload.mimeTypes || [];
    this.deleteSwal = this.swalConfirmService.warning(`¿Seguro de eliminar ${this.typeText}?`);

    this.setDefaultFile();
    this.getFiles();
  }

  private setDefaultFile(): void {
    if (this.modalPayload.defaultFile && !AngularUtil.isString(this.modalPayload.defaultFile)) {
      this.selected(this.modalPayload.defaultFile);
    }
  }

  debounceInput(text: string): void {
    this.currentSearchText = text;
    this.files = [];
    this.isLastPage = false;
    this.getFiles();
  }

  selectedDate(date: string): void {
    this.currentDate = date;
    this.openFilter = false;
    this.files = [];
    this.isLastPage = false;
    this.getFiles();
  }

  selected(file: IMultimediaResponse): void {
    this.openFilter = false;
    this.currentImage = this.currentImage === file ? null : file;
    this.createForm();
  }

  confirm(): void {
    ValidatorUtil.validateForm(this.form);
    if (this.form.valid) {
      if (this.currentImage) {
        this.updateImageProperties();
        this.currentImage.properties = this.form.getRawValue();
      }
      this.activeModal.close([this.currentImage]);
      this.currentImage = null;
    } else {
      this.notificationService.addWarning('Debe completar todos los campos');
    }
  }

  private updateImageProperties(): void {

    if (this.modalPayload.type === FILE_TYPE.image) {
      const properties = this.currentImage.properties || {} as IMultimediaProperty;
      const newProperties = this.form.getRawValue() || {} as IMultimediaProperty;

      if (!properties.alt) { properties.alt = newProperties.alt; }
      if (!properties.title) { properties.title = newProperties.title; }
      if (!properties.description) { properties.description = newProperties.description; }

      const subscribe = this.fileService.updateImage({ properties, id: this.currentImage.id } as IMultimediaResponse)
        .subscribe(() => subscribe.unsubscribe(), () => subscribe.unsubscribe());
    }
  }

  getFiles(): void {
    if (!this.isLastPage) {
      this.showLoading = true;
      const tmp = (this.currentDate || '').split('-');
      tmp.splice(tmp.length - 1);
      const date = tmp.join('-');

      this.fileService.getFilesByType(
        this.modalPayload.type,
        {
          value: this.currentSearchText || '',
          date,
          page: Number(Number((this.files.length / 24) + 1)
            .toFixed(0)),
          size: 24
        })
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(files => {
          files.content
            .filter(file => this.modalPayload.type !== FILE_TYPE.document && this.modalPayload.type !== FILE_TYPE.badge ?
              file.large && file.medium && file.small : file.uri)
            .forEach(img => this.files.push(img));
          this.showLoading = false;
          this.isLastPage = files.last;
        }, () => {
          this.showLoading = false;
          this.isLastPage = false;
        });
    }
  }

  delete(): void {
    this.openFilter = false;
    this.fileService.removeFileByType(this.modalPayload.type, this.currentImage.id, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        () => {
          const index = this.files.indexOf(this.currentImage);
          if (index > -1) {
            this.files.splice(index, 1);
          }

          this.currentImage = null;
        },
        () => {
          this.activeError(this.lang.errors.image.failDelete);
        });
  }

  openPreview(): void {
    if (this.modalPayload.type !== FILE_TYPE.document) {
      const modalConfig = {
        size: MODAL_BREAKPOINT.lg,
        showFooter: false
      } as IModalConfig;

      const modalRef = this.modalService.open(GalleryPreviewComponent, modalConfig);
      modalRef.setPayload({
        file: this.currentImage.url || this.currentImage.large.imageUri,
        mimetype: this.currentImage.mimeType,
        type: this.modalPayload.type
      });
      modalRef.result
        .then(() => {})
        .catch(() => {});
    } else {
      const nativeWindow = this._windowService.getNativeWindow();
      nativeWindow.open(this.currentImage.uri, '_blank');
    }
  }
}
