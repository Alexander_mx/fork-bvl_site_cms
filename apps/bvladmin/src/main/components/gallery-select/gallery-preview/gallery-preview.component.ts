import { Component, OnInit } from '@angular/core';
import { FILE_TYPE } from '@bvl-admin/statemanagement/models/file.interface';
import { IPayloadModalComponent } from '@bvl-core/shared/helpers/modal';

@Component({
  selector: 'admin-gallery-preview',
  templateUrl: './gallery-preview.component.html'
})
export class GalleryPreviewComponent implements OnInit, IPayloadModalComponent {

  modalPayload: { file: string; type: FILE_TYPE, mimetype: string };
  FILE_TYPE = FILE_TYPE;

  constructor() { }

  ngOnInit(): void { }

}
