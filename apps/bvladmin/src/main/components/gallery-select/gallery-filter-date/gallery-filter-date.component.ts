import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FileService } from '@bvl-admin/services';
import { DateUtil } from '@bvl-core/shared/helpers/util/date';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util/unsubscribe-on-destroy';
import { groupBy, map } from 'lodash-es';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-gallery-filter-date',
  templateUrl: './gallery-filter-date.component.html'
})
export class GalleryFilterDateComponent extends UnsubscribeOnDestroy implements OnInit {

  @Output() selected: EventEmitter<string> = new EventEmitter<string>();
  @Input() current: string;
  @Input() type: string;

  list: Array<{ dates: Array<string>, year: string }> = [];

  constructor(
    private fileService: FileService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(type?: string): void {
    this.fileService.rangeDateByType(type || this.type)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(range => {
        const dateList = DateUtil.dateRange(range.dateMin, range.dateMax);
        const groups = groupBy(dateList, (date: string) => date.split('-')[0]);
        this.list = map(groups, (dates: Array<string>, year: string) => ({ dates, year }))
                    .reverse();
      });
  }

  selectedFn(date: string): void {
    this.current = date === this.current ? null : date;
    this.selected.next(this.current);
  }

}
