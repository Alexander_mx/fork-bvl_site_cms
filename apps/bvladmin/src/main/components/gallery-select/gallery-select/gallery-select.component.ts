import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IMAGE_SIZE } from '@bvl-admin/components/dynamic-form/model/dynamic-form.interface';
import { FILE_RESPONSE, FILE_TYPE, IMultimediaResponse } from '@bvl-admin/statemanagement/models/file.interface';
import { IModalConfig, MODAL_BREAKPOINT, ModalService } from '@bvl-core/shared/helpers/modal';
import { ModalRef } from '@bvl-core/shared/helpers/modal/models/modal-ref';
import { coerceBooleanProp } from '@bvl-core/ui/common/helpers';
import { GalleryModalComponent } from '../gallery-modal/gallery-modal.component';

@Component({
  selector: 'admin-gallery-select',
  templateUrl: './gallery-select.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line: no-forward-ref
      useExisting: forwardRef(() => GallerySelectComponent),
      multi: true
    }
  ]
})
export class GallerySelectComponent implements OnInit, ControlValueAccessor {

  protected _value: any;
  @Input()
  get value(): any {
    return this._value;
  }
  set value(value: any) {
    this._value = value;
    this.propagateChange(this._value);
  }

  protected _error = false;
  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }

  @Input() maxLength: number;
  /** Valor en megabytes */
  @Input() maxSize: number;
  @Input() mimeTypes: Array<string>;
  @Input() type: FILE_TYPE;
  @Input() placeholder: string;
  @Input() buttonText: string;
  @Input() size: IMAGE_SIZE;
  @Input() responseFormat: FILE_RESPONSE = FILE_RESPONSE.object;

  private _model: Array<IMultimediaResponse> = [];
  get model(): Array<IMultimediaResponse> {
    return this._model;
  }
  set model(model: Array<IMultimediaResponse>) {
    this._model = model || [];

    if (this.responseFormat === FILE_RESPONSE.string) {
      this.value = this.model.map(mod => mod.url ? mod.url : mod.large.imageUri)
        .join('');
    } else {
      this.value = model;
    }

  }

  FILE_TYPE = FILE_TYPE;

  private modalConfig: IModalConfig;
  private modalRef: ModalRef;

  constructor(
    private modalService: ModalService
  ) { }

  ngOnInit(): void {
    const typeText = this.type === FILE_TYPE.document ? 'documento' :
      this.type === FILE_TYPE.video ? 'video' : 'imagen';

    this.buttonText = this.buttonText || `Subir ${typeText}`;

    this.modalConfig = {
      size: MODAL_BREAKPOINT.lg,
      titleText: `Seleccionar ${typeText}`,
      headerClass: 'text-left text-capitalize',
      bodyClass: 'pb-xs-0 pr-xs-3',
      showFooter: false
    };
  }

  openModal(): void {
    this.modalRef = this.modalService.open(GalleryModalComponent, this.modalConfig);
    this.modalRef.setPayload({
      mimeTypes: this.mimeTypes,
      maxSize: this.maxSize || 999999,
      type: this.type,
      defaultFile: this.value && this.value[0] || null
    });
    this.modalRef.result
      .then((images: Array<IMultimediaResponse>) => {
        if (images && images.length) {
          this.model = images;
        }
      })
      .catch(() => { });
  }

  removeImage(index: number): void {
    this.model.splice(index, 1);
    if (!this.model.length) { this.setModelNull(); }
  }

  /* Takes the value  */
  writeValue(value: any): void {
    if (value !== undefined) {
      this._value = value;
      this._model = this.responseFormat === FILE_RESPONSE.string && value ?
        [{ url: value } as IMultimediaResponse] : value;
    } else {
      this.setModelNull();
    }
  }

  private setModelNull(): void {
    this.model = null;
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

}
