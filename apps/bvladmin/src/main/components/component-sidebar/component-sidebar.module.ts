import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentIconModule } from '@bvl-admin/components/component-icon/component-icon.module';
import { PipesModule } from '@bvl-core/shared/helpers/pipes';
import { BvlAccordionModule } from '@bvl-core/ui/lib/components';
import { ComponentSidebarComponent } from './component-sidebar.component';

@NgModule({
  declarations: [
    ComponentSidebarComponent
  ],
  imports: [
    CommonModule,
    BvlAccordionModule,
    ComponentIconModule,
    PipesModule
  ],
  exports: [ComponentSidebarComponent]
})
export class ComponentSidebarModule { }
