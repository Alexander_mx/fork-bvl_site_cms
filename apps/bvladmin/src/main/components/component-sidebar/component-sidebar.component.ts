import { Component, OnInit } from '@angular/core';
import { ComponentService } from '@bvl-admin/services';
import { COMPONENT_CATEGORIES } from '@bvl-admin/settings/constants/component.constant';
import { TYPES_CODE } from '@bvl-admin/settings/constants/general.constant';
import { ComponentLang } from '@bvl-admin/settings/lang';
import { IComponentResponse } from '@bvl-admin/statemanagement/models/component.interface';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-component-sidebar',
  templateUrl: './component-sidebar.component.html',
  styleUrls: ['./component-sidebar.component.scss']
})
export class ComponentSidebarComponent extends UnsubscribeOnDestroy implements OnInit {

  COMPONENT_LANG = ComponentLang;

  COMPONENT_CATEGORIES = COMPONENT_CATEGORIES;

  components: Array<IComponentResponse>;

  constructor(
    private _componentService: ComponentService
  ) {
    super();
  }

  ngOnInit(): void {
    this._componentList();
  }

  private _componentList(): void {
    this._componentService.componentListByType(TYPES_CODE.all, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IComponentResponse>) => {
        this.components = (response || []).filter(res => res.type !== TYPES_CODE.popup);
        this._componentService.setComponentsEditor(this.components);
      });
  }
}
