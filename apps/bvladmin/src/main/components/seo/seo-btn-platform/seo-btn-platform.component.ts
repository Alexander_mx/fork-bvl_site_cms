import { Component, EventEmitter, HostBinding, OnInit, Output, ViewEncapsulation } from '@angular/core';

export interface ISeoPlatform {
  isMobile: boolean;
  isDesktop: boolean;
}

@Component({
  selector: 'admin-seo-btn-platform',
  templateUrl: './seo-btn-platform.component.html',
  styleUrls: ['./seo-btn-platform.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SeoBtnPlatformComponent implements OnInit {

  isMobile = true;

  @HostBinding('class') class = 'mt-1 mr-0-5';
  @Output() changePlatform: EventEmitter<ISeoPlatform> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onClick(mobile: boolean): void {
    this.isMobile = mobile;
    this.changePlatform.emit({ isMobile: mobile, isDesktop: !mobile } as ISeoPlatform);
  }

}
