import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SeoSnippetPreviewComponent } from './seo-snippet-preview.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SeoSnippetPreviewComponent],
  exports: [SeoSnippetPreviewComponent]
})
export class SeoSnippetPreviewModule { }
