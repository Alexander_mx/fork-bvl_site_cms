import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SeoFormModule } from './seo-form/seo-form.module';

@NgModule({
  imports: [
    CommonModule,
    SeoFormModule
  ],
  declarations: [],
  exports: [SeoFormModule]
})
export class SeoModule { }
