import { Component, OnInit } from '@angular/core';
import { SEOService } from '@bvl-admin/services';
import { IRuleSeoConfiguration, SEOConfiguration } from '@bvl-admin/statemanagement/models/seo.interface';

@Component({
  selector: 'admin-seo-valid-metadata',
  templateUrl: './seo-valid-metadata.component.html',
  styleUrls: ['./seo-valid-metadata.component.scss']
})
export class SeoValidMetadataComponent implements OnInit {

  seo: SEOConfiguration;
  listProblems: Array<IRuleSeoConfiguration> = [];
  listGood: Array<IRuleSeoConfiguration> = [];
  showCharacterNotAllowed = false;

  constructor(
    private seoService: SEOService
  ) { }

  ngOnInit(): void {
    this.seoService.configuration$.subscribe((seoConfiguration: SEOConfiguration) => {
      this.seo = seoConfiguration;
      this.listProblems = this.seo.rules.filter(r => !r.value);
      this.listGood = this.seo.rules.filter(r => r.value);
    });
  }

  changeKeyFocus($event: any): void {
    this.seoService.setKeywords({ focusKeyword: $event.target.value });
    this.validKeyword($event.target.value);
  }

  validKeyword(focusKeyword: string): void {
    const valid = focusKeyword.replace(/[&\/\\#,+();^~%.":*?<>{}]/g, 'character-not-allowed');
    this.showCharacterNotAllowed = valid.search('character-not-allowed') >= 0;
  }

}
