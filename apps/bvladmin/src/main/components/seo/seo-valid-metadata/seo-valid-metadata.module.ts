import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SeoValidMetadataComponent } from './seo-valid-metadata.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SeoValidMetadataComponent],
  exports: [SeoValidMetadataComponent]
})
export class SeoValidMetadataModule { }
