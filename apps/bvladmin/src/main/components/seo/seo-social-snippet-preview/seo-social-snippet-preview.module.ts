import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BvlInputModule, BvlTabsModule } from '@bvl-core/ui/lib/components';
import { GallerySelectModule } from '../../gallery-select/gallery-select.module';
import { SeoSocialSnippetPreviewComponent } from './seo-social-snippet-preview.component';

@NgModule({
  imports: [
    CommonModule,
    BvlInputModule,
    GallerySelectModule,
    FormsModule,
    BvlTabsModule
  ],
  declarations: [SeoSocialSnippetPreviewComponent],
  exports: [SeoSocialSnippetPreviewComponent]
})
export class SeoSocialSnippetPreviewModule { }
