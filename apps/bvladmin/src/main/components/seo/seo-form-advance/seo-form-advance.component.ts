import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { SEOService } from '@bvl-admin/services';
import { ADVANCED_ROBOTS_LIST, YES_NO_LIST } from '@bvl-admin/settings/constants/seo.constant';
import { SEOConfiguration } from '@bvl-admin/statemanagement/models/seo.interface';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'admin-seo-form-advance',
  templateUrl: './seo-form-advance.component.html',
  styleUrls: ['./seo-form-advance.component.scss']
})
export class SeoFormAdvanceComponent extends UnsubscribeOnDestroy implements OnInit {

  YES_NO_LIST = YES_NO_LIST;
  ADVANCED_ROBOTS_LIST = ADVANCED_ROBOTS_LIST;

  seo: SEOConfiguration;
  frmAdvanced: FormGroup;
  mIndex: AbstractControl;
  mFollow: AbstractControl;
  mAvancedRobots: FormGroup;
  mCanonical: AbstractControl;
  defaultNoIndex: string;

  constructor(
    private _seoService: SEOService,
    private _formBuilder: FormBuilder
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
    this._getConfiguration();
  }

  private _createForm(): void {
    this.frmAdvanced = this._formBuilder.group({
      mIndex: [''],
      mFollow: [''],
      mAvancedRobots: this._formBuilder.group(
        ADVANCED_ROBOTS_LIST.reduce((previous, current) => {
          previous[current.code] = [false];

          return previous;
        }, {})
      ),
      mCanonical: ['']
    });

    this.mIndex = this.frmAdvanced.get('mIndex');
    this.mFollow = this.frmAdvanced.get('mFollow');
    this.mAvancedRobots = this.frmAdvanced.get('mAvancedRobots') as FormGroup;
    this.mCanonical = this.frmAdvanced.get('mCanonical');
  }

  private _getConfiguration(): void {
    this._seoService.configuration$
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((seoConfiguration: SEOConfiguration) => {
        this.seo = seoConfiguration;
        this._setControls();
      });
  }

  private _setControls(): void {
    // Actualizamos el placeholder del noIndex con el valor por defecto(noindexSite)
    const noIndex = YES_NO_LIST.find(fv => fv.code === this.seo.noindexSite);
    this.defaultNoIndex = `Por defecto es Entradas, actualmente: ${(noIndex && noIndex.description) || ''}`;
    // Si existe el noIndex, lo mostramos por defecto en el select
    this.mIndex.setValue((this.seo.noindex) ? { code: this.seo.noindex } : '');
    // Actualizamos el noFollow
    this.mFollow.setValue(this.seo.nofollow);
    // Actualizamos el avancedRobots
    ADVANCED_ROBOTS_LIST.forEach(fv => {
      const value = !!(this.seo.avancedRobots || []).find(sv => sv === fv.code);
      this.mAvancedRobots.controls[fv.code].setValue(value);
    });
    // Actualizamos el canonical
    this.mCanonical.setValue(this.seo.canonical || '');
  }

  changeIndex(value: any): void {
    this._seoService.setNoIndex(value.code || value);
  }

  changeFollow(value: string): void {
    this._seoService.setNoFollow(value);
  }

  checkRobots(value: any): void {
    const avancedRobots = Object.keys(value)
                          .reduce((previous, current) => {
                            if (value[current]) {
                              previous.push(current);
                            }

                            return previous;
                          }, []);
    this._seoService.setAvancedRobots(avancedRobots);
  }

  changeCanonical(value: string): void {
    this._seoService.setCanonical(value);
  }

}
