import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { OrderByPipeModule } from '@bvl-core/shared/helpers/pipes';
import { BvlCheckboxModule, BvlInputModule, BvlRadioButtonModule, BvlSelectModule } from '@bvl-core/ui/lib/components';
import { SeoFormAdvanceComponent } from './seo-form-advance.component';

@NgModule({
  imports: [
    CommonModule,
    BvlSelectModule,
    BvlRadioButtonModule,
    BvlCheckboxModule,
    BvlInputModule,
    ReactiveFormsModule,
    OrderByPipeModule
  ],
  declarations: [SeoFormAdvanceComponent],
  exports: [SeoFormAdvanceComponent]
})
export class SeoFormAdvanceModule { }
