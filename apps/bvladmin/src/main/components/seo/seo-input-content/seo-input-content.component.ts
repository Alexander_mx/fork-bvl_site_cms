import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'admin-seo-input-content',
  templateUrl: './seo-input-content.component.html',
  styleUrls: ['./seo-input-content.component.scss']
})
export class SeoInputContentComponent implements OnInit {

  @ViewChild('contentWidth') span: any;
  @ViewChild('inputText') inputText: any;
  @Output() changeInput: EventEmitter<any> = new EventEmitter();
  @Input() content: string;
  @Input() last = false;

  constructor() { }

  ngOnInit(): void {
    this.resize();
  }

  inputContent(input: any): void {
    this.changeInput.emit(input);
    this.resize();
  }

  resize(): void {
    if (!this.last) {
      this.span.nativeElement.textContent = this.inputText.nativeElement.value;
      const offsetWidth = Number.parseInt(this.span.nativeElement.offsetWidth, 10);
      this.inputText.nativeElement.style.width = `${offsetWidth + 5}px`;
    }
  }

  focus(): void {
    this.inputText.nativeElement.focus();
  }

}
