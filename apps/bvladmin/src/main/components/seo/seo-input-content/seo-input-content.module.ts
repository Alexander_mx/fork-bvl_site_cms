import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SeoInputContentComponent } from './seo-input-content.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SeoInputContentComponent],
  exports: [SeoInputContentComponent]
})
export class SeoInputContentModule { }
