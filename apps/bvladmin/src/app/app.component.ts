import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'admin-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'bvladmin';

  constructor() { }

  ngOnInit(): void { }

}
