import { environment } from '@bvl-admin/environments/environment';

export class UserEndpoint {
  public static usersList = `${environment.API_URL}/user/list`;
  public static saveUser = `${environment.API_URL}/user/save`;
  public static updateUser = `${environment.API_URL}/user/update`;
  public static deleteUser = `${environment.API_URL}/user/delete`;
  public static bvlUsersList = `${environment.API_URL}/user/listUserAD`;
  public static userPermissions = `${environment.API_URL}/user/permision/getOptions`;
}
