import { environment } from '@bvl-admin/environments/environment';

export class RedirectEndpoint {
  public static created = `${environment.API_URL}/redirects`;
  public static updated = `${environment.API_URL}/redirects/{id}`;
  public static filter = `${environment.API_URL}/redirects/filters`;
  public static importExcel = `${environment.API_URL}/redirects/excel`;
}
