import { environment } from '@bvl-admin/environments/environment';

export class CommentEndpoint {
  public static saveComment = `${environment.API_URL}/comment`;
  public static commentsByIdPage = `${environment.API_URL}/comment/page/{idPage}`;
}
