import { environment } from '@bvl-admin/environments/environment';

export class GeneralEndpoint {
  public static states = `${environment.API_URL}/system/parameterSystem/findByCodeMasterSystem/1`;
}
