import { environment } from '@bvl-admin/environments/environment';

export class PermissionEndpoint {
  public static permissionsList = `${environment.API_URL}/permission/list`;
}
