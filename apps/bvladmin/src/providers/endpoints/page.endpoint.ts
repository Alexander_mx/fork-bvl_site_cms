import { environment } from '@bvl-admin/environments/environment';

export class PageEndpoint {
  public static siteMenu = `${environment.API_URL}/admin/page/listMenuOptions`;
  public static pagesList = `${environment.API_URL}/trays/page/list`;
  public static approvalsList = `${environment.API_URL}/trays/approbations/list`;
  public static templateList = `${environment.API_URL}/trays/template/list`;
  public static templateByCode = `${environment.API_URL}/trays/page/loadViewTemplateForEdition/{templateCode}`;
  public static savePage = `${environment.API_URL}/trays/page/save`;
  public static pageById = `${environment.API_URL}/trays/page/getPage/{idPage}`;
  public static pagePublishedById = `${environment.API_URL}/trays/page/getPublishing/{idPage}`;
  public static categoriesList = `${environment.API_URL}/pageHeader/categories`;
  public static editPage = `${environment.API_URL}/trays/page/editPage/{idPage}`;
  public static publishPage = `${environment.API_URL}/trays/page/publish`;
  public static requestPublication = `${environment.API_URL}/trays/page/requestPublishing`;
  public static cancelPageChanges = `${environment.API_URL}/trays/page/cancelPage`;
  public static observePage = `${environment.API_URL}/trays/page/observedPage`;
  public static revertPage = `${environment.API_URL}/trays/page/revertPage`;
  public static pageHisotry = `${environment.API_URL}/page/history/list/{idPage}`;
  public static restorePageHisotry = `${environment.API_URL}/page/history/restore/{id}`;
  public static getPagesAutocopleteByPath = `${environment.API_URL}/trays/page/autocomplete`;
  public static getPagesAutocopleteByPathAndTitle = `${environment.API_URL}/trays/page/autocomplete/filters`;
  public static deletePage = `${environment.API_URL}/trays/page/deletePage/{idPage}`;
}
