import { environment } from '@bvl-admin/environments/environment';

export class RoleEndpoint {
  public static rolesList = `${environment.API_URL}/role/permission/list`;
  public static rolesListPermissionsDetail = `${environment.API_URL}/role/permission/list/detail`;
  public static saveRole = `${environment.API_URL}/role/save`;
  public static updateRole = `${environment.API_URL}/role/update`;
  public static deleteRole = `${environment.API_URL}/role/permission/delete`;
}
