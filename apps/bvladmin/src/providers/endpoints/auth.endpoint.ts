import { environment } from '@bvl-admin/environments/environment';

export class AuthEndpoint {
  public static login = `${environment.API_URL}/api/authenticate`;
}
