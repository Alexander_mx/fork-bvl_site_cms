import { environment } from '@bvl-admin/environments/environment';

export class CategoriesEndpoint {
  public static categories = `${environment.API_URL}/categories`;
  public static save = `${environment.API_URL}/categories/save`;
  public static delete = `${environment.API_URL}/categories/delete`;
}
