import { environment } from '@bvl-admin/environments/environment';

export class SiteMenuEndpoint {
  public static getMenu = `${environment.API_URL}/menus/list`;
  public static addMenu = `${environment.API_URL}/menus/save`;
  public static updateMenu = `${environment.API_URL}/menus/update`;
  public static deleteMenu = `${environment.API_URL}/menus/delete`;
}
