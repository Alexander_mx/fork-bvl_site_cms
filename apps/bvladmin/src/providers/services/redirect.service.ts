import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { RedirectEndpoint } from '@bvl-admin/endpoints';
import { IRedirect, IRedirectFilter, IRedirectPagination } from '@bvl-admin/statemanagement/models/redirect.interface';
import { ApiService } from '@bvl-core/shared/helpers/api';

@Injectable({
  providedIn: 'root'
})
export class RedirectService {

  constructor(
    private _apiService: ApiService
  ) { }

  add(redirect: IRedirect, preloader: boolean = true): Observable<IRedirect> {
    return this._apiService.post(RedirectEndpoint.created, redirect, { preloader });
  }

  update(id: string, redirect: IRedirect, preloader: boolean = true): Observable<IRedirect> {
    return this._apiService.put(RedirectEndpoint.updated, redirect, { preloader, params: { id } });
  }

  filter(params: IRedirectFilter, preloader: boolean = true): Observable<IRedirectPagination> {
    return this._apiService.get(RedirectEndpoint.filter, { preloader, params });
  }

  importExcel(file: File, preloader: boolean = true): Observable<IRedirect> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('type', file.type);

    return this._apiService.post(RedirectEndpoint.importExcel, formData, { preloader });
  }

}
