import { Injectable } from '@angular/core';
import { CategoriesEndpoint } from '@bvl-admin/endpoints/categories.endpoint';
import { ICategories } from '@bvl-admin/statemanagement/models/categories.interface';
import { ApiService } from '@bvl-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(
    private _apiService: ApiService
  ) { }

  getCategories(params, showSpin: boolean = true): Observable<ICategories> {
    return this._apiService.get(CategoriesEndpoint.categories, { params, preloader: showSpin });
  }
  saveCategory(category, showSpin: boolean = true): Observable<ICategories> {
    return this._apiService.post(CategoriesEndpoint.save, category, { preloader: showSpin });
  }
  deleteCategory(categoryId, showSpin: boolean = true): Observable<ICategories> {
    return this._apiService.post(CategoriesEndpoint.delete, categoryId, { preloader: showSpin });
  }

}
