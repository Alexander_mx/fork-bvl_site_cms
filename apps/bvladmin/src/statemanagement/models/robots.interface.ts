import { IObservableArray } from '@bvl-core/shared/helpers/models/general.model';

export interface IRobotsRequest {
  userAgent: string;
  type: string; // Allowed | Disallowed | SiteMap
  value: string;
}

export interface IRobots {
  id: string;
  userAgent: string;
  type: string; // Allowed | Disallowed | SiteMap
  value: string;
}

export interface IUserAgent {
  id: string;
  name: string;
  code: string;
}

export interface IListRobots extends Array<IRobots> {}

export interface IObservableArrayRobots extends IObservableArray<IRobots> {}

export interface IListUserAgent extends Array<IUserAgent> {}

export interface IObservableArrayUserAgent extends IObservableArray<IUserAgent> {}
