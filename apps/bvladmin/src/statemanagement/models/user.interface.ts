import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';
import { IRoleResponse } from './role.interface';

export interface IGeneralUser {
  username: string;
  fullname: string;
}

export interface IUserPaginationRequest extends IGeneralSearchRequest {
  fullname: string;
}

export interface IUserResponse extends IGeneralUser {
  status: number;
  role: IRoleResponse;
}

export interface IUserView extends IUserResponse {
  disabled?: boolean;
  actionType?: string;
  checked?: boolean;
}

export interface IUserPaginationResponse extends IGeneralSearchResponse {
  content: Array<IUserResponse>;
}

export interface IUserPaginationView extends IGeneralSearchResponse {
  content: Array<IUserView>;
}

export interface ISaveUserRequest extends IGeneralUser {
  roleCode: string;
}

export interface IDeleteUserRequest {
  username: string;
}

export interface IBvlUserResponse extends IGeneralUser {
  firstName: string;
  lastName: string;
  email: string;
  createdDate: string;
  updatedDate: string;
}
