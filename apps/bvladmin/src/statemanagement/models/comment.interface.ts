export interface ISaveCommentRequest {
  idPage: string;
  idComponent: string;
  note: string;
  language: string;
}

export interface ICommentResponse extends ISaveCommentRequest {
  username: string;
}
