import { IComponent } from '@bvl-admin/components/dynamic-form';
import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';

export interface IComponentPaginationRequest extends IGeneralSearchRequest {
  componentName: string;
  status: Array<number>;
  userList: Array<string>;
  type?: number;
}

export interface IComponentItemResponse {
  id?: string;
  code: string;
  name: string;
  username: string;
  status: number;
  type: number;
  lastUpdate: string;
  languages: Array<string>;
}

export interface IComponentPaginationResponse extends IGeneralSearchResponse {
  content: Array<IComponentItemResponse>;
}

export interface IComponentResponse extends IComponent { }

export interface ILanguage {
  language: string;
  properties: Array<IProperty>;
}

export interface IProperty {
  code: string;
  value?: any;
}

export interface IComponentValueResponse {
  id?: string;
  code: string;
  name?: string;
  languages: Array<ILanguage>;
  lastUpdate?: string | Date;
}

export interface ISaveComponentValueRequest extends IComponentValueResponse { }
