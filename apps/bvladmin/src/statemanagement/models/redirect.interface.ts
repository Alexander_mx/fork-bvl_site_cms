import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';

export interface IRedirect {
  id?: string;
  oldUrl: string;
  newUrl: string;
  username?: string;
  updateDate?: string;
  state?: any;
}

export interface IRedirectFilter extends IRedirect, IGeneralSearchRequest { }

export interface IRedirectPagination extends IGeneralSearchResponse {
  content: Array<IRedirect>;
}
