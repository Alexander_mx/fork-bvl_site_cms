import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';

export interface ITrayItemButtonPermission {
  rolesCode?: Array<string>;
  pageTypes?: Array<number>;
  types?: Array<number>;
  statesCode?: Array<number>;
  editorTypes?: Array<number>;
}

export interface ITrayItemButton {
  description?: string;
  icon?: string;
  actionType?: string;
  permissions?: Array<ITrayItemButtonPermission>;
}

export interface IPageItemResponse {
  idPage: string;
  name: string;
  parentPath: string;
  username: string;
  status: number;
  pageType: number;
  type: number;
  lastUpdate: string;
  languages: Array<string>;
  commentsQuantity: number;
}

export interface ISiteMenuResponse extends IPageItemResponse {
  subMenu: Array<IPageItemResponse>;
}

export interface IPagePaginationRequest extends IGeneralSearchRequest {
  namePage: string;
  status: Array<number>;
  userList: Array<string>;
  startDate: string;
  endDate: string;
}

export interface IPageApprovalPaginationRequest extends IGeneralSearchRequest {
  namePage: string;
  userList: Array<string>;
}

export interface IPagePaginationResponse extends IGeneralSearchResponse {
  content: Array<IPageItemResponse>;
}

export interface IContent {
  html: string;
  refComponents: Array<string>;
}

export interface IMetadata {
  image?: string;
  title?: string;
  titleSnippet?: string;
  description?: string;
  ogSitename?: string;
  ogType?: string;
  ogUrl?: string;
  ogTitle?: string;
  ogDescription?: string;
  ogImage?: string;
  twitterCard?: string;
  twitterTitle?: string;
  twitterDescription?: string;
  twitterImage?: string;
  canonicalLink?: string;
  keywords?: string;
  slug?: string;
  noindex?: string;
  nofollow?: string;
  avancedRobots?: Array<string>;
}

export interface IPageByLanguage {
  language: string;
  canonical: string;
  path: string;
  parentPathType?: string;
  pagePath: string;
  parentPath: string;
  metadata: IMetadata;
  social: string;
  title: string;
  category: string;
  tags: Array<string>;
  content: IContent;
}

export interface ISavePageRequest {
  name: string;
  pageType: number;
  type: number;
  status: number;
  languages: Array<IPageByLanguage>;
}

export interface IPageResponse extends ISavePageRequest {
  id?: string;
  idPage: string;
  username: string;
  publishingDate?: string;
}

export interface IPageAutocomplete {
  idPage: string;
  path: string;
}

export interface IPageAutocompleteResponse extends Array<IPageAutocomplete> {}

export interface ICategoryByLanguage {
  category: string;
  language: string;
  path: string;
}

export interface ICategory {
  id: string;
  languages: Array<ICategoryByLanguage>;
}
