import { Observable } from 'rxjs';
import { IPermissionResponse } from './permission.interface';

export interface IUserProfile {
  rememberMe?: boolean;
  username?: string;
  fullname?: string;
}

export interface IUserPermissionsResponse {
  username: string;
  fullname: string;
  roleCode: string;
  roleName: string;
  rolDescription: string;
  permissionList: Array<IPermissionResponse>;
}

export interface IConfiguration {
  sitename: string;
  company: string;
  logo: string;
  icon: string;
  copyright: string;
  css: string;
  languages: Array<string>;
  defaultLanguage: string;
  metadata?: IMetadata;
  socialNetworking?: ISocialNetworking;
  keywords: string;
  domain?: string;
  fontIcons?: string;
  homePageId?: string;
  keys?: IKeysConfiguration;
}

export interface IMetadata {
  title?: string;
  description?: string;
  robots?: string;
  nositelinksearchbox?: string;
  nosnippet?: string;
  autor?: string;
  subject?: string;
  language?: string;
  revisitafter?: string;
  titleSnippet?: string;
  ogType?: string;
  ogUrl?: string;
  ogTitle?: string;
  ogDescription?: string;
  ogImage?: string;
  ogSitename?: string;
  twitterCard?: string;
  twitterTitle?: string;
  twitterDescription?: string;
  twitterImage?: string;
  canonicalLink?: string;
  noindex?: string;
  nofollow?: string;
  avancedRobots?: Array<string>;
}

export interface ISocialNetworking {
  urlFacebook: string;
  urlTwitter: string;
  urlInstagram: string;
  urlLinkedin: string;
  urlPinterest: string;
  urlYoutube: string;
  urlFlickr: string;
}

export interface ISiteIdentityRequest {
  sitename: string;
  description: string;
  iconSite: string;
}

export interface IKeysConfiguration {
  googleMaps: string;
  gtm: string;
}

export interface IObservableConfiguration extends Observable<IConfiguration> {}
