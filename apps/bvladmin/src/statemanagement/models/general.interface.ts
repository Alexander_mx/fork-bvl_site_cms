export interface IGeneralSearchRequest {
  size?: number;
  page?: number;
}

export interface IGeneralSearchResponse {
  totalElements: number;
  totalPages: number;
  first: boolean;
  last: boolean;
}

export interface ISearchFormView {
  search: string;
  users: Array<string>;
  statePages: Array<number>;
  startDate: string;
  endDate: string;
}

export interface IStateResponse {
  codeParameterSystem: number;
  parameterName: string;
}

export interface IStateView extends IStateResponse {
  checked?: boolean;
}

export interface IMenu {
  code: string;
  name: string;
  route: string;
  icon: string;
}
