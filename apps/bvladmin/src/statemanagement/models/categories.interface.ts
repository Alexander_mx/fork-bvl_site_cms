import { IGeneralSearchResponse } from './general.interface';

export interface ICategories  extends IGeneralSearchResponse {
  content: ICategory;
}

interface ICategory {
  id: number;
  languages: Array<{
      category: string;
      language: string;
      path: string;
  }>;
  state: number;
}
