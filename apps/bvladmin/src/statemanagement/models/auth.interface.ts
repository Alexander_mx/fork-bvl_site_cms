export interface IAuthRequest {
  username: string;
  password: string;
  rememberMe: boolean;
}

export interface IAuthResponse {
  id_token: string;
}
