export interface IPermissionResponse {
  permissionCode: string;
  permissionName: string;
  status: number;
}
