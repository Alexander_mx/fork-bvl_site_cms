export { EditorLang } from './editor.lang';
export { ComponentLang } from './component.lang';
export { FileLang } from './file.lang';
export { PageLang } from './page.lang';
export { GeneralLang } from './general.lang';
export { RoleLang } from './role.lang';
export { UserLang } from './user.lang';
export { LoginLang } from './login.lang';
