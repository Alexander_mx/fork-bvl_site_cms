export const EditorLang = {
  labels: {
    pageTitle: 'Título de la página',
    permanentLink: 'Enlace permanente',
    comment: 'Comentario',
    writeACommentHere: 'Escribe aquí un comentario...'
  }
};
