export const STORAGE_KEY = {
  userProfile: 'USER_PROFILE',
  userPermissions: 'USER_PERMISSIONS',
  menu: 'MENU',
  page: 'PAGE',
  pageEditor: 'PAGE_EDITOR'
};
