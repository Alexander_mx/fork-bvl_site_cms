import { SITE_MENU_ACTION } from '@bvl-core/shared/helpers/util/menu';
import { ACTION_TYPES } from './general.constant';

export const SITE_MENU_ACTIONS = [
  {
    code: SITE_MENU_ACTION.REDIRECT,
    description: 'Redirección'
  }
];

export const ITEM_MENU_BUTTONS = [
  {
    actionType: ACTION_TYPES.edit,
    description: 'Editar',
    icon: 'la-edit'
  },
  {
    actionType: ACTION_TYPES.delete,
    description: 'Eliminar',
    icon: 'la-trash'
  },
  {
    actionType: ACTION_TYPES.add,
    description: 'Agregar',
    icon: 'la-plus'
  }
];
