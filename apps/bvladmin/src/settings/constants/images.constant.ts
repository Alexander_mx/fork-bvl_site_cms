import { ASSETS_PATH } from './general.constant';

export const IMAGES = {
  logoBvl: {
    src: `${ASSETS_PATH.images}bvl-brand.png`,
    alt: 'Logo BVL'
  }
};
