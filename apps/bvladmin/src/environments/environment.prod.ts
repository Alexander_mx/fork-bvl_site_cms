import { ENVIRONMENT } from '@bvl-core/shared/helpers/environment';

export const environment = ENVIRONMENT.prod;
