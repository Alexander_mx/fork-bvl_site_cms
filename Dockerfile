### STAGE 1: Build ###

# We label our stage as 'builder'
FROM node:10-alpine as builder

ARG env=dev

COPY package.json package-lock.json ./

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app

## Choosing a WORKDIR
WORKDIR /ng-app

## Copy everything from host to /ng-app in the container
COPY . .

## Build the angular app in production mode and store the artifacts in dist folder
RUN npm run build:env-server --env=$env

RUN node --max_old_space_size=5048 &&  \
    npm run build:bvlsite:$env

RUN node --max_old_space_size=5048 && \
    npm run build:server:$env

RUN node --max_old_space_size=5048 && \
    npm run webpack:server

RUN rm -R modules
RUN rm -R apps
RUN rm -R core
RUN rm -R patternlab

EXPOSE 80:4000
CMD [ "node", "dist/server" ]