module.exports = {
  externals: {
    "@angular/common": "ng.common",
    "@angular/common/http": "ng.common.http",
    "@angular/core": "ng.core",
    "@angular/elements": "ng.elements",
    "@angular/platform-browser": "ng.platformBrowser",
    "@bvl/library": "bvl.library",
    "@ng-bootstrap/ng-bootstrap": "ngb",
    highcharts: "Highcharts",
    "rxjs/operators": "rxjs.operators",
    rxjs: "rxjs"
  }
};
