# BvlAdmin

## Pre-requisitos

1. Tener instalado lo siguiente (**en el mismo orden**):

- [Ruby][rubyInstall]

- [Node][nodeInstall]

- [SASS][sassInstall]
  ```sh
  npm install -g sass
  ```
- [Angular CLI][angularCliInstall]
  ```sh
  npm install -g @angular/cli
  ```

## Instalación

#### 1. Clonar el proyecto

Ejecutar el siguiente comando:
```sh
git clone https://MY_USER@bitbucket.org/mxperu/bvl_site_cms.git
```

Una vez clonado el proyecto ingresar a la carpeta raíz (bvl_site_cms) y posicionarse en la rama master:
```sh
git checkout master
```

#### 2. Instalar dependencias

Ejecutar el siguiente comando:
```sh
npm install
o
npm i
```

## Build (compilación del proyecto)

Ejecutar el comando:
```sh
ng build bvladmin --prod
```
El cual realizará la compilación del proyecto que se almacenará en la carpeta `dist/`


[rubyInstall]: <https://www.ruby-lang.org/es/documentation/installation/>
[sassInstall]: <http://sass-lang.com/install>
[nodeInstall]: <https://github.com/creationix/nvm#installation>
[angularCliInstall]: <https://cli.angular.io/>

## Subir compilado a bucket s3
### Instalar AWS CLI
Puedes encontrar la información necesaria para instalar AWS CLI
[aquí](https://docs.aws.amazon.com/es_es/cli/latest/userguide/cli-chap-install.html)

### Configurar credenciales
> aws configure

> AWS Access Key ID: (QA_AWS_Access_Key_ID)

> AWS Secret Access Key: (QA_AWS_Secret Access Key)

> Default region name: us-east-1

> Default output format: json

### Listar buckets
> aws s3 ls

### Subir archivos
Primero es necesario encontrarse dentro de la carpeta dist/, se puede obtener información de nuestra ubicación escribiendo:

> pwd

Una vez dentro de dist hacer uso de AWS CLI para subir los archivos al bucket asignado. 
> aws s3 cp . s3://cdn.qas.bvl.com.pe

Verificar que los archivos han subido correctamente revisando los mensajes en consola.
