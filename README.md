## Configuración Culqi / AWS Amplify(cognito)

Ingresar al archivo **`environment.js`** ubicado en **`core/shared/src/helpers/environment/environment.js`** y modificar los siguientes **`atributos`** con su respectiva **`KEY|CONFIG por environment:`**
```javascript
[prod|dev|qa]: {
  ...
  CULQI_PUBLIC_KEY: '', <= Ingresar el PUBLIC_KEY_BVL
  AWS_AMPLIFY_CONFIG: {} <= Ingresar la CONFIG_BVL
}
```

## Build (admin/site)

Para generar el build del admin y site

Ejecutar el siguiente comando:

- QA:
```sh
npm run build:qa
```

- PROD:
```sh
npm run build:prod
```

El cual realizará la compilación del proyecto que se almacenará en la carpeta **`dist/`**

