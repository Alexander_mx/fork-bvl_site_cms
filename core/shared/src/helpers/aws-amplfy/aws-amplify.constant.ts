export const COGNITO_ERROR_CODE = {
  NotAuthorizedException: 'El email o contraseña son incorrectos',
  UserNotFoundException: 'El email ingresado no se encuentra registrado.',
  UserNotConfirmedException: 'El email se encuentra en pendiente de confirmación',
  UsernameExistsException: 'El email ingresado ya se encuentra registrado',
  CodeMismatchException: 'El código de verifiación es incorrecto',
  ExpiredCodeException: 'El código de verificaión ha expirado',
  GeneralException: 'No se puede realizar la operación, vuelva a intentarlo en unos minutos',
  // tslint:disable-next-line:max-line-length
  InvalidPasswordException : 'La contraseña debe tener mínimo 8 caracteres, letras en mayúscula, letras en minúscula, números y carácteres especiales'
};
