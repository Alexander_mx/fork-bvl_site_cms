export interface ICognitoError {
  code: string;
  message: string;
  name: string;
}
