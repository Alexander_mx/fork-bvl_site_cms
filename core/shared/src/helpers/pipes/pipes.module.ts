import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FileSizePipe } from './file-size.pipe';
import { GroupByPipe } from './group-by.pipe';
import { SafeHtmlPipe } from './safe-html.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FileSizePipe,
    GroupByPipe,
    SafeHtmlPipe
  ],
  exports: [
    FileSizePipe,
    GroupByPipe,
    SafeHtmlPipe
  ]
})
export class PipesModule { }
