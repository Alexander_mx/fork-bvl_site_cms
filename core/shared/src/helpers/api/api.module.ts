import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NotificationModule } from './../../helpers/notification';
import { SpinnerModule } from './../../helpers/spinner';
import { ApiService } from './api.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule
  ],
  providers: [
    ApiService
  ],
  exports: [
    HttpClientModule
  ]
})
export class ApiModule { }
