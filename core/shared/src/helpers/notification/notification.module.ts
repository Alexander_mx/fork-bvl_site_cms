import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationService } from './notification.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    NotificationService
  ]
})
export class NotificationModule { }
