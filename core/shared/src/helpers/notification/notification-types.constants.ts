export const NOTIFICATION_TYPES = {
  success: 'S',
  alert: 'A',
  warning: 'W',
  message: 'M',
  error: 'E'
};
