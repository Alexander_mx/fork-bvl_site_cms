import { PENtoUSD } from './../../settings/constants/general.constants';

export const getCurrencySymbol = (symbol: string): string => ((symbol && symbol.trim()) || 'S/');
export const isDollar = (symbol: string): boolean => (symbol === '$');
export const convertToDollars = (amount: number): number => (amount * PENtoUSD);
