export const getPathArgument = (argument: string): string => {
   if (location.search.indexOf(argument)) {
      const url = new URLSearchParams(location.search);

      return url.get(argument);
   }
};
