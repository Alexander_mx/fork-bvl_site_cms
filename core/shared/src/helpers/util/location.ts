export const getOriginPath = (): string => (`${location.protocol}//${location.hostname}${location.port ? `:${location.port}` : ''}`);
export const getPath = (path: string): string => (`${getOriginPath()}/${path}`);
export const getPathWithParam = (path: string): string => (`${getPath(path)}${location.search}${location.hash}`);
export const isFile = (path: string): boolean => {
  const ANY_EXTENSION_REGEX = /\.[\w]+$/gi;

  return ANY_EXTENSION_REGEX.test(path);
};
