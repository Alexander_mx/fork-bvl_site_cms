import { LANGUAGE_TYPES_CODE } from './../../../settings/constants/general.constants';
import { SITE_MENU_ACTION, SITE_MENU_TYPE } from './menu.constant';

export interface ISiteMenu {
  id?: string;
  idPage: string;
  parentId: string;
  name: string;
  path: string;
  language: LANGUAGE_TYPES_CODE;
  type: SITE_MENU_TYPE;
  action: SITE_MENU_ACTION;
  order: number;
  flgVisible: boolean;
  owner?: any;
  state?: number;
  user?: string;
  createdDate?: string;
  updatedDate?: string;

  collapse?: boolean;
  children?: Array<ISiteMenu>;
  active?: boolean;
}
