export enum SITE_MENU_ACTION {
  REDIRECT = 'redirect'
}

export enum SITE_MENU_TYPE {
  HEADER = 'header',
  FOOTER = 'footer'
}
