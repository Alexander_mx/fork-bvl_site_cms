export const pagination = (list: Array<any>, page: number, paginationSize: number): Array<any> => {
  const newList = JSON.parse(JSON.stringify(list));
  const start = (page - 1) * paginationSize;
  const end = start + paginationSize;

  return newList.slice(start, end);
};
