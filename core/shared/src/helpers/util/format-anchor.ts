export const formatAnchor = (text: string): string => {
   return text.split(' ')
      .join('-')
      .toLowerCase()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]|¿|\?/gi, '');
};
