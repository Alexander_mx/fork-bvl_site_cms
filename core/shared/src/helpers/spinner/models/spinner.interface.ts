export interface ISpinnerConfig {
  spinnerClass?: string;
  spinnerIconClass?: string;
}

export interface IContainer {
  element: any;
  class: string;
}
