export const FN_ERROR_MESSAGES = {
  es: {
    // Generals
    min: (error: { min: number; actual: number }) => `El valor mínimo es ${error.min}`,
    max: (error: { max: number; actual: number }) => `El valor máximo es ${error.max}`,
    required: (error: boolean) => `El campo es requerido`,
    email: (error: boolean) => `Ingrese un correo electrónico válido`,
    minlength: (error: { requiredLength: number; actualLength: number }) =>
      `El campo ingresado debe tener al menos ${error.requiredLength} caracteres`,
    maxlength: (error: { requiredLength: number; actualLength: number }) =>
      `El campo ingresado debe tener como máximo ${error.requiredLength} caracteres`,
    pattern: (error: { requiredPattern: any; actualValue: any }) => `No cumple con el formato solicitado`,
    // Custom
    onlyNumber: (error: boolean) => `Solo es permitido números`,
    onlyLetter: (error: boolean) => `Solo es permitido letras`,
    alphanumeric: (error: boolean) => `Solo es permitido alfanumérico`,
    passwordPolicy: (error: boolean) => `La contraseña no cumple con las políticas establecidas`,
    confirmPassword: (error: boolean) => `Las contraseñas no coincide`,
    phoneNumber: (error: boolean) => `Sólo puede ingresar números y guiones`
  },
  en: {
    // Generals
    min: (error: { min: number; actual: number }) => `El valor mínimo es ${error.min}`,
    max: (error: { max: number; actual: number }) => `El valor máximo es ${error.max}`,
    required: (error: boolean) => `El campo es requerido`,
    email: (error: boolean) => `Ingrese un correo electrónico válido`,
    minlength: (error: { requiredLength: number; actualLength: number }) =>
      `El campo ingresado debe tener al menos ${error.requiredLength} caracteres`,
    maxlength: (error: { requiredLength: number; actualLength: number }) =>
      `El campo ingresado debe tener como máximo ${error.requiredLength} caracteres`,
    pattern: (error: { requiredPattern: any; actualValue: any }) => `No cumple con el formato solicitado`,
    // Custom
    onlyNumber: (error: boolean) => `Solo es permitido números`,
    onlyLetter: (error: boolean) => `Solo es permitido letras`,
    alphanumeric: (error: boolean) => `Solo es permitido alfanumérico`,
    passwordPolicy: (error: boolean) => `La contraseña no cumple con las políticas establecidas`,
    confirmPassword: (error: boolean) => `Las contraseñas no coincide`,
    phoneNumber: (error: boolean) => `Sólo puede ingresar números y guiones`
  }
};
