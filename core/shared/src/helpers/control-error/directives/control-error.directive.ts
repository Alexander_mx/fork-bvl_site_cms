import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Host,
  Input,
  OnInit,
  Optional,
  ViewContainerRef
} from '@angular/core';
import { AbstractControl, NgControl } from '@angular/forms';
import { EMPTY, merge, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { getStorageLanguage } from './../../../helpers/util/storage-language';
import { UnsubscribeOnDestroy } from './../../util';
import { FN_ERROR_MESSAGES } from './../constants/error-message.constant';
import { ControlErrorComponent } from './../shared-control-error/control-error.component';
import { ControlErrorContainerDirective } from './control-error-container.directive';
import { FormSubmitDirective } from './form-submit.directive';

@Directive({
  selector: '[sharedControlError], [formControl], [formControlName]'
})
export class ControlErrorDirective extends UnsubscribeOnDestroy implements OnInit {

  @Input() customErrorMessages: {};

  componentRef: ComponentRef<ControlErrorComponent>;
  container: ViewContainerRef;
  submit$: Observable<Event>;

  get control(): AbstractControl {
    return this._ngControl.control;
  }

  constructor(
    private _viewContainerRef: ViewContainerRef,
    private _componentFactoryResolver: ComponentFactoryResolver,
    @Optional() private _controlErrorContainer: ControlErrorContainerDirective,
    @Optional() @Host() private _formSubmit: FormSubmitDirective,
    private _ngControl: NgControl
  ) {
    super();
    this.customErrorMessages = {};
    this.container = this._controlErrorContainer
                      ? this._controlErrorContainer.viewContainerRef
                      : this._viewContainerRef;
    this.submit$ = (this._formSubmit)
                      ? this._formSubmit.submit$
                      : EMPTY;
  }

  ngOnInit(): void {
    merge(
      this.submit$,
      this.control.valueChanges
    )
    .pipe(
      takeUntil(this.unsubscribeDestroy$)
    )
    .subscribe(() => {
      const language = getStorageLanguage();
      let errorMessage = null;
      const controlErrors = this.control.errors;
      if (this.control.touched && controlErrors) {
        const firstKey = Object.keys(controlErrors)[0],
              fnErrorMessage = FN_ERROR_MESSAGES[language][firstKey],
              customErrorMessages = (this.customErrorMessages[firstKey])
                                      ? this.customErrorMessages[firstKey][language]
                                      : null;
        errorMessage = customErrorMessages || fnErrorMessage(controlErrors[firstKey]) || '';
      }
      this.setErrorMessage(errorMessage);
    });
  }

  setErrorMessage(message: string): void {
    if (!this.componentRef) {
      const factory = this._componentFactoryResolver.resolveComponentFactory(ControlErrorComponent);
      this.componentRef = this.container.createComponent(factory);
    }
    this.componentRef.instance.message = message;
  }

}
