import { Directive, ElementRef } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: 'form'
})
export class FormSubmitDirective {

  get element(): HTMLFormElement {
    return this._host.nativeElement;
  }

  submit$: Observable<Event>;

  constructor(
    private _host: ElementRef<HTMLFormElement>
  ) {
    this.submit$ = fromEvent(this.element, 'submit');
  }

}
