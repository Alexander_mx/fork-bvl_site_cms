import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[sharedControlErrorContainer]'
})
export class ControlErrorContainerDirective {

  constructor(
    public viewContainerRef: ViewContainerRef
  ) { }

}
