import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'shared-control-error',
  templateUrl: './control-error.component.html',
  styleUrls: ['./control-error.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlErrorComponent implements OnInit {

  showControl: boolean;

  protected _message: string;
  @Input()
  get message(): string {
    return this._message;
  }
  set message(value: string) {
    if (value !== this._message) {
      this._message = value;
      this.showControl = !!value;
      this._changeDetectorRef.detectChanges();
    }
  }

  constructor(
    private _changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void { }

}
