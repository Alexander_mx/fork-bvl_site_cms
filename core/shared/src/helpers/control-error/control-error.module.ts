import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ControlErrorContainerDirective, ControlErrorDirective, FormSubmitDirective } from './directives';
import { ControlErrorComponent } from './shared-control-error/control-error.component';

@NgModule({
  declarations: [
    ControlErrorDirective,
    ControlErrorComponent,
    ControlErrorContainerDirective,
    FormSubmitDirective
  ],
  imports: [
    CommonModule
  ],
  entryComponents: [ControlErrorComponent],
  exports: [
    ControlErrorDirective,
    ControlErrorContainerDirective,
    FormSubmitDirective
  ]
})
export class ControlErrorModule { }
