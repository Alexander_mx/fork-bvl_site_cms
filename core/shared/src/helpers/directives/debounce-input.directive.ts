import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, NgModule, Output } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[debounceInput]'
})
export class DebounceInputDirective implements AfterViewInit {

  @Output() debounceInput: EventEmitter<any> = new EventEmitter<any>();
  @Input() debounce = 500;

  constructor(private elm: ElementRef) { }

  ngAfterViewInit(): void {
    this.debounceEvent();
  }

  private debounceEvent(): void {
    fromEvent(this.elm.nativeElement, 'keyup')
      .pipe(
        // get value
        map((event: any) => {
          return event.target.value;
        })
        // Time in milliseconds between key events
        , debounceTime(this.debounce)
        // If previous query is diffent from current
        , distinctUntilChanged()
        // subscription for response
      )
      .subscribe((text: string) => {
        this.debounceInput.next(text);
      });
  }

}

@NgModule({ declarations: [DebounceInputDirective], exports: [DebounceInputDirective] })
export class DebounceInputDirectiveModule { }
