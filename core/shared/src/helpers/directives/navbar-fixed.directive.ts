import { Directive, ElementRef, EventEmitter, HostListener, Input, NgModule, OnChanges, Output, Renderer2 } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[navbarFixed]'
})
export class NavbarFixedDirective implements OnChanges {

  @Output() fixed: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() navbarFixed: string;
  @Input() offsetTop = 0;
  @Input() active = true;

  constructor(
    private elementRef: ElementRef,
    private renderer2: Renderer2
  ) { }

  @HostListener('window:scroll', ['$event']) onWindowScroll(): void {
    this.handlerClass();
  }

  ngOnChanges(): void {
    this.handlerClass();
  }

  private handlerClass(): void {
    const display = window.pageYOffset > this.offsetTop;
    this.renderer2[display && this.active ? 'addClass' : 'removeClass'](this.elementRef.nativeElement, this.navbarFixed || '');
    this.fixed.next(display && this.active);
  }

}

@NgModule({ declarations: [NavbarFixedDirective], exports: [NavbarFixedDirective] })
export class NavbarFixedDirectiveModule { }
