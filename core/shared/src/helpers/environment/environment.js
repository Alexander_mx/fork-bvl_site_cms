module.exports =  {
  prod: {
    code: 'prod',
    production: true,
    API_URL: 'https://loadbalancerprod.bvl.com.pe',
    WC_BUNDLE_PATH: 'assets/web-components',
    CULQI_PUBLIC_KEY: 'pk_test_kI7Rs2cG8iJhI3gl',
    AWS_AMPLIFY_CONFIG: {
      aws_project_region: 'us-east-1',
      aws_cognito_identity_pool_id: 'us-east-1:e43f994d-6e91-4535-9410-93eda3c05564',
      aws_cognito_region: 'us-east-1',
      aws_user_pools_id: 'us-east-1_IWAVH1Yys',
      aws_user_pools_web_client_id: '5sdmgc7dfdt1pi7gbocce9jdla',
      oauth: {}
    },
    RECAPTCHA_KEY: '6LcqWMcUAAAAAL-4TUqqi06t-A6wTZVi__AZVaGg'
  },
  dev: {
    code: 'dev',
    production: false,
    API_URL: 'https://loadbalancerdev.bvl.com.pe',
    WC_BUNDLE_PATH: 'assets/web-components/dev',
    CULQI_PUBLIC_KEY: 'pk_test_kI7Rs2cG8iJhI3gl',
    AWS_AMPLIFY_CONFIG: {
      aws_project_region: 'us-east-1',
      aws_cognito_identity_pool_id: 'us-east-1:74df0dbd-7b3c-498f-b727-1c7a209fac7e',
      aws_cognito_region: 'us-east-1',
      aws_user_pools_id: 'us-east-1_dXQGE6iIE',
      aws_user_pools_web_client_id: '61o3aqajcobu40v0tl42svbqid',
      oauth: {}
    },
    RECAPTCHA_KEY: '6LcqWMcUAAAAAL-4TUqqi06t-A6wTZVi__AZVaGg'
  },
  qa: {
    code: 'qa',
    production: false,
    API_URL: 'https://loadbalancerqa.bvl.com.pe',
    WC_BUNDLE_PATH: 'assets/web-components/qa',
    CULQI_PUBLIC_KEY: 'pk_test_kI7Rs2cG8iJhI3gl',
    AWS_AMPLIFY_CONFIG: {
      aws_project_region: 'us-east-1',
      aws_cognito_identity_pool_id: 'us-east-1:a6c11a3f-3311-4a6a-8d20-a51bd6fe8a42',
      aws_cognito_region: 'us-east-1',
      aws_user_pools_id: 'us-east-1_2Rx16q6jQ',
      aws_user_pools_web_client_id: '1j4t7vqkgoar45lo6cav8hi7sm',
      oauth: {}
    },
    RECAPTCHA_KEY: '6LcqWMcUAAAAAL-4TUqqi06t-A6wTZVi__AZVaGg'
  }
 };
