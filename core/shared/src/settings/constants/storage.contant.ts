export const STORAGE_KEY = {
  userProfile: 'USER_PROFILE'
};

export const STORAGE_REAL_TIME = 'REAL_TIME';

export const STORAGE_ONBOARDING = 'ONBOARDING';
