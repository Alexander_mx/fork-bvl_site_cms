import { Component } from '@angular/core';

@Component({
  selector: 'bvl-loader',
  template: '<div class="loader"></div>',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent { }
