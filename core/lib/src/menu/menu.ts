export interface IMenuLink {
    title: string;
    url: string;
    scrollPos?: number;
    id: string;
    anchor: string;
}
