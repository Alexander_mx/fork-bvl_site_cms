export interface IFilter {
    id: number;
    label: string;
    code?: string;
    active?: boolean;
    url?: string;
}
