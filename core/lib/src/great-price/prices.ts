export interface IPrice {
  title: string;
  subtitle: string;
  price: string;
  salePrice: string;
  textButton: string;
}
