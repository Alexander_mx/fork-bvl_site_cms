export interface IStartConfig {
  isNext: boolean;
  interval: number;
}
