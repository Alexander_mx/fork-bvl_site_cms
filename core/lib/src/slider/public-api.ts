export { SliderItemDirective } from './slider-item.directive';
export { SliderComponent } from './slider.component';
export { SliderModule } from './slider.module';
