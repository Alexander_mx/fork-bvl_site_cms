import { IListImage } from '../extra';

export interface ITestimony {
  name: string;
  backgroundImage?: Array<IListImage>;
  quote: string;
  position: string;
  company: string;
  image?: string;
}
