export const WITHOUT_DATA_TYPE = {
  chart: 'chart',
  table: 'table',
  video: 'video'
};
