/*
The MIT License(MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
  in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
*/

export class Trigger {
  constructor(public open: string, public close?: string) {
    if (!close) {
      this.close = open;
    }
  }

  isManual(): boolean { return this.open === 'manual' || this.close === 'manual'; }
}

const DEFAULT_ALIASES = {
  hover: ['mouseenter', 'mouseleave']
};

export const parseTriggers = (triggers: string, aliases = DEFAULT_ALIASES): Array<Trigger> => {
  const trimmedTriggers = (triggers || '').trim();

  if (trimmedTriggers.length === 0) {
    return [];
  }

  const parsedTriggers = trimmedTriggers.split(/\s+/)
  .map(trigger => trigger.split(':'))
  .map(triggerPair => {
    const alias = aliases[triggerPair[0]] || triggerPair;

    return new Trigger(alias[0], alias[1]);
  });

  const manualTriggers = parsedTriggers.filter(triggerPair => triggerPair.isManual());

  if (manualTriggers.length > 1) {
    throw new Error('Triggers parse error: only one manual trigger is allowed');
  }

  if (manualTriggers.length === 1 && parsedTriggers.length > 1) {
    throw new Error('Triggers parse error: manual trigger can\'t be mixed with other triggers');
  }

  return parsedTriggers;
};

const noopFn = () => {};

export const listenToTriggers = (renderer: any, nativeElement: any, triggers: string, openFn, closeFn, toggleFn): any => {
  const parsedTriggers = parseTriggers(triggers);
  const listeners = [];

  if (parsedTriggers.length === 1 && parsedTriggers[0].isManual()) {
    return noopFn;
  }

  parsedTriggers.forEach((trigger: Trigger) => {
    if (trigger.open === trigger.close) {
      listeners.push(renderer.listen(nativeElement, trigger.open, toggleFn));
    } else {
      listeners.push(
          renderer.listen(nativeElement, trigger.open, openFn), renderer.listen(nativeElement, trigger.close, closeFn));
    }
  });

  return () => { listeners.forEach(unsubscribeFn => unsubscribeFn()); };
};
