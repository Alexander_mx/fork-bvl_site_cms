import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { BvlFormInputComponent } from './form-input.component';

@NgModule({
  declarations: [BvlFormInputComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BvlFormInputComponent]
})
export class BvlFormInputModule { }
