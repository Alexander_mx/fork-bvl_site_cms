import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { BvlFormCheckboxComponent } from './form-checkbox.component';

@NgModule({
  declarations: [BvlFormCheckboxComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BvlFormCheckboxComponent]
})
export class BvlFormCheckboxModule { }
