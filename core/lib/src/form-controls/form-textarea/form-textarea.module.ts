import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { BvlFormTextareaComponent } from './form-textarea.component';

@NgModule({
  declarations: [BvlFormTextareaComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BvlFormTextareaComponent]
})
export class BvlFormTextareaModule { }
