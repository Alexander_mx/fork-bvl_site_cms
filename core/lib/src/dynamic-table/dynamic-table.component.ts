import { Component, OnInit, Renderer2 } from '@angular/core';
import { AngularUtil } from '../extra';

@Component({
  selector: 'bvl-dynamic-table',
  templateUrl: './dynamic-table.component.html'
})
export class DynamicTableComponent implements OnInit {

  modalPayload: any;
  tableData: any;

  get title(): string {
    return this.modalPayload.document[0].mainTitle || '';
  }

  constructor(
    private _renderer2: Renderer2
  ) {
    this.tableData = null;
  }

  ngOnInit(): void {
    this._createTable();
  }

  private _createTable(): void {
    this.tableData = null;
    const isCP = (this.modalPayload.documentType === 'CP'),
          SUB_TITLE_REGEX = (isCP)
                              ? /^Saldos/gi
                              : /^[0-9]{1}(d)[0-9]+(t|st)$|^[0-9]{1}(dt)[0-9]+$/gi;

    const table = this._renderer2.createElement('table');
    for (const r of this.modalPayload.document) {
      const row = this._renderer2.createElement('tr'),
            cellsNumber = +r.numberColumns;

      for (let c = -1; c <= cellsNumber; c++) {
        const cell = this._renderer2.createElement('td');
        let t = '', rowClassName = '', cellClassName = '';

        switch (c) {
          case -1:
            t = r.caccount || r.cAccount || '';
            rowClassName = (t)
                          ? (!isCP && SUB_TITLE_REGEX.test(t))
                            ? 'g-site-table-file--subtitle'
                            : ''
                          : 'g-site-table-file--title';
            break;
          case 0:
            t = r.title || '';
            rowClassName = (isCP && t.search(SUB_TITLE_REGEX) > -1)
                          ? 'g-site-table-file--subtitle'
                          : '';
            break;
          default:
            t = r[`value${c}`] || '';
            cellClassName = (AngularUtil.isNumber(t))
                            ? 'text-right'
                            : '';
            break;
        }

        const text = this._renderer2.createText(t);
        this._renderer2.appendChild(cell, text);
        if (cellClassName) { this._renderer2.addClass(cell, cellClassName); }
        this._renderer2.appendChild(row, cell);
        if (rowClassName) { this._renderer2.addClass(row, rowClassName); }
      }

      this._renderer2.appendChild(table, row);
    }
    this.tableData = table.innerHTML;
  }

}
