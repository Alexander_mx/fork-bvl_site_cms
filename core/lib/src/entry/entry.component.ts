import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { fromEvent, Observable, Subscription } from 'rxjs';

import { LANGUAGES } from '../extra/settings/constants';
import { GaUnsubscribeBase } from '../extra/utils/google';
import { getStorageLanguage } from '../extra/utils/storage-language';
import { ConfigurationService } from '../profile/configuration.service';
import { IUserProfile } from '../profile/profile.model';
import { WatchListService } from '../watchlist-providers/watchlist.services';
import { EntryAction, EntryStyle, IEntry, IPlayListRequest } from './entry';

@Component({
  selector: 'bvl-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EntryComponent extends GaUnsubscribeBase implements OnInit {

  private _userProfile: IUserProfile;

  obs: Observable<Event>;
  subscription: Subscription;
  actionsShown = false;

  @Input() entry: IEntry;
  @Input() actions?: Array<EntryAction> = [];
  @Input() variant?: EntryStyle = 'default';
  @Input() env?: string;

  @Output() changeEntry: EventEmitter<any>;

  TEXT = LANGUAGES[getStorageLanguage()];

  constructor(
    private _configService: ConfigurationService,
    private _watchListService: WatchListService,
    protected _elementRef: ElementRef
  ) {
    super(_elementRef);
    this._userProfile = this._configService.getUserProfile();
    this.changeEntry = new EventEmitter<any>();
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    if (this.env) {
      this._watchListService.setEnviroment(this.env);
    }
    if (this.actions.length) {
      this.obs = fromEvent(document, 'click');
    }
  }

  canDo(action): boolean {
    return this.actions.indexOf(action) !== -1;
  }

  showingActions(): void {
    this.actionsShown = true;
    this.subscription = this.obs.subscribe(event => {
      if (!this._elementRef.nativeElement.contains(event.target)) {
        this.hiddingActions();
      }
    });
  }

  hiddingActions(): void {
    this.actionsShown = false;
    this.subscription.unsubscribe();
  }
  updateVideoState(video): void {
    if (this._userProfile !== null && this.env) {
      const body: IPlayListRequest = {
        idUser: this._userProfile.cognitoUsername,
        title: video.title,
        url: video.url,
        category: video.label,
        author: video.author,
        add: !video.saved
      };

      this._watchListService.updatePlayList(body)
        .subscribe(() => {
          video.saved = !video.saved;
          this.hiddingActions();
          this.changeEntry.emit(video);
        });
    } else {
      location.href = `${location.origin}/login`;
    }
  }
  setUrl(url): string {
    if (url) {
      const findProtocol = url.match(/(https|http)/);

      return findProtocol !== null ? url : `https://${url}`;
    }

    return '#';
  }

}
