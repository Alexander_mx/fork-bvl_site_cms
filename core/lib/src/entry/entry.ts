import { IFile } from '../extra';

export type EntryStyle = 'default' | 'basic';

export type EntryAction = 'save' | 'share' | 'upvote' | 'downvote';

export interface IEntryCategory {
  id: number;
  label: string;
  url?: string;
}

export interface IEntryTag {
  id: number;
  label: string;
  url?: string;
}

export interface IEntryType {
  id: number;
  slug: string;
  label: string;
  labelPlural: string;
}

export interface IEntry {
  title: string;
  titles?: string;
  subtitle?: string;
  tags?: Array<IEntryTag>;
  author?: string;
  excerpt?: string;

  category?: IEntryCategory;
  description?: string;
  image?: string;
  date?: Date;
  url: string;
  type?: IEntryType;
  fileList?: boolean;
  files?: Array<IFile>;
  saved?: any;
  correlativeCodeBVL: string;
  rpjCode: string;
  shortLabel: string;
}

export interface IPlayListRequest {
  idUser: string;
  title: string;
  url: string;
  category: string;
  author: string;
  add: boolean;
}
