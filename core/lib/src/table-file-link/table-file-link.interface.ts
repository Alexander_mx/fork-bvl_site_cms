export interface ITableFileLinkItem {
  description: string;
  link: any;
}

export interface ITableFileLink {
  [key: string]: any;
  fileLink: ITableFileLinkItem;
}
