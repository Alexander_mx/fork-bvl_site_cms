
export interface IUserProfile {
  cognitoUsername: string;
  username: string;
  usernameMD5?: string;
  attributes?: IUserAttributes;
}

export interface IUserAttributes {
  'custom:personType': string;
  'custom:names': string;
  'custom:paternalSurname': string;
  'custom:maternalSurname': string;
  'custom:documentType': string;
  'custom:documentNumber': string;
  'custom:coreBusiness': string;
  'custom:representative': string;
  email?: string;
}
