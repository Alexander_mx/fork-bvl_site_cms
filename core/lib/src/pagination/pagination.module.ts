import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DomChangeDirectiveModule } from '../extra';
import { PaginationComponent } from './pagination.component';

@NgModule({
  imports: [
    CommonModule,
    DomChangeDirectiveModule
  ],
  declarations: [PaginationComponent],
  exports: [PaginationComponent]
})
export class PaginationModule { }
