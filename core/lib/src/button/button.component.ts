import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bvl-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ButtonComponent {
  @Input() title: string;
  @Input() icon: string;
  @Input() url: string;
}
