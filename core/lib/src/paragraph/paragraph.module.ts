import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ParagraphComponent } from './paragraph.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ParagraphComponent],
  exports: [ParagraphComponent],
  entryComponents: [ParagraphComponent]
})
export class ParagraphModule { }
