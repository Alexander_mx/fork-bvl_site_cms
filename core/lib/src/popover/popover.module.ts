import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PopoverComponent } from './popover.component';
import { PopoverDirective } from './popover.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [PopoverComponent, PopoverDirective],
  exports: [PopoverDirective],
  entryComponents: [PopoverComponent]
})
export class PopoverModule { }
