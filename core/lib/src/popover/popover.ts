export interface IPopover {
  title?: string;
  class?: string;
  body?: string;
  url?: string;
}
