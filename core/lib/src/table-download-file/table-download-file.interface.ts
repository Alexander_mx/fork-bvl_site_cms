export interface ITableDownloadFile {
  [key: string]: any;
  downloadFiles: Array<string>;
}
