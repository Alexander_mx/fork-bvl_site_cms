export type ToolbarCapability =
'search' | 'sortByLocation' | 'type' | 'filterByDate' |
'filterByNemonic' | 'year' | 'period' | 'periodAccount';

export interface IToolbarByTypeFilter {
    label: string;
    options: Array<{ id: number, label: string, value?: number | string }>;
}

export interface IToolbarDateFilter {
    startDate: Date;
    endDate: Date;
    defaultFrom?: Date;
    defaultTo?: Date;
}
export interface IToolbarNemonics {
    nemonics: Array<string>;
    currentNemonic?: string;
}
