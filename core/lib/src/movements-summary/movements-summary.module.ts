import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MovementsSummaryComponent } from './movements-summary.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MovementsSummaryComponent],
  exports: [MovementsSummaryComponent]
})
export class MovementsSummaryModule { }
