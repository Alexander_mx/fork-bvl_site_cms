export const getStorageLanguage = () => {
  return localStorage.getItem('language') || 'es';
};
