export interface IScreenResolution {
  type: string;
  min: number;
  max: number;
}
