import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';

export class AppError {
  public status = 0;
  constructor(originalError?: any, public message?: any) { }
}

export class NotFoundError extends AppError { }

export class ConflictError extends AppError { }

export class InternalServerError extends AppError {
  constructor(public originalError?: any) {
    super(originalError);
    this.status = 500;
  }
}

export class BusinessError extends AppError {
  constructor(public originalError?: any, public message?: any) {
    super(originalError, message);
    this.status = 406;
  }

  get errors(): any {
    if (this.originalError) {
      return this.originalError;
    }

    return null;
  }
}

export class BadRequestError extends AppError {
  constructor(public originalError?: any) {
    super(originalError);
  }

  get errors(): Array<string> {
    if (this.originalError) {
      return this.originalError;
    }

    return null;
  }
}

export class OptionsRequest {
  headers?: HttpHeaders | {
    [header: string]: string | Array<string>;
  };
  observe?: 'body';
  params?: {};
  responseType?: 'json';
  preloader?: Boolean;
  withCredentials?: boolean;
  retry?: number;
  reportProgress?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private _httpClient: HttpClient
  ) {

  }

  get<T>(endPoint: string, options?: OptionsRequest): Observable<T> {
    options = options || {};
    this.beforeRequest(options.preloader || false);

    const request = this.getUrlAndParameters(endPoint, options);
    const pipes = this.getPipesDefault(options);
    const observable = this._httpClient.get<T>(request.url, request.options);

    return this.formatObservablePipe(observable, pipes);
  }

  post<T>(endPoint: string, body: any, options?: OptionsRequest): Observable<T> {
    options = options || {};
    this.beforeRequest(options.preloader || false);

    const request = this.getUrlAndParameters(endPoint, options);
    const pipes = this.getPipesDefault(options);
    const observable = this._httpClient.post<T>(request.url, body, request.options);

    return this.formatObservablePipe(observable, pipes);
  }

  put<T>(endPoint: string, body: any, options?: OptionsRequest): any {
    options = options || {};
    this.beforeRequest(options.preloader || false);

    const request = this.getUrlAndParameters(endPoint, options);
    const pipes = this.getPipesDefault(options);
    const observable = this._httpClient.put<T>(request.url, body, request.options);

    return this.formatObservablePipe(observable, pipes);
  }

  del<T>(endPoint: string, options?: OptionsRequest): any {
    options = options || {};
    this.beforeRequest(options.preloader);

    const request = this.getUrlAndParameters(endPoint, options);
    const pipes = this.getPipesDefault(options);
    const observable = this._httpClient.delete<T>(request.url, request.options);

    return this.formatObservablePipe(observable, pipes);
  }

  postProgress<T>(endPoint: string, body: any, options?: OptionsRequest): T {
    options = options || {};

    this.beforeRequest(options.preloader || false);

    const request = this.getUrlAndParameters(endPoint, options);
    const pipes = this.getPipesDefault(options);
    const req = new HttpRequest('POST', request.url, body, options as any);
    const observable = this._httpClient.request<T>(req);

    return this.formatObservablePipe(observable, pipes) as any;
  }

  formatObservablePipe(observable: Observable<any>, pipes): Observable<any> {
    return observable.pipe(tap(), tap(), tap(), tap(), tap(), tap(), tap(), tap(), tap(), ...pipes);
  }

  private getPipesDefault(options?): Array<any> {
    const pipes = [];

    pipes.push(catchError((err: any) => this.onCatch(err)));
    pipes.push(finalize(() => this.onFinally(options.preloader)));

    return pipes;
  }

  /**
   * Control de Error
   */
  private onCatch(error: any): Observable<any> {
    try {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        // const messageError = `An error occurred: ${error.error.message}`;

        return throwError(error);
      }

      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      return this.handleError(error);
    } catch (error) {
      return this.handleError(error);
    }
  }

  /**
   * Antes de una solicitud
   */
  beforeRequest(preloader: Boolean): void {
    if (preloader) {
    }
  }

  /**
   * Despues de una solicitud
   */
  afterRequest(preloader: Boolean): void {
    if (preloader) {
    }
  }

  /**
   * Finalización de una solicitud
   */
  private onFinally(preloader: Boolean = false): void {
    // this.notifyService.consoleLog('onFinally-Log');
    this.afterRequest(preloader);
  }

  private handleError(error: HttpErrorResponse): any {
    switch (error.status) {
      case 400:
        return throwError(new BadRequestError(error));
      case 404:
        return throwError(new NotFoundError());
      case 409:
        return throwError(new ConflictError());
      case 406:
        return throwError(new BusinessError(error, error.error.message));
      case 500:
        return throwError(new InternalServerError(error));
      default:
        return throwError(new AppError(error));
    }
  }

  private isParameterInPath(endPoint: string, parameterKey: string): boolean {
    return !(endPoint.indexOf(`{${parameterKey}}`) === -1);
  }

  private getUrlAndParameters(url: string, optionsRequest: OptionsRequest): { url: string; options: OptionsRequest } {
    const options: OptionsRequest = new OptionsRequest();
    let paramsQuery = new HttpParams();

    if (optionsRequest.params) {
      Object.keys(optionsRequest.params)
        .forEach((parameterKey: string) => {
          if (this.isParameterInPath(url, parameterKey)) {
            url = url.replace(`{${parameterKey}}`, optionsRequest.params[parameterKey]);
          } else {
            paramsQuery = paramsQuery.append(parameterKey, optionsRequest.params[parameterKey]);
          }
        });
    }

    options.params = paramsQuery;
    options.headers = optionsRequest.headers;

    return {
      url,
      options
    };
  }

}
