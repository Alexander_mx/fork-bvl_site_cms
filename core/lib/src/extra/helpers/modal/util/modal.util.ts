export class ModalUtil {

  public static isString(value: any): boolean {
    return typeof value === 'string';
  }

}
