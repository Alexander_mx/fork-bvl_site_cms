import { MODAL_BREAKPOINT } from '../constants/modal.constants';

export type size = MODAL_BREAKPOINT.xl | MODAL_BREAKPOINT.lg | MODAL_BREAKPOINT.md | MODAL_BREAKPOINT.sm ;

export interface IModalConfig {
  modalClass?: string;
  size?: size;
  showHeader?: boolean;
  headerClass?: string;
  titleText?: string;
  showCloseButton?: boolean;
  closeButtonClass?: string;
  closeIconClass?: string;
  bodyClass?: string;
  showFooter?: boolean;
  footerClass?: string;
  showCancelButton?: boolean;
  cancelButtonClass?: string;
  cancelButtonText?: string;
  showConfirmButton?: boolean;
  confirmButtonClass?: string;
  confirmButtonText?: string;
}

export interface IPayloadModalComponent {
  modalPayload: any;
}
