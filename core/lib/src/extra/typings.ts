import { ITableDownloadFile } from '../table-download-file';

export declare type MainNavState = 'opened' | 'closed';

export interface ILink {
  title: string;
  url: string;
  label?: string;
}

export declare type Breakpoint =
  | 'xs'
  | 'sm'
  | 'md'
  | 'lg'
  | 'xl'
  | 'lt-sm'
  | 'lt-md'
  | 'lt-lg'
  | 'lt-xl'
  | 'gt-xs'
  | 'gt-sm'
  | 'gt-md'
  | 'gt-lg';

export interface IFancyTableColumn {
  name: string;
  label?: string;
  title?: string;
  valueType: string;
}

export declare interface ICurrency {
  code?: 'PEN' | 'USD' | any;
  symbol: '$' | 'S/' | any;
}

export interface IStock {
  id: number;
  name: string;
  price: number;
  currency: ICurrency;
  variation: number;
  negotiatedAmount?: number;
  operations?: number;
  inWatchlist: boolean;
  quotesHistory: string;
  links: Array<ILink>;
}

declare interface IMovement {
  id: number;
  name: string;
  mnemonic: string;
  sector: string;
  segment: string;
  previous: number;
  previousDate: Date;
  opening: number;
  last: number;
  variation: number;
  buy: number;
  sell: number;
  currency: ICurrency;
  stocks: number;
  operations: number;
  inWatchlist: boolean;
  quotesHistory: string;
  links: Array<ILink>;
}

declare interface IIndex {
  id: number;
  name: string;
  currency: ICurrency;
  last: number;
  previous: number;
  opening: number;
  max: number;
  min: number;
  variation: number;
  inWatchlist: boolean;
  quotesHistory: string;
  links: Array<ILink>;
}

declare interface IFact {
  label: string;
  shortLabel: string;
  date: Date;
  url: string;
}

declare interface IIssuer {
  id: number;
  name: string;
  presence: number;
  sector: string;
  mnemonic: string;
  indexes: Array<string>;
  newInfo: Array<IFact>;
  inWatchlist: boolean;
}

export interface IAgent {
  id: number;
  name: string;
  address: string;
  startDate: Date;
  functions: Array<string>;
}

export interface IValue {
  id: number;
  name: string;
  mnemonic: string;
  isin: string;
  currency: string;
}

export type TableEntry = IStock | IMovement | IIssuer | IIndex;

export interface IAgentDetails {
  id: number;
  name: string;
  ruc: string;
  stock: string;
  phones: Array<string>;
  email: string;
  website: string;
  generalManager: string;
  links: Array<ILink>;
  map?: string;
  longitude: number;
  latitude: number;
  code: string;
  authorizations?: Array<IAgentAuthorizations>;
  offices?: Array<IAgentOffice>;
  representatives?: Array<IAgentRepresentatives>;
  policies?: Array<IFile>;
  sanctions?: Array<ISanction>;
}
export interface ISanction {
  businessName: string;
  observation: string;
  resolution: string;
  document: string;
  startDate: string;
}
export interface IAgentAuthorizations {
  company: string;
  date: string;
  description?: string;
}
export interface IAgentOffice {
  active?: boolean;
  address?: string;
  companyName: string;
  department?: string;
  direction?: string;
  district?: string;
  phone?: string;
  representatives?: Array<{ name: string }>;
}

export interface IAgentRepresentatives {
  name: string;
  office: string;
}

export interface IImportantFacts {
  totalElements: number;
  totalPages: number;
  content: Array<ITableDownloadFile>;
}

export interface IFile {
  data?: any;
  date?: any;
  title: string;
  url: string;
  year?: any;
}
interface ITweet {
  author: string;
  time: string;
  text: string;
  url: string;
}

interface InstagramPost {
  imageUrl: string;
  url: string;
  caption: string;
}

export interface ITwitterData {
  accountUrl: string;
  tweets: Array<ITweet>;
}

export interface InstagramData {
  caption?: string;
  accountUrl: string;
  posts: Array<InstagramPost>;
}

export interface IListImage {
  name: string;
  small: IListImageDetail;
  medium: IListImageDetail;
  large: IListImageDetail;
  selected?: boolean;
}

export interface IListImageDetail {
  imageUri: string;
  size: number;
  height: number;
  width: number;
}

export interface IPopupResponse {
  id: string;
  code: string;
  name: string;
  languages: Array<{
    language: string;
    properties: Array<{
      code: string;
      value: string;
    }>;
  }>;
}
