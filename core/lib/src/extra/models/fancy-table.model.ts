import { IFancyTableColumn } from '../typings';

export type FancyTableTheme = 'green' | 'red' | 'blue';

export interface IFancyTable<T> {
  title?: string;
  iconTitle?: string;
  theme?: FancyTableTheme;
  entries: Array<T>;
}
export interface IFancyTableEntry {
  [key: string]: any;
  detailsShowed?: boolean;
}
export interface IFancyTableData {
  id: string;
  theme?: FancyTableTheme;
  iconTitle?: string;
  title?: string;
  entries: IFancyTableEntry;
  metadata: IFancyTableMetada;
}

export interface IFancyTableMetada {
  sortedBy: IFancyTableSortedBy;
  expandedElement: IFancyTableEntry;
  loadedCharts: Array<number>;
}

export interface IFancyTableSortedBy {
  column: IFancyTableColumn;
  ascendingOrder: boolean;
}
