export interface IBanner {
  backgroundImage: Array<IBackgroundImage>;
  title: string;
  description: string;
  isButton?: boolean;
  textButton?: string;
  urlButton?: string;
  isLink?: boolean;
  textLink?: string;
  urlLink?: string;
  isPopup?: boolean;
  popup?: string;
}

export interface IImage {
  name?: string;
  imageUri: string;
  size: number;
  height: number;
  width: number;
}

export interface IBackgroundImage {
  name?: string;
  small?: IImage;
  medium?: IImage;
  large?: IImage;
}

export interface IBackgroundVideo extends IBackgroundImage {
  url: string;
}

export interface IComponentPropertiesCode {
  title?: string;
  align?: string;
  description?: string;
  isLink?: boolean;
  urlLink?: string;
  textLink?: string;
  isButton?: string;
  urlButton?: string;
  textButton?: string;
  backgroundImage?: any | string | Array<IBackgroundImage>;
  videos?: any;
  plans?: any;
  titleDscto?: string;
  total?: string;
  totalDscto?: string;
  codeCulqui?: string;
  ga?: any;
}

export interface IComponentValueProperty {
  code: string;
  value?: any;
}

export interface IComponentValueLanguage {
  language: string;
  properties: Array<IComponentValueProperty>;
}

export interface IComponentValueResponse {
  code: string;
  languages: Array<IComponentValueLanguage>;
}
