export class WatchListEndPoints {
  public static listStockWatchList = `/static/watchlist/stock/{idUser}`;
  public static stockWatchList = `/static/watchlist/stock`;
  public static listIndexWatchList = `/static/watchlist/index/{idUser}`;
  public static indexWatchList = `/static/watchlist/index`;
  public static updatePlayList = `/static/watchlist/playList`;
  public static playList = `/static/watchlist/playlist`;
}
