// import { IIndex, IShare } from 'modules/fancy-table/src/app/fancy-table';

export interface IIndexWatchListResponse {
  idUser: string;
  indexes: Array<any>;
}

export interface IStockWatchListResponse {
  id: string;
  stocks: Array<any>;
}
