import { Injectable } from '@angular/core';
import { getStorageLanguage } from '@bvl-core/shared/helpers/util/storage-language';
import { LANGUAGES } from '@bvl/library';

@Injectable()
export class GraphicOptionsProvider {
  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)];

  getGraphicOptions(): any {
    return {
      chart: { spacing: [4, 13, 0, 0] },
      colors: this._getColors(),
      credits: { enabled: false },
      legend: { enabled: false },
      plotOptions: {
        series: {
          marker: {
            states: {
              hover: {
                animation: { duration: 0 },
                lineWidthPlus: 0,
                fillColor: 'red',
                halo: {
                  attributes: {
                    fill: 'red',
                    opacity: 0.15
                  },
                  size: 18
                }
              }
            }
          }
        }
      },
      series: [],
      title: null,
      tooltip: {
        animation: false,
        backgroundColor: this._getTooltipBackground(),
        borderRadius: 10,
        borderWidth: 0,
        headerFormat: `
          <span>${this.TEXT.general.date}: </span>
          <strong class="tooltip-header">{point.key}</strong><br>
        `,
        pointFormat: `
          <span>${this.TEXT.general.value}: </span>
          <b>{point.y}</b>
        `,
        padding: 15,
        shadow: false,
        style: { color: this._getTooltipColor() },
        useHTML: true
      },
      xAxis: {
        tickLength: 0,
        lineWidth: 0,
        tickInterval: this._getInterval(),
        type: 'category',
        labels: {  enabled: false }
      },
      yAxis: {
        gridLineWidth: 0,
        maxPadding: 0,
        tickAmount: 5,
        tickColor: this._getTickColor(),
        tickLength: 6,
        tickWidth: 1,
        title: { enabled: false },
        startOnTick: false
      }
    };
  }

  private _getInterval(): number {
    return 3600 * 1000;
  }

  private _getColors(): Array<string> {
    return ['#6a6a6a'];
  }

  private _getTickColor(): string {
    return '#b8b8b8';
  }

  private _getTooltipBackground(): string {
    return '#353535';
  }

  private _getTooltipColor(): string {
    return '#dcdcdc';
  }

  private _getMarkerSymbol(color = 'red'): string {
    switch (color) {
      case 'red':
      default:
        return (
          'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgA' +
          'AAA0AAAANCAYAAABy6+R8AAABC0lEQVQoU53SPy+EQRDH8c9INDR6L' +
          '0BBotY4aomeSoOCcEJJEI3yRCTkSCSSewdqfxqFTqKgV+iphJU9z/F' +
          'EzhW22dnZ/c7Mb2dCaSUmMYhHDKAX3bgKzltPo2UkDnCDRvBR8ndhG' +
          'iPBQvY3ocQuzoL70uOx4LJ0HsZcBiNRQX/QSPRhE9VS1bVgpQi+ges' +
          'M7WMpSIkalss6C3svqCZ6UMvQUTBfREptgKYrfqTUM1QPZhNjuPgLw' +
          'njWmDj+T6ajDB1iNXjtoGk72EpfJTY1jaIS7BS6fn9GEyjupvDU6lN' +
          'u7Glw26FPQ5gJ1lpQ3k/wjPXgvd1EYDG35nuMivQTuVS84QUPxQzel' +
          'WfvE+M3VH9h1KmfAAAAAElFTkSuQmCC)'
        );
    }
  }

  /*
    private _getTooltipShape() {
        Highcharts.SVGRenderer.prototype.symbols.bvltooltip = function (x, y, w, h, foo) {
            return [
                'M', x, y,
                'H', x + w,
                'V', y + h,
                'H', x,
                'Z'
            ];
        };
        if(Highcharts.VMLRenderer) {
            Highcharts.VMLRenderer.prototype.symbols.bvltooltip = Highcharts.SVGRenderer.prototype.symbols.bvltooltip;
        }
    }
    */
}
