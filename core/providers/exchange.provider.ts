import { Injectable } from '@angular/core';

@Injectable()
export class ExchangeProvider {
    private _PENtoUSD: number;
    // private _USDtoPEN: number;
    constructor() {
        this._PENtoUSD = 0.298191;
        // this._USDtoPEN = 3.35355;
    }
    convertToDollars(amount: number): number {
        return amount * this._PENtoUSD;
    }
}
