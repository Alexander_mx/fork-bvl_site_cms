import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class FooterService {
  private _elements: Array<{id: string, value: number}> = [];
  private _marginBottom = new BehaviorSubject<number>(0);

  getMarginBottom(): BehaviorSubject<number> {
    return this._marginBottom;
  }

  addMarginBottom(id: string, value: number): void {
    this._elements = this._elements
      .filter(el => el.id !== id)
      .concat([{id, value}]);
    this._setNewMarginBottom();
  }

  removeMarginBottom(elementId): void {
    this._elements = this._elements.filter(el => el.id !== elementId);
    this._setNewMarginBottom();
  }

  private _setNewMarginBottom(): void {
    this._marginBottom.next(
      this._elements.reduce((marginBottom, element) => {
        return marginBottom += element.value;
      }, 0)
    );
  }

}
