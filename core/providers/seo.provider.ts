import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable()
export class SEOProvider {
  constructor(
    private metaService: Meta,
    private titleService: Title) {}

  addMetaTags(metas: Array<{ name: string, content: string }>): void {
    metas.map(item => {
      if (this.metaService.getTag(`name = ${item.name}`) == null) {
        this.metaService.addTag({ name: item.name, content: item.content });
      }
    });
  }
  addPageTitle(title: string): void {
    this.titleService.setTitle(title);
  }
}
