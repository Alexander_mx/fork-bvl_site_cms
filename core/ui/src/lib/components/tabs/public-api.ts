export { BvlTabComponent as BvlTab } from './tab.component';
export { BvlTabsComponent as BvlTabs } from './tabs.component';
export { BvlTabsModule } from './tabs.module';
