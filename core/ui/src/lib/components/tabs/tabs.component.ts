import { AfterContentInit, Component, ContentChildren, EventEmitter, HostBinding, Input, Output, QueryList } from '@angular/core';
import { UnsubscribeOnDestroy } from '@bvl-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { BvlTabComponent } from './tab.component';

@Component({
  selector: 'bvl-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class BvlTabsComponent extends UnsubscribeOnDestroy implements AfterContentInit {

  @ContentChildren(BvlTabComponent) tabs: QueryList<BvlTabComponent>;

  protected _class: string;
  @HostBinding('attr.class')
  @Input()
  get class(): string {
    return this._class;
  }
  set class(value: string) {
    this._class = value;
  }

  @Output() selectTabChange: EventEmitter<string>;

  mSelectTab: any;

  constructor() {
    super();
    this._class = '';
    this.selectTabChange = new EventEmitter<string>();
  }

  ngAfterContentInit(): void {
    this.tabs.changes
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(response => {
        response.first.active = false;
        this._createTabs();
      });

    this._createTabs();
  }

  private _createTabs(): void {
    const activeTabs = this.tabs.find(fv => fv.active);

    this.selectTab((activeTabs)
                      ? activeTabs
                      : this.tabs.first,
                    false);
  }

  selectTab(tab: any, isSelectTabChange = true): void {
    this.tabs.toArray()
      .forEach(fv => fv.active = false);

    tab.active = true;
    this.mSelectTab = tab;
    if (isSelectTabChange) {
      this.selectTabChange.emit(tab.value);
    }
  }

}
