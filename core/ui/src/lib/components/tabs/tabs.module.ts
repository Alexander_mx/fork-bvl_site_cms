import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BvlTabComponent } from './tab.component';
import { BvlTabsComponent } from './tabs.component';

@NgModule({
  declarations: [
    BvlTabsComponent,
    BvlTabComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    BvlTabsComponent,
    BvlTabComponent
  ]
})
export class BvlTabsModule { }
