import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BvlSelectComponent } from './select.component';

@NgModule({
  declarations: [BvlSelectComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BvlSelectComponent]
})
export class BvlSelectModule { }
