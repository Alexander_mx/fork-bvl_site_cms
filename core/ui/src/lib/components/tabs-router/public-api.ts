export { BvlTabContentComponent as BvlTabContent } from './tab-content/tab-content.component';
export { BvlTabHeaderComponent as BvlTabHeader } from './tab-header/tab-header.component';
export { BvlTabsRouterModule } from './tabs-router.module';
