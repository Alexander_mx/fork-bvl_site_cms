import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bvl-alert-error',
  templateUrl: './alert-error.component.html',
  styleUrls: ['./alert-error.component.css']
})
export class BvlAlertErrorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
