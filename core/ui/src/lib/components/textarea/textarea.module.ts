import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BvlTextareaComponent } from './textarea.component';

@NgModule({
  declarations: [BvlTextareaComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BvlTextareaComponent]
})
export class BvlTextareaModule { }
