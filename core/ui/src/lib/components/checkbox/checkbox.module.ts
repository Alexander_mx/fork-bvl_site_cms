import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BvlCheckboxComponent } from './checkbox.component';

@NgModule({
  declarations: [BvlCheckboxComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BvlCheckboxComponent]
})
export class BvlCheckboxModule { }
