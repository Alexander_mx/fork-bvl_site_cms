import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BvlNotifyComponent } from './notify.component';

@NgModule({
  declarations: [BvlNotifyComponent],
  imports: [
    CommonModule
  ],
  exports: [BvlNotifyComponent]
})
export class BvlNotifyModule { }
