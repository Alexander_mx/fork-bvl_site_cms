export const coerceBooleanProp = (value: any): boolean =>
(value !== null && value !== undefined && `${value}`.toLowerCase() !== 'false' && `${value}`.toLowerCase() !== 'null');
