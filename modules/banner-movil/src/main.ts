import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { BannerMovilModule } from './app/banner-movil.module';

platformBrowserDynamic().bootstrapModule(BannerMovilModule)
  .catch(err => console.error(err));
