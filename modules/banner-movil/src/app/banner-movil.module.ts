import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { ApiModule, PipesModule } from '@bvl/library';
import { BannerMovilComponent } from './banner-movil.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    PipesModule,
    ApiModule
  ],
  declarations: [BannerMovilComponent],
  exports: [BannerMovilComponent],
  entryComponents: [BannerMovilComponent]
})
export class BannerMovilModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-banner-movil'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BannerMovilComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
