import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import {
  CommonComponentBase,
  IComponentPropertiesCode,
  IComponentValueResponse
} from '@bvl/library';
import { environment } from '../environments/environment';
import { ComponentService } from '../providers/component.service';

@Component({
  selector: 'bvl-banner-movil',
  templateUrl: './banner-movil.component.html',
  styleUrls: ['./banner-movil.component.scss']
})
export class BannerMovilComponent extends CommonComponentBase implements OnInit, OnDestroy {

  private _componentValueSub: Subscription;

  bannerMovil: IComponentPropertiesCode;

  constructor(
    protected _elementRef: ElementRef,
    private _componentService: ComponentService
  ) {
    super(_elementRef);
  }

  ngOnInit(): void {
    this._componentValue('bvl-banner-movil');
  }

  private _componentValue(componentCode: string): void {
    this._componentValueSub = this._componentService.componentValue(environment.API_URL, componentCode)
      .subscribe((response: IComponentValueResponse) => {
        const componentValue = this.componentValueByLanguage(response, this.language);
        this.bannerMovil = this.getComponentValuePropertiesCode(componentValue);
      });
  }

  ngOnDestroy(): void {
    this._componentValueSub.unsubscribe();
  }

}
