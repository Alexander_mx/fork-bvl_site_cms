import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService, IComponentValueResponse } from '@bvl/library';

@Injectable({
  providedIn: 'root'
})
export class ComponentService {

  constructor(
    private _apiService: ApiService
  ) { }

  componentValue(environment: string, componentCode: string): Observable<IComponentValueResponse> {
    const endPoint = `${environment}/trays/component/commonValue/get/{componentCode}`,
      params = { componentCode };

    return this._apiService.get(endPoint, { params });
  }

}
