import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { DomChangeDirectiveModule, SectionHeaderModule } from '@bvl/library';
import { LinksComponent } from './links.component';

@NgModule({
  declarations: [
    LinksComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    DomChangeDirectiveModule
  ],
  providers: [],
  entryComponents: [LinksComponent]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'bvl-links'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(LinksComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
