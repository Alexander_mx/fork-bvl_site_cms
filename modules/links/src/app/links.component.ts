import { Component, ElementRef, Input, ViewEncapsulation } from '@angular/core';

import { GaUnsubscribeBase } from '@bvl/library';

@Component({
  selector: 'bvl-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LinksComponent extends GaUnsubscribeBase {
  @Input() title: string;

  protected _links: Array<any> | any;
  @Input()
  get links(): Array<any> | any {
    return this._links;
  }
  set links(value: Array<any> | any) {
    this._links = (JSON.parse(value) || []).reduce((previous, current, index) => {
        const ind = (index > 5) ? 1 : 0;
        if (!previous[ind]) { previous.push([]); }
        previous[ind].push(current);

        return previous;
      }, []);
  }

  constructor(
    protected _element: ElementRef
  ) {
    super(_element);
  }
}
