import { ILink } from '@bvl/library';

export interface ILinksModule {
  title: string;
  links: Array<ILink>;
}
