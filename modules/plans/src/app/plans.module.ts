import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { ApiModule } from '@bvl/library';
import { PlansComponent } from './plans.component';

@NgModule({
  declarations: [
    PlansComponent
  ],
  imports: [
    BrowserModule,
    ApiModule
  ],
  providers: [],
  entryComponents: [PlansComponent]
})
export class PlansModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-plans'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(PlansComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
