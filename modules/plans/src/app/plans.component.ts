import { Component, Input, OnInit } from '@angular/core';

import { getStorageLanguage, IComponentPropertiesCode, LANGUAGES } from '@bvl/library';
import { camelCase } from 'lodash-es';

@Component({
  selector: 'bvl-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {

  protected _plans: any;
  @Input()
  get plans(): any {
    return this._plans;
  }
  set plans(value: any) {
    this._plans = this._camelCaseProperties(JSON.parse(value) || []);
  }
  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)
  ];

  constructor() { }

  ngOnInit(): void { }

  private _camelCaseProperties(value: any): Array<IComponentPropertiesCode> {
    return value.map(plan => {
      return Object.keys(plan)
        .reduce((previous, current) => {
          const code = camelCase(current);
          previous[code] = plan[current];

          return previous;
        }, {});
    }) as Array<IComponentPropertiesCode>;
  }

}
