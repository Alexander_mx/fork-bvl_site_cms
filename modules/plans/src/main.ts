import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { PlansModule } from './app/plans.module';

platformBrowserDynamic().bootstrapModule(PlansModule)
  .catch(err => console.error(err));
