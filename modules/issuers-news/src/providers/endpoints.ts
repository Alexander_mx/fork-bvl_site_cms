import { environment } from '../environments/environment';

export class IssuerNewsPoints {
  public static news = `${environment.API_URL}/static/news`;
}
