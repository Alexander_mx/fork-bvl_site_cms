import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { INewsRequest } from '../app/issuers-news';
import { IssuerNewsPoints } from './endpoints';

@Injectable()
export class IssuerNewsServices {

  constructor(
    private _apiService: ApiService
  ) { }

  getNews(params: INewsRequest): Observable<any> {
    return this._apiService.post(IssuerNewsPoints.news, params);
  }
}
