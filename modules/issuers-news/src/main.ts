import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { IssuersNewsModule } from './app/issuers-news.module';

platformBrowserDynamic().bootstrapModule(IssuersNewsModule)
  .catch(err => console.error(err));
