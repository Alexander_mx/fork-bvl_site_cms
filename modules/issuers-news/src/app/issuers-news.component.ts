import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';

import { getStorageLanguage, IEntry, LANGUAGES } from '@bvl/library';
import { IssuerNewsServices } from '../providers/services';
import { INewsRequest, newsDefaultRequest, newsDefaultTypes, setAnnouncements, setImportantFacts } from './issuers-news';

@Component({
  selector: 'bvl-issuers-news',
  templateUrl: './issuers-news.component.html',
  styleUrls: ['./issuers-news.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IssuersNewsComponent implements OnInit, OnDestroy {
  private _newsSubscription: Subscription;
  news: Array<IEntry>;
  newsTypes: any;
  totalPages = 0;
  filters = JSON.parse(JSON.stringify(newsDefaultRequest));
  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)
  ];

  constructor(public issuerNewsServices: IssuerNewsServices) {
    this.newsTypes = newsDefaultTypes;
  }

  ngOnInit(): void {
    this._getIssuerNews(this.filters);
  }
  ngOnDestroy(): void {
    if (this._newsSubscription) { this._newsSubscription.unsubscribe(); }
  }

  onFilterChange(filter: any): void {
    if (filter.length > 0) {
      filter.forEach(element => {
        this.filters[element.type] = element.value;
      });
      this.filters.page = 1;
    }
    this._getIssuerNews(this.filters);
  }
  emitChangePage(value: number): void {
    this.filters.page = value;
    this._getIssuerNews(this.filters);
  }

  private _getIssuerNews(filters: INewsRequest): void {
    this._newsSubscription = this.issuerNewsServices.getNews(filters)
      .subscribe(response => {
        this.totalPages = response.totalPages;
        this.news = [];
        const values: Array<IEntry> = [];
        Object.keys(response.content)
        .forEach(key => {
          response.content[key]
          .map(item => {
            const value = ((key === 'announcements')
              ? setAnnouncements(item, key)
              : setImportantFacts(item, key));
            values.push(value);
          });
        });
        this.news = values.sort((a, b) => b.date.getTime() - a.date.getTime());
      });
  }
}
