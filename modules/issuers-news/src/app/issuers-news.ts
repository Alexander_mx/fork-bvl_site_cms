
import { DateUtil, getStorageLanguage, IEntry, LANGUAGES } from '@bvl/library';

const TEXT = LANGUAGES[
  getStorageLanguage()
    .substring(0, 2)
];
export interface INewsRequest {
  page: number;
  size: number;
  search: string;
  type: number;
}

export const newsDefaultRequest: INewsRequest = {
  page: 1,
  size: 8,
  search: '',
  type: 2
};

export const newsDefaultRequestForIssuers: INewsRequest = {
  page: 1,
  size: 20,
  search: '',
  type: 1
};

export const setAnnouncements = (data: any, type: any): IEntry => (
  {
    title: data.text,
    // description: data.text,
    // date: DateUtil.convertStringToDateTime(`${data.date} ${data.hour}`),
    date: data.files && data.files.length && DateUtil.convertStringToDateTime(`${data.files[0].date} ${data.files[0].hour}`),
    // url: data.link,
    // category: {
    //   id: null,
    //   label: newsDefaultTypes.options.find(item => item.code === type).label,
    //   url: '#'
    // }
    fileList: true,
    files: data.files && data.files.map((item: any) => (
      {
        title: item.subTitle,
        url: item.link
      }
    ))
  } as IEntry
);

export const setImportantFacts = (data: any, type: any): IEntry => (
  {
    title: data.businessName,
    date: DateUtil.convertStringToDateTime(data.registerDate),
    rpjCode: data.rpjCode,
    correlativeCodeBVL: data.correlativeCodeBVL,
    category: {
      id: null,
      label: newsDefaultTypes.options.find(item => item.code === type).label,
      url: '#'
    },
    files: data.documents.map((item: any) => (
      {
        secuence: item.sequence,
        url: `https://www.bvl.com.pe${item.path}`
      }
    )),
    titles: data.codes.map(item => ({ secuence: item.sequence, text: item.descCodeHHII })),
    subtitle: data.observation
  } as IEntry
);

export const newsDefaultTypes = {
  label: TEXT.general.typeContent,
  options: [
    {
      id: 2,
      label: TEXT.issuers.importantNotices,
      code: 'announcements',
      type: 2,
      url: '#'
    },
    {
      id: 1,
      label: TEXT.issuers.importantFacts,
      code: 'importantFacts',
      type: 1,
      url: '#'
    }
  ]
};
