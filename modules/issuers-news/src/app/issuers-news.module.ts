import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { EntryModule, PaginationModule, ToolbarModule } from '@bvl/library';
import { IssuerNewsServices } from '../providers/services';
import { IssuersNewsComponent } from './issuers-news.component';

@NgModule({
  declarations: [
    IssuersNewsComponent
  ],
  imports: [
    BrowserModule,
    ToolbarModule,
    FlexLayoutModule,
    EntryModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule,
    PaginationModule
  ],
  providers: [IssuerNewsServices],
  entryComponents: [IssuersNewsComponent]
})
export class IssuersNewsModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap(): void {
    const name = 'bvl-issuers-news'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(IssuersNewsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
