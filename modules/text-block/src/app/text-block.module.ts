import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { PipesModule } from '@bvl-core/shared/helpers/pipes';
import { DomChangeDirectiveModule, SectionHeaderModule } from '@bvl/library';
import { TextBlockComponent } from './text-block.component';

@NgModule({
  imports: [
    CommonModule,
    SectionHeaderModule,
    BrowserModule,
    PipesModule,
    DomChangeDirectiveModule
  ],
  declarations: [TextBlockComponent],
  entryComponents: [TextBlockComponent]
})
export class TextBlockModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-text-block'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(TextBlockComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
