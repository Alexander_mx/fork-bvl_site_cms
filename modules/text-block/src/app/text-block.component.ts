import { Component, ElementRef, Input, ViewEncapsulation } from '@angular/core';

import { coerceBooleanProp, GaUnsubscribeBase } from '@bvl/library';

@Component({
  selector: 'bvl-text-block',
  templateUrl: './text-block.component.html',
  styleUrls: ['./text-block.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TextBlockComponent extends GaUnsubscribeBase {

  @Input() title: string;
  @Input() description: string;

  private _isLink: boolean;
  @Input()
  get isLink(): boolean {
    return this._isLink;
  }
  set isLink(value: boolean) {
    this._isLink = coerceBooleanProp(value);
  }

  @Input() urlLink: string;

  private _isButton: boolean;
  @Input()
  get isButton(): boolean {
    return this._isButton;
  }
  set isButton(value: boolean) {
    this._isButton = coerceBooleanProp(value);
  }
  @Input() urlButton: string;
  @Input() textButton: string;

  constructor(
    protected _element: ElementRef
  ) {
    super(_element);
  }

  setEventsGA(): void { }

}
