import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { TextBlockModule } from './app/text-block.module';

platformBrowserDynamic().bootstrapModule(TextBlockModule)
  .catch(err => console.error(err));
