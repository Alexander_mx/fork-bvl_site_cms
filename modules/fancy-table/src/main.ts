import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { FancyTableModule } from './app/fancy-table.module';

platformBrowserDynamic().bootstrapModule(FancyTableModule)
  .catch(err => console.error(err));
