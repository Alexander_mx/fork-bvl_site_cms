import { environment } from '../environments/environment';

export class TableEndPoints {
  public static home = `${environment.API_URL}/static/stockquote/home`;
  public static company = `${environment.API_URL}/static/company`;
  public static search = `${environment.API_URL}/static/company/search`;
  public static graph = `${environment.API_URL}/static/stockquote/dailyShart`;
  public static sectors = `${environment.API_URL}/static/sector`;
  public static market = `${environment.API_URL}/static/stockquote/market`;
  public static index = `${environment.API_URL}/static/index`;
  public static top = `${environment.API_URL}/static/stockquote/top`;
  public static codification = `${environment.API_URL}/static/codingIsin`;
  public static negotiatedAmount = `${environment.API_URL}/static/negotiatedAmount/daily`;
}
