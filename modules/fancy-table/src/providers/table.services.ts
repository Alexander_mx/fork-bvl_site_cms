import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { IFiltersQuotation } from 'modules/issuer-details/src/app/issuer-details';
import { IssuerDetailsPoints } from 'modules/issuer-details/src/providers/endpoints';
import {
  ICodificationRequest,
  ICompanyResponse,
  IIndexResponse,
  ISectorsResponse,
  IStockquoteDailyShartResponse,
  IStockQuoteHomeResponse,
  IStockQuoteMarketResponse,
  IStockQuoteRequest,
  IStockquoteTopResponse,
  IStockTop
} from '../app/fancy-table';
import { TableEndPoints } from './endpoints';

@Injectable()
export class TableServices {

   constructor(
      private _apiService: ApiService) { }

   getHome(params: IStockQuoteRequest): Observable<IStockQuoteHomeResponse> {
      return this._apiService.post(TableEndPoints.home, params);
   }
   getCompany(): Observable<any> {
      return this._apiService.get(TableEndPoints.company);
   }
   getCompanySearch(params: any): Observable<ICompanyResponse> {
      return this._apiService.post(`${TableEndPoints.search}`, params);
   }
   getGraph(nemonico, today = '2019-03-06'): Observable<IStockquoteDailyShartResponse> {
      return this._apiService.get(TableEndPoints.graph, { params: { nemonico, today } });
   }
   getSharedValues(name: string, filters: IFiltersQuotation): Observable<any> {
      return this._apiService.get(`${IssuerDetailsPoints.shareValues}/${name}`, { params: filters });
   }
   getSectors(): Observable<ISectorsResponse> {
      return this._apiService.get(TableEndPoints.sectors);
   }
   getMarket(params: IStockQuoteRequest): Observable<IStockQuoteMarketResponse> {
      return this._apiService.post(TableEndPoints.market, params);
   }
   getIndex(): Observable<IIndexResponse> {
      return this._apiService.get(TableEndPoints.index);
   }
   getTop(params: IStockTop): Observable<IStockquoteTopResponse> {
      return this._apiService.post(TableEndPoints.top, params);
   }
   getCodifications(filters: ICodificationRequest): Observable<any> {
      const letter = /todos/gi.test(filters.letter)
         ? 'all'
         : filters.letter;

      return this._apiService.get(`${TableEndPoints.codification}?character=${letter}&sectorCode=${filters.sector}`);
   }
   getResumeChart(date): Observable<any> {
      return this._apiService.post(TableEndPoints.negotiatedAmount, { date });
   }
}
