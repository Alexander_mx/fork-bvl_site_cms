import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable } from 'rxjs';

import { sortByAmount, sortByArray, sortByBoolean, sortByDate, sortByNumber, sortByString } from '@bvl-core/shared/helpers/util/sort';
import { IFancyTableColumn } from '@bvl/library';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { IFancyTableData } from './fancy-table';

export class FancyTableDataSource<T> extends DataSource<T> {
  constructor(
    private _inputData: Array<T>,
    private _moduleId,
    // TODO: Borrar en componente que usan esta clase
    private _exchangeProvider: ExchangeProvider
  ) {
    super();
  }

  private _renderData = new BehaviorSubject<Array<T>>(
    this._processData(this._inputData)
  );

  private _processData(data: any): Array<T> {
    return data.map((table, i) => ({
      id: `${this._moduleId}-table-${i}${table.title}`,
      theme: table.theme,
      title: table.title,
      iconTitle: table.iconTitle,
      entries: new BehaviorSubject<Array<T>>(
        table.entries.map(el => ({ ...el, detailsShowed: false }))
      ),
      metadata: {
        sortedBy: this.getSortedBy(table),
        expandedElement: null,
        loadedCharts: []
      }
    }));
  }

  private getSortedBy(table: any): any {
    const or = {
      column: table && table.metadata && table.metadata.sortedBy && table.metadata.sortedBy.column || null,
      ascendingOrder: !!(table && table.metadata && table.metadata.sortedBy && table.metadata.sortedBy.ascendingOrder)
    };

    return or;
  }

  public get data(): any {
    return this._renderData.value;
  }
  public set data(data: any) {
    this._renderData.next(this._processData(data));
  }

  update(data: Array<T>): void {
    this._renderData.next(data);
  }

  sort(column: IFancyTableColumn, table: IFancyTableData): void {
    const ascendingOrder =
      table.metadata.sortedBy.column === column
        ? !table.metadata.sortedBy.ascendingOrder
        : true;

    table.metadata.sortedBy.ascendingOrder = ascendingOrder;

    const orderedEntries = table.entries.value.sort((a, b) => {
      const aValue = a[column.name];
      const bValue = b[column.name];
      switch (column.valueType) {
        case 'string':
          return sortByString(aValue, bValue, ascendingOrder);
        case 'number':
        case 'price':
        case 'percent':
          return sortByNumber(aValue, bValue, ascendingOrder);
        case 'date':
          return sortByDate(aValue, bValue, ascendingOrder);
        case 'amount':
          return sortByAmount(a, b, column.name, ascendingOrder);
        case 'list':
        case 'facts':
          return sortByArray(aValue, bValue, ascendingOrder);
        case 'boolean':
          return sortByBoolean(aValue, bValue, ascendingOrder);
        default:
          break;
      }
    });
    table.entries.next(orderedEntries);
    table.metadata.sortedBy.column = column;
  }

  connect(): Observable<Array<T>> {
    return this._renderData;
  }

  disconnect(): void { }
}
