import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AmountCellModule, LabelModule, LinksListModule, TopScollbarModule } from '@bvl/library';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { StockTableHeaderComponent } from './fancy-table-header/fancy-table-header.component';
import { FancyTableComponent } from './fancy-table.component';

@NgModule({
  declarations: [
    FancyTableComponent,
    StockTableHeaderComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AmountCellModule,
    LabelModule,
    LinksListModule,
    FlexLayoutModule,
    TopScollbarModule,
    BrowserAnimationsModule
  ],
  entryComponents: [
    FancyTableComponent,
    StockTableHeaderComponent
  ],
  exports: [
    FancyTableComponent
  ],
  providers: [
    ExchangeProvider,
    GraphicOptionsProvider
  ]
})
export class FancyTableModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-fancy-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(FancyTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
