import { formatAnchor } from '@bvl-core/shared/helpers/util/format-anchor';
import { getStorageLanguage, IFancyTableColumn, LANGUAGES } from '@bvl/library';

export type FancyTableTheme = 'green' | 'red' | 'blue';

const TEXT = LANGUAGES[getStorageLanguage()];

export interface IFancyTable<T> {
  title?: string;
  iconTitle?: string;
  theme?: FancyTableTheme;
  entries: Array<T>;
}
export interface IFancyTableEntry {
  [key: string]: any;
  detailsShowed?: boolean;
}
export interface IFancyTableData {
  id: string;
  theme?: FancyTableTheme;
  iconTitle?: string;
  title?: string;
  entries: IFancyTableEntry;
  metadata: {
    sortedBy: {
      column: IFancyTableColumn;
      ascendingOrder: boolean;
    };
    expandedElement: IFancyTableEntry;
    loadedCharts: Array<number>;
  };
}
export interface IStockQuoteRequest {
  sector?: string;
  isToday: boolean;
  companyCode?: string;
  inputCompany?: string;
}
export interface IContentValues<T> {
  content: Array<T>;
  down: number;
  equal: number;
  up: number;
}
export interface IStockQuoteMarketResponse extends IContentValues<IStockQuote> { }
export interface IStockQuoteHomeResponse extends Array<IStockQuote> { }
export interface IStockQuote {
  companyCode: string;
  companyName: string;
  shortName: string;
  nemonico: string;
  sectorCode: string;
  sectorDescription: string;
  lastDate: string;
  previousDate: string;
  buy: number;
  sell: number;
  last: number;
  minimun: number;
  maximun: number;
  opening: number;
  previous: number;
  negotiatedQuantity: string;
  negotiatedAmount: number;
  operationsNumber: string;
  exderecho: string;
  percentageChange: number;
  currency: string;
  unity: string;
  segment: string;
}
export interface IStockTop {
  sector?: string;
  today?: boolean;
  date?: string;
  top?: number;
}
export interface IGraphRequest {
  nemonico: string;
  startDate: string;
  endDate: string;
}
export interface ICodificationRequest {
  letter: string;
  sector: string;
}
export interface IUpdateWatchListRequest {
  idUser: string;
  nemonico: string;
  add: boolean;
}
export interface IStockquoteDailyShartResponse extends Array<IStockquoteDailyShart> { }
export interface IStockquoteDailyShart {
  currency: string;
  lastDate: string;
  lastValue: number;
  nemonico: string;
}
export interface IShare {
  nemonico: string;
  companyName: string;
  companyCode: string;
  updatedDate: string;
  values: Array<IShareValue>;
  dailyValues: Array<Array<string>>;
}
export interface IShareValue {
  id: string;
  nemonico: string;
  date: string;
  open: number;
  close: number;
  high: number;
  low: number;
  average: number;
  quantityNegotiated: number;
  solAmountNegotiated: number;
  dollarAmountNegotiated: number;
  yesterday: string;
  yesterdayClose: number;
  currencySymbol: string;
}
export interface ITableLink {
  title: string;
  url: string;
}
export interface ICodificationTable {
  name: string;
  data: Array<{
    mode: string;
    isin: string;
    nemonic: string;
    group: string;
    type: string;
    currency: string;
    nominalValue: string;
    rate: string | number;
  }>;
}
export interface ISector {
  description: string;
  sectorCode: string;
}
export interface ISectorsResponse extends Array<ISector> { }

export interface ICompany {
  companyCode: string;
  companyName: string;
  index: Array<string>;
  memory: Array<IMemory>;
  sectorCode: string;
  sectorDescription: string;
  stock: Array<string>;
}

export interface IMemory {
  companyName: string;
  date: string;
  document: string;
  observation: string;
  path: string;
  rpjCode: string;
  sequence: number;
  type: string;
  year: number;
}

export interface ICompanyResponse extends Array<ICompany> { }

export interface IIndex {
  close: string;
  dailyValues: Array<Array<string>>;
  date: string;
  high: string;
  low: string;
  name: string;
  nemonico: string;
  open: string;
  shortName: string;
  value: string;
  variation: string;
  variationPoints: string;
}

export interface IIndexResponse extends IContentValues<IIndex> { }

export interface IStockquoteTopItem {
  companyCode: string;
  currency: string;
  last: number;
  nemon: string;
  percentageChange: number;
}
export interface IStockquoteTopResponse {
  gain: Array<IStockquoteTopItem>;
  loser: Array<IStockquoteTopItem>;
}

export const defaultLinks = [
  { title: TEXT.issuers.corporativeInfo },
  { title: TEXT.issuers.value },
  { title: TEXT.issuers.quotationHistory },
  { title: TEXT.issuers.importantFacts },
  { title: TEXT.issuers.finantialInfo },
  { title: TEXT.issuers.memories }
];
export const defaultIndexLinks = [
  { title: TEXT.general.whatIs },
  { title: TEXT.general.components },
  { title: TEXT.general.downloadDocumentos },
  { title: TEXT.general.frequentQuestions }
];

export const setLinks = (companyCode: string | number, index?: boolean): Array<ITableLink> => {
  const newLinks: Array<ITableLink> = [];
  if (companyCode !== 'XXX') {
    (index ? defaultIndexLinks : defaultLinks).forEach(item => {
      newLinks.push({
        title: item.title,
        url: (index)
          ? `${location.origin}${TEXT.indexDetailUrl[companyCode]}`
          : `${location.origin}/emisores/detalle?companyCode=${companyCode}#${formatAnchor(item.title)}`
      });
    });
  }

  return newLinks;
};
