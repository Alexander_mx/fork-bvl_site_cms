import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

import { coerceBooleanProp } from '@bvl/library';
import {
  FancyTableByInitialFilter,
  FancyTableCapability,
  IFancyTableByCategoryFilter,
  IFancyTableByTypeFilter
} from './fancy-table-header';

@Component({
  selector: 'bvl-fancy-table-header',
  templateUrl: './fancy-table-header.component.html',
  styleUrls: ['./fancy-table-header.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class StockTableHeaderComponent implements OnInit, OnDestroy {

  private _fullWidth: boolean;
  private _mediaWatcher: Subscription;
  private _activeFilters: Array<{ type: FancyTableCapability, value: any, filterId?: number }> = [];

  @Input() title: string;
  @Input() capabilities: Array<FancyTableCapability> = [];
  @Input() byTypeFilter: IFancyTableByTypeFilter;
  @Input() byInitialFilter: FancyTableByInitialFilter;
  @Input() byCategoryFilters: Array<IFancyTableByCategoryFilter> = [];
  @Input() entriesNumber: number;

  @Input()
  get fullWidth(): any { return this._fullWidth; }
  set fullWidth(value: any) { this._fullWidth = coerceBooleanProp(value); }

  @Output() activeFilters = new EventEmitter<any>();

  tableCapabilities: Array<FancyTableCapability>;
  filterOnlyToday: false;
  onlyTodayEnabled = true;
  searchTerm = '';
  searchFocused = false;
  searchInputShowed = true;
  selectedCategories: Array<{ filterId: number, value: number }> = [];
  selectedType: string | number | null = null;
  selectedInitial: string | null = null;
  selectedTime = 1;

  constructor(public media: ObservableMedia) { }

  ngOnInit(): void {
    this._setTableCapabilities();

    this._mediaWatcher = this.media.subscribe((change: MediaChange) => {
      if (change.mqAlias === 'xs' || change.mqAlias === 'sm') {
        if (!this.searchTerm.length) {
          this.searchInputShowed = false;
        }
      } else {
        this.searchInputShowed = true;
      }
    });

    if (this.byCategoryFilters) {
      this._setInitialCategories();
    }
  }

  ngOnDestroy(): void {
    this._mediaWatcher.unsubscribe();
  }

  isCapableOf(capability): boolean {
    return this.tableCapabilities.indexOf(capability) !== -1;
  }

  onCategoryChange(byCategoryFilterId): void {
    const categoryChanged = this.selectedCategories.find(cat => cat.filterId !== byCategoryFilterId);
    if (categoryChanged.value) {
      this._addActiveFilter('filterByCategory', categoryChanged.value, categoryChanged.filterId);
    } else {
      this._removeActiveFilter('filterByCategory', categoryChanged.filterId);
    }
  }
  onSearch(onlyIfEmpty = false): void {
    this.onlyTodayEnabled = this.searchTerm ? false : true;

    if (onlyIfEmpty && this.searchTerm.length) {
      return;
    }

    if (!this.searchTerm.length && !!this._activeFilters.find(f => f.type === 'search')) {
      return;
    }

    if (this.searchTerm) {
      this.filterOnlyToday = false;
      this._removeActiveFilter('filterOnlyToday', false);
      this._addActiveFilter('search', this.searchTerm);
    } else {
      this._removeActiveFilter('search');
    }
  }

  onOnlyTodayChange(): void {
    if (this.filterOnlyToday) {
      this._addActiveFilter('filterOnlyToday', this.selectedInitial);
    } else {
      this._removeActiveFilter('filterOnlyToday');
    }
  }

  onTypeChange(typeValue: number | string): void {
    if (this.selectedType === typeValue) {
      return;
    }
    this.selectedType = typeValue;
    if (this.selectedType === null) {
      this._removeActiveFilter('filterByType');
    } else {
      this._addActiveFilter('filterByType', this.selectedType);
    }
  }

  onInitialChange(initial: string): void {
    this.selectedInitial = this.selectedInitial === initial ? null : initial;
    if (this.selectedInitial === null) {
      this._removeActiveFilter('filterByInitial');
    } else {
      this._addActiveFilter('filterByInitial', this.selectedInitial);
    }
  }

  onTimeChange(): void {
    this._addActiveFilter('filterByTime', this.selectedTime);
  }

  setFocusToElement(elementRef): void {
    elementRef.focus();
  }

  toggleSearch(): void {
    this.searchInputShowed = !this.searchInputShowed;
  }

  resetSearch(): void {
    this.searchTerm = '';
    this.onSearch(true);
  }

  isNumber(val: any): boolean {
    return typeof val === 'number';
  }

  private _setTableCapabilities(): void {
    this.tableCapabilities = (this.byTypeFilter)
      ? [...this.capabilities, 'filterByType']
      : this.capabilities.filter(cap => cap !== 'filterByType');

    this.tableCapabilities = (this.byCategoryFilters)
      ? [...this.tableCapabilities, 'filterByCategory']
      : this.tableCapabilities.filter(cap => cap !== 'filterByCategory');

    this.tableCapabilities = (this.byInitialFilter)
      ? [...this.tableCapabilities, 'filterByInitial']
      : this.tableCapabilities.filter(cap => cap !== 'filterByInitial');
  }

  private _setInitialCategories(): void {
    this.selectedCategories = this.byCategoryFilters.map(catFilter => (
      {
        filterId: catFilter.filterId,
        value: catFilter.defaultOption || null
      }
    ));
  }

  private _addActiveFilter(
    type: FancyTableCapability,
    value: string | number | boolean,
    filterId = null
  ): void {
    if (filterId) {
      this._activeFilters = [
        ...this._activeFilters.filter(f => f.filterId !== filterId),
        { filterId, type, value }
      ];
    } else {
      this._activeFilters = [
        ...this._activeFilters.filter(f => f.type !== type),
        { type, value }
      ];
    }

    this.activeFilters.emit(this._activeFilters);
  }

  private _removeActiveFilter(type: FancyTableCapability, filterId = null): void {
    const previousActiveFilters = this._activeFilters;

    this._activeFilters = filterId
      ? this._activeFilters.filter(f => f.filterId !== filterId)
      : this._activeFilters.filter(f => f.type !== type);

    if (previousActiveFilters.length === this._activeFilters.length) {
      return;
    }

    this.activeFilters.emit(this._activeFilters);
  }
}
