export type FancyTableCapability = 'sort' | 'filter' | 'search' | 'filterOnlyToday'
                                          | 'filterByCategory' | 'filterByType' | 'filterByInitial'
                                          | 'filterByTime';

export type FancyTableByInitialFilter = Array<string>;

export interface IFancyTableByTypeFilter {
  label: string;
  options: Array<{ id: number, label: string }>;
}

export interface IFancyTableByCategoryFilter {
  filterId: number;
  title?: string;
  label?: string;
  defaultOption: number;
  options: Array<{ id: number, label: string }>;
}
