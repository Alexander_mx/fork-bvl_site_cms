import { animate, state, style, transition, trigger } from '@angular/animations';
import { formatDate } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  LOCALE_ID,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewEncapsulation
} from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import * as Highcharts from 'highcharts';
import { Subscription } from 'rxjs';

import { coerceBooleanProp, coerceNumberProp, IFancyTableColumn, IPopover, IStock, TableEntry } from '@bvl/library';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { IFancyTable, IFancyTableData, IFancyTableEntry } from './fancy-table';
import { FancyTableDataSource } from './fancy-table-data-source';
import {
  FancyTableByInitialFilter,
  FancyTableCapability,
  IFancyTableByCategoryFilter,
  IFancyTableByTypeFilter
} from './fancy-table-header/fancy-table-header';

let nextId = 0;

@Component({
  selector: 'bvl-fancy-table',
  templateUrl: './fancy-table.component.html',
  styleUrls: ['./fancy-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, hidden', style({ height: '0px', minHeight: '0' })),
      state('expanded, shown', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('.2s cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ],
  encapsulation: ViewEncapsulation.Emulated
})
export class FancyTableComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  id = `bvl-table-${nextId++}`;
  tablesData: FancyTableDataSource<IFancyTableData>;

  private _tables: Array<IFancyTable<TableEntry>> | any;
  private _byCategoryFilters: Array<IFancyTableByCategoryFilter> | any;
  private _fullWidth: boolean;
  private _withDetailsRow: boolean;
  private _showEntriesNumber: boolean;
  private _maxRows: number = null;
  private _tableMinWidth: number;
  private _elem: HTMLElement;
  private _tablesContainer: HTMLElement;
  private _tableElems: NodeListOf<HTMLElement>;
  private _mediaSubscription: Subscription;
  private _tableDataSubscription: Subscription;

  @Input() tablesTitle: string;
  @Input()
  get tables(): any {
    return this._tables;
  }
  set tables(tables: IFancyTable<TableEntry> | Array<IFancyTable<TableEntry>> | any) {
    if (typeof tables !== 'object') { tables = JSON.parse(tables); }
    if (Array.isArray(tables)) {
      this._updateDataSource(tables);
    } else {
      this._updateDataSource([tables]);
    }
  }

  @Input() tableColumns: any | Array<IFancyTableColumn>;
  @Input() url: string;
  @Input() capabilities: any | Array<FancyTableCapability>;

  @Input() byTypeFilter: any | IFancyTableByTypeFilter;
  @Input() byInitialFilter: any | FancyTableByInitialFilter;

  @Input()
  get byCategoryFilters(): Array<IFancyTableByCategoryFilter> | any { return this._byCategoryFilters; }
  set byCategoryFilters(byCategoryFilters: IFancyTableByCategoryFilter | Array<IFancyTableByCategoryFilter> | any) {
    byCategoryFilters = JSON.parse(byCategoryFilters);
    this._byCategoryFilters = Array.isArray(byCategoryFilters) ? byCategoryFilters : [byCategoryFilters];
  }

  @Input()
  get fullWidth(): any { return this._fullWidth; }
  set fullWidth(value: any) { this._fullWidth = coerceBooleanProp(value); }

  @Input()
  get withDetailsRow(): any { return this._showEntriesNumber; }
  set withDetailsRow(value: any) { this._showEntriesNumber = coerceBooleanProp(value); }

  @Input()
  get showEntriesNumber(): any { return this._withDetailsRow; }
  set showEntriesNumber(value: any) { this._withDetailsRow = coerceBooleanProp(value); }

  @Input()
  get maxRows(): any { return this._maxRows; }
  set maxRows(value: any) {
    this._maxRows = coerceNumberProp(value) ? value : null;
  }

  @Output() activeFilters = new EventEmitter<any>();
  @Output() tableEvents = new EventEmitter<any>();

  headerTitle: string;
  chartOptions: object;
  columnsName: Array<string>;
  entriesNumber: number;

  tablesFitInContainer = true;
  viewLoaded = false;
  selectedTable = undefined;
  expandedElement: { tableId: IFancyTableData['id'], element: IStock };

  scrollingTable = false;
  headerCellsWidth: Array<number> = [];

  constructor(
    private exchange: ExchangeProvider,
    public media: ObservableMedia,
    private _ngZone: NgZone,
    private _elementRef: ElementRef,
    private _renderer: Renderer2,
    private _cdr: ChangeDetectorRef,
    private _graphicOptions: GraphicOptionsProvider,
    @Inject(LOCALE_ID) private locale: string
  ) {

    this._mediaSubscription = this.media.subscribe(() => {
      if (this.viewLoaded) {
        this._setTablesMinWidth();
        this._setTablesFitting();
        this._setScrollingTable();
        this._setHeaderCellsWidth();
      }
    });

  }

  private isString = (object: any) => (typeof object === 'string');

  ngOnInit(): void {
    if (this.isString(this.capabilities)) { this.capabilities = JSON.parse(this.capabilities); }
    if (this.isString(this.tableColumns)) { this.tableColumns = JSON.parse(this.tableColumns); }
    if (this.isString(this.byTypeFilter)) { this.byTypeFilter = JSON.parse(this.byTypeFilter); }
    if (this.isString(this.byInitialFilter)) { this.byInitialFilter = JSON.parse(this.byInitialFilter); }

    this.chartOptions = this._graphicOptions.getGraphicOptions();
    this.headerTitle = this.tablesTitle ||
      (this.hasAnyTableTitle() && this.tablesData.data.length === 1
        ? this.tablesData.data[0].title : undefined);

    this.columnsName = this.tableColumns && this.tableColumns
      .filter(column => this.tablesData.data
        .every((table: any) => table.entries.value.every(entry => column.name in entry)))
      .map(column => column.name) || [];

    this._tableDataSubscription = this.tablesData.connect()
      .subscribe(tableData => {
        this.entriesNumber = tableData.reduce((totalEntries, table) => {
          return totalEntries += table.entries.value.length;
        }, 0);
      });
  }

  ngAfterViewInit(): void {
    this._elem = this._elementRef.nativeElement;
    this._tablesContainer = this._elem.querySelector('.tables-container');
    this._tableElems = this._elem.querySelectorAll('.table-article');

    if (this.tablesData.data.length > 1) {
      this.selectedTable = this.tablesData.data[0].id;
    }
  }

  ngOnChanges(): void {
    const onStableSubscription: Subscription = this._ngZone.onStable.subscribe(() => {
      this._setTablesMinWidth();
      this.viewLoaded = true;
      this._setTablesFitting();
      this._setScrollingTable();
      onStableSubscription.unsubscribe();
    });
  }

  ngOnDestroy(): void {
    if (this._mediaSubscription) { this._mediaSubscription.unsubscribe(); }
    if (this._tableDataSubscription) { this._tableDataSubscription.unsubscribe(); }
  }

  hasAnyTableTitle(): boolean {
    return !!this.tablesData.data.find(table => !!table.title);
  }

  onFilterChange(activeFilters): void {
    this.activeFilters.emit(activeFilters);
  }

  getAnimationState(element: IFancyTableEntry, table: IFancyTableData): string {
    const areDetailsShown = table.metadata && table.metadata.expandedElement === element;
    if (areDetailsShown) {
      return (this.media.isActive('gt-sm') && this.tablesData.data.length === 1)
        ? 'expanded' : 'shown';
    } else {
      return (this.media.isActive('gt-sm') && this.tablesData.data.length === 1)
        ? 'collapsed' : 'hidden';
    }
  }

  expandElement(element: IFancyTableEntry, table: IFancyTableData): any {
    if (element === table.metadata.expandedElement) {
      table.metadata.expandedElement = null;

      return;
    }

    table.metadata.expandedElement = element;
    element.detailsShowed = true;
    if (!this._isChartLoaded(element, table)) {
      this._loadChart(element, table.id);
      table.metadata.loadedCharts = [...table.metadata.loadedCharts, element.id];
    }
  }

  sortBy(column: IFancyTableColumn, table: IFancyTableData): void {
    this.tablesData.sort(column, table);
  }

  toggleInWatchList(element: IFancyTableEntry): void {
    element.inWatchlist = !element.inWatchlist;
    this.tableEvents.emit({
      eventType: 'watchList',
      elementId: element.id,
      value: element.inWatchlist
    });
  }

  getFactPopover(title, date, url?): IPopover {
    return {
      title,
      class: 'fact-popover',
      body: formatDate(date, 'dd/MM/yy', this.locale),
      url: url || undefined
    };
  }

  private _loadChart(element: IFancyTableEntry, tableId: FancyTableComponent['id']): void {
    Highcharts.chart(
      `${tableId}-graphic-${element.id}`, this.chartOptions
    );
  }

  private _isChartLoaded(element: any, table: IFancyTableData): boolean {
    return table.metadata.loadedCharts.indexOf(element.id) !== -1;
  }

  @HostListener('window:resize') onresize(): void {
    this._ngZone.run(() => {
      this._setTablesFitting();
      this._setScrollingTable();
    });
  }

  private _setTablesMinWidth(): void {
    this._renderer.removeClass(this._tablesContainer, 'loaded');
    this._tableMinWidth = Math.max(...Array.from(this._tableElems)
      .map(t => t.clientWidth + 0));
    this._renderer.addClass(this._tablesContainer, 'loaded');
  }

  private _setTablesFitting(): void {
    this.tablesFitInContainer = (this._tablesContainer.clientWidth > this._tableMinWidth * this.tablesData.data.length);
  }

  private _setScrollingTable(): void {
    if (!this.maxRows || this.tablesData.data.length > 1) {
      return;
    }
    const wrapper = this._elementRef.nativeElement.querySelector('.table-wrapper');
    const table = wrapper.querySelector('[bvl-table]');
    this._unsetScrollingTableStyles(wrapper, table);
    const visibleRows = Array.prototype.slice.call(
      table.querySelectorAll('tbody tr'),
      0,
      this.maxRows
    );
    const onStableSubscription: Subscription = this._ngZone.onStable.subscribe(() => {
      const headerRowHeight = table.querySelector('thead tr').clientHeight;
      const tableMaxHeight = visibleRows.reduce((height, row) => height += row.clientHeight, 0);
      if (tableMaxHeight < wrapper.clientHeight) {
        this.scrollingTable = true;
        this._setScrollingTableStyles(wrapper, table, tableMaxHeight, headerRowHeight);
        this._setHeaderCellsWidth();
      } else {
        this.scrollingTable = false;
        this._unsetScrollingTableStyles(wrapper, table);
      }
      onStableSubscription.unsubscribe();
    });
  }

  private _setScrollingTableStyles(wrapper, table, tableMaxHeight, headerRowHeight): void {
    this._setStyles(wrapper, { maxHeight: `${tableMaxHeight}px` });
    this._setStyles(table, {
      position: 'relative',
      top: `${-headerRowHeight}px`,
      marginBottom: `${-headerRowHeight}px`
    });
    this._setScrollingTableWidth(wrapper, table);
    this._cdr.detectChanges();
  }

  private _unsetScrollingTableStyles(wrapper, table): void {
    this._removeStyles(wrapper, ['maxHeight']);
    this._removeStyles(table, ['position', 'top', 'marginBottom']);
  }

  private _setHeaderCellsWidth(): void {
    const headerCells = Array.prototype.slice.call(this._elementRef.nativeElement.querySelectorAll('.bvl-header-cell'));
    this.headerCellsWidth = headerCells.map(cell => cell.clientWidth);
    this._cdr.detectChanges();
  }

  private _setScrollingTableWidth(wrapper, table): void {
    const onStableSubscription: Subscription = this._ngZone.onStable.subscribe(() => {
      const scrollingHeaderTable = wrapper.querySelector('.scrolling-table-header');
      this._setStyles(scrollingHeaderTable, { width: `${table.clientWidth}px` });
      onStableSubscription.unsubscribe();
    });
  }

  private _setStyles(element: HTMLElement, styles: { [prop: string]: any }): void {
    Object.keys(styles)
      .forEach(prop => {
        this._renderer.setStyle(element, prop, styles[prop]);
      });
  }

  private _removeStyles(element: HTMLElement, styles: Array<string>): void {
    styles.forEach(prop => {
      this._renderer.removeStyle(element, prop);
    });
  }

  private _updateDataSource(data: any): void {
    if (!this.tablesData) {
      this.tablesData = new FancyTableDataSource(data, this.id, this.exchange);
    } else {
      this.tablesData.data = data;
    }
  }
}
