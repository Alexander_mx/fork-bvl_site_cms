import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ImgInfoModule } from './app/img-info.module';

platformBrowserDynamic()
.bootstrapModule(ImgInfoModule)
  .catch(err => console.error(err));
