import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { DomChangeDirectiveModule, SectionHeaderModule } from '@bvl/library';
import { ImgInfoComponent } from './img-info.component';

@NgModule({
  imports: [CommonModule, SectionHeaderModule, BrowserModule, DomChangeDirectiveModule],
  declarations: [ImgInfoComponent],
  exports: [ImgInfoComponent],
  entryComponents: [ImgInfoComponent]
})
export class ImgInfoModule {
  constructor(private injector: Injector) { }
  ngDoBootstrap(): void {
    const name = 'bvl-img-info'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ImgInfoComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
