import { environment_client } from '@bvl-site/environments/environment';

export const environment = {
  production: environment_client.production,
  API_URL: environment_client.API_URL
};
