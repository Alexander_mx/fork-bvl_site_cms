interface IFile {
  name: string;
  url: string;
  ext: string;
}

export interface IAccordionResource {
  title: string;
  files: Array<IFile>;
}
