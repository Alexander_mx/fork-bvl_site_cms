import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule, LabelModule } from '@bvl/library';
import { AccordionResourcesComponent } from './accordion-resources.component';

@NgModule({
  declarations: [
    AccordionResourcesComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AccordionModule,
    LabelModule,
    FlexLayoutModule,
    BrowserAnimationsModule
  ],
  providers: [],
  entryComponents: [AccordionResourcesComponent]
})
export class AccordionResourcesModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap(): void {
    const name = 'bvl-accordion-resources'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(AccordionResourcesComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
