import { Component, Input } from '@angular/core';
import { IAccordionResource } from './accordion-resource';

@Component({
  selector: 'bvl-accordion-resources',
  templateUrl: './accordion-resources.component.html',
  styleUrls: ['./accordion-resources.component.scss']
})
export class AccordionResourcesComponent {
  private _resources: Array<IAccordionResource>;
  @Input()
  get resources(): any {
    return this._resources;
  }
  set resources(value: any) {
    this._resources = JSON.parse(value) as Array<IAccordionResource>;
  }
}
