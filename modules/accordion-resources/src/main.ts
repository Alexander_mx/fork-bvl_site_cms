import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AccordionResourcesModule } from './app/accordion-resources.module';

platformBrowserDynamic().bootstrapModule(AccordionResourcesModule)
  .catch(err => console.error(err));
