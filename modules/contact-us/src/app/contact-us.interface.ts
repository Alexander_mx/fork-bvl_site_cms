export interface ITermsConditions {
  title: string;
  description: string;
}
