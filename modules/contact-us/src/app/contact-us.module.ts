import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { createCustomElement } from '@angular/elements';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { ApiModule, BvlFormAlertModule, BvlFormCheckboxModule, BvlFormInputModule, ModalModule, SectionHeaderModule, MODAL_CONFIG } from '@bvl/library';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { ContactUsComponent } from './contact-us.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';

@NgModule({
  declarations: [
    ContactUsComponent,
    TermsConditionsComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    ReactiveFormsModule,
    BvlFormAlertModule,
    BvlFormInputModule,
    BvlFormCheckboxModule,
    RecaptchaFormsModule,
    RecaptchaModule,
    ControlErrorModule,
    ApiModule,
    ModalModule.forRoot(MODAL_CONFIG)
  ],
  providers: [],
  entryComponents: [ContactUsComponent, TermsConditionsComponent]
})
export class ContactUsModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-contact-us'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ContactUsComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
