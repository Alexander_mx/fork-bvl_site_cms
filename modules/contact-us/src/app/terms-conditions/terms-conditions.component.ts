import { Component, Input, OnInit } from '@angular/core';
import { ITermsConditions } from '../contact-us.interface';

@Component({
  selector: 'bvl-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss']
})
export class TermsConditionsComponent implements OnInit {

  @Input() termsConditions: Array<ITermsConditions>;

  constructor() { }

  ngOnInit(): void { }

}
