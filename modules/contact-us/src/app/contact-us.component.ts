import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { environment_client } from '@bvl-site/environments/environment';
import {
  GaUnsubscribeBase,
  getStorageLanguage,
  IModalConfig,
  LANGUAGES,
  ModalRef,
  ModalService
} from '@bvl/library';
import { takeUntil } from 'rxjs/operators';
import { IContactUs } from '../providers/contactus.interface';
import { ContactusService } from '../providers/contactus.service';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';

@Component({
  selector: 'bvl-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent extends GaUnsubscribeBase implements OnInit {

  @Input() identifylead: string;
  @Input() title: string;
  @Input() description: string;

  responseMessage: string;
  messageType: string;
  frmContactUs: FormGroup;
  mFullName: AbstractControl;
  mPhone: AbstractControl;
  mEmail: AbstractControl;
  mTerms: AbstractControl;
  recaptchaKey: string;

  TEXT: any;

  constructor(
    protected _element: ElementRef,
    private _formBuilder: FormBuilder,
    private _contactusService: ContactusService,
    private _modalService: ModalService
  ) {
    super(_element);
    this.TEXT = LANGUAGES[
      getStorageLanguage()
        .substring(0, 2)
    ];
    this.title = '';
    this.description = '';
  }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.frmContactUs = this._formBuilder.group({
      mFullName: [null, [Validators.required, Validators.maxLength(120), ValidatorUtil.onlyLetter()]],
      mPhone: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(20), ValidatorUtil.phoneNumber()]],
      mEmail: [null, [Validators.required, Validators.email, Validators.maxLength(120)]],
      mRecaptcha: [null, Validators.required],
      mTerms: [false, Validators.requiredTrue]
    });
    this.mFullName = this.frmContactUs.get('mFullName');
    this.mPhone = this.frmContactUs.get('mPhone');
    this.mEmail = this.frmContactUs.get('mEmail');
    this.mTerms = this.frmContactUs.get('mTerms');
    this.recaptchaKey = environment_client.RECAPTCHA_KEY;
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControl(this.frmContactUs, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmContactUs);

    return this.frmContactUs.valid;
  }

  private _paramsContactUs(): IContactUs {
    return {
      identifyLead: this.identifylead,
      fullName: this.mFullName.value,
      phone: this.mPhone.value,
      email: this.mEmail.value
    } as IContactUs;
  }

  private _message(messageType: string): void {
    this.messageType = messageType;
    this.responseMessage = this.TEXT.contactus.message[messageType];
    if (this.messageType === 'success') {
      this.frmContactUs.reset();
    }
    setTimeout(() => {
      this.messageType = '';
      this.responseMessage = '';
    }, 4000);
  }

  contact(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._paramsContactUs();
      this._contactusService.contactUs(params)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => {
          this._message('success');
        }, () => {
          this._message('error');
        });
    }
  }

  termsConditions(): void {
    const modalConfig = {
      titleText: this.TEXT.general.termsAndConditions,
      showFooter: false
    } as IModalConfig;
    const termsConditions: ModalRef = this._modalService.open(TermsConditionsComponent, modalConfig);
    termsConditions.componentInstance.termsConditions = this.TEXT.contactus.modal.termsAndConditions;
    termsConditions.result
      .then(() => true,
      () => false);
  }

}
