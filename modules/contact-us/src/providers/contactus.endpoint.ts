import { environment } from './../environments/environment';

export class ContactUsEndpoint {
  public static contactUs = `${environment.API_URL}/static/contactlead/save`;
}