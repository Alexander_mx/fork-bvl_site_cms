export interface IContactUs {
  identifyLead: string;
  fullName: string;
  phone: string;
  email: string;
}
