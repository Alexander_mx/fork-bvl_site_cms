import { Injectable } from '@angular/core';
import { ApiService } from '@bvl/library';
import { Observable } from 'rxjs';
import { ContactUsEndpoint } from './contactus.endpoint';
import { IContactUs } from './contactus.interface';

@Injectable({
  providedIn: 'root'
})
export class ContactusService {

  constructor(
    private _apiService: ApiService
  ) { }

  contactUs(request: IContactUs, preloader: boolean = false): Observable<any> {
    return this._apiService.post(ContactUsEndpoint.contactUs, request, { preloader });
  }
}
