import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ContactUsModule } from './app/contact-us.module';

platformBrowserDynamic().bootstrapModule(ContactUsModule)
  .catch(err => console.error(err));
