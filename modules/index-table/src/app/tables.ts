import { getStorageLanguage, LANGUAGES } from '@bvl/library';
import { ITableLink } from 'modules/fancy-table/src/app/fancy-table';

const TEXT = LANGUAGES[
  getStorageLanguage()
  .substring(0, 2)
];

export const INITIAL_BODY = {
  sector: '',
  isToday: true,
  companyCode: '',
  inputCompany: ''
};

export const DAILY_MOVEMENTS_COLUMNS = [
  {
    name: 'index',
    label: TEXT.general.index,
    valueType: 'string'
  },
  {
    name: 'last',
    label: TEXT.tables.last,
    valueType: 'amount'
  },
  {
    name: 'previous',
    label: TEXT.tables.ant,
    valueType: 'amount'
  },
  {
    name: 'open',
    label: TEXT.tables.open,
    valueType: 'amount'
  },
  {
    name: 'high',
    label: 'Max',
    valueType: 'amount'
  },
  {
    name: 'low',
    label: 'Min',
    valueType: 'amount'
  },
  {
    name: 'variation',
    label: `${TEXT.tables.var}%`,
    valueType: 'string'
  },
  {
    name: 'inWatchlist',
    label: 'Watchlist',
    valueType: 'boolean'
  }
];

export interface IIndexRowView {
  index: string;
  last: string;
  previous: string;
  open: string;
  high: string;
  low: string;
  variation: string;
  dailyValues: Array<Array<string>>;
  inWatchlist: boolean;
  mnemonic: string;
  links: Array<ITableLink>;
}
