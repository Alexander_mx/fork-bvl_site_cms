import { animate, state, style, transition, trigger } from '@angular/animations';
import { formatDate } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  LOCALE_ID,
  NgZone,
  OnChanges,
  OnInit,
  Output,
  Renderer2,
  ViewEncapsulation
} from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import * as Highcharts from 'highcharts';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { IFancyTableEntry, IFancyTableSortedBy } from '@bvl-core/shared/helpers/models/fancy-table.model';
import {
  coerceBooleanProp,
  ConfigurationService,
  DateUtil,
  GaUnsubscribeBase,
  getStorageLanguage,
  IFancyTableColumn,
  IPopover,
  IStock,
  LANGUAGES,
  WatchListService,
  WITHOUT_DATA_TYPE
} from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { IFancyTableData, IIndex, IUpdateWatchListRequest, setLinks } from 'modules/fancy-table/src/app/fancy-table';
import { FancyTableDataSource } from '../../../fancy-table/src/app/fancy-table-data-source';
import { environment } from '../environments/environment';
import { DAILY_MOVEMENTS_COLUMNS, IIndexRowView } from './tables';

let nextId = 0;
@Component({
  selector: 'bvl-index-table',
  templateUrl: './index-table.component.html',
  styleUrls: ['./index-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, hidden', style({ height: '0px', minHeight: '0' })),
      state('expanded, shown', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('.2s cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class IndexTableComponent extends GaUnsubscribeBase implements OnInit, AfterViewInit, OnChanges {
  WITHOUT_DATA_TYPE = WITHOUT_DATA_TYPE;

  id = `bvl-index-table-${nextId++}`;
  tablesData: FancyTableDataSource<IFancyTableData> = {} as FancyTableDataSource<IFancyTableData>;
  private _userProfile = this._configService.getUserProfile();
  private _elem: HTMLElement;
  private _tablesContainer: HTMLElement;

  private _privateSite = false;
  @Input()
  get privateSite(): boolean {
    return this._privateSite;
  }
  set privateSite(value: boolean) {
    this._privateSite = coerceBooleanProp(value);
  }

  tableColumns: Array<IFancyTableColumn> = DAILY_MOVEMENTS_COLUMNS;

  @Output() activeFilters = new EventEmitter<any>();
  @Output() tableEvents = new EventEmitter<any>();

  headerTitle: string;
  chartOptions: any;
  columnsName: Array<string>;
  entriesNumber: number;

  viewLoaded = false;
  expandedElement: { tableId: IFancyTableData['id'], element: IStock };

  up: number;
  equal: number;
  down: number;

  currentPage: number;
  completeData: Array<IIndexRowView>;
  maxRows = 15;
  TEXT = LANGUAGES[getStorageLanguage()];

  constructor(
    private exchange: ExchangeProvider,
    public media: ObservableMedia,
    private _ngZone: NgZone,
    protected _element: ElementRef,
    private _renderer: Renderer2,
    private _cdr: ChangeDetectorRef,
    private _tableServices: TableServices,
    private _graphicOptions: GraphicOptionsProvider,
    private _configService: ConfigurationService,
    private _watchListService: WatchListService,
    @Inject(LOCALE_ID) private locale: string
  ) {
    super(_element);
    this.media
      .asObservable()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        if (this.viewLoaded) {
          this._setTablesMinWidth();
          this._setHeaderCellsWidth();
        }
      });
  }

  ngOnInit(): void {
    this._watchListService.setEnviroment(environment.API_URL);
    this._getIndexData();
    this.chartOptions = this._graphicOptions.getGraphicOptions();
  }

  ngAfterViewInit(): void {
    this._elem = this._element.nativeElement;
    this._tablesContainer = this._elem.querySelector('.tables-container');
  }

  ngOnChanges(): void {
    const onStableSubscription: Subscription = this._ngZone.onStable.subscribe(() => {
      this._setTablesMinWidth();
      this.viewLoaded = true;
      onStableSubscription.unsubscribe();
    });
  }

  hasAnyTableTitle(): boolean {
    return !!this.tablesData.data.find(table => !!table.title);
  }

  getAnimationState(element: IFancyTableEntry, table: IFancyTableData): string {
    const areDetailsShown = table.metadata && table.metadata.expandedElement === element;
    const exp = this.tablesData.data.length === 1 && this.media.isActive('gt-sm');

    return areDetailsShown ? (exp ? 'expanded' : 'shown') : (exp ? 'collapsed' : 'hidden');
  }

  expandElement(element: IFancyTableEntry, table: IFancyTableData): void {
    if (element === table.metadata.expandedElement) {
      table.metadata.expandedElement = null;

      return;
    }

    table.metadata.expandedElement = element;
    element.detailsShowed = true;
    if (!this._isChartLoaded(element, table)) {
      this._loadChart(element, table.id);
      table.metadata.loadedCharts = [...table.metadata.loadedCharts, element.id];
    }
  }

  sortBy(column: IFancyTableColumn, table: IFancyTableData): void {
    this.tablesData.sort(column, table);
  }

  toggleInWatchList(element: IFancyTableEntry): void {
    if (this._userProfile !== null) {
      const value = !element.inWatchlist;
      const body: IUpdateWatchListRequest = {
        idUser: this._userProfile.cognitoUsername,
        nemonico: element.mnemonic,
        add: value
      };

      this._watchListService.updateIndexWatchList(body)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => element.inWatchlist = !element.inWatchlist);
    } else {
      setTimeout(() => {
        location.href = `${location.origin}/login`;
      }, 500);
    }
  }

  getLabelWatchList(element: IFancyTableEntry): any {
    const action = this._userProfile ? (!element.inWatchlist ? 'Agregar' : 'Quitar') : 'Ir a Login';

    return { ...element, action };
  }

  getFactPopover(title, date, url?): IPopover {
    return {
      title,
      class: 'fact-popover',
      body: formatDate(date, 'dd/MM/yy', this.locale),
      url: url || undefined
    };
  }

  private _loadChart(element: IFancyTableEntry, tableId: IndexTableComponent['id']): void {
    setTimeout(() => {
      this.chartOptions.series = [];
      this.chartOptions.plotOptions = {
        series: { turboThreshold: 2500 }
      };
      this.chartOptions.series.push({
        data: element.dailyValues.map(item => {

          return {
            name: DateUtil.getDateTimeStringFormat(item[0].indexOf('T') !== -1 ? item[0].replace('T', ' ') : item[0]),
            y: item[1] ? +item[1] : 0
          };
        })
      });
      Highcharts.chart(`graphic-${element.id}`, this.chartOptions);
    }, 100);
  }

  private _isChartLoaded(element: any, table: IFancyTableData): boolean {
    return table.metadata.loadedCharts.indexOf(element.id) !== -1;
  }

  private _setTablesMinWidth(): void {
    if (this._tablesContainer) {
      this._renderer.removeClass(this._tablesContainer, 'loaded');
      this._renderer.addClass(this._tablesContainer, 'loaded');
    }
  }

  private _setHeaderCellsWidth(): void {
    this._cdr.detectChanges();
  }

  private _setIndexData(data: Array<IIndexRowView>, sortedBy: IFancyTableSortedBy): FancyTableDataSource<IFancyTableData> {
    const formatedId = data[0].index
      .split(' ')
      .join('_')
      .toLowerCase();

    const formatData = {
      id: formatedId,
      entries: data.map((item, index) => ({ id: index, ...item })),
      metadata: {
        sortedBy: {
          column: sortedBy.column,
          ascendingOrder: sortedBy.ascendingOrder
        },
        expandedElement: null,
        loadedCharts: []
      }
    } as IFancyTableData;

    return new FancyTableDataSource([formatData], this.id, this.exchange);
  }

  private _getIndexData(): void {
    this._tableServices.getIndex()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        if (response) {
          this._ngZone.run(() => {
            const completeData = this._getTransformData(response.content);
            this.up = response.up;
            this.equal = response.equal;
            this.down = response.down;

            if (this._userProfile !== null) {
              this._watchListService.getIndexWatchList(this._userProfile.cognitoUsername)
                .pipe(takeUntil(this.unsubscribeDestroy$))
                .subscribe(data => {
                  const stocks = data.indexes.filter(stock => !!stock);
                  let dataWatchList = completeData.map(item => {
                    item.inWatchlist = !!stocks.find(el => el.nemonico === item.mnemonic);

                    return item;
                  });
                  if (this.privateSite) {
                    dataWatchList = dataWatchList.filter(wl => wl.inWatchlist);
                  }
                  this._setData(dataWatchList);
                });
            } else {
              this._setData(completeData);
            }
          });

        }
      });
  }

  private _getTransformData(content: Array<IIndex>): Array<IIndexRowView> {
    return content.map(item => ({
      index: item.name,
      last: item.value,
      previous: item.close,
      open: item.open,
      high: item.high,
      low: item.low,
      variation: item.variation,
      dailyValues: item.dailyValues,
      inWatchlist: false,
      mnemonic: item.nemonico,
      links: setLinks(item.nemonico, true)
    } as IIndexRowView));
  }

  private _setData(data: Array<IIndexRowView>, sortedBy: IFancyTableSortedBy = { column: null, ascendingOrder: true }): void {
    this.tablesData = {} as FancyTableDataSource<IFancyTableData>;
    this.completeData = (data || []);
    this.tablesData = this._setIndexData(this.completeData, sortedBy);
  }
}
