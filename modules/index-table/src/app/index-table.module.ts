import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  ApiModule,
  DomChangeDirectiveModule,
  LinksListModule,
  MovementsSummaryModule,
  PaginationModule,
  PipesModule,
  TopScollbarModule,
  WatchListService,
  WithoutDataModule
} from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { IndexTableComponent } from './index-table.component';

@NgModule({
  declarations: [
    IndexTableComponent
  ],
  imports: [
    ApiModule,
    BrowserAnimationsModule,
    BrowserModule,
    DomChangeDirectiveModule,
    FlexLayoutModule,
    FormsModule,
    LinksListModule,
    MovementsSummaryModule,
    PaginationModule,
    PipesModule,
    TopScollbarModule,
    WithoutDataModule
  ],
  entryComponents: [
    IndexTableComponent
  ],
  exports : [
    IndexTableComponent
  ],
  providers : [
    ExchangeProvider,
    GraphicOptionsProvider,
    TableServices,
    WatchListService
  ]
})
export class IndexTableModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'bvl-index-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(IndexTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
