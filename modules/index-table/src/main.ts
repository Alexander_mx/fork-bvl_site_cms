import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { IndexTableModule } from './app/index-table.module';

platformBrowserDynamic().bootstrapModule(IndexTableModule)
  .catch(err => console.error(err));
