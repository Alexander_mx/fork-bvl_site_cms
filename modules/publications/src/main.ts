import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { PublicationsModule } from './app/publications.module';

platformBrowserDynamic().bootstrapModule(PublicationsModule)
  .catch(err => console.error(err));
