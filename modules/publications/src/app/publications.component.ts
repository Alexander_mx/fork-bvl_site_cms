import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { takeUntil } from 'rxjs/operators';

import { formatMMM } from '@bvl-core/shared/helpers/util/date';
import { AccordionComponent, getStorageLanguage, LANGUAGES, UnsubscribeOnDestroy } from '@bvl/library';
import { PublicationServices } from '../providers/publication.service';

import {
  IPublication,
  IPublicationFilterRequest,
  IPublicationResponse,
  IPublications,
  ISimplePublication,
  publicationDailyBulletin,
  publicationDefaultBulletin,
  publicationFilters
} from './publications';

@Component({
  selector: 'bvl-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PublicationsComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild(AccordionComponent) accordion: AccordionComponent;

  toolbarFilters: any;
  toolbarFiltersLabels: any;
  option: any;
  publications: IPublications = {};
  dailyBulletin: IPublications = {};
  filter: IPublicationFilterRequest = publicationDefaultBulletin;
  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  constructor(
    public publicationServices: PublicationServices
  ) {
    super();
  }

  ngOnInit(): void {
    this.toolbarFilters = { label : publicationFilters.label, options: publicationFilters.options.filter(c => c.filter) };
    this.toolbarFiltersLabels = { label : publicationFilters.label, options: publicationFilters.options.filter(c => !c.filter) };
    this._getPublication(publicationDailyBulletin);
    this._getPublication(publicationDefaultBulletin);
  }

  onFilterChange(output: any): void {
    this.filter = JSON.parse(JSON.stringify(publicationDefaultBulletin));
    output.forEach(element => {
      if (element.type !== 'type') {
        this.filter[element.type] = element.value;
      } else {
        this.filter.categoryId = this.toolbarFilters.options.find(option => option.id === element.value).code;
      }
    });
    this._getPublication(this.filter);
  }

  gotToFile(value: any): void {
    window.open(value.url, '_blank');
  }

  getTitleSection(last: IPublication, bulletin: boolean, category: string, object: any): string {
     let title: string;

     if (category === '') {
       if (object.items.length > 0)  {
          const dformat = object.items[0].date.split('-');
          title = `Mes ${formatMMM(dformat[0])} del año ${dformat[1]}`;
       }
     } else {
       // tslint:disable-next-line: prefer-conditional-expression
       if (last || !bulletin) {
          title = (category === publicationFilters.options[2].label) ? `${category} ${object.year}` : category;
        } else {
          if (last) {
            title = this.TEXT.general.butteinYear;
          } else {
            title = `${this.TEXT.general.butteinYear} ${object.year}`;
          }
        }

      }

     return title;
  }

  private _getPublication(filter: IPublicationFilterRequest): void {
    if (filter.year === null) {
      filter.year = ((new Date()).getFullYear()).toString();
    }
    this.publicationServices.getPublications(filter)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        if (filter.year === '0000') {
          this.dailyBulletin.last = this._setPublication(filter, response);
          this.publications.last = this.dailyBulletin.last;
        } else {
          if (filter.categoryId === 'ibmat') {
            this.publications.searched = this._setPublication(filter, response, 'ibmat-c');
            this.publications.last = this._setPublication(filter, response, 'ibmat-h');
            this.publications.anexo = this._setPublication(filter, response, 'ibmat-a');
          } else {
            if (this.accordion) {
              this.accordion.expanded = false;
            }
            if (filter.categoryId === 'boldia') {
              this.publications.last = this.dailyBulletin.last;
            }
            this.publications.searched = this._setPublication(filter, response);
          }
        }
      });
  }

  private _setPublication(
    currentFilter: IPublicationFilterRequest,
    data: Array<IPublicationResponse>,
    subcategory: string = null
  ): IPublication {
    const options = subcategory ? this.toolbarFiltersLabels.options : this.toolbarFilters.options;
    const categoryId = subcategory || currentFilter.categoryId;

    return {
      year: currentFilter.year,
      category: options.find(option => option.code === categoryId).label,
      categoryId,
      items: this._setSimplePublication(data.filter(c => c.categoryId === categoryId))
    };
  }

  private _setSimplePublication(data: Array<IPublicationResponse>): Array<ISimplePublication> {
    return data.map(item => {
      return {
        title: item.descriptionFile,
        url: item.urlFile,
        type: item.fileType,
        date: item.formatDate,
        isFileToday: item.isFileToday
      } as ISimplePublication;
    });
  }

}
