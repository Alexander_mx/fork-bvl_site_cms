import { getStorageLanguage, LANGUAGES } from '@bvl/library';

const TEXT = LANGUAGES[
  getStorageLanguage()
  .substring(0, 2)
];
export interface IPublicationFilterRequest {
  search: string;
  year: string;
  categoryId: string;
}
export interface IPublicationResponse {
  categoryDescription: string;
  categoryId: string;
  descriptionFile: string;
  fileType: string;
  files: Array<IFilePublication>;
  formatDate: string;
  fullDate: string;
  image: string;
  isFileToday: boolean;
  isMain: boolean;
  urlFile: string;
  year: string;
}
export interface IFilePublication {
  sector: string;
  urlFile: string;
}
export interface IPublications {
  last?: IPublication;
  searched?: IPublication;
  anexo?: IPublication;
}
export interface IPublication {
  year: string;
  category: string;
  categoryId: string;
  items: Array<ISimplePublication | IVademecumPublication>;
}
export interface ISimplePublication {
  title: string;
  url: string;
  type?: string;
  date?: string;
  isFileToday?: boolean;
}
export interface IVademecumPublication {
  title?: string;
  img?: string;
  files?: Array<{
    text: string;
    value: number;
  }>;
}
export const publicationDailyBulletin = {
  search: '',
  year: '0000',
  categoryId: 'boldia'
};
export const publicationDefaultBulletin = {
  search: '',
  year: (new Date().getFullYear()).toString(),
  categoryId: 'boldia'
};
export const publicationFilters = {
  label: TEXT.general.typeContent,
  options: [
    {
      id: 0,
      label: TEXT.general.dailyBulletin,
      code: 'boldia',
      url: '#',
      filter: true
    },
    {
      id: 2,
      label: TEXT.general.stockInformation,
      code: 'ibmat',
      url: '#',
      filter: true
    },
    {
      id: 1,
      label: TEXT.general.financialStatements,
      code: 'eeffcom',
      url: '#',
      filter: true
    },
    {
      id: 3,
      label: TEXT.general.stockInformationHistorical,
      code: 'ibmat-h',
      url: '#',
      filter: false
    },
    {
      id: 4,
      label: TEXT.general.stockInformationCurrent,
      code: 'ibmat-c',
      url: '#',
      filter: false
    },
    {
      id: 5,
      label: TEXT.general.stockInformationAnexo,
      code: 'ibmat-a',
      url: '#',
      filter: false
    }
  ]
};
