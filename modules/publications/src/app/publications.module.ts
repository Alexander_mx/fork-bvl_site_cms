import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { OrderByPipeModule } from '@bvl-core/shared/helpers/pipes';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { AccordionModule, BvlFormSelectModule, SectionHeaderModule, ToolbarModule } from '@bvl/library';
import { PublicationServices } from '../providers/publication.service';
import { PublicationsComponent } from './publications.component';

@NgModule({
  declarations: [
    PublicationsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SectionHeaderModule,
    BvlFormSelectModule,
    AccordionModule,
    ToolbarModule,
    FlexLayoutModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule,
    FormsModule,
    OrderByPipeModule
  ],
  providers: [PublicationServices],
  entryComponents: [PublicationsComponent]
})
export class PublicationsModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap(): void {
    const name = 'bvl-publications'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(PublicationsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
