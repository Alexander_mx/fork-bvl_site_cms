import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { IPublicationFilterRequest, IPublicationResponse } from '../app/publications';
import { PublicationEndPoints } from './endpoints';

@Injectable()
export class PublicationServices {

  constructor(
    private _apiService: ApiService
  ) { }

  getPublication(params: IPublicationFilterRequest): Observable<any> {
    return this._apiService.post(PublicationEndPoints.publication, params);
  }

  getPublications(params: IPublicationFilterRequest): Observable<Array<IPublicationResponse>> {
    return this._apiService.post(PublicationEndPoints.publications, params);
  }
}
