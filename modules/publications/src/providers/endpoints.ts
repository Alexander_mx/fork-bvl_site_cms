import { environment } from '../environments/environment';

export class PublicationEndPoints {
  public static publication = `${environment.API_URL}/static/publication`;
  public static publications = `${environment.API_URL}/static/publication/all`;
}
