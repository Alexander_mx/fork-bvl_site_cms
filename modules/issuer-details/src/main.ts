import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { IssuerDetailsModule } from './app/issuer-details.module';
platformBrowserDynamic().bootstrapModule(IssuerDetailsModule)
  .catch(err => console.error(err));
