import { environment } from '../environments/environment';

export class IssuerDetailsPoints {
  public static company = `${environment.API_URL}/static/company`;
  public static quotationHistory = `${environment.API_URL}/static/company/stock/{name}`;
  public static importantFact = `${environment.API_URL}/static/importantfact`;
  public static finantial = `${environment.API_URL}/static/eeff/`;
  public static share = `${environment.API_URL}/static/stockquote/share`;
  public static shareValues = `${environment.API_URL}/static/stockquote/shareValues`;
  public static shareValuesName = `${environment.API_URL}/static/stockquote/shareValues/{name}`;
  public static index = `${environment.API_URL}/static/valueIndex`;
}
