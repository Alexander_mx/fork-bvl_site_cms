import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { IEeffPaginationResponse, IImportantfactPaginationResponse } from 'modules/tab-fidecomisos/src/providers';
import {
  IallNemonicsRequest,
  IFiltersQuotation,
  IfinantialInfoRequest,
  IimportantFactsRequest,
  IindexChartRequest,
  IShareValuesResponse
} from '../app/issuer-details';
import { IssuerDetailsPoints } from './endpoints';

@Injectable()
export class IssuerDetailsServices {

  constructor(
    private _apiService: ApiService
  ) { }

  getCompany(companyCode: string): Observable<any> {
    return this._apiService.get(`${IssuerDetailsPoints.company}/${companyCode}/value`);
  }
  getCompanyInfo(companyCode: string): Observable<any> {
    return this._apiService.get(`${IssuerDetailsPoints.company}/${companyCode}/info`);
  }
  getQuotationHistory(filters: IFiltersQuotation): Observable<any> {
    return this._apiService.get(IssuerDetailsPoints.quotationHistory, { params: filters });
  }
  getImportantFacts(params: IimportantFactsRequest): Observable<IImportantfactPaginationResponse> {
    return this._apiService.post(IssuerDetailsPoints.importantFact, params);
  }
  getFinantialInfo(params?: IfinantialInfoRequest | '', code?: string): Observable<IEeffPaginationResponse> {
    return params
      ? this._apiService.post(`${IssuerDetailsPoints.finantial}${code}`, params)
      : this._apiService.get(`${IssuerDetailsPoints.finantial}${code}`);
  }
  getCompanyDetails(companyCode: string): Observable<any> {
    return this._apiService.get(`${IssuerDetailsPoints.company}/${companyCode}`);
  }
  getAllNemonics(): Observable<any> {
    return this._apiService.get(IssuerDetailsPoints.share);
  }
  getStockChart(filters: IFiltersQuotation): Observable<any> {
    return this._apiService.get(IssuerDetailsPoints.shareValuesName, { params: filters });
  }
  getIndexChart(params: IindexChartRequest): Observable<any> {
    return this._apiService.post(IssuerDetailsPoints.index, params);
  }
  getNemonicsCharts(params: Array<IallNemonicsRequest>): Observable<IShareValuesResponse> {
    return this._apiService.post(IssuerDetailsPoints.shareValues, params);
  }
}
