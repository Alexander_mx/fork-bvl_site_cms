import { DatePipe } from '@angular/common';
import { Component, ElementRef, NgZone, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import * as Highcharts from 'highcharts';
import * as VariablePie from 'highcharts/modules/variable-pie';
import { takeUntil } from 'rxjs/operators';

import { OrderByPipe } from '@bvl-core/shared/helpers/pipes/order-by.pipe';
import { getCurrencySymbol, isDollar } from '@bvl-core/shared/helpers/util/currency';
import { getDateISO, getDefaultStartDate, getLastDayOfMonth, getMonthsAgo } from '@bvl-core/shared/helpers/util/date';
import { formatAnchor } from '@bvl-core/shared/helpers/util/format-anchor';
import { getTimeStamp } from '@bvl-core/shared/helpers/util/general';
import { getPathArgument } from '@bvl-core/shared/helpers/util/path-arguments';
import { COMPANY_VALUES } from '@bvl-site/settings/constants/general.constant';
import {
  AngularUtil,
  ConfigurationService,
  DateUtil,
  getStorageLanguage,
  IFile,
  IImportantFacts,
  IToolbarDateFilter,
  LANGUAGES, UnsubscribeOnDestroy, WatchListService
} from '@bvl/library';
import { IUpdateWatchListRequest } from 'modules/fancy-table/src/app/fancy-table';
import { IEeffPaginationResponse, IImportantfactPaginationResponse } from 'modules/tab-fidecomisos/src/providers';
import { COMPANY_BENEFICT_TYPE } from '../../../../apps/bvlsite/src/settings/constants/general.constant';
import { environment } from '../environments/environment';
import { IssuerDetailsServices } from '../providers/services';

import {
  finantialDefaultPeriods,
  finantialDefaultTypes,
  GA_BUTTON_TAB,
  GA_CHART_OPTIONS,
  ICompany,
  ICurrentQuotation,
  IDirectory,
  IFiltersQuotation,
  IfinantialInfoRequest,
  IimportantFactsRequest,
  IssuerDetails
} from './issuer-details';
VariablePie(Highcharts);

@Component({
  selector: 'bvl-issuer-details',
  templateUrl: './issuer-details.component.html',
  styleUrls: ['./issuer-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe, OrderByPipe]
})
export class IssuerDetailsComponent extends UnsubscribeOnDestroy implements OnInit, OnDestroy {

  COMPANY_VALUES = COMPANY_VALUES;

  labelsForm: any = {};
  showFieldForCompany: boolean;
  startDate: Date = getMonthsAgo(1);
  details: IssuerDetails | any;
  nemonics: Array<string>;
  currentNemonic: string;
  quotationData: Array<ICurrentQuotation>;
  allQuotation: Array<ICurrentQuotation>;
  memories: Array<IFile>;
  finantialInfo: any;
  finantialInfoTable: any;
  companyInfo: ICompany;
  valueData: Array<any>;
  importantFacts: IImportantFacts | any;
  maxRows = 15;
  companyName = '';
  finantialTypes = finantialDefaultTypes;
  periods = finantialDefaultPeriods;
  dateRange: IToolbarDateFilter = {
    startDate: DateUtil.subtractMonth(new Date(), 1),
    endDate: new Date()
  };
  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];
  currentTab = formatAnchor(this.TEXT.issuers.corporativeInfo);
  companyNemonic: string;
  currentYear: string;
  watchListCheck: boolean;

  gaTab = [GA_BUTTON_TAB];
  gaChartOptions = [GA_CHART_OPTIONS];

  API_URL = environment.API_URL;

  private _memoriesCopy: Array<IFile>;
  private pieColor: Array<string> = [
    '#00a677',
    '#005a5b',
    '#007369',
    '#008c72'
  ];
  private _userProfile = this._configService.getUserProfile();

  constructor(
    protected _element: ElementRef,
    public media: ObservableMedia,
    public issuerServices: IssuerDetailsServices,
    public datePipe: DatePipe,
    private _configService: ConfigurationService,
    private _watchListService: WatchListService,
    private _orderByPipe: OrderByPipe,
    private _ngZone: NgZone
  ) {
    super();
    this._element.nativeElement.setAttribute('keyc', getTimeStamp());
  }

  ngOnInit(): void {
    this._watchListService.setEnviroment(environment.API_URL);
    this.currentYear = (new Date()).getFullYear()
      .toString();
    this._getDetailData();
    this._getCompanyInfo();
    this._getCurrentAnchor();
  }

  onNemonicChange(nemonic: string): void {
    setTimeout(() => this.currentNemonic = nemonic);
    try {
      this.details = this._setDetailData(
        this.valueData.find(item => item.nemonico === nemonic),
        getPathArgument('companyCode')
      );
      // tslint:disable-next-line:no-console
    } catch (error) { console.log(error); }
  }

  emitChangePage(data): void {
    this.quotationData = this._transformQuotationData(data);
  }

  onDateFilterChange(filters: any): void {
    const findNemonic = filters.find((item: any) => item.type === 'filterByNemonic');
    const findDate = filters.find((item: any) => item.type === 'filterByDate');

    setTimeout(() => this.currentNemonic = findNemonic ? findNemonic.value : this.nemonics[0]);

    const dates = findDate ? findDate.value : [
      this.datePipe.transform(DateUtil.subtractMonth(new Date(), 1), 'yyyy-MM-dd'),
      this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    ];
    this._getQuotationData({
      name: this.currentNemonic,
      startDate: dates[0],
      endDate: dates[1]
    });
  }

  isActiveTab(code: string): boolean {
    const tabEs = formatAnchor(LANGUAGES.es.issuers[code] || '');
    const tabEn = formatAnchor(LANGUAGES.en.issuers[code] || '');

    return this.currentTab === tabEs || this.currentTab === tabEn;
  }

  onTabChange(value: any): void {
    this.currentTab = formatAnchor(value);

    switch (this.currentTab) {
      case formatAnchor(this.TEXT.issuers.corporativeInfo):
        this._getCompanyInfo();
        break;
      case formatAnchor(this.TEXT.issuers.value):
        this._getDetailData();
        break;
      case formatAnchor(this.TEXT.issuers.quotationHistory):
        this._getQuotationData();
        break;
      case formatAnchor(this.TEXT.issuers.importantFacts):
        this._getImportantFacts();
        break;
      case formatAnchor(this.TEXT.issuers.finantialInfo):
        this._getFinantialInfo({
          rpjCode: this.details ? this.details.rpjCode : '',
          search: '',
          type: 1,
          period: 1,
          yearPeriod: this.currentYear,
          periodAccount: '',
          page: 1,
          size: 12
        }, true);
        break;
      case formatAnchor(this.TEXT.issuers.memories):
        this._getMemories();
        break;
      default:
        break;
    }
  }

  getAnchor(): string {
    return location.hash;
  }

  toggleWatchList(watchListValue): void {
    if (this._userProfile !== null) {
      const value = !watchListValue;
      const body: IUpdateWatchListRequest = {
        idUser: this._userProfile.cognitoUsername,
        nemonico: this.companyNemonic,
        add: value
      };

      this._watchListService.updateStockWatchList(body)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => this.watchListCheck = value);

    } else {
      location.href = `${location.origin}/login`;
    }
  }

  onResourcesFilter(value: any, type: string): any {
    const findSearch = value.find(item => item.type === 'search');
    const findDates = value.find(item => item.type === 'filterByDate');
    const findType = value.find(item => item.type === 'type');
    const findPeriod = value.find(item => item.type === 'period');
    const findPage = value.find(item => item.type === 'page');
    const findYear = value.find(item => item.type === 'year');
    const findPeriodAccount = value.find(item => item.type === 'periodAccount');

    if (type === 'important') {
      this._getImportantFacts({
        rpjCode: this.details ? this.details.rpjCode : '',
        page: findPage ? findPage.value : 1,
        size: 12,
        search: findSearch ? findSearch.value : '',
        startDate: findDates ? findDates.value[0] : getDateISO(getDefaultStartDate()),
        endDate: findDates ? findDates.value[1] : getDateISO(getLastDayOfMonth())
      });
    }
    if (type === 'finantial') {
      const period = findPeriod ? findPeriod.value : 1;
      this._getFinantialInfo({
        rpjCode: this.details ? this.details.rpjCode : '',
        period,
        type: findType ? (+findType.value + 1) : 1,
        search: findSearch ? findSearch.value : '',
        page: findPage ? findPage.value : 1,
        yearPeriod: findYear ? String(findYear.value) : this.currentYear,
        periodAccount: this._isAnual(period) ? 'A' : (findPeriodAccount ? findPeriodAccount.value : ''),
        size: 12
      }, false);
    }
    if (type === 'memories') {
      this.memories = !findYear.value
        ? this._memoriesCopy
        : this._memoriesCopy.filter(item => item.year === findYear.value);
    }
  }

  private _labelsInfoCompany(sectorType: string): void {
    if (sectorType === COMPANY_VALUES.sectorCodeFI) {
      this.showFieldForCompany = false;
      this.labelsForm = {
        companyName: this.TEXT.general.fondoBusinessName,
        directory: this.TEXT.general.committee,
        foundation: this.TEXT.general.dateStartOperation
      };
    } else {
      this.showFieldForCompany = true;
      this.labelsForm = {
        companyName: this.TEXT.general.businessName,
        directory: this.TEXT.general.directory,
        foundation: this.TEXT.general.foundation
      };
    }
  }

  private _isAnual(periodo: number): boolean {
    return periodo === 3;
  }

  private _getCurrentAnchor(): void {
    let anchor = location.hash;
    if (anchor) {
      anchor = anchor.replace('#', '');
      this.currentTab = anchor;
      setTimeout(() => {
        const tabs: HTMLElement = document.querySelector('bvl-tabs');
        window.scrollTo(0, tabs.offsetTop);
        this.onTabChange(this.currentTab);
      }, 800);
    }
  }

  private _getWatchList(): void {
    const idUser = this._userProfile;
    if (idUser && idUser.cognitoUsername) {
      this._watchListService.getStockWatchList(idUser.cognitoUsername)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(response => {
          const findNemonic = response.stocks.find(item => item.nemonico === this.companyNemonic);
          this.watchListCheck = findNemonic ? true : false;
        });
    }
  }

  private _getDetailData(): void {
    this.issuerServices
      .getCompany(getPathArgument('companyCode'))
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        if (response.length) {
          this.nemonics = [];
          this.valueData = response;
          this.companyNemonic = response[0].nemonico;
          response.forEach(element => {
            this.nemonics.push(element.nemonico);
          });

          this.currentNemonic = this.nemonics[0];
          this.details = this._setDetailData(
            response[0],
            getPathArgument('companyCode')
          );

          this._getWatchList();
        }
      });
  }

  private _getCompanyInfo(): void {
    this.issuerServices
      .getCompanyInfo(getPathArgument('companyCode'))
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this._ngZone.run(() => {
          this.companyInfo = this._setCompanyInfo(response);
          this.companyName = this.companyInfo.name;
          this._labelsInfoCompany(this.companyInfo.sectorCode);
          this.companyInfo.shareComposition.forEach((item, index) => {
            this._initPieChart(item.compositions, index);
          });
        });
      });
  }

  private _getQuotationData(filters?: IFiltersQuotation): void {
    if (!filters) {
      filters = {
        name: this.currentNemonic || this.nemonics[0],
        startDate: this.datePipe.transform(this.dateRange.startDate, 'yyyy-MM-dd'),
        endDate: this.datePipe.transform(this.dateRange.endDate, 'yyyy-MM-dd')
      };
    }
    this.issuerServices.getQuotationHistory(filters)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.allQuotation = this._orderByPipe.transform(response, ['-date']);
        this.quotationData = this._transformQuotationData(response.slice(0, this.maxRows));
      });
  }

  private _transformQuotationData(data: Array<any>): Array<ICurrentQuotation> {
    const symbol = getCurrencySymbol((data && data[0] || {}).currencySymbol);

    return data.map(q => {
      const negotiatedAmount = this._getNegotiatedAmount(symbol, Number(q.dollarAmountNegotiated), Number(q.solAmountNegotiated));
      const avarage = negotiatedAmount / Number(q.quantityNegotiated);

      return {
        date: DateUtil.getDateStringFormat(q.date),
        open: this._getTwoDecimal(Number(q.open)),
        close: this._getTwoDecimal(Number(q.close)),
        high: this._getTwoDecimal(Number(q.high)),
        low: this._getTwoDecimal(Number(q.low)),
        average: this._getTwoDecimal(avarage),
        currencySymbol: symbol,
        quantityNegotiated: this._getTwoDecimal(Number(q.quantityNegotiated)),
        negotiatedAmount: this._getTwoDecimal(negotiatedAmount),
        yesterday: DateUtil.getDateStringFormat(q.yesterday),
        yesterdayClose: this._getTwoDecimal(Number(q.yesterdayClose))
      };
    });
  }

  private _getNegotiatedAmount(symbol: string, dollar: number, sol: number): number {
    return isDollar(symbol) ? dollar : sol;
  }

  private _getTwoDecimal(number: number): string {
    return AngularUtil.formatNumberToString(number, 2);
  }

  private _getImportantFacts(filters?: IimportantFactsRequest): void {
    if (!filters) {
      filters = {
        rpjCode: this.details ? this.details.rpjCode : '',
        page: 1,
        size: 12,
        search: '',
        startDate: getDateISO(getMonthsAgo(1)),
        endDate: getDateISO(getLastDayOfMonth())
      };
    }
    this.issuerServices.getImportantFacts(filters)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.importantFacts = this._setimportantFacts(response);
      });
  }
  private _getFinantialInfo(filters?: IfinantialInfoRequest, getTable?: boolean): void {
    this.issuerServices.getFinantialInfo(filters, '')
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.finantialInfo = this._setfinantialInfo(response);
      }, err => { });
    if (getTable && this.details && this.details.rpjCode) {
      this.issuerServices.getFinantialInfo('', this.details ? this.details.rpjCode : '')
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(response => {
          this.finantialInfoTable = this._setfinantialInfoTable(response);
        }, err => { });
    }
  }

  private _getMemories(): void {
    this.issuerServices.getCompanyDetails(getPathArgument('companyCode'))
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.memories = this._setMemories(response.listMemoryEEFF);
        this._memoriesCopy = JSON.parse(JSON.stringify(this.memories));
      });
  }

  private _setfinantialInfoTable(data: any): Object {
    const head = [this.TEXT.general.financialIndexes];
    const body = [];

    data[0].finantialIndexYears.forEach(year => {
      head.push(year.year);
    });

    data.forEach(item => {
      const valuesArray = [];
      valuesArray.push({ value: item.dRatio, type: 'string' });
      item.finantialIndexYears.forEach(year => {
        valuesArray.push({ value: AngularUtil.formatNumberToString(Number(year.nImporteA), 4), type: 'number' });
      });
      body.push(valuesArray);
    });

    return { head, body };
  }

  private _isFile(path: string): boolean {
    const ANY_EXTENSION_REGEX = /\.[\w]+$/gi;

    return ANY_EXTENSION_REGEX.test(path);
  }

  private _setfinantialInfo(data: IEeffPaginationResponse): any {
    return {
      totalPages: data.totalPages,
      content: data.content.map(item => ({
        fileLink: {
          description: item.documentName,
          link: this._isFile(item.path) ? item.path : item
        },
        date: DateUtil.getDateStringFormat(item.dateHour),
        hour: DateUtil.getTimeStringFormat(item.dateHour)
      }))
    };
  }

  private _setMemories(data: any): Array<IFile> {
    return data.map(item => {
      const splitedDate = new Date(item.date);

      return {
        fileLink: {
          description: `${item.document} - ${this.TEXT.general.part} ${item.sequence}`,
          link: this._isFile(item.path) ? item.path : item
        },
        date: this.datePipe.transform(splitedDate, 'dd/MM/yyyy'),
        hour: this.datePipe.transform(splitedDate, 'HH:mm:ss'),
        year: item.year
      };
    });
  }

  private _setimportantFacts(data: IImportantfactPaginationResponse): IImportantFacts {
    return {
      totalElements: data.totalElements,
      totalPages: data.totalPages,
      content: data.content.map(value => ({
        description: {
          list: value.codes.map(item => item.descCodeHHII),
          text: (value.observation || '').trim()
        },
        date: DateUtil.getDateStringFormat(value.registerDate),
        hour: DateUtil.getTimeStringFormat(value.registerDate),
        downloadFiles: value.documents.map(item => item.path)
      }))
    };
  }

  private _setCompanyInfo(data: any): ICompany {
    return {
      rpjCode: data.rpjCode,
      name: data.companyName,
      phone: data.phone,
      fax: data.fax,
      address: data.companyAddress,
      website: data.website,
      sectorCode: data.sectorCode,
      fundation: data.dateFundation,
      descripcion: data.esActDescription,
      shareComposition: data.listTypeCompositionAcc.map(item => {
        return {
          type: item.structureDescription,
          year: item.year,
          compositions: item.listComposiytionAcc.map((list, index) => {
            return {
              descripcion: (() => {
                const formatedText = list.typeDescription.toLowerCase();

                return `${formatedText[0].toUpperCase()}${formatedText.slice(1)}`;
              })(),
              value: list.valueParticipation,
              color: this.pieColor[index]
            };
          })
        };
      }),
      validity: data.listDirectory && data.listDirectory[0] && data.listDirectory[0].validity,
      directory: this._getDirectoryCompanyInfo(data.listDirectory),
      manager: this._getDirectoryCompanyInfo(data.listManager)
    };
  }

  private _getDirectoryCompanyInfo(list: Array<any>): Array<IDirectory> {
    return (list || []).map(directory => {
      return {
        position: directory.description,
        name: directory.personDescription
      };
    });
  }

  private _setDetailData(data: any, id: string | number): IssuerDetails {
    this.companyName = data.listLastValue[0].companyName;
    const currencySymbol = (data.listStock[0] || data.listBenefit[0]).coin.replace('.', '');
    const firstStock = (data.listStock[0] || {});

    return {
      id,
      name: data.listLastValue[0].companyName,
      rpjCode: data.listLastValue[0].rpjCode,
      values: [
        {
          id: firstStock.isin,
          name: firstStock.nemonico,
          date: firstStock.date && DateUtil.convertStringToDate(firstStock.date),
          shares: (firstStock.quantity)
            ? (data.listLastValue[0].sectorCode === COMPANY_VALUES.sectorCodeFI)
              ? AngularUtil.formatNumberToString(Number(firstStock.quantity || 0), 0)
              : AngularUtil.formatNumberToString(Number(firstStock.quantity || 0), 0)
            : 0,
          nominalValue: (firstStock.nominalValue) ? AngularUtil.formatNumberToString(Number(firstStock.nominalValue || 0), 2) : 0,
          dateInscription: data.dateInscription && DateUtil.convertStringToDateTime(data.dateInscription),
          currency: {
            symbol: currencySymbol
          },
          capitalizationValue: (firstStock.capital) ? AngularUtil.formatNumberToString(Number(firstStock.capital || 0), 2) : 0,
          cotizacionMessage: firstStock.desWebMessage,
          lastBenefits: data.listBenefit.map(item => {
            return {
              name: `${currencySymbol} ${item.benefitValue} ${ COMPANY_BENEFICT_TYPE[item.benefitType] }`,
              agreementDate: DateUtil.convertStringToDate(item.dateAgreement),
              cutoffDate: DateUtil.getDateStringFormat(item.dateCut),
              registrationDate: DateUtil.getDateStringFormat(item.dateRegistry),
              deliveryDate: DateUtil.getDateStringFormat(item.dateDelivery)
            };
          }),
          lastMovement: {
            date: data.listLastValue[0].dateTimS
              ? DateUtil.convertStringToDateTime(data.listLastValue[0].dateTimS)
              : null,
            actual: AngularUtil.formatNumberToString(Number(data.listLastValue[0].last), 2),
            opening: AngularUtil.formatNumberToString(Number(data.listLastValue[0].open), 2),
            variation: AngularUtil.formatNumberToString(
              this._calculateVariation(Number(data.listLastValue[0].open), Number(data.listLastValue[0].var))
              , 2),
            variationPercent: AngularUtil.formatNumberToString(Number(data.listLastValue[0].var), 2),
            negotiatedAmount: AngularUtil.formatNumberToString(Number(data.listLastValue[0].amount), 2)
          },
          lastStockMarketDay: {
            date: data.listLastValue[0].dateTimP
              ? DateUtil.convertStringToDate(data.listLastValue[0].dateTimP)
              : null,
            open: AngularUtil.formatNumberToString(Number(data.listLastValue[0].open), 2),
            close: AngularUtil.formatNumberToString(Number(data.listLastValue[0].close), 2),
            min: AngularUtil.formatNumberToString(Number(data.listLastValue[0].min), 2),
            max: AngularUtil.formatNumberToString(Number(data.listLastValue[0].max), 2),
            actual: AngularUtil.formatNumberToString(Number(data.listLastValue[0].last), 2),
            opening: AngularUtil.formatNumberToString(Number(data.listLastValue[0].open), 2),
            variation: AngularUtil.formatNumberToString(
              this._calculateVariation(Number(data.listLastValue[0].open), Number(data.listLastValue[0].var))
              , 2),
            variationPercent: AngularUtil.formatNumberToString(Number(data.listLastValue[0].var), 2),
            negotiatedAmount: AngularUtil.formatNumberToString(Number(data.listLastValue[0].amount), 2),
            quantityNegotiated: AngularUtil.formatNumberToString(Number(data.listLastValue[0].quantityNegotiated), 0)
          }
        }
      ]
    };
  }

  getLogoCompany(rpjCode: string): string {
    return `${this.API_URL}/site/company/images/${rpjCode}`;
  }

  private _calculateVariation(open: number, varPercent: number): Number {
    return open * varPercent / 100;
  }

  private _initPieChart(data, index): void {
    setTimeout(() => {
      Highcharts.chart(`pie-chart-${index}`, {
        chart: {
          type: 'variablepie',
          height: 100
        },
        exporting: {
          enabled: false
        },
        title: {
          text: ''
        },
        tooltip: {
          pointFormat: `<b>{point.y}</b><br/>`,
          borderRadius: 10,
          borderWidth: 0,
          backgroundColor: '#353535',
          style: { color: '#dcdcdc' }
        },
        credits: { enabled: false },
        series: [{
          minPointSize: 10,
          innerSize: '100%',
          zMin: 0,
          dataLabels: {
            enabled: false
          },
          data: data.map((item: any) => {
            return {
              name: item.descripcion,
              y: item.value ? +item.value : 0,
              color: item.color
            };
          })
        }]
      } as any);
    }, 100);
  }
}
