import { CommonModule, DatePipe } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { OrderByPipeModule } from '@bvl-core/shared/helpers/pipes';
import { ApiModule, CarouselModule, PaginationModule, TabsModule, ToolbarModule, WatchListService } from '@bvl/library';
import { BannerModule } from 'modules/banner/src/app/banner.module';
import { DetailsChartModule } from 'modules/details-chart/src/app/details-chart.module';
import { ItemsGridModule } from 'modules/items-grid/src/app/items-grid.module';
import { ResourcesModule } from 'modules/resources/src/app/resources.module';
import { SimpleTableModule } from 'modules/simple-table/src/app/simple-table.module';
import { IssuerDetailsServices } from '../providers/services';
import { IssuerDetailsComponent } from './issuer-details.component';

@NgModule({
  declarations: [
    IssuerDetailsComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    CommonModule,
    TabsModule,
    FlexLayoutModule,
    CarouselModule,
    ResourcesModule,
    ApiModule,
    ToolbarModule,
    PaginationModule,
    BannerModule,
    DetailsChartModule,
    ItemsGridModule,
    SimpleTableModule,
    OrderByPipeModule
  ],
  providers: [
    IssuerDetailsServices,
    DatePipe,
    WatchListService
  ],
  entryComponents: [IssuerDetailsComponent]
})
export class IssuerDetailsModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-issuer-details'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(IssuerDetailsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
