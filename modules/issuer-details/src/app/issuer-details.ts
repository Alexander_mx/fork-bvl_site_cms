import { ICurrency } from '@bvl/library';

interface IssuerValueBenefit {
  name: string;
  agreementDate: Date;
  cutoffDate: Date;
  registrationDate: Date;
  deliveryDate: Date;
}

export interface IissuerValue {
  id: number;
  name: string;
  date: Date;
  shares: number | string;
  nominalValue: number | string;
  currency: ICurrency;
  capitalizationValue: number | string;
  cotizacionMessage: boolean;
  dateInscription: Date;
  lastBenefits: Array<IssuerValueBenefit>;
  lastMovement: {
    date: Date;
    actual: number | string;
    opening: number | string;
    variation: number | string;
    variationPercent?: number | string;
    negotiatedAmount: number | string;
  };
  lastStockMarketDay: {
    date: Date;
    open: number | string;
    close: number | string;
    min: number | string;
    max: number | string;
    actual?: number | string;
    opening?: string;
    variation?: number | string;
    variationPercent?: number | string;
    negotiatedAmount?: number | string;
    quantityNegotiated?: string;
  };
}

export interface IssuerDetails {
  id: string | number;
  name: string;
  rpjCode: string;
  values: Array<IissuerValue>;
}

export interface ICurrentQuotation {
  date?: string;
  open?: number | string;
  close?: number | string;
  high?: number | string;
  low?: number | string;
  max?: number | string;
  min?: number | string;
  average?: number | string;
  quantityNegotiated?: number | string;
  negotiatedAmount?: number | string;
  currencySymbol?: string;
  yesterday?: string;
  yesterdayClose?: number | string;
}
export interface IFiltersQuotation {
  name?: string;
  startDate: any;
  endDate: any;
}
export interface IimportantFactsRequest {
  rpjCode: string;
  page: number;
  size: number;
  search: string;
  startDate: string;
  endDate: string;
}
export interface IfinantialInfoRequest {
  rpjCode: string;
  search: string;
  type: number;
  period: number;
  page: number;
  size: number;
  yearPeriod?: string;
  periodAccount?: string;
}
export interface ICompany {
  rpjCode?: string;
  name: string;
  phone: string;
  sectorCode: string;
  fax?: string;
  address: string;
  website: string;
  fundation: string;
  descripcion: string;
  shareComposition: Array<{
    type: string;
    year: string;
    compositions: Array<IShareComposition>
  }>;
  validity: string;
  directory: Array<IDirectory>;
  manager: Array<IDirectory>;
}
interface IShareComposition {
  descripcion: string;
  value: string;
  color?: string;
}
export interface IDirectory {
  position: string;
  name: string;
}
export interface IallNemonicsRequest {
  nemonic: string;
  dates: Array<string>;
}
export interface IindexChartRequest {
  nemonico: string;
  startDate: string;
  endDate: string;
}

export interface IShareValuesResponse {
  shares: Array<IShareValueResponse>;
}

export interface IShareValueResponse {
  currencySymbol: string;
  nemonico: string;
  values: Array<Array<string>>;
}

export const finantialDefaultTypes = {
  label: 'Tipo de contenido',
  options: [
    {
      id: 0,
      label: 'Individual',
      code: 'individual',
      type: 1
    },
    {
      id: 1,
      label: 'Consolidado',
      code: 'consolidated',
      type: 2
    }
  ]
};
export const finantialDefaultPeriods = {
  label: 'Período',
  options: [
    {
      id: 1,
      label: 'Trimestral No Auditada',
      code: 'Quaterly',
      value: 1
    },

    {
      id: 3,
      label: 'Auditada Anual',
      code: 'Annual',
      value: 3
    }
  ]
};

export const GA_BUTTON_TAB = {
  control: 'buttonTab',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - {{qs(h1)}}',
  label: '{{button}}',
  value: null,
  dimentions: null
};

export const GA_CHART_OPTIONS = {
  control: 'chartOption',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - Ver gráfica',
  label: '{{chartOption}}',
  value: null,
  dimentions: null
};
