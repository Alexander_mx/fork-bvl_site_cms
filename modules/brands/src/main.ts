import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { BrandsModule } from './app/brands.module';

platformBrowserDynamic().bootstrapModule(BrandsModule)
  .catch(err => console.error(err));
