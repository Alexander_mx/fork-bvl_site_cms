import { Component, ElementRef, Input, ViewEncapsulation } from '@angular/core';

import { coerceBooleanProp, GaUnsubscribeBase } from '@bvl/library';

@Component({
  selector: 'bvl-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BrandsComponent extends GaUnsubscribeBase {

  private _brands: Array<any>;
  @Input()
  get brands(): any {
    return this._brands;
  }
  set brands(value: any) {
    this._brands = JSON.parse(value) as Array<any>;
  }

  private _isButton: boolean;
  @Input()
  get isButton(): boolean {
    return this._isButton;
  }
  set isButton(value: boolean) {
    this._isButton = coerceBooleanProp(value);
  }

  @Input() urlButton: string;
  @Input() textButton: string;

  constructor(
    protected _element: ElementRef
  ) {
    super(_element);
  }

}
