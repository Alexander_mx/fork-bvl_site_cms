import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { DomChangeDirectiveModule, ImgSrcsetModule } from '@bvl/library';
import { BrandsComponent } from './brands.component';

@NgModule({
  declarations: [
    BrandsComponent
  ],
  imports: [
    BrowserModule,
    ImgSrcsetModule,
    DomChangeDirectiveModule
  ],
  providers: [],
  entryComponents: [BrandsComponent]
})
export class BrandsModule {

  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'bvl-brands'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BrandsComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
