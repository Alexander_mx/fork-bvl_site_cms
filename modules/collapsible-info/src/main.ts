import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { CollapsibleInfoModule } from './app/collapsible-info.module';

platformBrowserDynamic().bootstrapModule(CollapsibleInfoModule)
  .catch(err => console.error(err));
