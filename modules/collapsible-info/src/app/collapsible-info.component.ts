import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ICollapsibleInfo } from './collapsible-info';

@Component({
  selector: 'bvl-collapsible-info',
  templateUrl: './collapsible-info.component.html',
  styleUrls: ['./collapsible-info.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CollapsibleInfoComponent {
  private _info: Array<ICollapsibleInfo>;
  @Input()
  get info(): any {
    return this._info;
  }
  set info(value: any) {
    this._info = JSON.parse(value) as Array<ICollapsibleInfo>;
  }
}
