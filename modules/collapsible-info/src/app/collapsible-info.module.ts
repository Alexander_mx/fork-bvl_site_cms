import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule, ParagraphModule, SectionHeaderModule } from '@bvl/library';
import { CollapsibleInfoComponent } from './collapsible-info.component';

@NgModule({
  declarations: [
    CollapsibleInfoComponent
  ],
  imports: [
    BrowserModule,
    AccordionModule,
    ParagraphModule,
    SectionHeaderModule,
    FlexLayoutModule,
    BrowserAnimationsModule
  ],
  providers: [],
  entryComponents: [CollapsibleInfoComponent]
})
export class CollapsibleInfoModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-collapsible-info'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(CollapsibleInfoComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
