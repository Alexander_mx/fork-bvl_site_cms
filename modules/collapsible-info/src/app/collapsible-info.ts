export interface ICollapsibleInfo {
  title: string;
  description: string;
}
