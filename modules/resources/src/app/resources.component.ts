import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewEncapsulation } from '@angular/core';

import { getDefaultStartDate, getLastDayOfMonth, getMonthsAgo } from '@bvl-core/shared/helpers/util/date';
import { getStorageLanguage, LANGUAGES } from '@bvl/library';
import { IResource, ResourceEntity, ResourceType } from './resources';

@Component({
  selector: 'bvl-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResourcesComponent implements OnInit, OnChanges {
  @Input() startDate: Date = getDefaultStartDate();
  @Input() entity: ResourceEntity;
  @Input() resourceType: ResourceType;
  @Input() fromDate?: Date = getMonthsAgo(3);
  @Input() toDate?: Date = getLastDayOfMonth();
  @Input() itemsPerRow: string;
  @Input() resources: Array<IResource>;
  @Input() objects: Array<IResource>;
  @Input() byTypeFilter: Array<string> = [];
  @Input() periods: Array<string> = [];
  @Input() datesFilter = true;
  @Input() tableDownload = false;
  @Input() totalPages: number;
  @Input() yearRange: boolean;
  @Input() yearLabel: string;
  @Input() periodByYear: boolean;
  @Input() paginateByService = true;
  @Input() toolBarFilterIsVisible: boolean;
  @Input() capabilities = ['search'];
  @Input() maxObjects = 20;
  @Output() toolbarFilter = new EventEmitter<any>();
  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)
  ];

  currentToolbarFilter: any = [];
  tableHeader: any;

  constructor() {
    this.tableHeader = [this.TEXT.general.subjectDetail, this.TEXT.general.date, this.TEXT.general.hour];
  }

  ngOnInit(): void {
    this._setObjects(this.resources);
  }

  onToolbarFilterChange(value: any): void {
    this.currentToolbarFilter = value;
    this.toolbarFilter.emit(value);
  }

  emitChangePage(value): void {
    if (this.paginateByService) {
      const findPage = this.currentToolbarFilter.filter(item => item.type === 'page');
      if (findPage.length) {
        findPage[0].value = value;
      } else {
        this.currentToolbarFilter.push({ type: 'page', value });
      }
      this.toolbarFilter.emit(this.currentToolbarFilter);
    } else {
      this.objects = value;
    }
  }

  ngOnChanges(changes: any): void {
    if (changes.resources) {
      this._setObjects(changes.resources.currentValue);
    }
  }

  private _setObjects(value): void {
    this.objects = value.length > this.maxObjects ? value.slice(0, this.maxObjects) : value;
  }
}
