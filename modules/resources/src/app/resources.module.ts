import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { PaginationModule, TableDownloadFileModule, TableFileLinkModule, ToolbarModule } from '@bvl/library';
import { ResourcesComponent } from './resources.component';

@NgModule({
  declarations: [
    ResourcesComponent
  ],
  imports: [
    BrowserModule,
    ToolbarModule,
    PaginationModule,
    TableFileLinkModule,
    TableDownloadFileModule
  ],
  exports: [ResourcesComponent],
  entryComponents: [ResourcesComponent]
})
export class ResourcesModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'bvl-resources'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ResourcesComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
