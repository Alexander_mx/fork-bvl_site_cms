export type ResourceType = 'imporantFacts' | 'financialReports' | 'memorandums' | 'others';
export type ResourceEntity = 'issuer';

export interface IResource {
  title: string;
  date: Date;
  url: string;
}
