import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ResourcesModule } from './app/resources.module';

platformBrowserDynamic().bootstrapModule(ResourcesModule)
  .catch(err => console.error(err));
