import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { IssuersListTableModule } from './app/issuers-list-table.module';

platformBrowserDynamic().bootstrapModule(IssuersListTableModule)
  .catch(err => console.error(err));
