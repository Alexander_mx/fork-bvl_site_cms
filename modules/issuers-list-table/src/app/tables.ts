export interface ICompanyItemView {
  companyCode?: string;
  company?: string;
  sector?: string;
  mnemonic?: Array<string>;
  indexes?: Array<string>;
  memory?: Array<any>;
  importantFact?: Array<any>;
}

export interface IListICompanyItemView extends Array<ICompanyItemView> { }

export const INITIAL_BODY = {
  firstLetter: '',
  sectorCode: '',
  companyName: ''
};

export const TABLE_INITIALS = [
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'L',
  'M',
  'N',
  'O',
  'P',
  'R',
  'S',
  'T',
  'U',
  'V',
  'X',
  'Y',
  'Z'
];
