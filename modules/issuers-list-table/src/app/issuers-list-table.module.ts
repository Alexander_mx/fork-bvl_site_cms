import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  ApiModule,
  DomChangeDirectiveModule,
  LinksListModule,
  PaginationModule,
  PipesModule,
  PopoverModule,
  ToggleByPositionModule,
  ToolTipModule,
  TopScollbarModule
} from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { IssuerNewsServices } from 'modules/issuers-news/src/providers/services';
import { StockTableHeaderComponent } from './fancy-table-header/fancy-table-header.component';
import { IssuersListTableComponent } from './issuers-list-table.component';

@NgModule({
  declarations: [
    IssuersListTableComponent,
    StockTableHeaderComponent
  ],
  imports: [
    PopoverModule,
    CommonModule,
    FormsModule,
    BrowserModule,
    LinksListModule,
    FlexLayoutModule,
    TopScollbarModule,
    BrowserAnimationsModule,
    ApiModule,
    PipesModule,
    ToolTipModule,
    PaginationModule,
    ToggleByPositionModule,
    DomChangeDirectiveModule
  ],
  entryComponents: [
    IssuersListTableComponent
  ],
  exports: [
    IssuersListTableComponent
  ],
  providers: [
    ExchangeProvider,
    TableServices,
    GraphicOptionsProvider,
    IssuerNewsServices
  ]
})
export class IssuersListTableModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-issuers-list-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(IssuersListTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
