import { animate, state, style, transition, trigger } from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  LOCALE_ID,
  NgZone,
  OnChanges,
  OnInit,
  Output,
  Renderer2,
  ViewEncapsulation
} from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { IFancyTableSortedBy } from '@bvl-core/shared/helpers/models/fancy-table.model';
import { pagination } from '@bvl-core/shared/helpers/util';
import { setFilterRequest } from '@bvl-core/shared/helpers/util/body-request';
import { getFancyTableSortBy, sortFancyTable } from '@bvl-core/shared/helpers/util/sort';
import {
  GaUnsubscribeBase,
  getStorageLanguage,
  hideToolTips,
  IEntry,
  IFancyTableColumn,
  IStock,
  LANGUAGES,
  positioning
} from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { FancyTableDataSource } from 'modules/fancy-table/src/app/fancy-table-data-source';
import { IssuerNewsServices } from 'modules/issuers-news/src/providers/services';
import { ICompany, IFancyTableData } from '../../../fancy-table/src/app/fancy-table';
import { INewsRequest, newsDefaultRequestForIssuers, setImportantFacts } from '../../../issuers-news/src/app/issuers-news';
import { ICompanyItemView, IListICompanyItemView, INITIAL_BODY } from './tables';

let nextId = 0;

@Component({
  selector: 'bvl-issuers-list-table',
  templateUrl: './issuers-list-table.component.html',
  styleUrls: ['./issuers-list-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, hidden', style({ height: '0px', minHeight: '0' })),
      state('expanded, shown', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('.2s cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class IssuersListTableComponent extends GaUnsubscribeBase implements OnInit, AfterViewInit, OnChanges {

  @Output() activeFilters = new EventEmitter<any>();
  @Output() tableEvents = new EventEmitter<any>();

  private _filter: INewsRequest = JSON.parse(JSON.stringify(newsDefaultRequestForIssuers));
  private _copyInitialBody = JSON.parse(JSON.stringify(INITIAL_BODY));
  private _initialBody = INITIAL_BODY;
  private _elem: HTMLElement;
  private _tablesContainer: HTMLElement;

  TEXT = LANGUAGES[getStorageLanguage()];
  id = `bvl-table-${nextId++}`;
  tablesData: FancyTableDataSource<IFancyTableData> = {} as FancyTableDataSource<IFancyTableData>;
  tableColumns: Array<IFancyTableColumn> = [];

  viewLoaded = false;
  expandedElement: { tableId: IFancyTableData['id']; element: IStock };

  scrollingTable = false;

  currentPage = 1;
  maxRows = 15;
  completeData: IListICompanyItemView;
  existData = true;
  newsImportantFact: Array<IEntry>;
  popoverTitleDescription: any = {
    memory: '',
    indexes: '',
    mnemonic: ''
  };

  constructor(
    protected _element: ElementRef,
    private exchange: ExchangeProvider,
    public media: ObservableMedia,
    private _ngZone: NgZone,
    private _renderer: Renderer2,
    private _cdr: ChangeDetectorRef,
    private _tableServices: TableServices,
    public issuerNewsServices: IssuerNewsServices,
    @Inject(LOCALE_ID) private locale: string
  ) {
    super(_element);
    this.media
      .asObservable()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        if (this.viewLoaded) {
          this._setTablesMinWidth();
          this._setScrollingTable();
          this._setHeaderCellsWidth();
        }
      });

    this.tableColumns = this._getColumnsName(this.TEXT);

    this.popoverTitleDescription = {
      memory: this.TEXT.issuersPopover.memory,
      indexes: this.TEXT.issuersPopover.indexes,
      mnemonic: this.TEXT.issuersPopover.mnemonic,
      important: this.TEXT.issuersPopover.important
    };
  }

  @HostListener('document:click', ['$event']) documentClick(event: Event): void {
    const selector = (event.target as any).closest('.tooltip, .more-number');
    if (!selector) {
      hideToolTips();
    }
  }

  @HostListener('scroll') onscroll(): void {
    hideToolTips();
  }

  @HostListener('window:resize') onresize(): void {
    hideToolTips();
  }

  ngOnInit(): void {
    this._getCompanyData();
  }

  ngAfterViewInit(): void {
    this._elem = this._element.nativeElement;
    this._tablesContainer = this._elem.querySelector('.tables-container');
  }

  ngOnChanges(): void {
    const onStableSubscription: Subscription = this._ngZone.onStable.subscribe(
      () => {
        this._setTablesMinWidth();
        this.viewLoaded = true;
        this._setScrollingTable();
        onStableSubscription.unsubscribe();
      }
    );
  }

  onFilterChange(activeFilters): void {
    let body: any;
    if (activeFilters.type === 'all') {
      this._initialBody = JSON.parse(JSON.stringify(this._copyInitialBody));
      body = this._initialBody;
    } else {
      body = setFilterRequest(this._initialBody, activeFilters, true);
    }
    this._getCompanyData(body);
  }

  showToolTip(id: string, columnName, event: any): void {
    const tooltip: HTMLElement = document.querySelector(`#tooltip-${columnName}-${id} > .tooltip`);
    hideToolTips(tooltip);
    positioning(tooltip, event.target.closest('td'));
  }

  getUrlDetail(companyCode: string): string {
    return `${location.origin}/emisores/detalle?companyCode=${companyCode}`;
  }

  sortBy(column: IFancyTableColumn, table: IFancyTableData): void {
    const orderedData = sortFancyTable<ICompanyItemView>(column, table, this.completeData);
    const sortBy = getFancyTableSortBy(column, table.metadata);
    this._setData(orderedData, sortBy);
  }

  getTotalPages(): number {
    return Math.ceil((this.completeData || []).length / this.maxRows);
  }

  emitChangePage(currentPage: number): void {
    this.currentPage = currentPage;
    this.tablesData = this._setCompanyData(pagination(this.completeData, this.currentPage, this.maxRows), this._getSortBy());
  }

  private _getNewsImportantFact(filters: INewsRequest, companies: IListICompanyItemView): void {
    this.issuerNewsServices.getNews(filters)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.newsImportantFact = [];
        const values: Array<IEntry> = [];
        Object.keys(response.content)
          .forEach(key => {

            response.content[key].map(item => {
              const value = setImportantFacts(item, key);
              values.push(value);
            });

            companies.map(item => {
              values.forEach(iValues => {
                if (item.memory.length > 0) {
                  if (item.memory[0].rpjCode === iValues.rpjCode) {
                    iValues.shortLabel = 'HI';
                    item.importantFact.push(iValues);
                  }
                }
              });
            });

            this._setData(companies);
          });
        this.newsImportantFact = values.sort((a, b) => b.date.getTime() - a.date.getTime());
      });
  }

  private _setTablesMinWidth(): void {
    this._renderer.removeClass(this._tablesContainer, 'loaded');
    this._renderer.addClass(this._tablesContainer, 'loaded');
  }

  private _setScrollingTable(): void {
    const wrapper = this._element.nativeElement.querySelector('.table-wrapper');
    if (wrapper) {
      const table = wrapper.querySelector('[bvl-issuers-list-table]');
      this._unsetScrollingTableStyles(wrapper, table);

      this._ngZone.onStable
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          () => {
            this.scrollingTable = false;
            this._unsetScrollingTableStyles(wrapper, table);
          }
        );
    }
  }

  private _unsetScrollingTableStyles(wrapper: HTMLElement, table: HTMLElement): void {
    this._removeStyles(wrapper, ['maxHeight']);
    this._removeStyles(table, ['position', 'top', 'marginBottom']);
  }

  private _setHeaderCellsWidth(): void {
    this._cdr.detectChanges();
  }

  private _removeStyles(element: HTMLElement, styles: Array<string>): void {
    if (element) {
      styles.forEach(prop => {
        this._renderer.removeStyle(element, prop);
      });
    }
  }

  private _getCompanyData(body?: any): void {
    if (!body) {
      body = this._initialBody;
    }
    this._tableServices.getCompanySearch(body)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this._ngZone.run(() => {
          this.currentPage = 1;
          this.existData = !((response || []).length === 0);
          const completeData = this._getTransformData(response);

          this._getNewsImportantFact(this._filter, completeData);

          this._setData(completeData);
        });
      });
  }

  private _getTransformData(companies: Array<ICompany>): IListICompanyItemView {
    return companies.map(company => ({
      company: company.companyName,
      sector: company.sectorDescription,
      mnemonic: company.stock,
      indexes: company.index,
      companyCode: company.companyCode,
      memory: company.memory.map(value => ({ ...value, shortLabel: 'M', type: 'memory' })),
      importantFact: []
    } as ICompanyItemView));
  }

  private _setData(data: IListICompanyItemView, sortedBy: IFancyTableSortedBy = { column: null, ascendingOrder: true }): void {
    this.tablesData = {} as FancyTableDataSource<IFancyTableData>;
    this.completeData = (data || []);
    this.tablesData = this._setCompanyData(pagination(this.completeData, this.currentPage, this.maxRows), sortedBy);
  }

  private _setCompanyData(data: IListICompanyItemView, sortedBy: IFancyTableSortedBy): FancyTableDataSource<IFancyTableData> {
    const name = data && data[0] && data[0].company || 'nothing data';
    const formatedId = name
      .split(' ')
      .join('_')
      .toLowerCase();

    const formatData = {
      id: formatedId,
      entries: data.map((item, index) => ({ id: index, ...item })),
      metadata: {
        sortedBy: {
          column: sortedBy.column,
          ascendingOrder: sortedBy.ascendingOrder
        },
        expandedElement: null,
        loadedCharts: []
      }
    } as IFancyTableData;

    return new FancyTableDataSource([formatData], this.id, this.exchange);
  }

  private _getSortBy(): any {
    return this.tablesData.data[0].metadata.sortedBy;
  }

  private _getColumnsName(lang: any): Array<IFancyTableColumn> {
    return [
      {
        name: 'company',
        title: lang.tables.company,
        valueType: 'string'
      },
      {
        name: 'sector',
        title: lang.tables.sector,
        valueType: 'string'
      },
      {
        name: 'mnemonic',
        title: lang.general.actions,
        valueType: 'list'
      },
      {
        name: 'indexes',
        title: lang.general.indexes,
        valueType: 'list'
      },
      {
        name: 'memory',
        title: lang.tables.newInfo,
        valueType: 'list'
      }
    ];
  }
}
