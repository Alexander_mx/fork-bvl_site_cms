import {
  Component,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { takeUntil } from 'rxjs/operators';

import { GaUnsubscribeBase, getStorageLanguage, LANGUAGES } from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { IFancyTableByTypeFilter } from 'modules/market-table/src/app/fancy-table-header/fancy-table-header';
import { TABLE_INITIALS } from '../tables';
import {
  FancyTableByInitialFilter,
  FancyTableCapability
} from './fancy-table-header';

@Component({
  selector: 'bvl-issuers-list-table-header',
  templateUrl: './fancy-table-header.component.html',
  styleUrls: ['./fancy-table-header.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class StockTableHeaderComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @Output() activeFilters = new EventEmitter<any>();
  currentFilter: { type: FancyTableCapability; value: any; filterId?: number };
  byInitialFilter: FancyTableByInitialFilter = TABLE_INITIALS;
  byTypeFilter: IFancyTableByTypeFilter;
  searchTerm = '';
  sector = '';
  searchFocused = false;
  searchInputShowed = true;
  selectedType: string | number | null = '';
  selectedInitial: string | null = '';
  selectedTime = 1;

  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)
  ];

  constructor(
    protected _element: ElementRef,
    public media: ObservableMedia,
    private _tableServices: TableServices
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this._getSectors();
    this.media
    .asObservable()
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe((change: MediaChange) => {
      if (change.mqAlias === 'xs' || change.mqAlias === 'sm') {
        if (!this.searchTerm.length) {
          this.searchInputShowed = false;
        }
      } else {
        this.searchInputShowed = true;
      }
    });
  }

  getSelectedLabel(code, object): string {
    return object.find(item => item.code === code).label;
  }

  resetFilters(): void {
    this.searchTerm = '';
    this.selectedType = '';
    this.selectedInitial = '';
    this._addActiveFilter('all', '');
  }

  onCategoryChange(category: string): void {
    this.selectedType = category;
    this._addActiveFilter('sectorCode', category);
  }

  onSearch(): void {
    this._addActiveFilter('companyName', this.searchTerm);
  }

  onInitialChange(initial: string): void {
    this.selectedInitial = initial;
    this._addActiveFilter('firstLetter', this.selectedInitial);
  }

  setFocusToElement(elementRef: HTMLElement): void {
    elementRef.focus();
  }

  toggleSearch(): void {
    this.searchInputShowed = !this.searchInputShowed;
  }

  resetSearch(): void {
    this.searchTerm = '';
    this.onSearch();
  }

  private _addActiveFilter(
    type: FancyTableCapability,
    value: string | number | boolean
  ): void {
    this.currentFilter = {
      type,
      value
    };
    this.activeFilters.emit(this.currentFilter);
  }

  private _getSectors(): void {
    this._tableServices.getSectors()
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe(response => {
      this.byTypeFilter = {
        label: 'SECTOR',
        options: response.map((item, index) => ({
            id: index,
            label: item.description,
            code: item.sectorCode
          }))
      };
    });
  }
}
