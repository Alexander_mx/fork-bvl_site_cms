import { Component, ViewEncapsulation } from '@angular/core';

import { DateUtil } from '@bvl-core/shared/helpers/util';
import { addGoogleScript } from '@bvl-core/shared/helpers/util/google-maps';
import { getPathArgument } from '@bvl-core/shared/helpers/util/path-arguments';

import {
  FILES_PATH,
  getStorageLanguage,
  IAgentAuthorizations,
  IAgentDetails,
  IAgentOffice,
  IAgentRepresentatives,
  IFile,
  ISanction,
  LANGUAGES,
  STORAGE_TYPE,
  StorageService
} from '@bvl/library';
import { AgentsServices } from 'modules/agents-list/src/providers/agents.services';
declare var google;

@Component({
  selector: 'bvl-agent-details',
  templateUrl: './agent-details.component.html',
  styleUrls: ['./agent-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AgentDetailsComponent {

  agent: IAgentDetails;
  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  GA_MENU = [
    {
      control: 'link',
      controlAction: 'click',
      event: null,
      category: '{{g.section}} - {{g.subsection}}',
      action: 'BVL - Menu - {{title}}',
      label: 'Ir a: {{textLink}}',
      value: null,
      dimentions: null
    },
    {
      control: 'button',
      controlAction: 'click',
      event: null,
      category: '{{g.section}} - {{g.subsection}}',
      action: 'BVL - Botón',
      label: '{{textButton}}',
      value: null,
      dimentions: null
    }
  ];

  constructor(
    private _agentServices: AgentsServices,
    private _storageService: StorageService
  ) {
    this._storageService.setConfig({ storageType: STORAGE_TYPE.local });
    const keys = this._storageService.getStorage('keys_site');
    addGoogleScript(keys && keys.googleMaps);
    this._getBrokerDetails();
  }

  accordionState(value, office): void {
    office.active = value === 'expanded' ? true : false;
  }

  private _getBrokerDetails(): void {
    this._agentServices.getBrokers(getPathArgument('agentCode'))
      .subscribe(response => {
        this.agent = this._setAgent(response);
        setTimeout(() => {
          if (this.agent.latitude != null && this.agent.longitude != null) {
            this._addMap(this.agent.latitude, this.agent.longitude, this.agent.code);
          }
        }, 800);
      });
  }

  private _setAgent(data: any): IAgentDetails {
    return {
      id: null,
      name: data.companyName,
      ruc: data.ruc,
      stock: data.capital,
      phones: [data.phoneNumber],
      email: data.mail,
      website: data.webSite,
      generalManager: data.generalManager,
      links: [],
      latitude: data.latitude,
      longitude: data.longitude,
      code: data.sabCode,
      authorizations: data.authorizations.map(item => {
        return {
          date: item.startDate,
          company: item.companyName,
          description: item.description
        };
      }) as Array<IAgentAuthorizations>,
      offices: data.offices.map(office => {
        return {
          active: false,
          companyName: office.companyName,
          address: office.direction,
          district: office.district,
          department: office.department,
          direction: office.direction,
          phone: office.phoneNumber,
          representatives: office.representatives.map(representative => {
            return {
              name: representative
            };
          })
        };
      }) as Array<IAgentOffice>,
      representatives: data.representatives.map(repres => {
        return {
          name: repres.representativeName,
          office: repres.companyName
        };
      }) as Array<IAgentRepresentatives>,
      policies: data.policies.map(policy => {
        return {
          title: policy.companyName,
          url: `${FILES_PATH}/legal/${policy.filePolicy}`
        };
      }) as Array<IFile>,
      sanctions: data.sanctions.map(policy => {
        return {
          businessName: policy.businessName,
          observation: policy.observation,
          resolution: policy.resolution,
          document: policy.sanctionRoute.replace('/var/www/bvl.com.pe', '//www.bvl.com.pe'),
          startDate: DateUtil.getDateStringFormat(policy.startDate)
        };
      }) as Array<ISanction>
    };
  }

  private _addMap(lat, lng, code): void {
    setTimeout(() => {
      const coord = { lat, lng };
      const map = new google.maps.Map(document.getElementById(`map-${code}`), {
        zoom: 16,
        center: coord,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true
      });
      // tslint:disable-next-line: no-unused
      const marker = new google.maps.Marker({
        position: coord,
        map
      });
    });
  }
}
