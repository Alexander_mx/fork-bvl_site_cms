import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { AccordionModule, MenuModule, ParagraphModule, SectionHeaderModule } from '@bvl/library';

import { AgentsServices } from 'modules/agents-list/src/providers/agents.services';
import { ItemsGridModule } from 'modules/items-grid/src/app/items-grid.module';
import { AgentDetailsComponent } from './agent-details.component';

@NgModule({
  declarations: [
    AgentDetailsComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    MenuModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule,
    ItemsGridModule,
    AccordionModule,
    FlexLayoutModule,
    NoopAnimationsModule,
    ParagraphModule
  ],
  providers: [AgentsServices],
  entryComponents: [AgentDetailsComponent]
})
export class AgentDetailsModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-agent-details'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(AgentDetailsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
