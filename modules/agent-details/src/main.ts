import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AgentDetailsModule } from './app/agent-details.module';
platformBrowserDynamic().bootstrapModule(AgentDetailsModule)
  .catch(err => console.error(err));
