import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { TodayModule } from './app/today.module';

platformBrowserDynamic().bootstrapModule(TodayModule)
  .catch(err => console.error(err));
