import { environment } from '../environments/environment';

export class TodayEndPoints {
   public static daily = `${environment.API_URL}/static/negotiatedAmount/daily`
}