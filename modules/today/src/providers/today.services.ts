import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { TodayEndPoints } from './endpoints';

@Injectable()
export class TodayServices {

  constructor(
    private _apiService: ApiService
  ) { }

  getDaily(): Observable<any> {
    return this._apiService.get(TodayEndPoints.daily);
  }
}
