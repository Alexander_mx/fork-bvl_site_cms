import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';

import { ApiModule, FiltersModule } from '@bvl/library';
import { TodayServices } from '../providers/today.services';
import { TodayComponent } from './today.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    ApiModule,
    FiltersModule,
    FlexLayoutModule
  ],
  providers: [
    TodayServices
  ],
  declarations: [TodayComponent],
  entryComponents: [TodayComponent]
})
export class TodayModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-today'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(TodayComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
