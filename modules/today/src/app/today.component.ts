import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';

import { GaUnsubscribeBase, getStorageLanguage, IFilter, LANGUAGES } from '@bvl/library';
import { TodayServices } from '../providers/today.services';

@Component({
  selector: 'bvl-today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.scss']
})
export class TodayComponent extends GaUnsubscribeBase implements OnInit {
  transactions: number;
  solesAmount: number;
  dollarAmount: number;
  activeView: IFilter;

  @Input() activeFilter: string;

  TEXT = LANGUAGES[getStorageLanguage()];

  views: Array<any>;

  constructor(
    protected _element: ElementRef,
    private _todayService: TodayServices,
    private _ngZone: NgZone
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this.views = [
      {
        id: 20,
        label: 'General',
        url: this.TEXT.home.general
      },
      {
        id: 21,
        label: this.TEXT.general.education,
        url: this.TEXT.home.education
      }
    ];
    this._getDaily();
    this.activeView = this.views.find(view => view.url === location.pathname);
  }
  onViewChanged(activeViewId: number): void {
    this.activeView = this.views.find(view => view.id === activeViewId);
    setTimeout(() => {
      window.location.href = location.origin + this.activeView.url;
    }, 500);
  }
  private _getDaily(): void {
    this._todayService.getDaily()
      .subscribe(response => {
        this._ngZone.run(() => {
          response = response || {};
          this.transactions = response.numberOperations;
          this.solesAmount = response.amountSoles;
          this.dollarAmount = response.amountDollars;
        });
      });
  }
}
