import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MarketTableModule } from './app/market-table.module';

platformBrowserDynamic().bootstrapModule(MarketTableModule)
  .catch(err => console.error(err));
