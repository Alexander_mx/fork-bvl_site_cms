import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import {
  AmountCellModule,
  DomChangeDirectiveModule,
  LabelModule,
  LinksListModule,
  PipesModule,
  ToggleByPositionModule,
  TopScollbarModule
} from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { StockTableHeaderComponent } from './fancy-table-header/fancy-table-header.component';
import { MarketTableComponent } from './market-table.component';

@NgModule({
  declarations: [
    MarketTableComponent,
    StockTableHeaderComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AmountCellModule,
    LabelModule,
    LinksListModule,
    FlexLayoutModule,
    TopScollbarModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule,
    PipesModule,
    ToggleByPositionModule,
    DomChangeDirectiveModule
  ],
  entryComponents: [
    MarketTableComponent
  ],
  exports: [
    MarketTableComponent
  ],
  providers: [
    ExchangeProvider,
    GraphicOptionsProvider,
    TableServices,
    DatePipe
  ]
})
export class MarketTableModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-market-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(MarketTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
