import { animate, state, style, transition, trigger } from '@angular/animations';
import {
  Component,
  ElementRef,
  EventEmitter,
  NgZone,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import * as Highcharts from 'highcharts';
import HighchartMore from 'highcharts/highcharts-more';
import HighchartsExporting from 'highcharts/modules/exporting';
import { takeUntil } from 'rxjs/operators';

import { GaUnsubscribeBase, getStorageLanguage, IFancyTableColumn, IStock, LANGUAGES } from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { FancyTableTheme, IStockquoteTopItem, IStockquoteTopResponse, IStockTop, setLinks } from 'modules/fancy-table/src/app/fancy-table';
import { IFancyTableData, IFancyTableEntry } from '../../../fancy-table/src/app/fancy-table';
import { FancyTableDataSource } from '../../../fancy-table/src/app/fancy-table-data-source';
import { IFancyTableByCategoryFilter } from './fancy-table-header/fancy-table-header';
import { MARKET_TABLES_COLUMNS } from './tables';
HighchartMore(Highcharts);
HighchartsExporting(Highcharts);

let nextId = 0;
let chart;
@Component({
  selector: 'bvl-market-table',
  templateUrl: './market-table.component.html',
  styleUrls: ['./market-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, hidden', style({ height: '0px', minHeight: '0' })),
      state('expanded, shown', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('.2s cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class MarketTableComponent extends GaUnsubscribeBase implements OnInit {
  id = `bvl-table-${nextId++}`;
  tablesData: FancyTableDataSource<IFancyTableData> = {} as FancyTableDataSource<IFancyTableData>;
  tableColumns: Array<IFancyTableColumn> = MARKET_TABLES_COLUMNS;
  byCategoryFilters: IFancyTableByCategoryFilter | Array<IFancyTableByCategoryFilter>;

  @Output() activeFilters = new EventEmitter<any>();
  @Output() tableEvents = new EventEmitter<any>();

  url: string;
  chartOptions: any;
  columnsName: Array<string>;
  viewLoaded = false;
  tablesFitInContainer = true;
  selectedTable = undefined;
  expandedElement: { tableId: IFancyTableData['id'], element: IStock };
  endDate = new Date();

  scrollingTable = false;
  headerCellsWidth: Array<number> = [];
  chartData: any;
  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  constructor(
    private exchange: ExchangeProvider,
    public media: ObservableMedia,
    protected _element: ElementRef,
    private _graphicOptions: GraphicOptionsProvider,
    private _tableServices: TableServices,
    private _ngZone: NgZone
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this._getMarketData();
    this.chartOptions = this._graphicOptions.getGraphicOptions();
    this.chartOptions.exporting = { enabled: false };
  }

  setEventsGA(): void { }

  hasAnyTableTitle(): boolean {
    return !!this.tablesData.data.find(table => !!table.title);
  }

  onFilterChange(activeFilters): void {
    const body = {};
    activeFilters.map(item => {
      body[item.type] = this._getValueFilter(item);
    });
    this._getMarketData(body);
  }

  private _getValueFilter(filter: any): any {
    return filter.type !== 'date' ? filter.value : filter.value
      .split('/')
      .reverse()
      .join('-');
  }

  getAnimationState(element: IFancyTableEntry, table: IFancyTableData): string {
    const areDetailsShown = table.metadata && table.metadata.expandedElement === element;
    const isGtSm = (this.media.isActive('gt-sm') && this.tablesData.data.length === 1);

    return (areDetailsShown) ? (isGtSm ? 'expanded' : 'shown') : (isGtSm ? 'collapsed' : 'hidden');
  }

  expandElement(element: IFancyTableEntry, table: IFancyTableData, title: string): void {
    if (element === table.metadata.expandedElement) {
      table.metadata.expandedElement = null;

      return;
    }

    table.metadata.expandedElement = element;
    element.detailsShowed = true;

    this._loadChart(element, title);
    table.metadata.loadedCharts = [...table.metadata.loadedCharts, element.id];
  }

  private _loadChart(element: IFancyTableEntry, title: string): void {
    this._tableServices.getGraph(element.mnemonic)
      .subscribe((response: any) => {
        this.chartOptions.series = [];

        const values = response.sort((a, b) => a.lastDate.localeCompare(b.lastDate));
        element.chartData = values;

        if (element.chartData.length) {
          this.chartOptions.series.push({
            data: values.map(item => ({
              name: item.lastDate.replace('T', ' '),
              y: item.lastValue || 0
            }))
          });
          Highcharts.chart(`graphic-${title}-${element.id}`, this.chartOptions);
        }
      });
  }

  sortBy(column: IFancyTableColumn, table: IFancyTableData): void {
    this.tablesData.sort(column, table);
  }

  toggleInWatchList(element: IFancyTableEntry): void {
    element.inWatchlist = !element.inWatchlist;
    this.tableEvents.emit({
      eventType: 'watchList',
      elementId: element.id,
      value: element.inWatchlist
    });
  }

  private _setTopTableData(data: IStockquoteTopResponse): FancyTableDataSource<IFancyTableData> {
    const formatData = [data.gain, data.loser].map((table, i) => {
      return {
        id: i.toString(),
        title: i === 0 ? this.TEXT.general.rise : this.TEXT.general.low,
        iconTitle: i === 0 ? 'icon-arrow-up' : 'icon-arrow-down',
        theme: (i === 0 ? 'green' : 'red') as FancyTableTheme,
        entries: table.map((item: IStockquoteTopItem, y: number) => {
          return {
            id: y,
            mnemonic: item.nemon,
            last: item.last,
            variation: item.percentageChange,
            currency: item.currency,
            links: setLinks(item.companyCode),
            companyCode: item.companyCode
          };
        }),
        metadata: {
          sortedBy: {
            column: null,
            ascendingOrder: true
          },
          expandedElement: null,
          loadedCharts: []
        }
      };
    });

    return new FancyTableDataSource(formatData, this.id, this.exchange);
  }

  private _getMarketData(request?: IStockTop): void {
    const body = {
      sector: request && request.sector ? request.sector : '',
      date: request && request.date ? request.date : '',
      today: !request ? true : request.today
    };

    this._tableServices.getTop(body)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this._ngZone.run(() => {
          this.tablesData = this._setTopTableData(response);
        });
      });
  }
}
