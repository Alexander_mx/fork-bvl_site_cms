import { DatePipe } from '@angular/common';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';

import { coerceBooleanProp, GaUnsubscribeBase, getStorageLanguage, LANGUAGES } from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { takeUntil } from 'rxjs/operators';
import {
  FancyTableCapability,
  IFancyTableByTypeFilter,
  IFilterObject
} from './fancy-table-header';

@Component({
  selector: 'bvl-market-table-header',
  templateUrl: './fancy-table-header.component.html',
  styleUrls: ['./fancy-table-header.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class StockTableHeaderComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  private _fullWidth: boolean;

  currentFilter: Array<IFilterObject> = [];
  byTypeFilter: IFancyTableByTypeFilter;

  @Input()
  get fullWidth(): any {
    return this._fullWidth;
  }
  set fullWidth(value: any) {
    this._fullWidth = coerceBooleanProp(value);
  }

  @Output() activeFilters = new EventEmitter<any>();

  tableCapabilities: Array<FancyTableCapability>;
  filterOnlyToday: false;
  onlyTodayEnabled = true;
  searchTerm = '';
  sector = '';
  searchFocused = false;
  searchInputShowed = true;
  selectedCategories: Array<{ filterId: number; value: number }> = [];
  selectedType: string | number | null = null;
  selectedInitial: string | null = null;
  selectedTime = 1;
  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  constructor(
    protected _element: ElementRef,
    public media: ObservableMedia,
    private _tableServices: TableServices,
    private datePipe: DatePipe
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this._getSectors();
    this.media
      .asObservable()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((change: MediaChange) => {
        if (change.mqAlias === 'xs' || change.mqAlias === 'sm') {
          if (!this.searchTerm.length) {
            this.searchInputShowed = false;
          }
        } else {
          this.searchInputShowed = true;
        }
      });
    this.setDefaultFilters();
  }

  onTypeChange(typeValue: number | string): void {
    if (this.selectedType === typeValue) {
      return void 0;
    }
    this.selectedType = typeValue;
    this.sector = typeValue.toString();
    this._addActiveFilter('sector', typeValue);
  }

  private setDefaultFilters(): void {
    const date = new Date(new Date().setDate(new Date().getDate() - this.selectedTime));
    const formated = this.datePipe.transform(date, 'dd/MM/yyyy');
    this.setFilterToday();
    this._setValueFilter('date', formated);
  }

  onTimeChange(): void {
    const date = new Date(new Date().setDate(new Date().getDate() - this.selectedTime));
    const formated = this.datePipe.transform(date, 'dd/MM/yyyy');
    this.setFilterToday();
    this._addActiveFilter('date', formated);
  }

  private setFilterToday(): void {
    this._setValueFilter('today', this.selectedTime === 1);
  }

  getSelectedLabel(code, object): string {
    return object.find(item => item.code === code).label;
  }
  isNumber(val: any): boolean {
    return typeof val === 'number';
  }

  private _setValueFilter(type: FancyTableCapability, value: string | number | boolean): void {
    const findedObject = this.currentFilter.find(item => item.type === type);
    if (!findedObject) {
      this.currentFilter.push({ type, value });
    } else {
      const position = this.currentFilter.indexOf(findedObject);
      this.currentFilter[position] = { type, value };
    }
  }

  private _addActiveFilter(type: FancyTableCapability, value: string | number | boolean): void {
    this._setValueFilter(type, value);
    this.activeFilters.emit(this.currentFilter);
  }

  private _getSectors(): void {
    this._tableServices.getSectors()
      .subscribe(response => {
        this.byTypeFilter = {
          label: 'SECTOR',
          options: response.map((item, index) => {
            return {
              id: index,
              label: item.description,
              code: item.sectorCode
            };
          })
        };
      });
  }
}
