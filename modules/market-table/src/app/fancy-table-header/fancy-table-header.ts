export type FancyTableCapability = 'sort' | 'filter' | 'sector' | 'date' | 'inWatchList' | 'today';

export type FancyTableByInitialFilter = Array<string>;

export interface IFancyTableByTypeFilter {
    label: string;
    options: Array<{ id: number; label: string; code?: string; }>;
}
export interface IFilterObject {
    type: FancyTableCapability;
    value: any;
    filterId?: number;
}
export interface IFancyTableByCategoryFilter {
    filterId: number;
    title?: string;
    label?: string;
    defaultOption: number;
    options: Array<{ id: number, label: string }>;
}
