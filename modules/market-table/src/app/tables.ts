import { getStorageLanguage, LANGUAGES } from '@bvl/library';

const TEXT = LANGUAGES[
  getStorageLanguage()
    .substring(0, 2)
];

export const MARKET_TABLES_COLUMNS = [
  {
    name: 'mnemonic',
    label: TEXT.general.nemonics,
    valueType: 'string'
  },
  {
    name: 'last',
    label: TEXT.tables.last,
    valueType: 'price'
  },
  {
    name: 'variation',
    label: `${TEXT.tables.var}%`,
    valueType: 'percent'
  }
];
