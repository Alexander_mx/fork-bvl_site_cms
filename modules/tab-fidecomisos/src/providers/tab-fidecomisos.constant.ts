export const GA_AUTOCOMPLETE = {
  control: 'autocomplete',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - Bolsa News - {{qs(bvl-tabs a.active)}}',
  label: 'Busqueda: {{autocomplete}}',
  value: null,
  dimentions: null
};

export const GA_SELECT = {
  control: 'select',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - Bolsa News - {{qs(bvl-tabs a.active)}}',
  label: '{{label}}: {{select}}',
  value: null,
  dimentions: null
};

export const GA_BUTTON_TAB = {
  control: 'buttonTab',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - Bolsa News',
  label: '{{button}}',
  value: null,
  dimentions: null
};

export const GA_ISSUER_TYPE_RADIO = {
  control: 'issuerTypeRadio',
  controlAction: 'change',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - Bolsa News - {{qs(bvl-tabs a.active)}}',
  label: '{{radio}}',
  value: null,
  dimentions: null
};

export const GA_CLEAR_BUTTON = {
  control: 'clearButton',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - Bolsa News - {{qs(bvl-tabs a.active)}}',
  label: 'Limpiar',
  value: null,
  dimentions: null
};

export const GA_SEARCH_BUTTON = {
  control: 'searchButton',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}} - {{g.subsection}}',
  action: 'BVL - Bolsa News - {{qs(bvl-tabs a.active)}}',
  label: 'Buscar',
  value: null,
  dimentions: null
};

export const CONTROL_TYPE = {
  radio: 'radio',
  tabs: 'tabs',
  autocomplete: 'autocomplete',
  select: 'select',
  datepicker: 'datepicker',
  label: 'label'
};

export const FILTER_CONTROLS = {
  top: 'mTop',
  middle: 'mMiddle',
  bottom: 'mBottom'
};

export const YEAR_PERIOD_INVESTMENTFUND_ASSETS = [
  {
    code: 'T1',
    description: 'Primer Trimestre',
    codePeriod: 1
  },
  {
    code: 'T2',
    description: 'Segundo Trimestre',
    codePeriod: 1
  },
  {
    code: 'T3',
    description: 'Tercer Trimestre',
    codePeriod: 1
  },
  {
    code: 'T4',
    description: 'Cuarto Trimestre',
    codePeriod: 1
  },
  {
    code: 'AA',
    description: 'Auditados',
    codePeriod: 2
  }
];

export const PERIOD_INVESTMENTFUND_ASSETS = [
  {
    code: 1,
    description: 'Trimestral Individual'
  },
  {
    code: 2,
    description: 'Anual Individual'
  }
];

export const YEAR_PERIOD = [
  {
    code: '1',
    description: 'Primer Trimestre'
  },
  {
    code: '2',
    description: 'Segundo Trimestre'
  },
  {
    code: '3',
    description: 'Tercer Trimestre'
  },
  {
    code: '4',
    description: 'Cuarto Trimestre'
  },
  {
    code: 'A',
    description: 'Auditados'
  }
];

export const PERIOD = [
  {
    code: 1,
    description: 'Trimestral No Auditada'
  },
  {
    code: 2,
    description: 'Semestral No Auditada'
  },
  {
    code: 3,
    description: 'Auditada Anual'
  }
];

export const TYPE = [
  {
    code: 1,
    description: 'Individual'
  },
  {
    code: 2,
    description: 'Consolidada'
  }
];

export const ISSUER_TYPE = {
  issuers: 'E',
  investmentFund: 'F',
  trustAssets: 'T'
};

export const INFORMATION_TYPE = {
  importantFacts: 'IF',
  financialStatements: 'FS',
  memories: 'ME'
};
