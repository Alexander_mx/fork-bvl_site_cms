import { environment } from './../environments/environment';

export class TabFidecomisosEndpoint {
  public static getCompanies = `${environment.API_URL}/static/company/simple`;
  public static getSubjectTypes = `${environment.API_URL}/static/importantfact/listSub`;
  public static searchImportantFacts = `${environment.API_URL}/static/importantfact`;
  public static searchFinancialStatementsIssuers = `${environment.API_URL}/static/eeff`;
  public static getInvestmentFundAndAssets = `${environment.API_URL}/static/securitizer/filters`;
  public static searchInvestmentFundAndAssets = `${environment.API_URL}/static/securitizer`;
  public static searchMemoriesIssuers = `${environment.API_URL}/static/company/{companyCode}`;
}
