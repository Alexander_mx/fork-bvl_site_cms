import { IPaginationResponse } from '@bvl-core/shared/helpers/models';

export interface ISearchOption {
  informationType: string;
  issuerType: string;
}

export interface ICompanyResponse {
  companyAddress: string;
  companyCode: string;
  companyName: string;
  dateFundation: string;
  description: string;
  descriptionFundation: string;
  enActDescription: string;
  esActDescription: string;
  fax: string;
  listDirectory: Array<IDirectory>;
  listIndexComposition: Array<IIndexComposition>;
  listManager: Array<IDirectory>;
  listMemoryEEFF: Array<IMemoryEeff>;
  listTypeCompositionAcc: Array<ITypeCompositionAcc>;
  listValue: Array<IValueCompany>;
  originCode: string;
  phone: string;
  rpjCode: string;
  sectorCode: string;
  subSectorCode: string;
  website: string;
}

export interface IIndexComposition {
  bloomTicker: string;
  companyDescription: string;
  currencyCode: string;
  cusip: string;
  effectiveDate: string;
  gvKey: string;
  indexCode: string;
  indexKey: string;
  indexName: string;
  isin: string;
  ricCode: string;
  sedol: string;
  ticker: string;
}

export interface IValueCompany {
  dateInscription: string;
  isin: string;
  listBenefit: Array<IBenefit>;
  listLastValue: Array<ILastValue>;
  listStock: Array<IStock>;
  nemonico: string;
  serie: string;
  valueType: string;
}

export interface IStock {
  capital: string;
  coin: string;
  date: string;
  desWebMessage: string;
  isin: string;
  nemonico: string;
  nominalValue: string;
  quantity: string;
}

export interface ILastValue {
  amount: string;
  buy: string;
  close: string;
  coin: string;
  companyCode: string;
  companyName: string;
  dateTimP: string;
  dateTimS: string;
  description: string;
  eeffName: string;
  exd: string;
  last: string;
  max: string;
  min: string;
  open: string;
  operations: string;
  quantityNegotiated: string;
  rpjCode: string;
  sectorCode: string;
  sell: string;
  tkrCode: string;
  unity: string;
  valueGroup: string;
  valueType: string;
  var: string;
}

export interface IBenefit {
  benefitType: string;
  benefitValue: string;
  coin: string;
  dateAgreement: string;
  dateCut: string;
  dateDelivery: string;
  dateEntry: string;
  dateRegistry: string;
  isin: string;
  nemonico: string;
  notesAgreement: string;
  notesCut: string;
  notesDelivery: string;
  notesLaw: string;
  notesRegistry: string;
  notesValue: string;
  secMovBe: string;
  secMovDi: string;
}

export interface IDirectory {
  description: string;
  lastUpdate: string;
  personDescription: string;
  positionCode: string;
  validity: string;
}

export interface ITypeCompositionAcc {
  listComposiytionAcc: Array<ICompositionAcc>;
  orderValue: number;
  structureCode: number;
  structureDescription: string;
  year: number;
}

export interface ICompositionAcc {
  distributionDate: string;
  structureCode: string;
  typeCode: string;
  typeDescription: string;
  valueParticipation: string;
  valueStock: string;
}

export interface IMemoryEeff {
  companyName: string;
  date: string;
  document: string;
  observation: string;
  path: string;
  rpjCode: string;
  sequence: number;
  type: string;
  year: number;
}

export interface ISecuritizer {
  correlative: string;
  dateHour: string;
  documentName: string;
  documentOrder: string;
  documentType: string;
  dscPatrimony: string;
  id: string;
  lstData: Array<any>;
  observation: string;
  partcount: string;
  path: string;
  periodAccountType: string;
  rpj: string;
  sequence: string;
  sequenceCod: string;
  socialBusiness: string;
  title: string;
  type1: string;
  type2: string;
  year: string;
}

export interface IDataSecuritizer {
  date: string;
  document: string;
  documentInformation: IDocumentInformation;
  documentType: string;
  id: string;
  order: string;
  path: string;
  quantity: string;
  sequence: string;
}

export interface IDocumentInformation {
  currency: string;
  detail: Array<IDetailDocumentInformation>;
  dscPatrimony: string;
  format: string;
  type: string;
}

export interface IDetailDocumentInformation {
  column1: string;
  column2: string;
  column3: string;
  column4: string;
  count: string;
  order: string;
  subType: string;
  type: string;
}

export interface IEeff {
  businessName: string;
  correlative: string;
  dateHour: string;
  document: Array<IDocumentEeff>;
  documentName: string;
  documentOrder: string;
  documentType: string;
  observation: string;
  orderNumberRpj: string;
  path: string;
  period: string;
  quantityParts: string;
  sequence: string;
  yearPeriod: string;
}

export interface IDocumentEeff {
  businessName: string;
  caccount: string;
  cflag: string;
  error: string;
  existData: string;
  mainTitle: string;
  messageError: string;
  numberColumns: string;
  orderNumber: string;
  title: string;
  value1: string;
  value2: string;
  value3: string;
  value4: string;
  value5: string;
  value6: string;
  value7: string;
  value8: string;
  value9: string;
  value10: string;
  value11: string;
  value12: string;
  value13: string;
  value14: string;
  value15: string;
  value16: string;
  value17: string;
  value18: string;
  value19: string;
}

export interface IImportantfact {
  businessName: string;
  codes: Array<ICodeImportantfact>;
  columnNumber: string;
  correlativeCodeBVL: string;
  date: string;
  documents: Array<IDocumentImportantfact>;
  observation: string;
  registerDate: string;
  registerDateD: string;
  rpjCode: string;
  session: string;
  sessionDate: string;
}

export interface ICodeImportantfact {
  codeHHII: string;
  descCodeHHII: string;
  sequence: string;
}

export interface IDocumentImportantfact {
  path: string;
  sequence: string;
}

export interface ISecuritizerPaginationResponse extends IPaginationResponse<ISecuritizer> {}
export interface IEeffPaginationResponse extends IPaginationResponse<IEeff> {}
export interface IImportantfactPaginationResponse extends IPaginationResponse<IImportantfact> {}
