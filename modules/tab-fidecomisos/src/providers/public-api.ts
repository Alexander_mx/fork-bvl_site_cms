export * from './tab-fidecomisos.interface';
export * from './tab-fidecomisos.constant';
export { TabFidecomisosService } from './tab-fidecomisos.service';
export { TabFidecomisosEndpoint } from './tab-fidecomisos.endpoint';
