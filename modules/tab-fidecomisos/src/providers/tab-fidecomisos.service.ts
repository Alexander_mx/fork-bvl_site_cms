import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ApiService } from '@bvl/library';
import { TabFidecomisosEndpoint } from './tab-fidecomisos.endpoint';
import {
  ICompanyResponse,
  IEeffPaginationResponse,
  IImportantfactPaginationResponse,
  ISecuritizerPaginationResponse
} from './tab-fidecomisos.interface';

@Injectable({
  providedIn: 'root'
})
export class TabFidecomisosService {

  constructor(
    private _apiService: ApiService
  ) { }

  getCompanies(showSpin: boolean = false): Observable<Array<ICompanyResponse>> {
    return this._apiService.get(TabFidecomisosEndpoint.getCompanies, { preloader: showSpin });
  }

  getSubjectTypes(showSpin: boolean = false): Observable<Array<any>> {
    return this._apiService.get(TabFidecomisosEndpoint.getSubjectTypes, { preloader: showSpin })
            .pipe(
              tap((preResponse: Array<any>) => {
                preResponse.forEach(fv => {
                  fv.codeHHIIDescCodeHHII = `${fv.codeHHII} - ${fv.descCodeHHII}`;
                });
              })
            );
  }

  getYears(): Observable<Array<any>> {
    const date = new Date(),
          currentYear = date.getFullYear(),
          initYear = date.getFullYear() - 19,
          yearsList = [];
    for (let i = currentYear; i >= initYear; i--) {
      yearsList.push({
        code: i.toString(),
        description: i
      });
    }

    return of(yearsList);
  }

  searchImportantFacts(params: any, showSpin: boolean = false): Observable<IImportantfactPaginationResponse> {
    return this._apiService.post(TabFidecomisosEndpoint.searchImportantFacts, params, { preloader: showSpin });
  }

  searchFinancialStatementsIssuers(params: any, showSpin: boolean = false): Observable<IEeffPaginationResponse> {
    return this._apiService.post(TabFidecomisosEndpoint.searchFinancialStatementsIssuers, params, { preloader: showSpin });
  }

  getInvestmentFundAndAssets(type: string, showSpin: boolean = false): Observable<Array<any>> {
    const params = { type };

    return this._apiService.get(TabFidecomisosEndpoint.getInvestmentFundAndAssets, { params, preloader: showSpin });
  }

  searchInvestmentFundAndAssets(params: any, showSpin: boolean = false): Observable<ISecuritizerPaginationResponse> {
    return this._apiService.post(TabFidecomisosEndpoint.searchInvestmentFundAndAssets, params, { preloader: showSpin });
  }

  searchMemoriesIssuers(companyCode: string, showSpin: boolean = false): Observable<ICompanyResponse> {
    const params = { companyCode };

    return this._apiService.get(TabFidecomisosEndpoint.searchMemoriesIssuers, { params, preloader: showSpin });
  }

}
