import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { TabFidecomisosModule } from './app/tab-fidecomisos.module';

platformBrowserDynamic().bootstrapModule(TabFidecomisosModule)
  .catch(err => console.error(err));
