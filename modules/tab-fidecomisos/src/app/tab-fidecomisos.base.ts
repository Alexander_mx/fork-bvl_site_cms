import { DatePipe } from '@angular/common';
import { ElementRef, OnInit, ViewChild } from '@angular/core';
import { finalize, takeUntil } from 'rxjs/operators';

import { pagination } from '@bvl-core/shared/helpers/util';
import { DateUtil, GaUnsubscribeBase, getStorageLanguage, ITableDownloadFile, ITableFileLink, LANGUAGES, StringUtil } from '@bvl/library';
import { INFORMATION_TYPE, ISearchOption, ISSUER_TYPE, TabFidecomisosService } from '../providers';
import { SearcherComponent } from './search-filter/searcher/searcher.component';

export abstract class TabFidecomisosBase extends GaUnsubscribeBase implements OnInit {

  @ViewChild(SearcherComponent) searcher: SearcherComponent;

  searchOption: ISearchOption;
  searcherControls: Array<any>;
  searchParams: any;
  tableHeader: Array<string>;
  tableAllData: Array<ITableDownloadFile | ITableFileLink>;
  tableData: Array<ITableDownloadFile | ITableFileLink>;
  tableMessage: string;
  tableMessageNoData: string;
  totalPages: number;

  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)];

  constructor(
    protected _element: ElementRef,
    public tabFidecomisosService: TabFidecomisosService,
    public datePipe: DatePipe
  ) {
    super(_element);
    this.tableHeader = [
      this.languageText.general.subjectDetail,
      this.languageText.general.date,
      this.languageText.general.hour
    ];
    this._clear();
  }

  setEventsGA(): void { }

  ngOnInit(): void { }

  private _clear(): void {
    this.searchParams = '';
    this.tableAllData = [];
    this.tableData = [];
    this.tableMessage = 'Aún no ha realizado una búsqueda';
    this.tableMessageNoData = 'No existen datos para la búsqueda realizada';
    this.totalPages = 0;
  }

  clear(): void {
    this._clear();
  }

  private _searchedClear(): void {
    this.tableData = [];
    this.tableMessage = '...';
    this.totalPages = 0;
  }

  private _searchImportantFacts(searchParams: any): void {
    this.tabFidecomisosService.searchImportantFacts(searchParams, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        finalize(() => {
          this.tableMessage = this.tableMessageNoData;
        })
      )
      .subscribe(response => {
        if (response.content.length) {
          this.totalPages = response.totalPages;
          this.tableData = response.content.map(fv => (
            {
              enterprise: fv.businessName,
              description: {
                list: fv.codes.map(sv => sv.descCodeHHII),
                text: (fv.observation || '').trim()
              },
              date: DateUtil.getDateStringFormat(fv.registerDate),
              hour: DateUtil.getTimeStringFormat(fv.registerDate),
              downloadFiles: fv.documents.map(sv => sv.path)
            })) as Array<ITableDownloadFile>;
        }
      });
  }

  private _searchIssuers(searchParams: any): void {
    if (this.searchOption.informationType === INFORMATION_TYPE.financialStatements) {
      this._searchFinancialStatementsIssuers(searchParams);
    } else {
      this._searchMemoriesIssuers(searchParams);
    }
  }

  private _searchFinancialStatementsIssuers(searchParams: any): void {
    this.tabFidecomisosService.searchFinancialStatementsIssuers(searchParams, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        finalize(() => {
          this.tableMessage = this.tableMessageNoData;
        })
      )
      .subscribe(response => {
        if (response.content.length) {
          this.totalPages = response.totalPages;
          this.tableData = response.content.map(fv => (
            {
              fileLink: {
                description: fv.documentName,
                link: (StringUtil.isAnyFileExtension(fv.path)) ? fv.path : fv
              },
              date: DateUtil.getDateStringFormat(fv.dateHour),
              hour: DateUtil.getTimeStringFormat(fv.dateHour)
            } as ITableFileLink));
        }
      });
  }

  private _searchMemoriesIssuers(searchParams: any): void {
    this.tabFidecomisosService.searchMemoriesIssuers(searchParams.companyCode, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        finalize(() => {
          this.tableMessage = this.tableMessageNoData;
        })
      )
      .subscribe(response => {
        if (response.listMemoryEEFF.length) {
          this.tableAllData = response.listMemoryEEFF
            .filter(fv => (fv.year === Number(searchParams.exercise)))
            .map(fv => ({
              fileLink: {
                description: `${fv.document} - ${this.TEXT.general.part} ${fv.sequence}`,
                link: StringUtil.isAnyFileExtension(fv.path) ? fv.path : ''
              },
              date: DateUtil.getDateStringFormat(fv.date),
              hour: DateUtil.getTimeStringFormat(fv.date)
            })) as Array<ITableFileLink>;
          this.tableData = pagination(this.tableAllData, 1, 10);
        }
      });
  }

  private _searchInvestmentFundAndAssets(searchParams: any): void {
    this.tabFidecomisosService.searchInvestmentFundAndAssets(searchParams, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        finalize(() => {
          this.tableMessage = this.tableMessageNoData;
        })
      )
      .subscribe(response => {
        if (response.content.length) {
          this.totalPages = response.totalPages;
          this.tableData = response.content.reduce((previous, current) => {

            if (current.path) {
              previous.push({
                fileLink: {
                  description: current.documentName,
                  link: current.path
                },
                date: DateUtil.getDateStringFormat(current.dateHour),
                hour: DateUtil.getTimeStringFormat(current.dateHour)
              });
            }

            const items = (current.lstData || []).map(fv => (
              {
                fileLink: {
                  description: fv.document,
                  link: (StringUtil.isAnyFileExtension(fv.path))
                    ? fv.path
                    : ''
                },
                date: DateUtil.getDateStringFormat(fv.date),
                hour: DateUtil.getTimeStringFormat(fv.date)
              } as ITableFileLink));

            previous = [...previous, ...items];

            return previous;
          }, []);
        }
      });
  }

  search(searchParams: any, isPagination = false): void {
    if (!isPagination) {
      this._searchedClear();
    }

    switch (this.searchOption.informationType) {
      case INFORMATION_TYPE.importantFacts:
        this._searchImportantFacts(searchParams);
        break;
      default:
        if (this.searchOption.issuerType === ISSUER_TYPE.issuers) {
          this._searchIssuers(searchParams);
        } else {
          this._searchInvestmentFundAndAssets(searchParams);
        }
        break;
    }
  }

  get languageText(): any {
    const currentLanguage: string = getStorageLanguage()
      .substring(0, 2);

    return LANGUAGES[currentLanguage];
  }
}
