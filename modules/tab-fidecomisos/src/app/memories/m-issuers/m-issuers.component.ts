import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';

import { ITableFileLink } from '@bvl/library';
import {
  CONTROL_TYPE,
  GA_SELECT,
  INFORMATION_TYPE,
  ISSUER_TYPE,
  TabFidecomisosService
} from 'modules/tab-fidecomisos/src/providers';
import { TabFidecomisosBase } from '../../tab-fidecomisos.base';

@Component({
  selector: 'bvl-m-issuers',
  templateUrl: './m-issuers.component.html',
  styleUrls: ['./m-issuers.component.scss']
})
export class MIssuersComponent extends TabFidecomisosBase implements OnInit {

  tableAllData: Array<ITableFileLink>;
  tableData: Array<ITableFileLink>;

  constructor(
    protected _element: ElementRef,
    public tabFidecomisosService: TabFidecomisosService,
    public datePipe: DatePipe
  ) {
    super(_element, tabFidecomisosService, datePipe);
    this.searchOption = {
      informationType: INFORMATION_TYPE.memories,
      issuerType: ISSUER_TYPE.issuers
    };
  }

  private _addSelectEvent(label: string, selectValue: string): void {
    GA_SELECT.dimentions = 'ejercicio:{{mExercise}}';
    const formValue = this.searcher.getFormValue();
    const dimentions = {
      mExercise: (formValue.mExercise) ? formValue.mExercise.description : '- Seleccione -'
    };
    const data = { ...{ label, select: dimentions[selectValue] }, ...dimentions };
    this.addEvent(GA_SELECT, data);
  }

  ngOnInit(): void {
    this.searcherControls = [
      {
        code: 'mCompany',
        type: CONTROL_TYPE.autocomplete,
        placeholder: 'Buscar empresa',
        list: this.tabFidecomisosService.getCompanies(),
        matchField: 'companyName',
        textField: 'companyName',
        parentClass: 'col-md-8 col-lg-6 g-mb--15 g-mb-md--0',
        class: '',
        validators: [Validators.required],
        selectItem: (value: any) => { }
      },
      {
        code: 'mExercise',
        type: CONTROL_TYPE.select,
        label: 'Ejercicio',
        placeholder: '-Seleccione-',
        list: this.tabFidecomisosService.getYears(),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-md-4 col-lg-3',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => {
          this._addSelectEvent('Ejercicio', 'mExercise');
        }
      }
    ];
  }

  formValue(formValue: any): void {
    this.searchParams = {
      companyCode: formValue.mCompany.companyCode,
      exercise: formValue.mExercise.code
    };
    this.search(this.searchParams);
  }

  pagination(tableData: any): void {
    this.tableData = tableData;
  }

}
