import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';

import { ITableFileLink } from '@bvl/library';
import {
  CONTROL_TYPE,
  GA_SELECT,
  INFORMATION_TYPE,
  ISSUER_TYPE,
  TabFidecomisosService
} from 'modules/tab-fidecomisos/src/providers';
import { TabFidecomisosBase } from '../../tab-fidecomisos.base';

@Component({
  selector: 'bvl-m-investment-fund',
  templateUrl: './m-investment-fund.component.html',
  styleUrls: ['./m-investment-fund.component.scss']
})
export class MInvestmentFundComponent extends TabFidecomisosBase implements OnInit {

  tableData: Array<ITableFileLink>;

  constructor(
    protected _element: ElementRef,
    public tabFidecomisosService: TabFidecomisosService,
    public datePipe: DatePipe
  ) {
    super(_element, tabFidecomisosService, datePipe);
    this.searchOption = {
      informationType: INFORMATION_TYPE.memories,
      issuerType: ISSUER_TYPE.investmentFund
    };
  }

  private _addSelectEvent(label: string, selectValue: string): void {
    GA_SELECT.dimentions = 'fondoInversion:{{mInvestmentFund}},ejercicio:{{mExercise}}';
    const formValue = this.searcher.getFormValue();
    const dimentions = {
      mInvestmentFund: (formValue.mInvestmentFund) ? formValue.mInvestmentFund.patrimonyDescription : '- Seleccione -',
      mExercise: (formValue.mExercise) ? formValue.mExercise.description : '- Seleccione -'
    };
    const data = { ...{ label, select: dimentions[selectValue] }, ...dimentions };
    this.addEvent(GA_SELECT, data);
  }

  ngOnInit(): void {
    this.searcherControls = [
      {
        code: 'mInvestmentFund',
        type: CONTROL_TYPE.select,
        label: 'Fondo de Inversión',
        placeholder: '- Seleccione -',
        list: this.tabFidecomisosService.getInvestmentFundAndAssets(ISSUER_TYPE.investmentFund),
        valueField: 'rpjCode',
        textField: 'patrimonyDescription',
        parentClass: 'col-md-6',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => {
          this._addSelectEvent('Fondo de Inversión', 'mInvestmentFund');
        }
      },
      {
        code: 'mExercise',
        type: CONTROL_TYPE.select,
        label: 'Ejercicio',
        placeholder: '-Seleccione-',
        list: this.tabFidecomisosService.getYears(),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-md-6 col-lg-3',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => {
          this._addSelectEvent('Ejercicio', 'mExercise');
        }
      }
    ];
  }

  formValue(formValue: any): void {
    this.searchParams = {
      documentType: INFORMATION_TYPE.memories,
      rpj:          formValue.mInvestmentFund.rpjCode,
      correlative:  formValue.mInvestmentFund.correlative,
      year:         formValue.mExercise.code,
      page:           1,
      size:           10
    };
    this.search(this.searchParams);
  }

  pagination(page: number): void {
    this.searchParams.page = page;
    this.search(this.searchParams, true);
  }

}
