import { Component, ElementRef, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';

import { GaUnsubscribeBase } from '@bvl/library';
import { GA_ISSUER_TYPE_RADIO, ISSUER_TYPE } from '../../providers';

@Component({
  selector: 'bvl-memories',
  templateUrl: './memories.component.html',
  styleUrls: ['./memories.component.scss']
})
export class MemoriesComponent extends GaUnsubscribeBase implements OnInit {

  ISSUER_TYPE = ISSUER_TYPE;

  radioControls: Array<any>;
  frmIssuerType: FormGroup;
  mIssuerType: AbstractControl;

  constructor(
    protected _element: ElementRef,
    private _formBuilder: FormBuilder
  ) {
    super(_element);
    this.ga = [GA_ISSUER_TYPE_RADIO];
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    this.radioControls = [
      {
        code: 'mIssuerType',
        description: 'Emisores',
        value: ISSUER_TYPE.issuers
      },
      {
        code: 'mIssuerType',
        description: 'Fondos de inversión',
        value: ISSUER_TYPE.investmentFund
      }
    ];
    this._createForm();
  }

  private _createForm(): void {
    this.frmIssuerType = this._formBuilder.group({
      mIssuerType: [ISSUER_TYPE.issuers]
    });
    this.mIssuerType = this.frmIssuerType.get('mIssuerType');
  }

}
