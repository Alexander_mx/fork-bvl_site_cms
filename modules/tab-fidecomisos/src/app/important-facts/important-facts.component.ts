import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Observable, Observer } from 'rxjs';

import { CONTROL_TYPE, GA_AUTOCOMPLETE, GA_SELECT, INFORMATION_TYPE, TabFidecomisosService } from '../../providers';
import { TabFidecomisosBase } from '../tab-fidecomisos.base';

@Component({
  selector: 'bvl-important-facts',
  templateUrl: './important-facts.component.html',
  styleUrls: ['./important-facts.component.scss']
})
export class ImportantFactsComponent extends TabFidecomisosBase implements OnInit {

  private _observerMinDate: Observer<any>;
  private _observerMaxDate: Observer<any>;

  constructor(
    protected _element: ElementRef,
    public tabFidecomisosService: TabFidecomisosService,
    public datePipe: DatePipe
  ) {
    super(_element, tabFidecomisosService, datePipe);
    this.searchOption = {
      informationType: INFORMATION_TYPE.importantFacts,
      issuerType: null
    };
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.tableHeader = [
      this.languageText.tables.company,
      ...this.tableHeader
    ];
    const currentDate = new Date();

    this.searcherControls = [
      {
        code: 'mCompany',
        type: CONTROL_TYPE.autocomplete,
        placeholder: this.languageText.general.searchCompany,
        list: this.tabFidecomisosService.getCompanies(),
        matchField: 'companyName',
        textField: 'companyName',
        parentClass: 'col-12 g-mb--10',
        class: '',
        selectItem: (value: any) => {
          this.addEvent(GA_AUTOCOMPLETE, { autocomplete: value.companyName });
        }
      },
      {
        code: 'mSubjectType',
        type: CONTROL_TYPE.select,
        label: this.languageText.general.subjectType,
        placeholder: `-${this.languageText.filters.all}-`,
        list: this.tabFidecomisosService.getSubjectTypes(),
        orderBy: 'codeHHII',
        valueField: 'codeHHII',
        textField: 'codeHHIIDescCodeHHII',
        parentClass: 'col-12 g-mb--10',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [],
        changeSelect: (value: any) => {
          const data = {
            label: 'Tipo de asunto',
            select: (value) ? value.codeHHIIDescCodeHHII : '-Todos-'
          };
          this.addEvent(GA_SELECT, data);
        }
      },
      {
        code: 'mFrom',
        type: CONTROL_TYPE.datepicker,
        label: this.languageText.general.since,
        format: 'dd-MM-yyyy',
        parentClass: 'col-sm-6 col-md-4 col-lg-3 g-my--15 g-my-md--0',
        value: currentDate,
        maxDate: new Observable((observer: Observer<any>) => {
          this._observerMaxDate = observer;
          this._observerMaxDate.next(currentDate);
        }),
        validators: [Validators.required],
        changeDate: (value: Date) => {
          this._observerMinDate.next(value);
        }
      },
      {
        code: 'mTo',
        type: CONTROL_TYPE.datepicker,
        label: this.languageText.general.until,
        format: 'dd-MM-yyyy',
        parentClass: 'col-sm-6 col-md-4 col-lg-3',
        value: currentDate,
        validators: [Validators.required],
        minDate: new Observable((observer: Observer<any>) => {
          this._observerMinDate = observer;
          this._observerMinDate.next(currentDate);
        }),
        changeDate: (value: Date) => {
          this._observerMaxDate.next(value);
        }
      }
    ];

    // Se lanza petición inicial
    this.formValue({ mFrom: currentDate, mTo: currentDate });
  }

  formValue(formValue: any): void {
    this.searchParams = {
      rpjCode: formValue.mCompany ? formValue.mCompany.rpjCode : '',
      subIFCode: formValue.mSubjectType ? formValue.mSubjectType.codeHHII : '',
      startDate: this.datePipe.transform(formValue.mFrom, 'yyyy-MM-dd'),
      endDate: this.datePipe.transform(formValue.mTo, 'yyyy-MM-dd'),
      search: '',
      page: 1,
      size: 10
    };

    this.search(this.searchParams);
  }

  pagination(page: number): void {
    this.searchParams.page = page;
    this.search(this.searchParams, true);
  }

}
