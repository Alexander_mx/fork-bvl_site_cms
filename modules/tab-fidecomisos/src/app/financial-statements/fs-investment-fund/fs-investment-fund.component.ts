import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Observable, Observer, of } from 'rxjs';

import { ITableFileLink } from '@bvl/library';
import {
  CONTROL_TYPE,
  GA_SELECT,
  INFORMATION_TYPE,
  ISSUER_TYPE,
  PERIOD_INVESTMENTFUND_ASSETS,
  TabFidecomisosService,
  YEAR_PERIOD_INVESTMENTFUND_ASSETS
} from 'modules/tab-fidecomisos/src/providers';
import { TabFidecomisosBase } from '../../tab-fidecomisos.base';

@Component({
  selector: 'bvl-fs-investment-fund',
  templateUrl: './fs-investment-fund.component.html',
  styleUrls: ['./fs-investment-fund.component.scss']
})
export class FsInvestmentFundComponent extends TabFidecomisosBase implements OnInit {

  private _observerExercisePeriod: Observer<Array<any>>;

  tableData: Array<ITableFileLink>;

  constructor(
    protected _element: ElementRef,
    public tabFidecomisosService: TabFidecomisosService,
    public datePipe: DatePipe
  ) {
    super(_element, tabFidecomisosService, datePipe);
    this.searchOption = {
      informationType: INFORMATION_TYPE.financialStatements,
      issuerType: ISSUER_TYPE.investmentFund
    };
  }

  private _addSelectEvent(label: string, selectValue: string): void {
    GA_SELECT.dimentions = 'fondoInversion:{{mInvestmentFund}},periodo:{{mPeriod}},ejercicio:{{mExercise}}';
    const formValue = this.searcher.getFormValue();
    const dimentions = {
      mInvestmentFund: (formValue.mInvestmentFund) ? formValue.mInvestmentFund.patrimonyDescription : '- Seleccione -',
      mPeriod: (formValue.mPeriod) ? formValue.mPeriod.description : '- Seleccione -',
      mExercise: (formValue.mExercise) ? formValue.mExercise.description : '- Seleccione -'
    };
    const data = { ...{ label, select: dimentions[selectValue] }, ...dimentions };
    this.addEvent(GA_SELECT, data);
  }

  ngOnInit(): void {
    this.searcherControls = [
      {
        code: 'mInvestmentFund',
        type: CONTROL_TYPE.select,
        label: 'Fondo de Inversión',
        placeholder: '-Seleccione-',
        list: this.tabFidecomisosService.getInvestmentFundAndAssets(ISSUER_TYPE.investmentFund),
        valueField: 'rpjCode',
        textField: 'patrimonyDescription',
        parentClass: 'col-12 g-mb--10',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => {
          this._addSelectEvent('Fondo de Inversión', 'mInvestmentFund');
        }
      },
      {
        code: 'mPeriod',
        type: CONTROL_TYPE.select,
        label: 'Periodo',
        placeholder: '- Seleccione -',
        list: of(PERIOD_INVESTMENTFUND_ASSETS),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-md-4 col-lg-3 g-mb--10 g-mb-md--0',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => {
          if (value) {
            const period = YEAR_PERIOD_INVESTMENTFUND_ASSETS.filter(fv => fv.codePeriod === value.code);
            this._observerExercisePeriod.next(period);
          }
          this._addSelectEvent('Periodo', 'mPeriod');
        }
      },
      {
        code: 'mExercise',
        type: CONTROL_TYPE.select,
        label: 'Ejercicio',
        placeholder: '- Seleccione -',
        list: this.tabFidecomisosService.getYears(),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-sm-6 col-md-4 col-lg-3',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => {
          this._addSelectEvent('Ejercicio', 'mExercise');
        }
      },
      {
        code: 'mExercisePeriod',
        type: CONTROL_TYPE.select,
        placeholder: '-Seleccione-',
        list: new Observable((observer: Observer<Array<any>>) => {
          this._observerExercisePeriod = observer;
          this._observerExercisePeriod.next([]);
        }),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-sm-6 col-md-4 col-lg-3',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => { }
      }
    ];
  }

  formValue(formValue: any): void {
    this.searchParams = {
      documentType:       INFORMATION_TYPE.financialStatements,
      rpj:                formValue.mInvestmentFund.rpjCode,
      correlative:        formValue.mInvestmentFund.correlative,
      year:               formValue.mExercise.code,
      periodAccountType:  formValue.mExercisePeriod.code,
      page:               1,
      size:               10
    };
    this.search(this.searchParams);
  }

  pagination(page: number): void {
    this.searchParams.page = page;
    this.search(this.searchParams, true);
  }

}
