import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { of } from 'rxjs';

import { ITableFileLink } from '@bvl/library';
import {
  CONTROL_TYPE,
  GA_SELECT,
  INFORMATION_TYPE,
  ISSUER_TYPE,
  PERIOD,
  TabFidecomisosService,
  TYPE,
  YEAR_PERIOD
} from 'modules/tab-fidecomisos/src/providers';
import { FilterComponent } from '../../search-filter/filter/filter.component';
import { TabFidecomisosBase } from '../../tab-fidecomisos.base';

@Component({
  selector: 'bvl-fs-issuers',
  templateUrl: './fs-issuers.component.html',
  styleUrls: ['./fs-issuers.component.scss']
})
export class FsIssuersComponent extends TabFidecomisosBase implements OnInit {

  @ViewChild(FilterComponent) filterComp: FilterComponent;

  filterControls: Array<any>;
  tableData: Array<ITableFileLink>;
  filterParams: {};

  constructor(
    protected _element: ElementRef,
    public tabFidecomisosService: TabFidecomisosService,
    public datePipe: DatePipe
  ) {
    super(_element, tabFidecomisosService, datePipe);
    this.searchOption = {
      informationType: INFORMATION_TYPE.financialStatements,
      issuerType: ISSUER_TYPE.issuers
    };
    this.filterParams = {};
  }

  private _addSelectEvent(label: string, selectValue: string): void {
    GA_SELECT.dimentions = 'tipo:{{mType}},periodo:{{mPeriod}}';
    const formValue = this.searcher.getFormValue();
    const dimentions = {
      mType: (formValue.mType) ? formValue.mType.description : '- Seleccione -',
      mPeriod: (formValue.mPeriod) ? formValue.mPeriod.description : '- Seleccione -'
    };
    const data = { ...{ label, select: dimentions[selectValue] }, ...dimentions };
    this.addEvent(GA_SELECT, data);
  }

  ngOnInit(): void {
    this.searcherControls = [
      {
        code: 'mCompany',
        type: CONTROL_TYPE.autocomplete,
        placeholder: 'Buscar empresa',
        list: this.tabFidecomisosService.getCompanies(),
        matchField: 'companyName',
        textField: 'companyName',
        parentClass: 'col-12 g-mb--10',
        class: '',
        validators: [Validators.required],
        selectItem: (value: any) => { }
      },
      {
        code: 'mType',
        type: CONTROL_TYPE.select,
        label: 'Tipo',
        placeholder: '- Seleccione -',
        list: of(TYPE),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-md-6 col-lg-3',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => {
          this._addSelectEvent('Tipo', 'mType');
        }
      },
      {
        code: 'mPeriod',
        type: CONTROL_TYPE.select,
        label: 'Periodo',
        placeholder: '- Seleccione -',
        list: of(PERIOD),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-md-6 col-lg-3',
        class: 's-form-select--inline w-100 s-form-select--default',
        validators: [Validators.required],
        changeSelect: (value: any) => {
          this._addSelectEvent('Periodo', 'mPeriod');
        }
      }
    ];

    this.filterControls = [
      {
        type: CONTROL_TYPE.label,
        label: 'Información Presentada:',
        parentClass: 'col-md-3 g-mb--15 g-mb-md--0',
        class: 'g-fw-bold g-mr--15 text-nowrap'
      },
      {
        code: 'mYear',
        type: CONTROL_TYPE.select,
        label: 'Año',
        placeholder: '- Seleccione -',
        list: this.tabFidecomisosService.getYears(),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-md-3',
        class: 's-form-select--inline w-100 s-form-select--default'
      },
      {
        code: 'mYearPeriod',
        type: CONTROL_TYPE.select,
        label: 'Periodo',
        placeholder: '- Seleccione -',
        list: of(YEAR_PERIOD),
        valueField: 'code',
        textField: 'description',
        parentClass: 'col-md-3',
        class: 's-form-select--inline w-100 s-form-select--default'
      }
    ];
  }

  formValue(formValue: any): void {
    const searchParams = {
      rpjCode:        formValue.mCompany.rpjCode,
      type:           formValue.mType.code,
      period:         formValue.mPeriod.code,
      yearPeriod:     '',
      periodAccount:  '',
      search:         '',
      page:           1,
      size:           10
    };
    this.searchParams = {...searchParams, ...this.filterParams };
    this.search(this.searchParams);
  }

  filter(formValue): void {
    this.filterParams = {
      yearPeriod: (formValue.mYear)
                    ? formValue.mYear.code
                    : '',
      periodAccount: (formValue.mYearPeriod)
                        ? formValue.mYearPeriod.code
                        : ''
    };

    if (this.searchParams) {
      this.searchParams = {...this.searchParams, ...this.filterParams };
      this.search(this.searchParams);
    }
  }

  pagination(page: number): void {
    this.searchParams.page = page;
    this.search(this.searchParams, true);
  }

  formClear(): void {
    this.filterComp.clear();
    this.clear();
  }

}
