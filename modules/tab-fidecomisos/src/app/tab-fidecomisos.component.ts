import { Component, ElementRef, HostBinding, OnInit } from '@angular/core';

import { getTimeStamp } from '@bvl-core/shared/helpers/util/general';
import { GaUnsubscribeBase } from '@bvl/library';
import { GA_BUTTON_TAB } from '../providers';

@Component({
  selector: 'bvl-tab-fidecomisos',
  templateUrl: './tab-fidecomisos.component.html',
  styleUrls: ['./tab-fidecomisos.component.scss']
})
export class TabFidecomisosComponent extends GaUnsubscribeBase implements OnInit {

  @HostBinding('attr.class') attrClass = 'w-100';

  constructor(
    protected _element: ElementRef
  ) {
    super(_element);
    this._element.nativeElement.setAttribute('keyc', getTimeStamp());
    this.ga = [GA_BUTTON_TAB];
  }

  setEventsGA(): void { }

  ngOnInit(): void { }

}
