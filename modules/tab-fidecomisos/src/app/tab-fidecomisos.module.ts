import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DatePipe } from '@angular/common';
import { createCustomElement } from '@angular/elements';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlErrorModule } from '@bvl-core/shared/helpers/control-error';
import { OrderByPipeModule } from '@bvl-core/shared/helpers/pipes';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { SPINNER_CONFIG } from '@bvl-site/settings/constants/general.constant';
import {
  ApiModule,
  BvlFormAutocompleteModule,
  BvlFormDatepickerModule,
  BvlFormRadioModule,
  BvlFormSelectModule,
  DomChangeDirectiveModule,
  PaginationModule,
  TableDownloadFileModule,
  TableFileLinkModule,
  TabsModule
} from '@bvl/library';
import { FinancialStatementsComponent } from './financial-statements/financial-statements.component';
import { FsInvestmentFundComponent } from './financial-statements/fs-investment-fund/fs-investment-fund.component';
import { FsIssuersComponent } from './financial-statements/fs-issuers/fs-issuers.component';
import { FsTrustAssetsComponent } from './financial-statements/fs-trust-assets/fs-trust-assets.component';
import { ImportantFactsComponent } from './important-facts/important-facts.component';
import { MInvestmentFundComponent } from './memories/m-investment-fund/m-investment-fund.component';
import { MIssuersComponent } from './memories/m-issuers/m-issuers.component';
import { MemoriesComponent } from './memories/memories.component';
import { FilterComponent } from './search-filter/filter/filter.component';
import { SearcherComponent } from './search-filter/searcher/searcher.component';
import { TabFidecomisosComponent } from './tab-fidecomisos.component';

@NgModule({
  declarations: [
    TabFidecomisosComponent,
    ImportantFactsComponent,
    FinancialStatementsComponent,
    MemoriesComponent,
    FilterComponent,
    FsIssuersComponent,
    FsInvestmentFundComponent,
    FsTrustAssetsComponent,
    SearcherComponent,
    MIssuersComponent,
    MInvestmentFundComponent
  ],
  imports: [
    BrowserModule,
    TabsModule,
    BvlFormAutocompleteModule,
    BvlFormSelectModule,
    PaginationModule,
    BvlFormRadioModule,
    BvlFormDatepickerModule,
    ApiModule,
    TableDownloadFileModule,
    TableFileLinkModule,
    ReactiveFormsModule,
    SpinnerModule.forRoot(SPINNER_CONFIG),
    ControlErrorModule,
    OrderByPipeModule,
    DomChangeDirectiveModule
  ],
  providers: [
    DatePipe
  ],
  entryComponents: [TabFidecomisosComponent]
})
export class TabFidecomisosModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-tab-fidecomisos'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(TabFidecomisosComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
