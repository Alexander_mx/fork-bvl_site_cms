import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ValidatorUtil } from '@bvl-core/shared/helpers/util';
import { GA_CLEAR_BUTTON, GA_SEARCH_BUTTON } from 'modules/tab-fidecomisos/src/providers';
import { SearchFilterBase } from '../search-filter.base';

@Component({
  selector: 'bvl-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.scss']
})
export class SearcherComponent extends SearchFilterBase implements OnInit {

  @Input() searcherControls: Array<any>;
  @Output() formValue: EventEmitter<any>;
  @Output() formClear: EventEmitter<any>;

  form: FormGroup;

  constructor(
    protected _element: ElementRef,
    public formBuilder: FormBuilder
  ) {
    super(_element, formBuilder);
    this.searcherControls = [];
    this.formValue = new EventEmitter<any>();
    this.formClear = new EventEmitter<any>();
    this.ga = [GA_CLEAR_BUTTON, GA_SEARCH_BUTTON];
  }

  ngOnInit(): void {
    this.form = this.createForm(this.searcherControls);
  }

  clear(): void {
    this.form.reset();
    this.formClear.emit(true);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.form);

    return this.form.valid;
  }

  search(): void {
    const valid = this._validateForm();
    if (valid) {
      const formValue = this.form.getRawValue();
      this.formValue.emit(formValue);
    }
  }

  getFormValue(): any {
    return this.form.getRawValue();
  }

}
