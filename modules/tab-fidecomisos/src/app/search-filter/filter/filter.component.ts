import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { SearchFilterBase } from '../search-filter.base';

@Component({
  selector: 'bvl-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterComponent extends SearchFilterBase implements OnInit {

  @Input() filterControls: Array<any>;
  @Output() formValue: EventEmitter<any>;
  @ViewChild('frmToolbarFilter') frmToolbarFilter: HTMLFormElement;

  form: FormGroup;

  constructor(
    protected _element: ElementRef,
    public formBuilder: FormBuilder
  ) {
    super(_element, formBuilder);
    this.filterControls = [];
    this.formValue = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.form = this.createForm(this.filterControls);
  }

  changeSelect(): void {
    if (this.form.valid) {
      const formValue = this.form.getRawValue();
      this.formValue.emit(formValue);
    }
  }

  clear(): void {
    this.frmToolbarFilter.nativeElement.reset();
  }

}
