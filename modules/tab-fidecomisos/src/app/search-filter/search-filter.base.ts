import { ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { GaUnsubscribeBase } from '@bvl/library';
import { CONTROL_TYPE } from '../../providers';

export abstract class SearchFilterBase extends GaUnsubscribeBase {

  CONTROL_TYPE = CONTROL_TYPE;

  constructor(
    protected _element: ElementRef,
    public formBuilder: FormBuilder
  ) {
    super(_element);
  }

  createForm(controls: Array<any>): FormGroup {
    const form = this.formBuilder.group({});
    controls
      .filter(control => control.code)
      .forEach(control => {
        const validators = control.validators || [];
        form.addControl(control.code, this.formBuilder.control(control.value || null, validators));
    });

    return form;
  }

}
