import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { ImgSrcsetModule, ParagraphModule, SectionHeaderModule } from '@bvl/library';
import { PopupVideoComponent } from './popup-video.component';

@NgModule({
  imports: [
    BrowserModule,
    ImgSrcsetModule,
    ParagraphModule,
    SectionHeaderModule
  ],
  declarations: [PopupVideoComponent],
  entryComponents: [PopupVideoComponent],
  providers: []
})
export class PopupVideoModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-popup-video'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(PopupVideoComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
