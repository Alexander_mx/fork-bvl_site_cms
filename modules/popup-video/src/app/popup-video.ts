import { IBackgroundImage, IBackgroundVideo } from '@bvl/library';

export interface IPopupSlider {
  backgroundVideo: Array<IBackgroundVideo>;
  backgroundImage: Array<IBackgroundImage>;
}
