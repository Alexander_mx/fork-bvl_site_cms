import { Component, Input } from '@angular/core';

import { camelcaseKeys, coerceBooleanProp } from '@bvl/library';
import { IPopupSlider } from './popup-video';

@Component({
  selector: 'bvl-popup-video',
  templateUrl: './popup-video.component.html',
  styleUrls: ['./popup-video.component.scss']
})
export class PopupVideoComponent {
  @Input() titleAlign: string;
  @Input() description: string;
  @Input() textLink: string;
  @Input() title: string;
  @Input() urlLink: string;

  private _isLink: boolean;
  @Input()
  get isLink(): boolean {
    return this._isLink;
  }
  set isLink(value: boolean) {
    this._isLink = coerceBooleanProp(value);
  }

  private _sliders: Array<IPopupSlider>;
  @Input()
  get sliders(): Array<IPopupSlider> {
    return this._sliders || [];
  }
  set sliders(value: Array<IPopupSlider>) {
    this._sliders = (JSON.parse(value as any) || []).map(camelcaseKeys.bind(this));
  }

}
