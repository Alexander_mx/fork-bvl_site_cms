import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { PopupVideoModule } from './app/popup-video.module';

platformBrowserDynamic()
  .bootstrapModule(PopupVideoModule)
  .catch(err => console.error(err));
