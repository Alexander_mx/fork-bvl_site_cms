import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { BannerFeaturedModule } from './app/banner-featured.module';

platformBrowserDynamic().bootstrapModule(BannerFeaturedModule)
  .catch(err => console.error(err));
