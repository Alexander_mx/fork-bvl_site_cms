import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { coerceBooleanProp, IListImage } from '@bvl/library';

@Component({
  selector: 'bvl-banner-featured',
  templateUrl: './banner-featured.component.html',
  styleUrls: ['./banner-featured.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BannerFeaturedComponent implements OnInit {
  private _backgroundImage: Array<IListImage>;
  @Input()
  get backgroundImage(): any {
    return this._backgroundImage;
  }
  set backgroundImage(value: any) {
    this._backgroundImage = JSON.parse(value) as Array<IListImage>;
  }
  @Input() title: string;
  @Input() text: string;

  private _isLink: boolean;
  @Input()
  get isLink(): boolean {
    return this._isLink;
  }
  set isLink(value: boolean) {
    this._isLink = coerceBooleanProp(value);
  }
  @Input() urlLink: string;
  @Input() textLink: string;

  image: string;

  ngOnInit(): void {
    if (Array.isArray(this.backgroundImage) && this.backgroundImage.length) {
      this.image = this.backgroundImage[0].large.imageUri;
    }
  }
}
