import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { BannerFeaturedComponent } from './banner-featured.component';

@NgModule({
  imports: [CommonModule, BrowserModule],
  declarations: [BannerFeaturedComponent],
  exports: [BannerFeaturedComponent],
  entryComponents: [BannerFeaturedComponent]
})
export class BannerFeaturedModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-banner-featured'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BannerFeaturedComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
