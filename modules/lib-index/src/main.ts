import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { LibIndexModule } from './app/lib-index.module';
platformBrowserDynamic().bootstrapModule(LibIndexModule)
  .catch(err => console.error(err));
