import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import {
  ButtonComponent,
  ButtonModule,
  ImageComponent,
  ImageModule,
  MenuComponent,
  MenuModule,
  ParagraphComponent,
  ParagraphModule,
  SectionHeaderComponent,
  SectionHeaderModule
} from '../../../../core/lib/src';

@NgModule({
  imports: [
    BrowserModule,
    SectionHeaderModule,
    MenuModule,
    ParagraphModule,
    ButtonModule,
    ImageModule
  ]
})
export class LibIndexModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const titleName = 'bvl-title';
    const titleElement = createCustomElement(SectionHeaderComponent, {
      injector: this.injector
    });
    customElements.define(titleName, titleElement);

    const menuName = 'bvl-menu';
    const menuElement = createCustomElement(MenuComponent, {
      injector: this.injector
    });
    customElements.define(menuName, menuElement);

    const paragraphName = 'bvl-paragraph';
    const paragraphElement = createCustomElement(ParagraphComponent, {
      injector: this.injector
    });
    customElements.define(paragraphName, paragraphElement);

    const buttonName = 'bvl-button';
    const buttonElement = createCustomElement(ButtonComponent, {
      injector: this.injector
    });
    customElements.define(buttonName, buttonElement);

    const imageName = 'bvl-image';
    const imageElement = createCustomElement(ImageComponent, {
      injector: this.injector
    });
    customElements.define(imageName, imageElement);
  }
}
