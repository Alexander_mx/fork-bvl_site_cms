import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BannerFullModule } from './app/banner-full.module';

platformBrowserDynamic().bootstrapModule(BannerFullModule)
  .catch(err => console.error(err));
