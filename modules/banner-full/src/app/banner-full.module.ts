import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { ImgSrcsetModule } from '@bvl/library';
import { BannerFullComponent } from './banner-full.component';

@NgModule({
  imports: [CommonModule, BrowserModule, ImgSrcsetModule],
  declarations: [BannerFullComponent],
  entryComponents: [BannerFullComponent]
})
export class BannerFullModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-banner-full'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BannerFullComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
