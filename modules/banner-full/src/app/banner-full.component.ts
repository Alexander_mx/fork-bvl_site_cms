import { AfterViewInit, Component, Input, ViewEncapsulation } from '@angular/core';

import { IListImage } from '@bvl/library';

@Component({
  selector: 'bvl-banner-full',
  templateUrl: './banner-full.component.html',
  styleUrls: ['./banner-full.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BannerFullComponent implements AfterViewInit {

  @Input() title: string;

  private _showVideo: any;

  get showVideo(): any {
    return this._showVideo;
  }
  set showVideo(value: any) {
    this._showVideo = value;
    document.querySelector('body').style.overflow = this._showVideo
      ? 'hidden'
      : 'initial';
  }

  private _size: boolean;
  @Input()
  get size(): any {
    return this._size || 'large';
  }
  set size(value: any) {
    this._size = value;
  }

  private _sliders: Array<IListImage>;
  @Input()
  get sliders(): any {
    return this._sliders;
  }
  set sliders(value: any) {
    this._sliders = JSON.parse(value)[0] as Array<IListImage>;
  }

  ngAfterViewInit(): void {
    this.runVideo();
    this._listendKeyPress();
  }

  runVideo(): void {
    const video = document.querySelector('video');
    if (video && video.paused) {
      setTimeout(() => { video.play(); }, 1800);
    }
  }

  private _listendKeyPress(): void {
    window.addEventListener('keydown', event => {
      if (event.key === 'Escape') {
        this.showVideo = false;
      }
    }, false);
  }
}
