import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { RealTimeModule } from './app/real-time.module';

platformBrowserDynamic().bootstrapModule(RealTimeModule)
  .catch(err => console.error(err));
