import { Component, ElementRef, NgZone, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

import { AngularUtil, getStorageLanguage, ILink, LANGUAGES } from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { FooterService } from 'core/providers/footer.service';
import { IRealTimeStock } from './real-time';

@Component({
  selector: 'bvl-real-time',
  templateUrl: './real-time.component.html',
  styleUrls: ['./real-time.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RealTimeComponent implements OnInit, OnDestroy {
  _mediaSubscription: Subscription;
  stocks: Array<IRealTimeStock> = [];
  footerLink: ILink;

  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)
  ];

  constructor(
    private _media: ObservableMedia,
    private _ngZone: NgZone,
    private _elRef: ElementRef,
    private _footerService: FooterService,
    private _tableServices: TableServices
  ) {}

  ngOnInit(): void {
    this._getStocks();
    this._mediaSubscription = this._media.subscribe(() => {
      const ngZoneSuscription = this._ngZone.onStable.subscribe(() => {
        if (this._media.isActive('gt-md')) {
          this._footerService.addMarginBottom('realTime', this._elRef.nativeElement.clientHeight);
        } else {
          this._footerService.removeMarginBottom('realTime');
        }
        ngZoneSuscription.unsubscribe();
      });
    });
  }

  ngOnDestroy(): void {
    this._mediaSubscription.unsubscribe();
  }

  private _getStocks(): void {
    this._tableServices.getTop({today: true, top: 5})
    .subscribe(response => {
      response.gain.map((item, index) => {
        this.stocks.push({
          name: item.nemon,
          value: AngularUtil.formatNumberToString(item.last, 3),
          variation: item.percentageChange
        });
        if (response.loser[index]) {
          this.stocks.push({
            name: response.loser[index] && response.loser[index].nemon,
            value: response.loser[index] && AngularUtil.formatNumberToString(response.loser[index].last, 3),
            variation: response.loser[index] && response.loser[index].percentageChange
          });
        }
      });
    });
  }
}
