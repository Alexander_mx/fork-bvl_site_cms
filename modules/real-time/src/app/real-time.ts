export interface IRealTimeStock {
  name: string;
  value: number | string;
  variation: number;
}
