import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { AmountCellModule } from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { FooterService } from 'core/providers/footer.service';
import { RealTimeComponent } from './real-time.component';

@NgModule({
  declarations: [
    RealTimeComponent
  ],
  imports: [
    BrowserModule,
    AmountCellModule,
    FlexLayoutModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule
  ],
  providers: [
    FooterService,
    TableServices
  ],
  exports: [RealTimeComponent],
  entryComponents: [RealTimeComponent]
})
export class RealTimeModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-real-time'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(RealTimeComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
