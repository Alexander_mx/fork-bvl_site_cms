import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { AgentsEndPoints } from './endpoints';

@Injectable()
export class AgentsServices {

   constructor(private _apiService: ApiService) {}

   getBrokers(sabCode?: string): Observable<any> {
      return this._apiService.get(`${AgentsEndPoints.broker}${sabCode ? `/${sabCode}` : ''}`);
   }
   getNearBrokes(params: {lattitude: number; longitude: number}): Observable<any> {
      return this._apiService.post(AgentsEndPoints.near, params);
   }
}
