import { environment } from '../environments/environment';

export class AgentsEndPoints {
  public static broker = `${environment.API_URL}/static/broker`;
  public static near = `${environment.API_URL}/static/broker/near`;
}
