import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AgentsListModule } from './app/agents-list.module';

platformBrowserDynamic().bootstrapModule(AgentsListModule)
  .catch(err => console.error(err));
