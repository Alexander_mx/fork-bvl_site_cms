import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  AccordionModule,
  ApiModule,
  DomChangeDirectiveModule,
  LinksListModule,
  SectionHeaderModule,
  ToolbarModule
} from '@bvl/library';
import { AgentsServices } from '../providers/agents.services';
import { AgentsListComponent } from './agents-list.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    AccordionModule,
    SectionHeaderModule,
    LinksListModule,
    FlexLayoutModule,
    ApiModule,
    ToolbarModule,
    DomChangeDirectiveModule
  ],
  entryComponents: [AgentsListComponent],
  providers: [AgentsServices],
  declarations: [AgentsListComponent]
})
export class AgentsListModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-agents-list'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(AgentsListComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
