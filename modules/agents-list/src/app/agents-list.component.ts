import { Component, ElementRef, NgZone, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';

import { addGoogleScript } from '@bvl-core/shared/helpers/util/google-maps';

import {
  AccordionComponent,
  GaUnsubscribeBase,
  getStorageLanguage,
  IAgentDetails,
  LANGUAGES,
  STORAGE_TYPE,
  StorageService,
  StringUtil
} from '@bvl/library';
import { AgentsServices } from '../providers/agents.services';
declare var google;

@Component({
  selector: 'bvl-agents-list',
  templateUrl: './agents-list.component.html',
  styleUrls: ['./agents-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AgentsListComponent extends GaUnsubscribeBase implements OnInit {

  @ViewChildren(AccordionComponent) allAccordions: QueryList<AccordionComponent>;
  @ViewChild(AccordionComponent) accordion: AccordionComponent;

  agents: IAgentDetails | any;
  fullData: IAgentDetails | any;
  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  constructor(
    protected _element: ElementRef,
    private _ngZone: NgZone,
    private _agentsServices: AgentsServices,
    private _storageService: StorageService
  ) {
    super(_element);
    this._storageService.setConfig({ storageType: STORAGE_TYPE.local });
    const keys = this._storageService.getStorage('keys_site');
    addGoogleScript(keys && keys.googleMaps);
  }

  ngOnInit(): void {
    this._getAgents();
  }
  onFilterChange(filter: any): void {
    if (filter.type === 'sortByLocation') {
      this._getNearAgents({
        lattitude: filter.value.latitude,
        longitude: filter.value.longitude
      });
    }
    if (filter.type === 'search') {
      const findedData = [];
      this.fullData.forEach(item => {
        const name = item.name.toLowerCase();
        if (name.indexOf(filter.value.toLowerCase()) !== -1) {
          findedData.push(item);
        }
      });
      this.agents = findedData;
    }
    if (Object.keys(filter).length === 0) {
      this.agents = this.fullData;
    }
  }
  openElement(agent: IAgentDetails, index: number, state: string): void {
    (this.allAccordions as any)._results.forEach(accordion => {
      if (accordion.id !== index) {
        accordion.expanded = false;
      }
    });

    if (state !== 'expanded') {
      return void 0;
    }

    this._addMap(agent.latitude, agent.longitude, agent.code);
  }

  getValueType(value): string {
    return typeof value;
  }

  private _addMap(lat, lng, code): void {
    setTimeout(() => {
      const coord = { lat, lng };
      const map = new google.maps.Map(document.getElementById(`map-${code}`), {
        zoom: 16,
        center: coord,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      // tslint:disable-next-line: no-unused
      const marker = new google.maps.Marker({
        position: coord,
        map
      });
    });
  }

  private _getAgents(): void {
    this._agentsServices.getBrokers()
      .subscribe(data => {
        this._ngZone.run(() => {
          this.agents = this._setAgents(data);
          this.fullData = this.agents;
        });
      });
  }

  private _setAgents(data: any): Array<IAgentDetails> {
    return data.map((item, index) => {
      return {
        id: index,
        name: item.companyName,
        ruc: item.ruc,
        stock: item.capital,
        phones: [item.phoneNumber],
        email: item.mail,
        website: item.webSite,
        generalManager: item.generalManager,
        links: [
          {
            title: this.TEXT.general.basicInformation,
            url: `${location.pathname}/detalle?agentCode=${item.sabCode}`
          },
          {
            title: this.TEXT.general.authorization,
            url: `${location.pathname}/detalle?agentCode=${item.sabCode}#${this._setAnchor(this.TEXT.general.authorization)}`
          },
          {
            title: this.TEXT.general.representatives,
            url: `${location.pathname}/detalle?agentCode=${item.sabCode}#${this._setAnchor(this.TEXT.general.representatives)}`
          },
          {
            title: this.TEXT.general.offices,
            url: `${location.pathname}/detalle?agentCode=${item.sabCode}#${this._setAnchor(this.TEXT.general.offices)}`
          },
          {
            title: this.TEXT.general.clientPoliticsMemories,
            url: `${location.pathname}/detalle?agentCode=${item.sabCode}#${this._setAnchor(this.TEXT.general.clientPoliticsMemories)}`
          },
          {
            title: this.TEXT.general.clientSantions,
            url: `${location.pathname}/detalle?agentCode=${item.sabCode}#${this._setAnchor(this.TEXT.general.clientSantions)}`
          }
        ],
        latitude: item.latitude,
        longitude: item.longitude,
        code: item.sabCode
      };
    });
  }

  private _getNearAgents(coords: { lattitude: number; longitude: number }): void {
    this._agentsServices.getNearBrokes(coords)
      .subscribe(data => {
        this.agents = this._setAgents(data);
      });
  }

  private _setAnchor(value): string {
    return StringUtil.slugify(StringUtil.withoutDiacritics(value));
  }
}
