import { Component, Input, OnInit } from '@angular/core';

import { getStorageLanguage, LANGUAGES } from '@bvl/library';
import { IOffice } from './office';

@Component({
  selector: 'bvl-offices',
  templateUrl: './offices.component.html',
  styleUrls: ['./offices.component.scss']
})
export class OfficesComponent implements OnInit {
  @Input() offices: Array<IOffice> | any;
  expandedOffices: Array<number> = [];
  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)
  ];

  ngOnInit(): void {
    this.offices = JSON.parse(this.offices);
  }

  onToggleOffice(state, officeIdx): void {
    switch (state) {
      case 'expanded':
        this.expandedOffices = [...this.expandedOffices.filter(idx => idx !== officeIdx), officeIdx];
        break;
      case 'collapsed':
        this.expandedOffices = this.expandedOffices.filter(idx => idx !== officeIdx);
        break;
      default:
        return;
    }
  }

  getOfficeState(officeIdx): boolean {
    return this.expandedOffices.indexOf(officeIdx) > -1;
  }
}
