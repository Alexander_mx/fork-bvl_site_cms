import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from '@bvl/library';
import { OfficesComponent } from './offices.component';

@NgModule({
  declarations: [
    OfficesComponent
  ],
  imports: [
    BrowserModule,
    AccordionModule,
    FlexLayoutModule,
    BrowserAnimationsModule
  ],
  providers: [],
  entryComponents: [OfficesComponent]
})
export class AppModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-offices'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(OfficesComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
