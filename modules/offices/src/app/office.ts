export interface IOffice {
  address: string;
  phone: string;
  salesmen: Array<string>;
}
