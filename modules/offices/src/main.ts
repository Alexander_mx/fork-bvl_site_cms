import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/offices.module';
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
