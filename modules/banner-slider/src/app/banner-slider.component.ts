import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { keys } from 'lodash-es';
import { take } from 'rxjs/operators';

import { addScriptTag } from '@bvl-core/shared/helpers/util/web-components';
import { camelcaseKeys, CommonComponentBase, getStorageLanguage, IBanner, IPopupResponse, ModalService } from '@bvl/library';
import { environment } from '../environments/environment';
import { BannerSliderService } from '../providers/banner-slider.service';

@Component({
  selector: 'bvl-banner-slider',
  templateUrl: './banner-slider.component.html',
  styleUrls: ['./banner-slider.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BannerSliderComponent extends CommonComponentBase implements OnInit {

  private _interval: number;
  @Input()
  get interval(): any {
    return this._interval;
  }
  set interval(value: any) {
    this._interval = JSON.parse(value);
  }

  private _banners: Array<IBanner>;
  @Input()
  get banners(): any {
    return this._banners || [];
  }
  set banners(value: any) {
    this._banners = (JSON.parse(value) || []).map((item, index) => {
      item.active = index === 0;

      return camelcaseKeys(item);
    }) as Array<IBanner>;
  }

  constructor(
    protected _elementRef: ElementRef,
    @Inject(DOCUMENT) private _document: Document,
    private _bannerSliderService: BannerSliderService,
    private _modalService: ModalService
  ) {
    super(_elementRef);
  }

  ngOnInit(): void { }

  showItem(position): void {
    this.banners.map((item, index) => {
      item.active = position === index;
    });
  }

  loadPopUp(banner: IBanner): void {
    if (!banner.isPopup || !banner.popup) {
      return void 0;
    }

    this._bannerSliderService
      .getCommonComponent(banner.popup, true)
      .pipe(take(1))
      .subscribe(this._openPopUp.bind(this));
  }

  private _openPopUp(res: IPopupResponse): void {
    if (!res || (res && !res.languages)) {
      return void 0;
    }

    const currentLanguage: string = getStorageLanguage()
      .substring(0, 2);
    const elementData = res.languages.find(item => item.language === currentLanguage);

    if (!elementData) {
      return void 0;
    }

    addScriptTag([res.code.replace('bvl-', '')], environment.WC_BUNDLE_PATH);

    const formatData = this.getComponentValuePropertiesCode(elementData, ['background-image'], false);

    const element = this._document.createElement(res.code);
    keys(formatData)
      .forEach(key => {
        const value = formatData[key];
        const formatValue = typeof value === 'string' ? value : JSON.stringify(value);

        if (!formatValue) {
          return void 0;
        }

        element.setAttribute(key, formatValue);
      });

    this._modalService.open(element, { showFooter: false });
  }
}
