import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import {
  ApiModule,
  BackgroundImgUrlModule,
  DomChangeDirectiveModule,
  MODAL_CONFIG,
  ModalModule,
  SectionHeaderModule,
  SliderModule
} from '@bvl/library';
import { BannerSliderComponent } from './banner-slider.component';

@NgModule({
  declarations: [BannerSliderComponent],
  imports: [
    ApiModule,
    BrowserModule,
    CommonModule,
    ModalModule.forRoot(MODAL_CONFIG),
    SectionHeaderModule,
    BackgroundImgUrlModule,
    SliderModule,
    DomChangeDirectiveModule
  ],
  entryComponents: [BannerSliderComponent]
})
export class BannerSliderModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-banner-slider'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BannerSliderComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
