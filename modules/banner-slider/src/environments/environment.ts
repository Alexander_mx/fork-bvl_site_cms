import { environment_client } from '@bvl-site/environments/environment';

export const environment = {
  API_URL: environment_client.API_URL,
  production: environment_client.production,
  WC_BUNDLE_PATH: environment_client.WC_BUNDLE_PATH
};
