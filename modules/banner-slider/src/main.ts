import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BannerSliderModule } from './app/banner-slider.module';

platformBrowserDynamic().bootstrapModule(BannerSliderModule)
  .catch(err => console.error(err));
