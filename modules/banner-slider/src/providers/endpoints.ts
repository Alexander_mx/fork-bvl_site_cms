import { environment } from '../environments/environment';

export class BannerSliderEndPoints {
  public static valueCommon = `${environment.API_URL}/trays/component/valueCommon/{id}`;
}
