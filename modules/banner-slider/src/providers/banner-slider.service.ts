import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { BannerSliderEndPoints } from './endpoints';

@Injectable({
  providedIn: 'root'
})
export class BannerSliderService {
  constructor(private _apiService: ApiService) { }

  getCommonComponent(id: string, preloader?: boolean): Observable<any> {
    return this._apiService.get(`${BannerSliderEndPoints.valueCommon}`, { params: { id }, preloader });
  }
}
