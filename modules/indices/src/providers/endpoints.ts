import { environment } from '../environments/environment';

export class IndicesEndPoints {
   public static today = `${ environment.API_URL }/static/indexToday`;
   public static details = `${ environment.API_URL }/site/getIndex`;
}
