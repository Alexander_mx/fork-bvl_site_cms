import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { IndicesEndPoints } from './endpoints';

@Injectable()
export class IndicesServices {

  constructor(
    private _apiService: ApiService
  ) { }

  getIndexToday(): Observable<any> {
    return this._apiService.get(IndicesEndPoints.today);
  }
  getIndexDetails(id: string): Observable<any> {
    return this._apiService.get(`${IndicesEndPoints.details}/${id}`);
  }
}
