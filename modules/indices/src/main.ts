import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { IndicesModule } from './app/indices.module';

platformBrowserDynamic().bootstrapModule(IndicesModule)
  .catch(err => console.error(err));
