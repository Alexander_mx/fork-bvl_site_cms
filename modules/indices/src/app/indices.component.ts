import {
  Component,
  ElementRef,
  HostBinding,
  Input,
  NgZone,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { getPath } from '@bvl-core/shared/helpers';
import { STATIC_PAGES } from '@bvl-site/settings/constants/general.constant';
import { DateUtil, GaUnsubscribeBase, getStorageLanguage, LANGUAGES } from '@bvl/library';
import { IStockIndex } from '../components/stock-index';
import { IndicesServices } from '../providers/indices.services';

@Component({
  selector: 'bvl-indices',
  templateUrl: './indices.component.html',
  styleUrls: ['./indices.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IndicesComponent extends GaUnsubscribeBase implements OnInit {
  indices: Array<IStockIndex>;
  carouselActivated = true;
  isInCoumn = false;
  mediaWatcher: Subscription;

  STATIC_PAGES = STATIC_PAGES;

  ORDER_REGEX = [/S&P\/BVL PERU SE/g, /S&P\/BVL PERU GEN/g, /S&P\/BVL IBGC/g];

  private _isVertical: boolean;
  @Input()
  get isVertical(): any {
    return this._isVertical;
  }
  set isVertical(value: any) {
    this._isVertical = JSON.parse(value) as boolean;
  }
  @HostBinding('class.in-column') inColumnHostClass;

  TEXT = LANGUAGES[getStorageLanguage()];

  language = getStorageLanguage();

  constructor(
    protected _element: ElementRef,
    private _indicesServices: IndicesServices,
    private _ngZone: NgZone
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this.inColumnHostClass = this.isVertical;
    this._getIndex();
  }

  getUrlMarketIndex(): string {
    return getPath(this.STATIC_PAGES[this.language] && this.STATIC_PAGES[this.language]['marketIndex'] || '');
  }

  private _getIndex(): void {
    this._indicesServices.getIndexToday()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<any>) => {
        if (response.length > 0) {
          this._ngZone.run(() => {
            this.indices = response
              .sort((a, b) => this._getSortRank(a) - this._getSortRank(b))
              .map(item => {
                return {
                  name: item.shortName,
                  variation: item.variation,
                  value: +item.value,
                  url: '#',
                  quotesHistory: item.dailyValues.map(el => [DateUtil.getDateTimeStringFormat(el[0]), el[1]])
                };
              });
          });
        }
      });
  }

  private _getSortRank(unit: any): number {
    for (let i = 0; i < this.ORDER_REGEX.length; i++) {
      if (this.ORDER_REGEX[i].test(unit.shortName)) {
        return i;
      }
    }

    return this.ORDER_REGEX.length;
  }

}
