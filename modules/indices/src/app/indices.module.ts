import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { DomChangeDirectiveModule } from '@bvl/library';
import { StockIndexModule } from '../components/stock-index';
import { IndicesServices } from '../providers/indices.services';
import { IndicesComponent } from './indices.component';

@NgModule({
  declarations: [
    IndicesComponent
  ],
  imports: [
    BrowserModule,
    StockIndexModule,
    FlexLayoutModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    DomChangeDirectiveModule
  ],
  providers: [
    IndicesServices
  ],
  entryComponents: [IndicesComponent]
})
export class IndicesModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap(): void {
    const name = 'bvl-indices'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(IndicesComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
