import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AmountCellModule } from '@bvl/library';
import { StockIndexComponent } from './stock-index.component';

@NgModule({
  imports: [
    CommonModule,
    AmountCellModule
  ],
  declarations: [StockIndexComponent],
  exports: [StockIndexComponent]
})
export class StockIndexModule { }
