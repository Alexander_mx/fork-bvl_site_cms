import {
  AfterViewInit,
  Component,
  Input,
  ViewEncapsulation
} from '@angular/core';
import * as Highcharts from 'highcharts';

import { CHART_OPTIONS_LANG, getStorageLanguage, LANGUAGES } from '@bvl/library';
import { IStockIndex } from './stock-index';

@Component({
  selector: 'bvl-stock-index',
  templateUrl: './stock-index.component.html',
  styleUrls: ['./stock-index.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StockIndexComponent implements AfterViewInit {
  private _size: 'sm' | 'md';
  @Input() index: IStockIndex;
  @Input()
  get size(): 'sm' | 'md' | null {
    return this._size;
  }
  set size(size: 'sm' | 'md' | null) {
    this._size = size || 'md';
  }

  TEXT = LANGUAGES[getStorageLanguage()];

  constructor() {}

  ngAfterViewInit(): void {
    this._loadGraphics();
  }
  private _loadGraphics(): void {
    const lang = CHART_OPTIONS_LANG;
    if (typeof this.index.quotesHistory === 'object' && this.index.variation) {
      Highcharts.chart(`${this.index.name}-graphic`, {
        chart: {
          height: 100,
          width: 180
        },
        lang,
        title: {
          text: ''
        },
        xAxis: {
          type: 'category',
          labels: {
            enabled: false
          },
          visible: false
        },
        yAxis: {
          gridLineColor: '#fff',
          title: {
            text: ''
          },
          labels: {
            enabled: false
          }
        },
        legend: {
          enabled: false
        },
        tooltip: {
          headerFormat: `
            <span>${this.TEXT.general.date}: </span>
            <strong class="tooltip-header">{point.key}</strong><br>
          `,
          pointFormat: `
            <span>${this.TEXT.general.index}: </span>
            <b>{point.y}</b>
          `,
          borderRadius: 10,
          borderWidth: 0,
          backgroundColor: '#353535',
          style: { color: '#dcdcdc' }
        },
        credits: { enabled: false },
        plotOptions: {
          states: {
            hover: {
              enabled: false
            }
          },
          area: {
            fillColor: {
              linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 0.8
              },
              stops: this._setColors(this.index.variation).areaColor
            },
            lineWidth: 1,
            lineColor: this._setColors(this.index.variation).lineColor,
            threshold: null
          },
          series: {
            cursor: { enabled: false },
            turboThreshold: 3500
          }
        },
        series: [
          {
            type: 'area',
            data: this.index.quotesHistory.map(item => {
              return {
                name: item[0].indexOf('T') !== -1 ? item[0].replace('T', ' ') : item[0],
                y: +item[1]
              };
            })
          }
        ]
      } as any);
    }
  }
  private _setColors = variation => {
    if (variation > 0) {
      return {
        lineColor: '#6bbd45',
        areaColor: [
          [0, Highcharts.getOptions().colors[2]],
          [
            1,
            (Highcharts.Color(Highcharts.getOptions().colors[2]) as any)
              .setOpacity(0)
              .get('rgba')
          ]
        ]
      };
    } else if (variation < 0) {
      return {
        lineColor: '#eb5a5a',
        areaColor: [
          [0, Highcharts.getOptions().colors[5]],
          [
            1,
            (Highcharts.Color(Highcharts.getOptions().colors[5]) as any)
              .setOpacity(0)
              .get('rgba')
          ]
        ]
      };
    } else {
      return {
        lineColor: '#34bdb2',
        areaColor: [
          [0, Highcharts.getOptions().colors[0]],
          [
            1,
            (Highcharts.Color(Highcharts.getOptions().colors[0]) as any)
              .setOpacity(0)
              .get('rgba')
          ]
        ]
      };
    }
  }
}
