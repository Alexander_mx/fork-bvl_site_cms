import { IFile } from '@bvl/library';

export interface IStockIndex {
  name: string;
  variation: number;
  value: number;
  url: string;
  quotesHistory: any;
}

export interface IDetalleIndice {
  title: string;
  content: Array<{
    id: string;
    title: string;
    description: string;
    files?: Array<IFile>;
  }>;
}
