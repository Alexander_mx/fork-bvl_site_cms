import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { IndexDetailsModule } from './app/index-details.module';

platformBrowserDynamic().bootstrapModule(IndexDetailsModule)
  .catch(err => console.error(err));
