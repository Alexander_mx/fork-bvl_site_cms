import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';

import { getPathArgument } from '@bvl-core/shared/helpers/util/path-arguments';
import { IDetalleIndice } from 'modules/indices/src/components/stock-index';
import { IndicesServices } from 'modules/indices/src/providers/indices.services';

@Component({
  selector: 'bvl-index-details',
  templateUrl: './index-details.component.html',
  styleUrls: ['./index-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IndexDetailsComponent implements OnDestroy {
  private _indexSubscription: Subscription;
  indice: IDetalleIndice;

  constructor(public indicesService: IndicesServices) {
    this._getIndexDetails();
  }
  ngOnDestroy(): void {
    if (this._indexSubscription) { this._indexSubscription.unsubscribe(); }
  }
  private _getIndexDetails(): void {
    this._indexSubscription = this.indicesService.getIndexDetails(getPathArgument('indexCode'))
    .subscribe(response => {
      this.indice = this._setIndexData(response);
    });
  }
  private _setIndexData(data: any): IDetalleIndice {
    return {
      title: data[0].name,
      content: data[0].content.map((item, index) => {
        return {
          id: `${data[0].name}-${index}`,
          title: item.title,
          description: item.description,
          files: (() => {
            if (item.files.length > 0) {
              return item.files.map(file => {
                return {
                  title: file.title,
                  url: file.url,
                  date: file.localDateTime
                };
              });
            }

            return [];
          })()
        };
      })
    };
  }
}
