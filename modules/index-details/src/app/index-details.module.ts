import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { MenuModule, SectionHeaderModule } from '@bvl/library';
import { DetailsChartModule } from 'modules/details-chart/src/app/details-chart.module';
import { IndicesServices } from 'modules/indices/src/providers/indices.services';
import { ItemsGridModule } from 'modules/items-grid/src/app/items-grid.module';
import { IndexDetailsComponent } from './index-details.component';

@NgModule({
  declarations: [
    IndexDetailsComponent
  ],
  imports: [
    BrowserModule,
    MenuModule,
    SectionHeaderModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule,
    ItemsGridModule,
    DetailsChartModule
  ],
  providers: [
    IndicesServices
  ],
  entryComponents: [IndexDetailsComponent]
})
export class IndexDetailsModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap(): void {
    const name = 'bvl-index-details'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(IndexDetailsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
