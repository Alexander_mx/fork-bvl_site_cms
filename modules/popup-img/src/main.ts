import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { PopupImgModule } from './app/popup-img.module';

platformBrowserDynamic()
  .bootstrapModule(PopupImgModule)
  .catch(err => console.error(err));
