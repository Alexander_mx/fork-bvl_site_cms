import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { CarouselModule, ImageGalleryModule, ParagraphModule, SectionHeaderModule } from '@bvl/library';
import { PopupImgComponent } from './popup-img.component';

@NgModule({
  imports: [
    BrowserModule,
    CarouselModule,
    ImageGalleryModule,
    ParagraphModule,
    SectionHeaderModule
  ],
  declarations: [PopupImgComponent],
  entryComponents: [PopupImgComponent],
  providers: []
})
export class PopupImgModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-popup-img'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(PopupImgComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
