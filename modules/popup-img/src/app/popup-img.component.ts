import { Component, Input } from '@angular/core';

import { camelcaseKeys, coerceBooleanProp } from '@bvl/library';
import { IPopupCarousel } from './popup-img';

@Component({
  selector: 'bvl-popup-img',
  templateUrl: './popup-img.component.html',
  styleUrls: ['./popup-img.component.scss']
})
export class PopupImgComponent {
  @Input() title: string;
  @Input() titleAlign: string;

  @Input() description: string;

  @Input() textLink: string;
  @Input() urlLink: string;

  private _carousel: Array<IPopupCarousel>;
  @Input()
  get carousel(): any {
    return this._carousel || [];
  }
  set carousel(value: any) {
    this._carousel = (JSON.parse(value) || []).map(camelcaseKeys.bind(this));
  }

  private _isLink: boolean;
  @Input()
  get isLink(): boolean {
    return this._isLink;
  }
  set isLink(value: boolean) {
    this._isLink = coerceBooleanProp(value);
  }
}
