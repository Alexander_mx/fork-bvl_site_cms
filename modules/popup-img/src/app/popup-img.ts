import { IBackgroundImage } from '@bvl/library';

export interface IPopupCarousel {
  backgroundImage: Array<IBackgroundImage>;
  isLink?: boolean;
  urlLink?: string;
}
