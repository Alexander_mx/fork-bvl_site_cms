import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { EntriesTableModule } from './app/entries-table.module';

platformBrowserDynamic().bootstrapModule(EntriesTableModule)
  .catch(err => console.error(err));
