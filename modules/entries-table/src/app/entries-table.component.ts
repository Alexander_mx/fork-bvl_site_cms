import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  LOCALE_ID,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2
} from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import * as Highcharts from 'highcharts';
import { Subscription } from 'rxjs';

import { ExchangeProvider } from 'core/providers/exchange.provider';
import { FancyTableDataSource } from '../../../fancy-table/src/app/fancy-table-data-source';

import {
  IFancyTableData,
  IFancyTableEntry
} from '../../../fancy-table/src/app/fancy-table';

import { IFancyTableColumn, IStock } from '@bvl/library';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { VALUES_COLUMNS, VALUES_TABLE } from './tables';

let nextId = 0;

@Component({
  selector: 'bvl-entries-table',
  templateUrl: './entries-table.component.html',
  styleUrls: ['./entries-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, hidden', style({ height: '0px', minHeight: '0' })),
      state('expanded, shown', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('.2s cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ]
})
export class EntriesTableComponent
  implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  id = `bvl-table-${nextId++}`;
  tablesData: FancyTableDataSource<IFancyTableData>;

  private _tableMinWidth: number;
  private _elem: HTMLElement;
  private _tablesContainer: HTMLElement;
  private _tableElems: NodeListOf<HTMLElement>;
  private _mediaSubscription: Subscription;
  private _tableDataSubscription: Subscription;

  tableColumns: Array<IFancyTableColumn> = VALUES_COLUMNS;

  @Output() activeFilters = new EventEmitter<any>();
  @Output() tableEvents = new EventEmitter<any>();

  url: string;
  headerTitle: string;
  chartOptions: object;
  columnsName: Array<string>;
  entriesNumber: number;

  tablesFitInContainer = true;
  viewLoaded = false;
  selectedTable = undefined;
  expandedElement: { tableId: IFancyTableData['id']; element: IStock };

  scrollingTable = false;
  headerCellsWidth: Array<number> = [];

  constructor(
    private exchange: ExchangeProvider,
    public media: ObservableMedia,
    private _ngZone: NgZone,
    private _elementRef: ElementRef,
    private _renderer: Renderer2,
    private _cdr: ChangeDetectorRef,
    private _graphicOptions: GraphicOptionsProvider,
    @Inject(LOCALE_ID) private locale: string
  ) {
    this._mediaSubscription = this.media.subscribe(() => {
      if (this.viewLoaded) {
        this._setTablesMinWidth();
        this._setTablesFitting();
        this._setScrollingTable();
        this._setHeaderCellsWidth();
      }
    });
  }

  ngOnInit(): void {
    this._updateDataSource(VALUES_TABLE);
    this.chartOptions = this._graphicOptions.getGraphicOptions();

    this.columnsName =
      (this.tableColumns &&
        this.tableColumns
          .filter(column =>
            this.tablesData.data.every((table: any) =>
              table.entries.value.every(entry => column.name in entry)
            )
          )
          .map(column => column.name)) ||
      [];

    this._tableDataSubscription = this.tablesData
      .connect()
      .subscribe(tableData => {
        this.entriesNumber = tableData.reduce((totalEntries, table) => {
          return (totalEntries += table.entries.value.length);
        }, 0);
      });
  }

  ngAfterViewInit(): void {
    this._elem = this._elementRef.nativeElement;
    this._tablesContainer = this._elem.querySelector('.tables-container');
    this._tableElems = this._elem.querySelectorAll('.table-article');

    if (this.tablesData.data.length > 1) {
      this.selectedTable = this.tablesData.data[0].id;
    }
  }

  ngOnChanges(): void {
    const onStableSubscription: Subscription = this._ngZone.onStable.subscribe(
      () => {
        this._setTablesMinWidth();
        this.viewLoaded = true;
        this._setTablesFitting();
        this._setScrollingTable();
        onStableSubscription.unsubscribe();
      }
    );
  }

  ngOnDestroy(): void {
    if (this._mediaSubscription) { this._mediaSubscription.unsubscribe(); }
    if (this._tableDataSubscription) { this._tableDataSubscription.unsubscribe(); }
  }

  hasAnyTableTitle(): boolean {
    return !!this.tablesData.data.find(table => !!table.title);
  }

  onFilterChange(activeFilters): void {
    this.activeFilters.emit(activeFilters);
  }

  getAnimationState(element: IFancyTableEntry, table: IFancyTableData): string {
    const areDetailsShown =
      table.metadata && table.metadata.expandedElement === element;
    if (areDetailsShown) {
      return this.media.isActive('gt-sm') && this.tablesData.data.length === 1
        ? 'expanded'
        : 'shown';
    } else {
      return this.media.isActive('gt-sm') && this.tablesData.data.length === 1
        ? 'collapsed'
        : 'hidden';
    }
  }

  expandElement(element: IFancyTableEntry, table: IFancyTableData): any {
    if (element === table.metadata.expandedElement) {
      table.metadata.expandedElement = null;

      return;
    }

    table.metadata.expandedElement = element;
    element.detailsShowed = true;
    if (!this._isChartLoaded(element, table)) {
      this._loadChart(element, table.id);
      table.metadata.loadedCharts = [
        ...table.metadata.loadedCharts,
        element.id
      ];
    }
  }

  sortBy(column: IFancyTableColumn, table: IFancyTableData): void {
    this.tablesData.sort(column, table);
  }

  toggleInWatchList(element: IFancyTableEntry): void {
    element.inWatchlist = !element.inWatchlist;
    this.tableEvents.emit({
      eventType: 'watchList',
      elementId: element.id,
      value: element.inWatchlist
    });
  }

  private _loadChart(
    element: IFancyTableEntry,
    tableId: EntriesTableComponent['id']
  ): void {
    Highcharts.chart(`${tableId}-graphic-${element.id}`, this.chartOptions);
  }

  private _isChartLoaded(element: any, table: IFancyTableData): boolean {
    return table.metadata.loadedCharts.indexOf(element.id) !== -1;
  }

  @HostListener('window:resize') onresize(): void {
    this._ngZone.run(() => {
      this._setTablesFitting();
      this._setScrollingTable();
    });
  }

  private _setTablesMinWidth(): void {
    this._renderer.removeClass(this._tablesContainer, 'loaded');
    this._tableMinWidth = Math.max(
      ...Array.from(this._tableElems)
        .map(t => t.clientWidth + 0)
    );
    this._renderer.addClass(this._tablesContainer, 'loaded');
  }

  private _setTablesFitting(): void {
    this.tablesFitInContainer = (this._tablesContainer.clientWidth > this._tableMinWidth * this.tablesData.data.length);
  }

  private _setScrollingTable(): void {
    const wrapper = this._elementRef.nativeElement.querySelector(
      '.table-wrapper'
    );
    const table = wrapper.querySelector('[bvl-issuers-table]');
    this._unsetScrollingTableStyles(wrapper, table);

    const onStableSubscription: Subscription = this._ngZone.onStable.subscribe(
      () => {
        this.scrollingTable = false;
        this._unsetScrollingTableStyles(wrapper, table);
        onStableSubscription.unsubscribe();
      }
    );
  }

  private _unsetScrollingTableStyles(wrapper, table): void {
    this._removeStyles(wrapper, ['maxHeight']);
    this._removeStyles(table, ['position', 'top', 'marginBottom']);
  }

  private _setHeaderCellsWidth(): void {
    const headerCells = Array.prototype.slice.call(
      this._elementRef.nativeElement.querySelectorAll('.bvl-header-cell')
    );
    this.headerCellsWidth = headerCells.map(cell => cell.clientWidth);
    this._cdr.detectChanges();
  }

  private _removeStyles(element: HTMLElement, styles: Array<string>): void {
    styles.forEach(prop => {
      this._renderer.removeStyle(element, prop);
    });
  }

  private _updateDataSource(data): void {
    if (!this.tablesData) {
      this.tablesData = new FancyTableDataSource(data, this.id, this.exchange);
    } else {
      this.tablesData.data = data;
    }
  }
}
