import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TopScollbarModule } from '@bvl/library';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { EntriesTableComponent } from './entries-table.component';
import { StockTableHeaderComponent } from './fancy-table-header/fancy-table-header.component';

@NgModule({
  declarations: [
    EntriesTableComponent,
    StockTableHeaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    FlexLayoutModule,
    TopScollbarModule,
    BrowserAnimationsModule
  ],
  entryComponents: [
    EntriesTableComponent
  ],
  exports: [
    EntriesTableComponent
  ],
  providers: [
    ExchangeProvider,
    GraphicOptionsProvider
  ]
})
export class EntriesTableModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-entries-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(EntriesTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
