export const VALUES_TABLE = [
  {
    entries: [
      {
        id: 1,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 2,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 3,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 4,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 5,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 6,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 7,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 8,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 9,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 10,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 11,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 12,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 13,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 14,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 15,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      },
      {
        id: 16,
        name: 'A.F.P. CAPITAL S.A.',
        mnemonic: 'Alicorp1',
        isin: 'CL0000003096',
        currency: 'Peso Chileno'
      }
    ]
  }
];
export const VALUES_TABLE_FILTERS = [
  {
    filterId: 3,
    title: 'Valores',
    defaultOption: 1,
    options: [
      {
        id: 1,
        label: 'peruanos'
      },
      {
        id: 2,
        label: 'chilenos'
      }
    ]
  },
  {
    filterId: 4,
    title: 'Valores',
    defaultOption: 2,
    options: [
      {
        id: 1,
        label: 'peruanos'
      },
      {
        id: 2,
        label: 'chilenos'
      }
    ]
  }
];

export const VALUES_COLUMNS = [
  {
    name: 'name',
    label: 'Nombre del emisor',
    valueType: 'string'
  },
  {
    name: 'mnemonic',
    label: 'Nemónico',
    valueType: 'string'
  },
  {
    name: 'isin',
    label: 'Isin',
    valueType: 'string'
  },
  {
    name: 'currency',
    label: 'Moneda',
    valueType: 'string'
  }
];
