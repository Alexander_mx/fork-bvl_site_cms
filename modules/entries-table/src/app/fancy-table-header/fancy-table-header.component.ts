import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

import { VALUES_TABLE_FILTERS } from '../tables';
import {
  FancyTableCapability,
  IFancyTableByCategoryFilter
} from './fancy-table-header';

@Component({
  selector: 'bvl-entries-header',
  templateUrl: './fancy-table-header.component.html',
  styleUrls: ['./fancy-table-header.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class StockTableHeaderComponent implements OnInit, OnDestroy {

  private _mediaWatcher: Subscription;
  private _activeFilters: Array<{ type: FancyTableCapability, value: any, filterId?: number }> = [];

  byCategoryFilters: Array<IFancyTableByCategoryFilter> = VALUES_TABLE_FILTERS;
  entriesNumber = 16;

  @Output() activeFilters = new EventEmitter<any>();

  tableCapabilities: Array<FancyTableCapability>;
  filterOnlyToday: false;
  onlyTodayEnabled = true;
  searchTerm = '';
  searchFocused = false;
  searchInputShowed = true;
  selectedCategories: Array<{ filterId: number, value: number }> = [];
  selectedType: string | number | null = null;
  selectedInitial: string | null = null;
  selectedTime = 1;

  constructor(public media: ObservableMedia) { }

  ngOnInit(): void {

    this._mediaWatcher = this.media.subscribe((change: MediaChange) => {
      if (change.mqAlias === 'xs' || change.mqAlias === 'sm') {
        if (!this.searchTerm.length) {
          this.searchInputShowed = false;
        }
      } else {
        this.searchInputShowed = true;
      }
    });

    if (this.byCategoryFilters) {
      this._setInitialCategories();
    }
  }

  ngOnDestroy(): void {
    this._mediaWatcher.unsubscribe();
  }

  onCategoryChange(byCategoryFilterId): void {
    const categoryChanged = this.selectedCategories.find(cat => cat.filterId !== byCategoryFilterId);
    if (categoryChanged.value) {
      this._addActiveFilter('filterByCategory', categoryChanged.value, categoryChanged.filterId);
    } else {
      this._removeActiveFilter('filterByCategory', categoryChanged.filterId);
    }
  }

  private _setInitialCategories(): void {
    this.selectedCategories = this.byCategoryFilters.map(catFilter => (
      {
        filterId: catFilter.filterId,
        value: catFilter.defaultOption || null
      }
    ));
  }

  private _addActiveFilter(
    type: FancyTableCapability,
    value: string | number | boolean,
    filterId = null
  ): void {
    if (filterId) {
      this._activeFilters = [
        ...this._activeFilters.filter(f => f.filterId !== filterId),
        { filterId, type, value }
      ];
    } else {
      this._activeFilters = [
        ...this._activeFilters.filter(f => f.type !== type),
        { type, value }
      ];
    }

    this.activeFilters.emit(this._activeFilters);
  }

  private _removeActiveFilter(type: FancyTableCapability, filterId = null): void {
    const previousActiveFilters = this._activeFilters;

    this._activeFilters = filterId
      ? this._activeFilters.filter(f => f.filterId !== filterId)
      : this._activeFilters.filter(f => f.type !== type);

    if (previousActiveFilters.length === this._activeFilters.length) {
      return;
    }

    this.activeFilters.emit(this._activeFilters);
  }
}
