import { Component, Input } from '@angular/core';

@Component({
  selector: 'bvl-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent {

  @Input() title: string;
  @Input() description: string;
  @Input() textLink: string;
  @Input() urlLink: string;

}
