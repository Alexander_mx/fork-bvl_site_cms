import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { PipesModule } from '@bvl-core/shared/helpers/pipes';
import { ArticleComponent } from './article.component';

@NgModule({
  declarations: [
    ArticleComponent
  ],
  imports: [
    BrowserModule,
    PipesModule
  ],
  providers: [],
  entryComponents: [ArticleComponent]
})
export class ArticleModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-article'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ArticleComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
