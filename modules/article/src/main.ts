import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ArticleModule } from './app/article.module';

platformBrowserDynamic().bootstrapModule(ArticleModule)
  .catch(err => console.error(err));
