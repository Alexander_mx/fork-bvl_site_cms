import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { DailyTableModule } from './app/daily-table.module';

platformBrowserDynamic().bootstrapModule(DailyTableModule)
  .catch(err => console.error(err));
