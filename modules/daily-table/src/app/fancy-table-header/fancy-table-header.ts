export type FancyTableCapability = 'sort' | 'filter' | 'inputCompany' | 'today'
                                    | 'filterByCategory' | 'sector' | 'any';

export type FancyTableByInitialFilter = Array<string>;

export interface IFancyTableByTypeFilter {
    label: string;
    options: Array<IOptionFancyTableByTypeFilter>;
}

export interface IOptionFancyTableByTypeFilter {
  id: number;
  label: string;
  code: string;
}

export interface IFancyTableByCategoryFilter {
    filterId: number;
    title?: string;
    label?: string;
    defaultOption: number;
    options: Array<{ id: number, label: string }>;
}
