import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { takeUntil } from 'rxjs/operators';

import { ConfigurationService, GaUnsubscribeBase, getStorageLanguage, LANGUAGES } from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import {
  FancyTableCapability,
  IFancyTableByTypeFilter,
  IOptionFancyTableByTypeFilter
} from './fancy-table-header';

@Component({
  selector: 'bvl-daily-table-header',
  templateUrl: './fancy-table-header.component.html',
  styleUrls: ['./fancy-table-header.component.scss']
})
export class DailyTableHeaderComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  currentFilter: { type: FancyTableCapability, value: any, filterId?: number };

  @Input() privateSite = false;
  @Input() filterOnlyToday = false;

  @Output() activeFilters = new EventEmitter<any>();

  private _userProfile = this._configService.getUserProfile();
  byTypeFilter: IFancyTableByTypeFilter;
  onlyTodayEnabled = true;
  searchTerm = '';
  sector = '';
  searchFocused = false;
  searchInputShowed = true;
  selectedType: string | number | null = '';
  selectedInitial: string | null = null;
  selectedTime = 1;

  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  constructor(
    protected _element: ElementRef,
    public media: ObservableMedia,
    private _configService: ConfigurationService,
    private _tableServices: TableServices
  ) {
    super(_element);
    this.byTypeFilter = {
      label: 'SECTOR',
      options: []
    };
  }

  ngOnInit(): void {
    this._getSectors();

    this.media
      .asObservable()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((change: MediaChange) => {
        if (change.mqAlias === 'xs' || change.mqAlias === 'sm') {
          if (!this.searchTerm.length) {
            this.searchInputShowed = false;
          }
        } else {
          this.searchInputShowed = true;
        }
      });
  }

  onSearch(onlyIfEmpty = false): void {
    if (!(onlyIfEmpty && this.searchTerm.length)) {
      this.filterOnlyToday = false;
      this._addActiveFilter('inputCompany',
        this.searchTerm
          .toUpperCase()
          .trim()
      );
    }
  }

  onOnlyTodayChange(): void {
    this._addActiveFilter('today', this.filterOnlyToday);
  }

  onTypeChange(value: number | string): void {
    if (this.selectedType === value) {
      return void 0;
    }
    this.filterOnlyToday = false;
    this.selectedType = value;
    this.searchTerm = '';
    this.sector = value.toString();
    this._addActiveFilter('sector', value);
  }

  getSelectedLabel(code: string, object: Array<IOptionFancyTableByTypeFilter>): string {
    const value = object.find(item => item.code === code);

    return value && value.label || this.byTypeFilter.label;
  }

  setFocusToElement(elementRef: any): void {
    elementRef.focus();
  }

  toggleSearch(): void {
    this.searchInputShowed = !this.searchInputShowed;
  }

  resetSearch(): void {
    this.searchTerm = '';
    this.onSearch();
  }

  isNumber(val: any): boolean {
    return typeof val === 'number';
  }

  private _addActiveFilter(
    type: FancyTableCapability,
    value: string | number | boolean,
    filterId = null
  ): void {
    this.currentFilter = { type, value };
    if (filterId) { this.currentFilter.filterId = filterId; }
    this.activeFilters.emit(this.currentFilter);
  }

  private _getSectors(): void {
    this._tableServices.getSectors()
      .subscribe(response => {
        this.byTypeFilter = {
          label: 'SECTOR',
          options: response.map((item, index) => ({ id: index, label: item.description, code: item.sectorCode }))
        };
      });
  }

  getAction(): string {
    return this._userProfile ? 'Listar movimiento' : 'Ir a Login';
  }
}
