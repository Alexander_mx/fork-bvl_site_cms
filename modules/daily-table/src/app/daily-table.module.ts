import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  AmountCellModule,
  ApiModule,
  DomChangeDirectiveModule,
  LinksListModule,
  MovementsSummaryModule,
  PaginationModule,
  PipesModule,
  ToggleByPositionModule,
  TopScollbarModule,
  WatchListService,
  WithoutDataModule
} from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { DailyTableComponent } from './daily-table.component';
import { DailyTableHeaderComponent } from './fancy-table-header/fancy-table-header.component';

@NgModule({
  declarations: [
    DailyTableComponent,
    DailyTableHeaderComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    AmountCellModule,
    LinksListModule,
    FlexLayoutModule,
    TopScollbarModule,
    BrowserAnimationsModule,
    MovementsSummaryModule,
    PipesModule,
    ApiModule,
    PaginationModule,
    ToggleByPositionModule,
    WithoutDataModule,
    DomChangeDirectiveModule
  ],
  entryComponents: [
    DailyTableComponent
  ],
  exports: [
    DailyTableComponent
  ],
  providers: [
    ExchangeProvider,
    GraphicOptionsProvider,
    TableServices,
    WatchListService
  ]
})
export class DailyTableModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-daily-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(DailyTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
