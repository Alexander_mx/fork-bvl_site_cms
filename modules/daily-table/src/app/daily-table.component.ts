import * as animations from '@angular/animations';
import { formatDate } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  LOCALE_ID,
  NgZone,
  OnChanges,
  OnInit,
  Output,
  Renderer2,
  ViewEncapsulation
} from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import * as Highcharts from 'highcharts';
import { takeUntil } from 'rxjs/operators';

import { IFancyTableSortedBy } from '@bvl-core/shared/helpers/models/fancy-table.model';
import { pagination } from '@bvl-core/shared/helpers/util';
import { setFilterRequest } from '@bvl-core/shared/helpers/util/body-request';
import { getFancyTableSortBy, sortFancyTable } from '@bvl-core/shared/helpers/util/sort';
import { TableServices } from '@bvl/table-services';

import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { IStockQuote, IUpdateWatchListRequest, setLinks } from 'modules/fancy-table/src/app/fancy-table';
import { TABLE_COLUMN } from 'modules/home-table/src/app/data';
import { IFancyTableData, IFancyTableEntry } from '../../../fancy-table/src/app/fancy-table';
import { FancyTableDataSource } from '../../../fancy-table/src/app/fancy-table-data-source';
import { environment } from '../environments/environment';
import { DAILY_MOVEMENTS_COLUMNS, INITIAL_BODY, IStockQuoteView } from './tables';

import {
  coerceBooleanProp,
  ConfigurationService,
  GaUnsubscribeBase,
  getStorageLanguage,
  IFancyTableColumn,
  IPopover,
  IStock,
  LANGUAGES,
  WatchListService,
  WITHOUT_DATA_TYPE
} from '@bvl/library';

let nextId = 0;

@Component({
  selector: 'bvl-daily-table',
  templateUrl: './daily-table.component.html',
  styleUrls: ['./daily-table.component.scss'],
  animations: [
    animations.trigger('detailExpand', [
      animations.state('collapsed, hidden', animations.style({ height: '0px', minHeight: '0' })),
      animations.state('expanded, shown', animations.style({ height: '*' })),
      animations.transition('expanded <=> collapsed', animations.animate('.2s cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ],
  encapsulation: ViewEncapsulation.Emulated
})
export class DailyTableComponent extends GaUnsubscribeBase implements OnInit, AfterViewInit, OnChanges {

  WITHOUT_DATA_TYPE = WITHOUT_DATA_TYPE;

  id = `bvl-table-${nextId++}`;
  tablesData: FancyTableDataSource<IFancyTableData> = {} as FancyTableDataSource<IFancyTableData>;
  private _userProfile = this._configService.getUserProfile();
  private _elem: HTMLElement;
  private _tablesContainer: HTMLElement;
  private _currentRequest: any;

  private _privateSite = false;
  @Input()
  get privateSite(): boolean {
    return this._privateSite;
  }
  set privateSite(value: boolean) {
    this._privateSite = coerceBooleanProp(value);
  }

  @Output() activeFilters = new EventEmitter<any>();

  tableColumns: Array<IFancyTableColumn>;
  headerTitle: string;
  chartOptions: any;
  columnsName: Array<string>;
  entriesNumber: number;

  tablesFitInContainer = true;
  viewLoaded = false;
  expandedElement: { tableId: IFancyTableData['id'], element: IStock };

  scrollingTable = false;
  headerCellsWidth: Array<number> = [];

  up: number;
  equal: number;
  down: number;

  currentPage = 1;
  completeData: Array<IStockQuoteView>;
  maxRows = 15;

  INITIAL_BODY = INITIAL_BODY;
  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  constructor(
    @Inject(LOCALE_ID) private locale: string,
    private _cdr: ChangeDetectorRef,
    private _configService: ConfigurationService,
    private _graphicOptions: GraphicOptionsProvider,
    private _ngZone: NgZone,
    private _renderer: Renderer2,
    private _tableServices: TableServices,
    private _watchListService: WatchListService,
    private exchange: ExchangeProvider,
    protected _elementRef: ElementRef,
    public media: ObservableMedia
  ) {
    super(_elementRef);
    this.media
      .asObservable()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        if (this.viewLoaded) {
          this._setTablesMinWidth();
          this._setHeaderCellsWidth();
        }
      });
  }

  ngOnInit(): void {
    this._watchListService.setEnviroment(environment.API_URL);
    this.tableColumns = this.privateSite ? TABLE_COLUMN : DAILY_MOVEMENTS_COLUMNS;

    this._getMarketData();
    this.chartOptions = this._graphicOptions.getGraphicOptions();
  }

  ngAfterViewInit(): void {
    this._elem = this._elementRef.nativeElement;
    this._tablesContainer = this._elem.querySelector('.tables-container');
  }

  ngOnChanges(): void {
    this._ngZone.onStable
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this._setTablesMinWidth();
        this.viewLoaded = true;
      });
  }

  hasAnyTableTitle(): boolean {
    return !!this.tablesData.data.find(table => !!table.title);
  }

  onFilterChange(activeFilters): void {
    if (activeFilters.value !== 'inWatchList') {
      this._getMarketData(setFilterRequest(INITIAL_BODY, activeFilters));
    } else {
      if (this._userProfile !== null) {
        const sortedWatchlist = this.completeData.sort((a: IStockQuoteView, b: IStockQuoteView) => +b.inWatchlist - +a.inWatchlist);
        this._setData(sortedWatchlist);
      } else {
        setTimeout(() => {
          location.href = `${location.origin}/login`;
        }, 500);
      }
    }
  }

  getAnimationState(element: IFancyTableEntry, table: IFancyTableData): string {
    const areDetailsShown = table.metadata && table.metadata.expandedElement === element;
    const dataLength = this.media.isActive('gt-sm') && this.tablesData.data.length === 1;

    return areDetailsShown ? (dataLength ? 'expanded' : 'shown') : (dataLength ? 'collapsed' : 'hidden');
  }

  expandElement(element: IFancyTableEntry, table: IFancyTableData): any {
    if (element === table.metadata.expandedElement) {
      table.metadata.expandedElement = null;

      return;
    }

    table.metadata.expandedElement = element;
    element.detailsShowed = true;
    if (!this._isChartLoaded(element, table)) {
      this._loadChart(element, table.id);
      table.metadata.loadedCharts = [...table.metadata.loadedCharts, element.id];
    }
  }

  sortBy(column: IFancyTableColumn, table: IFancyTableData): void {
    const orderedData = sortFancyTable<IStockQuoteView>(column, table, this.completeData);
    const sortBy = getFancyTableSortBy(column, table.metadata);
    this._setData(orderedData, sortBy);
  }

  toggleInWatchList(element: IFancyTableEntry): void {
    if (this._userProfile !== null) {
      const value = !element.inWatchlist;
      const body: IUpdateWatchListRequest = {
        idUser: this._userProfile.cognitoUsername,
        nemonico: element.mnemonic,
        add: value
      };

      this._watchListService.updateStockWatchList(body)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => element.inWatchlist = !element.inWatchlist);

    } else {
      location.href = `${location.origin}/login`;
    }
  }

  getFactPopover(title, date, url?): IPopover {
    return {
      title,
      class: 'fact-popover',
      body: formatDate(date, 'dd/MM/yy', this.locale),
      url: url || undefined
    };
  }

  private _loadChart(element: IFancyTableEntry, tableId: DailyTableComponent['id']): void {
    this._tableServices.getGraph(element.mnemonic)
      .subscribe((response: any) => {
        this.chartOptions.series = [];

        const values = response.sort((a, b) => a.lastDate.localeCompare(b.lastDate));
        element.chartData = values;

        if (element.chartData.length) {
          this.chartOptions.series.push({
            data: values.map(item => ({
              name: (new Date(`${item.lastDate}+0500`)).toISOString()
              .replace('T', ' ')
              .substring(0, 19),
              y: item.lastValue || 0
            }))
          });
          Highcharts.chart(`${tableId}-graphic-${element.id}`, this.chartOptions);
        }
      });
  }

  private _isChartLoaded(element: any, table: IFancyTableData): boolean {
    return table.metadata.loadedCharts.indexOf(element.id) !== -1;
  }

  private _setTablesMinWidth(): void {
    this._renderer.removeClass(this._tablesContainer, 'loaded');
    this._renderer.addClass(this._tablesContainer, 'loaded');
  }

  private _setHeaderCellsWidth(): void {
    const headerCells = Array.prototype.slice.call(this._elementRef.nativeElement.querySelectorAll('.bvl-header-cell'));
    this.headerCellsWidth = headerCells
      .map((cell: any) => cell.clientWidth);

    this._cdr.detectChanges();
  }

  private _setMarketData(data: Array<IStockQuoteView>, sortedBy: IFancyTableSortedBy): FancyTableDataSource<IFancyTableData> {
    const name = data[0].company || data[0].segment;
    const formatedId = name
      .split(' ')
      .join('_')
      .toLowerCase();

    const formatData = {
      id: formatedId,
      entries: data.map((item, index) => ({ id: index, ...item })),
      metadata: {
        sortedBy: {
          column: sortedBy.column,
          ascendingOrder: sortedBy.ascendingOrder
        },
        expandedElement: null,
        loadedCharts: []
      }
    } as IFancyTableData;

    return new FancyTableDataSource([formatData], this.id, this.exchange);
  }

  private _getMarketData(body?: any): void {
    if (!body) {
      body = INITIAL_BODY;
    }

    if (JSON.stringify(body) !== this._currentRequest) {
      this._currentRequest = JSON.stringify(body);
      this._tableServices.getMarket(body)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(response => {
          if (response) {
            this._ngZone.run(() => {
              const completeData = this.getTransformData(response.content);
              this.currentPage = 1;

              this.up = response.up;
              this.equal = response.equal;
              this.down = response.down;
              if (this._userProfile !== null) {
                this._watchListService.getStockWatchList(this._userProfile.cognitoUsername)
                  .pipe(takeUntil(this.unsubscribeDestroy$))
                  .subscribe(data => {
                    const stocks = data.stocks.filter(stock => !!stock);
                    let dataWatchList = completeData.map(item => {
                      item.inWatchlist = !!stocks.find(el => el.nemonico === item.mnemonic);

                      return item;
                    });
                    if (this.privateSite) {
                      dataWatchList = dataWatchList.filter(wl => wl.inWatchlist);
                    }
                    this._setData(dataWatchList);
                  });
              } else {
                this._setData(completeData);
              }
            });
          }
        });
    }
  }

  private getTransformData(content: Array<IStockQuote>): Array<IStockQuoteView> {
    return content.map(item => ({
      companyCode: item.companyCode,
      company: item.companyName,
      mnemonic: item.nemonico,
      sector: item.sectorDescription,
      segment: item.segment,
      previous: item.previous,
      previousDate: item.previousDate,
      opening: item.opening,
      last: item.last,
      variation: item.percentageChange,
      buy: item.buy,
      sell: item.sell,
      amount: item.negotiatedAmount,
      operationsNumber: item.operationsNumber,
      inWatchlist: false,
      currency: {
        code: item.currency
      },
      links: setLinks(item.companyCode),
      price: item.last,
      negotiatedAmount: item.negotiatedAmount,
      operations: item.operationsNumber,
      negotiatedQuantity : item.negotiatedQuantity
    } as IStockQuoteView));
  }

  emitChangePage(currentPage: number): void {
    this.currentPage = currentPage;
    this.tablesData = this._setMarketData(pagination(this.completeData, this.currentPage, this.maxRows), this.getSortBy());
  }

  getSortBy(): any {
    return this.tablesData.data[0].metadata.sortedBy;
  }

  getTotalPages(): number {
    return Math.ceil((this.completeData || []).length / this.maxRows);
  }

  private _setData(data: Array<IStockQuoteView>, sortedBy: IFancyTableSortedBy = { column: null, ascendingOrder: true }): void {
    this.tablesData = {} as FancyTableDataSource<IFancyTableData>;
    this.completeData = (data || []);
    this.tablesData = this._setMarketData(pagination(this.completeData, this.currentPage, this.maxRows), sortedBy);
  }
}
