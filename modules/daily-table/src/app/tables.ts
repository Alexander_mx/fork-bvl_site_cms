import { getStorageLanguage, LANGUAGES } from '@bvl/library';

const TEXT = LANGUAGES[
  getStorageLanguage()
  .substring(0, 2)
];

export const INITIAL_BODY = {
  sector: '',
  today: true,
  companyCode: '',
  inputCompany: ''
};

export const DAILY_MOVEMENTS_COLUMNS = [
  {
    name: 'company',
    label: TEXT.tables.company,
    valueType: 'string'
  },
  {
    name: 'mnemonic',
    label: TEXT.codification.nemonic,
    valueType: 'string'
  },
  {
    name: 'sector',
    label: TEXT.tables.sector,
    valueType: 'string'
  },
  {
    name: 'segment',
    label: TEXT.tables.seg,
    valueType: 'string'
  },
  {
    name: 'previous',
    label: TEXT.tables.ant,
    valueType: 'amount'
  },
  {
    name: 'previousDate',
    label: TEXT.tables.prevDate,
    valueType: 'date'
  },
  {
    name: 'opening',
    label: TEXT.general.opening,
    valueType: 'amount'
  },
  {
    name: 'last',
    label: TEXT.tables.last,
    valueType: 'amount'
  },
  {
    name: 'variation',
    label: `${TEXT.tables.var}%`,
    valueType: 'percent'
  },
  {
    name: 'buy',
    label: TEXT.tables.buy,
    valueType: 'price'
  },
  {
    name: 'sell',
    label: TEXT.tables.sell,
    valueType: 'price'
  },
  {
    name: 'amount',
    label: TEXT.tables.amount,
    valueType: 'amount'
  },
  {
    name: 'negotiatedQuantity',
    label: TEXT.tables.quantity,
    valueType: 'number'
  },
  {
    name: 'operationsNumber',
    label: TEXT.tables.noOp,
    valueType: 'number'
  },
  {
    name: 'inWatchlist',
    label: 'Watchlist',
    valueType: 'boolean'
  }
];

export interface IStockQuoteView {
  companyCode: string;
  company: string;
  mnemonic: string;
  sector: string;
  segment: string;
  previous: Number;
  previousDate: string;
  opening: number;
  last: number;
  variation: number;
  buy: number;
  sell: number;
  amount: number;
  operationsNumber: string;
  inWatchlist: boolean;
  currency: { code:  string };
  links: any;
  price: number;
  negotiatedAmount: number;
  operations: string;
  negotiatedQuantity: string;
}
