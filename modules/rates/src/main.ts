import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { RatesModule } from './app/rates.module';
platformBrowserDynamic().bootstrapModule(RatesModule)
  .catch(err => console.error(err));
