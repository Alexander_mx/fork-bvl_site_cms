import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { createCustomElement } from '@angular/elements';
import { GreatPriceModule } from '@bvl/library';
import { RatesComponent } from './rates.component';

@NgModule({
  declarations: [
    RatesComponent
  ],
  imports: [
    BrowserModule,
    GreatPriceModule
  ],
  providers: [],
  entryComponents: [RatesComponent]
})
export class RatesModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-rates'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(RatesComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
