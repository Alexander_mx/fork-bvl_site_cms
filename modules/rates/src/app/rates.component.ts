import { Component, Input, ViewEncapsulation } from '@angular/core';

import { IPrice } from '@bvl/library';

@Component({
  selector: 'bvl-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RatesComponent {
  private _prices: Array<IPrice>;
  @Input()
  get prices(): any {
    return this._prices;
  }
  set prices(value: any) {
    this._prices = JSON.parse(value) as Array<IPrice>;
  }
}
