import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ItemsGridModule } from './app/items-grid.module';
platformBrowserDynamic().bootstrapModule(ItemsGridModule)
  .catch(err => console.error(err));
