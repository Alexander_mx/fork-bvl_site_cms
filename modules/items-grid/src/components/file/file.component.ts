import { Component, ElementRef, Input } from '@angular/core';

import {
  DynamicTableComponent,
  GaUnsubscribeBase,
  IFile,
  IModalConfig,
  MODAL_BREAKPOINT,
  ModalRef,
  ModalService
} from '@bvl/library';

@Component({
  selector: 'bvl-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent extends GaUnsubscribeBase {

  @Input() file: IFile;

  constructor(
    protected _element: ElementRef,
    private _modalService: ModalService
  ) {
    super(_element);
  }

  action(file: IFile): void {
    if (file.url) {
      window.open(file.url, '_blank');
    } else {
      const config = {
        titleText: file.data.businessName,
        size: MODAL_BREAKPOINT.lg,
        showFooter: false
      } as IModalConfig;
      const modalRef: ModalRef = this._modalService.open(DynamicTableComponent, config);
      modalRef.setPayload(file.data);
    }
  }

}
