import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MODAL_CONFIG } from '@bvl-core/shared/settings/constants/general.constants';

import { DomChangeDirectiveModule, DynamicTableModule, ModalModule, PipesModule } from '@bvl/library';
import { FileComponent } from './file.component';

@NgModule({
  imports: [
    CommonModule,
    DomChangeDirectiveModule,
    DynamicTableModule,
    ModalModule.forRoot(MODAL_CONFIG),
    PipesModule
  ],
  declarations: [
    FileComponent
  ],
  exports: [FileComponent]
})
export class FileModule { }
