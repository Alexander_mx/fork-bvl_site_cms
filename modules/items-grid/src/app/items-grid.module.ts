import { CommonModule, registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Injector, LOCALE_ID, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
registerLocaleData(es);

import { EntryModule, getStorageLanguage } from '@bvl/library';
import { FileModule } from '../components/file';
import { ItemsGridComponent } from './items-grid.component';

@NgModule({
  declarations: [
    ItemsGridComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    EntryModule,
    FileModule
  ],
  exports: [ItemsGridComponent],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: getStorageLanguage()
    }
  ],
  entryComponents: [ItemsGridComponent]
})
export class ItemsGridModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-items-grid'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ItemsGridComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
