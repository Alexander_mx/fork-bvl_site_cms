import { Component, ElementRef, Input, OnInit, ViewEncapsulation } from '@angular/core';

import { EntryStyle, GaUnsubscribeBase, IEntry, IFile } from '@bvl/library';
import { ItemsType } from './items-grid';

@Component({
  selector: 'bvl-items-grid',
  templateUrl: './items-grid.component.html',
  styleUrls: ['./items-grid.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ItemsGridComponent extends GaUnsubscribeBase implements OnInit {

  @Input() items: Array<(IFile|IEntry)> | any;
  @Input() itemsType: ItemsType;
  @Input() itemsVariant: EntryStyle = 'basic';
  @Input() url?: string;
  @Input() space: '1' | '2' | '3' | '4';
  @Input() column: string;
  isBasic = true;

  constructor(
    protected _element: ElementRef
  ) {
    super(_element);
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    this.isBasic = this.itemsType === 'Básico';
    this._setColums();
    if (typeof this.items === 'string') {
      this.items = JSON.parse(this.items);
    }
  }
  private _setColums(): void {
    if (this.space) {
      const columnWidth = 12 / +this.space;
      this.column = `col-sm-${columnWidth}`;
    } else {
      this.column = 'col';
    }
  }
}
