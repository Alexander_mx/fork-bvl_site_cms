import { Component, ElementRef, Input, ViewEncapsulation } from '@angular/core';

import { GaUnsubscribeBase, ITestimony } from '@bvl/library';

@Component({
  selector: 'bvl-testimonials',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TestimonyCarouselComponent extends GaUnsubscribeBase {

  private _testimonials: Array<ITestimony>;
  @Input()
  get testimonials(): any {
    return this._testimonials;
  }
  set testimonials(value: any) {
    if (typeof value === 'string') {
      this._testimonials = JSON.parse(value) as Array<ITestimony>;
    }
  }

  constructor(
    protected _element: ElementRef
  ) {
    super(_element);
  }

  setEventsGA(): void { }

}
