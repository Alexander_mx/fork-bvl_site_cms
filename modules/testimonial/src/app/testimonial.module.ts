import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { CarouselModule, TestimonyModule } from '@bvl/library';
import { TestimonyCarouselComponent } from './testimonial.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    CarouselModule,
    TestimonyModule
  ],
  declarations: [TestimonyCarouselComponent],
  entryComponents: [TestimonyCarouselComponent]
})
export class TestimonyCarouselModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-testimonial'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(TestimonyCarouselComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
