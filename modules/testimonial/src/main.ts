import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { TestimonyCarouselModule } from './app/testimonial.module';
platformBrowserDynamic().bootstrapModule(TestimonyCarouselModule)
  .catch(err => console.error(err));
