import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { BannerBgModule } from './app/banner-background.module';

platformBrowserDynamic().bootstrapModule(BannerBgModule)
  .catch(err => console.error(err));
