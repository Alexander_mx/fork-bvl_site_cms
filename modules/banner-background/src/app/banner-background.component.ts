import { Component, ElementRef, Input, OnInit, ViewEncapsulation } from '@angular/core';

import { coerceBooleanProp, GaUnsubscribeBase, IListImage } from '@bvl/library';

@Component({
  selector: 'bvl-banner-background',
  templateUrl: './banner-background.component.html',
  styleUrls: ['./banner-background.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BannerBgComponent extends GaUnsubscribeBase implements OnInit {

  private _backgroundImage: Array<IListImage>;
  @Input()
  get backgroundImage(): any {
    return this._backgroundImage;
  }
  set backgroundImage(value: any) {
    this._backgroundImage = JSON.parse(value) as Array<IListImage>;
  }
  @Input() title: string;
  @Input() text: string;

  private _isButton: boolean;
  @Input()
  get isButton(): boolean {
    return this._isButton;
  }
  set isButton(value: boolean) {
    this._isButton = coerceBooleanProp(value);
  }
  @Input() urlButton: string;
  @Input() textButton: string;

  private _isLink: boolean;
  @Input()
  get isLink(): boolean {
    return this._isLink;
  }
  set isLink(value: boolean) {
    this._isLink = coerceBooleanProp(value);
  }
  @Input() urlLink: string;
  @Input() textLink: string;

  image: string;

  constructor(
    protected _element: ElementRef
  ) {
    super(_element);
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    if (Array.isArray(this.backgroundImage) && this.backgroundImage.length) {
      this.image = this.backgroundImage[0].large.imageUri;
    }
  }
}
