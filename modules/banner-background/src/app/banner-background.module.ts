import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { createCustomElement } from '@angular/elements';
import { DomChangeDirectiveModule } from '@bvl/library';
import { BannerBgComponent } from './banner-background.component';

@NgModule({
  declarations: [
    BannerBgComponent
  ],
  imports: [
    BrowserModule,
    DomChangeDirectiveModule
  ],
  providers: [],
  entryComponents: [BannerBgComponent]
})
export class BannerBgModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-banner-background'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BannerBgComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
