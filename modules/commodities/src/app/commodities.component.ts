import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { Subscription } from 'rxjs';

import { ICommodity, ICommodityCategory } from './commodities';

@Component({
  selector: 'bvl-commodities',
  templateUrl: './commodities.component.html',
  styleUrls: ['./commodities.component.scss'],
  animations: [
    trigger('expand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('.2s cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class CommoditiesComponent implements OnInit, OnDestroy {
  private _mediaSubscription: Subscription;
  commodities: Array<ICommodity>;
  categories: Array<ICommodityCategory>;
  selectedCategory: ICommodityCategory;
  expanded = false;

  constructor(private _media: ObservableMedia) {
    this.categories = [
      {
        id: 1,
        label: 'Minería'
      },
      {
        id: 2,
        label: 'Energía'
      }];
    this.selectedCategory = this.categories.find(c => c.id === 1);
    this.commodities = [
      {
        id: 11,
        label: 'Acero',
        value: 11,
        variation: 1.2,
        currency: {
          code: 'PEN',
          symbol: 'S/'
        }
      },
      {
        id: 12,
        label: 'Cobre',
        value: 220,
        variation: 2.2,
        currency: {
          code: 'USD',
          symbol: '$'
        }
      },
      {
        id: 13,
        label: 'Aluminio',
        value: 2.28,
        variation: 0,
        currency: {
          code: 'PEN',
          symbol: 'S/'
        }
      },
      {
        id: 14,
        label: 'Oro',
        value: 600.1,
        variation: -1.3,
        currency: {
          code: 'USD',
          symbol: '$'
        }
      },
      {
        id: 15,
        label: 'Hierro',
        value: 102.9,
        variation: 1.9,
        currency: {
          code: 'PEN',
          symbol: 'S/'
        }
      },
      {
        id: 16,
        label: 'Magnesio',
        value: 189,
        variation: 0,
        currency: {
          code: 'PEN',
          symbol: 'S/'
        }
      }];
  }

  ngOnInit(): void {
    this._mediaSubscription = this._media.subscribe(() => {
      this.expanded = !this._media.isActive('lt-md');
    });
  }

  toggle(): void {
    this.expanded = !this.expanded;
  }

  categoryChanged(category: ICommodityCategory): void {
    this.selectedCategory = category;
  }

  ngOnDestroy(): void {
    this._mediaSubscription.unsubscribe();
  }
}
