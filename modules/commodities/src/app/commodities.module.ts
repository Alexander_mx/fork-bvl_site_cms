import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AmountCellModule } from '@bvl/library';
import { CommoditiesComponent } from './commodities.component';

@NgModule({
  declarations: [
    CommoditiesComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AmountCellModule,
    FlexLayoutModule,
    BrowserAnimationsModule
  ],
  providers: [],
  entryComponents: [CommoditiesComponent]
})
export class AppModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-commodities'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(CommoditiesComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
