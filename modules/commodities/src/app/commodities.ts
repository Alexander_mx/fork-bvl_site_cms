import { ICurrency } from '@bvl/library';

export interface ICommodity {
    id: number;
    label: string;
    value: number;
    variation: number;
    currency: ICurrency;
}

export interface ICommodityCategory {
    id: number;
    label: string;
}
