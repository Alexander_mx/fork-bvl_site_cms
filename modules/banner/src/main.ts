import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { BannerModule } from './app/banner.module';

platformBrowserDynamic().bootstrapModule(BannerModule)
  .catch(err => console.error(err));
