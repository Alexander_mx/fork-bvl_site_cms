import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { DomChangeDirectiveModule, SectionHeaderModule } from '@bvl/library';
import { BannerComponent } from './banner.component';

@NgModule({
  declarations: [
    BannerComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    DomChangeDirectiveModule
  ],
  exports: [BannerComponent],
  entryComponents: [BannerComponent]
})
export class BannerModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-banner'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BannerComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
