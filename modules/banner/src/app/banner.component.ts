import { Component, ElementRef, Input, OnInit, ViewEncapsulation } from '@angular/core';

import { coerceBooleanProp, GaUnsubscribeBase, IListImage } from '@bvl/library';

@Component({
  selector: 'bvl-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BannerComponent extends GaUnsubscribeBase implements OnInit {

  @Input() title: string;
  @Input() description: string;
  @Input() path: string;
  @Input() itemsSizes: 'Pequeño' | 'Mediano' | 'Grande';

  private _backgroundImage: Array<IListImage>;
  @Input()
  get backgroundImage(): any {
    return this._backgroundImage;
  }
  set backgroundImage(value: any) {
    this._backgroundImage = JSON.parse(value) as Array<IListImage>;
  }

  @Input() urlLink: string;
  @Input() textLink: string;

  private _isLink: boolean;
  @Input()
  get isLink(): boolean {
    return this._isLink;
  }
  set isLink(value: boolean) {
    this._isLink = coerceBooleanProp(value);
  }

  @Input() urlButton: string;
  @Input() textButton: string;

  private _isButton: boolean;
  @Input()
  get isButton(): boolean {
    return this._isButton;
  }
  set isButton(value: boolean) {
    this._isButton = coerceBooleanProp(value);
  }

  image: string;
  size: string;

  constructor(
    protected _elementRef: ElementRef
  ) {
    super(_elementRef);
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    this.size = `g-site-size--${this.itemsSizes
                            ? this.itemsSizes
                              .normalize('NFD')
                              .replace(/[\u0300-\u036f]/g, '')
                              .toLowerCase()
                            : ''}`;

    if (Array.isArray(this.backgroundImage) && this.backgroundImage.length) {
      this.image = this.backgroundImage[0].large.imageUri;
    }
  }

}
