import { getStorageLanguage, LANGUAGES } from '@bvl/library';

const TEXT = LANGUAGES[
  getStorageLanguage()
    .substring(0, 2)
];

export const CHART_COLORS = [
  {
    name: 'grey',
    lineColor: 'rgba(120, 120, 120, 1)',
    stops: ['rgba(120, 120, 120, 0.1)', 'rgba(120, 120, 120, 0)']
  },
  {
    name: 'yellow',
    lineColor: 'rgba(242, 225, 29, 0.9)',
    stops: ['rgba(242, 225, 29, 0.1)', 'rgba(242, 225, 29, 0)']
  },
  {
    name: 'blue',
    lineColor: 'rgba(41, 168, 243, 1)',
    stops: ['rgba(41, 168, 243, 0.1)', 'rgba(41, 168, 243, 0)']
  },
  {
    name: 'red',
    lineColor: 'rgba(255, 0, 0, 0.9)',
    stops: ['rgba(255, 0, 0, 0.1)', 'rgba(255, 0, 0, 0)']
  },
  {
    name: 'green',
    lineColor: 'rgba(55, 210, 102, 0.9)',
    stops: ['rgba(55, 210, 102, 0.1)', 'rgba(55, 210, 102, 0)']
  },
  {
    name: 'purpple',
    lineColor: 'rgba(151, 71, 255, 0.9)',
    stops: ['rgba(151, 71, 255, 0.1)', 'rgba(151, 71, 255, 0)']
  },
  {
    name: 'orange',
    lineColor: 'rgba(255, 118, 0, 0.9)',
    stops: ['rgba(255, 118, 0, 0.1)', 'rgba(255, 118, 0, 0)']
  }
];

export const getConfigDetailsChart = (symbol: string, Highcharts: any, gaAddEvent: any): any => {
  return {
    // tslint:disable
    chart: {
      backgroundColor: '#302e2e',
      type: 'area',
      styledMode: true,
      zoomType: 'x',
      events: {
        render: () => {
          const button: HTMLElement = document.querySelector('.highcharts-button');
          if (button) {
            button.onclick(null);
            button.style.display = 'none';
          }
        }
      }
    },
    lang: {
      decimalPoint: '.',
      thousandsSep: ','
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      gridLineWidth: 1,
      lineColor: 'rgba(170, 170, 170, 0.6)',
      tickColor: 'rgba(170, 170, 170, 0.6)',
      gridLineColor: 'rgba(120, 120, 120, 0.2)',
      categories: [],
      labels: {
        formatter: function () {
          const pos = String(this.pos)
          const lastNumber = pos[pos.length - 1];
          const byTenCondition = pos === '0' || lastNumber === ('0' || '5');

          return byTenCondition
            ? this.value
            : '';
        }
      }
    },
    yAxis: {
      gridLineColor: 'rgba(120, 120, 120, 0.2)',
      title: {
        text: ''
      },
      min: 0,
      labels: {
        formatter: function () {
          return `${symbol} ${Highcharts.numberFormat(this.value, 2)}`;
        }
      }
    },
    rangeSelector: {
      selected: 4
    },
    credits: { enabled: false },
    legend: {
      enabled: false
    },
    formatter: {},
    plotOptions: {
      series: {
        cursor: 'pointer',
        compare: 'porcent',
        showInNavigator: true,
        marker: {
          lineWidth: 1,
          radius: 2
        }
      }
    },
    tooltip: {
      shared: true,
      crosshairs: true,
      borderRadius: 10,
      borderWidth: 0,
      backgroundColor: '#353535',
      style: { color: '#dcdcdc' },
      formatter: function () {
        const context = this;
        let tooltipContent = '';

        context.points.forEach(item => {
          tooltipContent +=
            `
        <strong>${item.series.userOptions.id}</strong>
        <span style="font-size:26px; color:${item.series.userOptions.lineColor}">   •</span><br>
        <span>${TEXT.general.date}: </span><strong>${item.key}</strong><br>
        <span>${TEXT.general.value}: </span><strong>${context.points.length > 1 ? item.y + ' %' : `${symbol} ` + Highcharts.numberFormat(item.y, 2)}</strong><br>
        `;
        });

        return tooltipContent;
      }
    },
    series: [],
    exporting: {
      buttons: {
        contextButton: {
          menuItems: [
            {
              text: '<i class="icon-add"></i>',
              onclick: function () {
                gaAddEvent.chartOption('Aumentar');
                const context = this;
                const max = context.yAxis[0].max;
                const min = context.yAxis[0].min;
                const zoomRange = 1;
                if (max - zoomRange !== min) {
                  context.yAxis[0].setExtremes(
                    +min + zoomRange,
                    max - zoomRange
                  );
                }
              }
            },
            {
              text: '<i class="icon-minus-circle"></i>',
              onclick: function () {
                gaAddEvent.chartOption('Disminuir');
                const context = this;
                const max = context.yAxis[0].max;
                const min = context.yAxis[0].min;
                const zoomRange = 1;
                if (max > min) {
                  context.yAxis[0].setExtremes(
                    min - zoomRange,
                    +max + zoomRange
                  );
                }
              }
            },
            {
              text: '<i class="icon-camera"></i>',
              onclick: function () {
                gaAddEvent.chartOption('Descargar imagen');
                this.exportChart({
                  type: 'image/png'
                });
              }
            },
            {
              text: 'PDF',
              onclick: function () {
                gaAddEvent.chartOption('Descargar PDF');
                this.exportChart({
                  type: 'application/pdf'
                });
              }
            }
          ]
        }
      }
    }
  };
};
