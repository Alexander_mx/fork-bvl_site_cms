import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AutocompleteModule } from '@bvl/library';
import { IssuerDetailsServices } from 'modules/issuer-details/src/providers/services';
import { DetailsChartComponent } from './details-chart.component';

@NgModule({
  declarations: [
    DetailsChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AutocompleteModule
  ],
  providers: [IssuerDetailsServices],
  exports: [DetailsChartComponent],
  entryComponents: [DetailsChartComponent]
})
export class DetailsChartModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'bvl-details-chart'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(DetailsChartComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
