import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import * as Highcharts from 'highcharts';
import HighchartsExporting from 'highcharts/modules/exporting';
import { takeUntil } from 'rxjs/operators';

import { getCurrencySymbol } from '@bvl-core/shared/helpers/util/currency';
import { DateUtil, GaUnsubscribeBase, getStorageLanguage, LANGUAGES } from '@bvl/library';

import {
  IFiltersQuotation,
  IindexChartRequest,
  IShareValueResponse,
  IShareValuesResponse
} from 'modules/issuer-details/src/app/issuer-details';
import { IssuerDetailsServices } from 'modules/issuer-details/src/providers/services';
import { CHART_COLORS, getConfigDetailsChart } from './details-chart-options';
HighchartsExporting(Highcharts);

@Component({
  selector: 'bvl-details-chart',
  templateUrl: './details-chart.component.html',
  styleUrls: ['./details-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetailsChartComponent extends GaUnsubscribeBase implements AfterContentChecked, OnChanges, OnInit, OnDestroy {
  private _allNemonicsData: any;
  private _initDate: any;
  @Input() indexName: string;
  chartData: any = [];
  selectedTime = 365;
  nemonic: string;
  nemonics: Array<string>;
  allNemonics: Array<string>;
  nemonicsToCompare: Array<{ name: string; color: string }> = [];
  allSeries: Array<{ name: string; data: Array<{ name: string; y: number }> }> = [];
  active: boolean;
  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  colors: Array<any> = CHART_COLORS;
  @Input() selectData?: Array<any> = [];
  chartOption: any;

  constructor(
    protected _element: ElementRef,
    private cdRef: ChangeDetectorRef,
    private detailsChartServices: IssuerDetailsServices
  ) {
    super(_element);
  }

  ngOnInit(): void {
    if (this.ga) {
      this.chartOption = this.ga.find(fv => fv.control === 'chartOption');
    }

    this.cdRef.detectChanges();
    if (!this.indexName) {
      this._getAllNemonics();
    } else {
      this._getIndexChart({
        nemonico: this.indexName,
        startDate: this._dateSeleted(this.selectedTime)[0],
        endDate: this._dateSeleted(this.selectedTime)[1]
      });
    }
  }

  ngOnChanges(change: any): void {
    if (change.selectData && change.selectData.currentValue[0] !== undefined) {
      this.nemonics = change.selectData.currentValue[0];
      this.nemonic = change.selectData.currentValue[1]
        ? change.selectData.currentValue[1]
        : this.nemonics[0];

      const dates = this._dateSeleted(7);
      this._getLastActivityDay({
        name: this.nemonic,
        startDate: dates[0],
        endDate: dates[1]
      });
    }
  }

  ngAfterContentChecked(): void {
    this.cdRef.detectChanges();
  }

  onNemonicChange(nemonic): void {
    const dates = this._dateSeleted(this.selectedTime);
    this._getLastActivityDay({
      name: nemonic,
      startDate: dates[0],
      endDate: dates[1]
    });
    this.allNemonics = this._allNemonicsData.filter(item => item.nemonico !== this.nemonic);
    this.nemonicsToCompare = [];
  }

  onItemSelected(item): void {
    const dates = this._dateSeleted(this.selectedTime);
    this.active = item.active;
    const body: IFiltersQuotation = {
      name: item.item,
      startDate: dates[0],
      endDate: dates[1]
    };
    if (this.nemonicsToCompare.length < this.colors.length) {
      this.detailsChartServices.getStockChart(body)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(data => {
          const findNemonic = this.nemonicsToCompare.find(compare => (compare as any).name === item.item);
          if (!findNemonic) {
            this.nemonicsToCompare.push(
              {
                name: item.item,
                color: this.colors[this.nemonicsToCompare.length + 1]
              }
            );

            this.allNemonics = this.allNemonics.filter(nemonic => {
              return !this.nemonicsToCompare.some(compare => {
                return (nemonic as any).nemonico === (compare as any).name;
              });
            });

            const values = this._sortByDate(data.values);

            const currentVal = {
              name: item.item,
              data: values.map(val => {
                return {
                  name: val[0],
                  y: +val[1]
                };
              })
            };
            this.chartData.push(currentVal);
            this._loadChart();
          }
        });
    }
  }

  getChartByDate(): void {
    const body = {
      startDate: (() => {
        if (this.selectedTime !== 1) {
          return this._dateSeleted(this.selectedTime)[0];
        } else {
          const selectedYesterday = new Date(`${this._initDate}T00:00`).getDate() - 1;
          const setYesterday = new Date().setDate(selectedYesterday);

          return DateUtil.dateToString(new Date(setYesterday), 'yyyy-MM-dd');
        }
      })(),
      endDate: this._initDate,

      [this.indexName ? 'nemonico' : 'nemonicos']: (() => {
        if (this.indexName) {
          return this.indexName;
        }

        return (() => {
          const allNemonics = [];
          allNemonics[0] = this.nemonic;

          this.nemonicsToCompare.forEach(item => {
            allNemonics.push(item.name);
          });

          return allNemonics;
        })();
      })()
    };
    this._getAllCharts(body);
  }

  removeNemonic(nemonic): void {
    this.chartData = this.chartData.filter(item => item.name !== nemonic.name);
    this.allNemonics.push(this._allNemonicsData.find(item => item.nemonico === nemonic.name));
    this.nemonicsToCompare = this.nemonicsToCompare.filter(item => item.name !== nemonic.name);
    this.nemonicsToCompare.forEach((item, index) => item.color = this.colors[index + 1]);
    this._loadChart();
  }

  private _dateSeleted(dateFilter: number): Array<any> {

    const date = (() => {
      if (this._initDate) {
        return new Date(`${this._initDate}T00:00`);
      } else {
        return new Date();
      }
    })();

    const startDate = (() => {
      switch (dateFilter) {
        case 365:
          return date.setFullYear(date.getFullYear() - 1);
        case 180:
          return date.setMonth(date.getMonth() - 6);
        case 90:
          return date.setMonth(date.getMonth() - 3);
        case 30:
          return date.setMonth(date.getMonth() - 1);
        default:
          return new Date().setDate(new Date().getDate() - dateFilter);
      }
    })();

    return DateUtil.setDefaulDate([
      new Date(startDate),
      new Date()
    ]);
  }

  private _getAllNemonics(): void {
    this.detailsChartServices.getAllNemonics()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this._allNemonicsData = response;
        this.allNemonics = response.filter(item => item.nemonico !== this.nemonic);
      });
  }

  private _getIndexChart(params: IindexChartRequest): void {
    this.detailsChartServices.getIndexChart(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.chartData = [];
        response.forEach((item, index) => {
          this.chartData[index] = [item.date, item.value];
        });
        this._loadChart();
      });
  }

  private _getLastActivityDay(params: IFiltersQuotation): void {
    this.detailsChartServices.getStockChart(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        if (response.values && response.values.length) {
          const values = this._sortByDate(response.values);
          this._initDate = values[values.length - 1][0];
          this.getChartByDate();
        }
      });
  }

  private _getAllCharts(body: any): void {
    this.detailsChartServices.getNemonicsCharts(body)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IShareValuesResponse) => {
        this.chartData = [];
        const symbol = response && response.shares && response.shares[0] && response.shares[0].currencySymbol;

        this.chartData = response.shares
          .map((item: IShareValueResponse) => {
            return {
              name: item.nemonico,
              data: this._sortByDate(item.values)
                .map(value => {
                  return {
                    name: value[0],
                    y: +value[1]
                  };
                })
            };
          });
        this._loadChart(symbol);
      });
  }

  private _minValue(data): number {
    const values = [];
    data.forEach(item => {
      item.data.forEach(value => {
        values.push(value.y);
      });
    });

    return Math.min.apply(null, values) - 0.5;
  }

  private _sortByDate(values): Array<any> {
    return values.sort((a, b) => a[0].localeCompare(b[0]));
  }

  private _xAxisCategories(seriesData): Array<string> {
    const lengths = seriesData.map(val => val.data.length);

    const longestSerie = seriesData.find(val => val.data.length === Math.max.apply(null, lengths));

    const getDaysArray = (start, end) => {
      const arr = [];
      for (start; start <= end; start.setDate(+start.getDate() + 1)) {
        arr.push(new Date(start));
      }

      return arr;
    };

    const daylist = getDaysArray(
      new Date(longestSerie.data[0].name),
      new Date(longestSerie.data[longestSerie.data.length - 1].name)
    );

    return daylist.map(v => v
      .toISOString()
      .slice(0, 10));
  }
  private setValueByVariation(value, initValue): number {
    return +(((+(value || 0) - initValue) / initValue) * 100).toFixed(3);
  }
  private _loadChart(symbol: string = null): void {
    const currencySymbol = getCurrencySymbol(symbol);
    const gaAddEvent = {
      chartOption: (chartOption: string) => {
        this.addEvent(this.chartOption, { chartOption });
      }
    };
    const options = getConfigDetailsChart(currencySymbol, Highcharts, gaAddEvent);
    options.series = [];

    Highcharts.chart('details-chart', (() => {
      this.chartData.forEach((first, firstIndex) => {
        const initValue = first.data[0] && first.data[0].y;
        options.series.push({
          id: first.name,
          data: first.data.map(second => {
            return {
              name: second.name,
              y: (() => {
                if (this.chartData.length > 1) {
                  return this.setValueByVariation(second.y, initValue);
                }

                return +second.y || 0;
              })()
            };
          }),
          lineColor: this.colors[firstIndex].lineColor,
          color: {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
              [0, this.colors[firstIndex].stops[0]],
              [1, this.colors[firstIndex].stops[1]]
            ]
          }
        });
      });

      options.xAxis.categories = this._xAxisCategories(this.chartData);
      options.yAxis.min = this.chartData.length > 1 ? null : this._minValue(this.chartData);
      if (this.chartData.length > 1) {
        options.yAxis.labels = {
          // tslint:disable
          formatter: function () {
            return `${this.axis.defaultLabelFormatter.call(this)}%`;
          }
        }
      }

      return options;
    })());
  }
}
