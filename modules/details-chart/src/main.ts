import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { DetailsChartModule } from './app/details-chart.module';
platformBrowserDynamic().bootstrapModule(DetailsChartModule)
  .catch(err => console.error(err));
