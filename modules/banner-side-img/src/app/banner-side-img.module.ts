import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { DomChangeDirectiveModule } from '@bvl/library';
import { BannerSideImgComponent } from './banner-side-img.component';

@NgModule({
  declarations: [
    BannerSideImgComponent
  ],
  imports: [
    BrowserModule,
    DomChangeDirectiveModule
  ],
  providers: [],
  entryComponents: [BannerSideImgComponent]
})
export class BannerSideImgModule {
  constructor(private injector: Injector) { }
  ngDoBootstrap(): void {
    const name = 'bvl-banner-side-img'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BannerSideImgComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
