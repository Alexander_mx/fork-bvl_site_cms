import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BannerSideImgModule } from './app/banner-side-img.module';

platformBrowserDynamic()
.bootstrapModule(BannerSideImgModule)
  .catch(err => console.error(err));
