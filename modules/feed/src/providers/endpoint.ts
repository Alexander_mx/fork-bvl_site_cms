import { environment } from '../environments/environment';
export class FeedEndPoints {
    public static instagram = `https://api.instagram.com/v1/users/self/media/recent/`;
    public static twitter = `${environment.API_URL}/twitter/timeline`;
}
