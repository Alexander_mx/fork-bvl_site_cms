import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@bvl/library';
import { FeedEndPoints } from './endpoint';

@Injectable()
export class FeedServices {
  constructor(private _apiService: ApiService) {}

  getInstagramPosts(params): Observable<any> {
    return this._apiService.get(FeedEndPoints.instagram, { params });
  }
  getTweets(params): Observable<any> {
    return this._apiService.post(FeedEndPoints.twitter, params);
  }
}
