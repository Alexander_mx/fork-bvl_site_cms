import { Component, ElementRef, Input, OnInit } from '@angular/core';

import { DateUtil } from '@bvl-core/shared/helpers/util';
import { NETWORKS_KEYS } from '@bvl-site/settings/constants/networks-keys';
import { GaUnsubscribeBase, getStorageLanguage, InstagramData, ITwitterData, LANGUAGES } from '@bvl/library';
import { FeedServices } from '../providers/feed.services';

@Component({
  selector: 'bvl-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent extends GaUnsubscribeBase implements OnInit {

  @Input() title?: string;
  twitter: ITwitterData;
  instagram: InstagramData;
  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)
  ];

  constructor(
    protected _element: ElementRef,
    private _feedServices: FeedServices
  ) {
    super(_element);
  }

  setEventsGA(): void { }

  ngOnInit(): void {
    this._getTweetPosts();
    this._getInsPosts();
  }

  private _getTweetPosts(): void {
    this._feedServices.getTweets({ page: 1, count: 10 })
    .subscribe(response => this.twitter = this._setTweets(response));
  }

  private _getInsPosts(): void {
    this._feedServices.getInstagramPosts({access_token: NETWORKS_KEYS.instagram, count: 6})
    .subscribe(response => this.instagram = this._setPost(response.data));
  }
  private _setPost(data: any): InstagramData {
    return {
      accountUrl: 'https://www.instagram.com/bvloficial/',
      posts: (() => {
        const postImage = data.filter(item => item.videos === undefined);

        return postImage.map(item => {
          return {
            caption: item.caption.text,
            imageUrl: item.images.standard_resolution.url,
            url: item.link
          };
        });
      })()
    };
  }
  private _setTweets(data: any): ITwitterData {
    return {
      accountUrl: 'https://twitter.com/bvlperu',
      tweets: data.map(item => {
        const dateHour = item.createdAt;

        return {
          author: item.user.name,
          time: DateUtil.getDateTimeStringFormat(dateHour),
          text: item.text,
          url: item.urlentities.length ? item.urlentities[0].expandedURL : '#'
        };
      })
    };
  }
}
