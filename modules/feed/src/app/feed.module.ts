import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { ApiModule, CarouselModule, DomChangeDirectiveModule, EllipsisModule, SectionHeaderModule } from '@bvl/library';
import { FeedServices } from '../providers/feed.services';
import { FeedComponent } from './feed.component';

@NgModule({
  declarations: [
    FeedComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    EllipsisModule,
    ApiModule,
    CarouselModule,
    DomChangeDirectiveModule
  ],
  providers: [
    FeedServices
  ],
  entryComponents: [FeedComponent]
})
export class FeedModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-feed'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(FeedComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
