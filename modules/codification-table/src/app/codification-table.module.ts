import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import { TableServices } from '@bvl/table-services';
import { ICodificationTableComponent } from './codification-table.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FlexLayoutModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule,
    FormsModule
  ],
  providers: [TableServices],
  declarations: [ICodificationTableComponent],
  exports: [ICodificationTableComponent],
  entryComponents: [ICodificationTableComponent]
})
export class ICodificationTableModule {
  constructor(private injector: Injector) { }
  ngDoBootstrap(): void {
    const name = 'bvl-codification-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ICodificationTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
