import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { getStorageLanguage, LANGUAGES } from '@bvl/library';

import { TableServices } from '@bvl/table-services';
import { IFancyTableByTypeFilter } from 'modules/daily-table/src/app/fancy-table-header/fancy-table-header';
import { ICodificationRequest, ICodificationTable } from 'modules/fancy-table/src/app/fancy-table';
import { TABLE_INITIALS } from 'modules/issuers-list-table/src/app/tables';

@Component({
  selector: 'bvl-codification-table',
  templateUrl: './codification-table.component.html',
  styleUrls: ['./codification-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ICodificationTableComponent implements OnInit {

  TEXT = LANGUAGES[
    getStorageLanguage()
    .substring(0, 2)
  ];

  activeFilter = 1;
  letters: Array<string> = [this.TEXT.filters.all, ...TABLE_INITIALS];
  currentLetter: string = this.letters[0];
  sectors: IFancyTableByTypeFilter;
  sector: any;
  tableData: Array<ICodificationTable>;
  showData = 0;

  private _defaultFilter: ICodificationRequest = {
    letter: this.currentLetter,
    sector: ''
  };

  constructor(public tableServices: TableServices) {}

  ngOnInit(): void {
    this._getSectors();
    this._getCodificationData();
  }
  onFilterChange(value: string, type: string): void {
    const filters = JSON.parse(JSON.stringify(this._defaultFilter));
    filters[type] = value;
    this._getCodificationData(filters);
  }
  getSelectedLabel(sector, objects): string {
    if (!sector) {
      return objects.label;
    }
    const findObject = objects.options.find(item => item.id === sector.id);

    return findObject ? findObject.label : '';
  }
  private _getSectors(): void {
    this.tableServices.getSectors()
    .subscribe(response => {
      this.sectors = {
        label: 'SECTOR',
        options: [{ id: 0, label: 'TODOS', code: '' }]
      };
      response.map((item, index) => {
        this.sectors.options.push({
          id: index,
          label: item.description,
          code: item.sectorCode
        });
      });
      this.sectors.options.push({ id: 10000, label: 'OTRAS INSTITUCIONES', code: 'OI' });
    });
  }
  private _getCodificationData(filters?: ICodificationRequest): void {
    if (!filters) {
      filters = this._defaultFilter;
    }
    this.tableServices.getCodifications(filters)
    .subscribe(response => {
      this.tableData = this._setTableData(response);
    });
  }
  private _setTableData(data: any): Array<ICodificationTable> {

    return data.map(item => {
      return {
        name: item.businnesName,
        data: item.codeRelations.map(elem => {
          return {
            mode: elem.mode,
            isin: elem.isin,
            nemonic: elem.nemonico,
            group: elem.group,
            type: elem.type,
            currency: elem.currency,
            nominalValue: elem.nominalValue,
            rate: elem.rate
          };
        })
      };
    });
  }
}
