import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ICodificationTableModule } from './app/codification-table.module';

platformBrowserDynamic().bootstrapModule(ICodificationTableModule)
  .catch(err => console.error(err));
