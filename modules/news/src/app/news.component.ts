import { Component, ElementRef, HostBinding, NgZone } from '@angular/core';
import { takeUntil } from 'rxjs/operators';

import { GaUnsubscribeBase, getStorageLanguage, IEntry, IFilter, LANGUAGES } from '@bvl/library';
import {
  INewsRequest,
  newsDefaultRequest,
  newsDefaultTypes,
  setAnnouncements,
  setImportantFacts
} from 'modules/issuers-news/src/app/issuers-news';
import { IssuerNewsServices } from 'modules/issuers-news/src/providers/services';

@Component({
  selector: 'bvl-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent extends GaUnsubscribeBase {

  @HostBinding('attr.class') attrClass = 'g-transparent g-transparent--gray';

  private _filter: INewsRequest = JSON.parse(JSON.stringify(newsDefaultRequest));
  filters: Array<IFilter> = newsDefaultTypes.options;
  news: Array<IEntry>;
  activeFilter: IFilter = this.filters[0];

  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];

  constructor(
    protected _element: ElementRef,
    public issuerNewsServices: IssuerNewsServices,
    private _ngZone: NgZone
  ) {
    super(_element);
    this._getNews(this._filter);
  }

  setEventsGA(): void { }

  onFilterChanged(selectedFilterId: number): void {
    this.activeFilter = this.filters.find(filter => filter.id === selectedFilterId);
    this._filter.type = (this.activeFilter as any).type;
    this._getNews(this._filter);
  }

  private _getNews(filters: INewsRequest): void {
    this.issuerNewsServices.getNews(filters)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this._ngZone.run(() => {
          this.news = [];
          const values: Array<IEntry> = [];
          Object.keys(response.content)
            .forEach(key => {
              response.content[key].map(item => {
                const value = ((key === 'announcements')
                  ? setAnnouncements(item, key)
                  : setImportantFacts(item, key));
                values.push(value);
              });
            });

          const withoutDate = values.filter(value => !value.date);
          const withDate = values.filter(value => value.date)
            .sort((a, b) => b.date.getTime() - a.date.getTime());

          this.news = [...withDate, ...withoutDate];
        });
      });
  }

}
