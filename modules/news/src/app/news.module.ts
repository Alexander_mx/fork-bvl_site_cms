import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';

import { NotificationModule } from '@bvl-core/shared/helpers/notification';
import { SpinnerModule } from '@bvl-core/shared/helpers/spinner';
import {
  CarouselModule,
  EntryModule,
  FiltersModule,
  PipesModule,
  SectionHeaderModule
} from '@bvl/library';
import { IssuerNewsServices } from 'modules/issuers-news/src/providers/services';
import { NewsComponent } from './news.component';

@NgModule({
  declarations: [
    NewsComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    PipesModule,
    SectionHeaderModule,
    FiltersModule,
    CarouselModule,
    EntryModule,
    FlexLayoutModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule
  ],
  providers: [
    IssuerNewsServices
  ],
  entryComponents: [NewsComponent]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'bvl-news'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(NewsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
