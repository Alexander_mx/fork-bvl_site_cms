import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewEncapsulation } from '@angular/core';
import * as Highcharts from 'highcharts';
import * as HighchartsData from 'highcharts/modules/data';
import HighchartsDrilldown from 'highcharts/modules/drilldown';
import HighchartsExporting from 'highcharts/modules/exporting';

import { getCurrentDateISO } from '@bvl-core/shared/helpers/util/date';
import { StringSlugify } from '@bvl-core/shared/helpers/util/string';
import { GaUnsubscribeBase, getStorageLanguage, GroupByPipe, LANGUAGES } from '@bvl/library';
import { TableServices } from '@bvl/table-services';

HighchartsExporting(Highcharts);
HighchartsData(Highcharts);
HighchartsDrilldown(Highcharts);

@Component({
  selector: 'bvl-resume-chart',
  templateUrl: './resume-chart.component.html',
  styleUrls: ['./resume-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe, GroupByPipe]
})
export class ResumeChartComponent extends GaUnsubscribeBase implements OnInit {

  private _defaultRange = 1;

  selectedTime = 1;
  dataChart: Array<any>;
  dataDrilldownChart: Array<any>;

  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];
  chartOption: any;
  showLoader: boolean;

  constructor(
    private _datePipe: DatePipe,
    private _groupByPipe: GroupByPipe,
    private _tableServices: TableServices,
    protected _element: ElementRef
  ) {
    super(_element);
  }

  ngOnInit(): void {
    if (this.ga) {
      this.chartOption = this.ga.find(fv => fv.control === 'chartOption');
    }
    this._getNegotiatedAmout();
  }

  onTimeChange(daysRange): void {
    this.selectedTime = daysRange;
    this._getNegotiatedAmout(this.selectedTime);
  }

  private _getNegotiatedAmout(daysRange?: number): void {
    const date = new Date(new Date().setDate(new Date().getDate() - (daysRange || this._defaultRange)));
    this.showLoader = true;
    this._tableServices.getResumeChart(date)
      .subscribe(this._mapNegotiatedAmout.bind(this), this._mapNegotiatedAmout.bind(this));
  }

  private _mapNegotiatedAmout(response: Array<any>): void {
    if (!response || response && !response.length) {
      this.dataChart = [];
      this.showLoader = false;

      return void 0;
    }

    this.dataDrilldownChart = [];
    const group = this.selectedTime >= 365
      ? this._loadByYear(response)
      : this.selectedTime >= 30
        ? this._loadByWeek(response)
        : this._loadByDay(response);

    const soles = { name: 'Soles', data: [] };
    const dollars = { name: this.TEXT.general.dolars, data: [] };

    group.forEach(value => {
      const name = value.groupCode;
      const solesDrillDownName = `${name} - ${soles.name}`;
      const dollarsDrillDownName = `${name} - ${dollars.name}`;

      soles.data.push({
        name,
        drilldown: solesDrillDownName,
        y: value.amountSoles
      });
      dollars.data.push({
        name,
        drilldown: dollarsDrillDownName,
        y: value.amountDollars
      });

      if (this.selectedTime >= 365) {
        this._loadDrillYear(value, solesDrillDownName, dollarsDrillDownName);

        return void 0;
      }

      if (this.selectedTime >= 30) {

        this._loadDrillWeek(value, solesDrillDownName, dollarsDrillDownName);

        return void 0;
      }

      this._loadDrillDay(value, solesDrillDownName, dollarsDrillDownName);
    });

    this.dataChart = [soles, dollars];
    setTimeout(this._loadChart.bind(this), 100);
  }

  private _loadByDay(response): Array<any> {
    const values = response.sort((a, b) => a.createdDate.localeCompare(b.createdDate));
    const format = values.map(value => {
      return {
        ...value,
        createdDate: this._formatDate(value.createdDate, 'dd/MM/yyyy HH:00 - HH:59'),
        createdDateOriginal: this._formatDate(value.createdDate, 'dd/MM/yyyy HH:mm')
      };
    });

    return this._groupByPipe
      .transform(format, 'createdDate')
      .map(item => {
        return {
          ...item,
          amountSoles: this._twoDecimal(item.groupItems.reduce((a, b) => Number(a) + Number(b.amountSoles), 0)),
          amountDollars: this._twoDecimal(item.groupItems.reduce((a, b) => Number(a) + Number(b.amountDollars), 0))
        };
      });
  }

  private _loadByWeek(response): Array<any> {
    const format = response.map(value => {
      return {
        ...value,
        numberWeek: this._formatDate(value.createdDate, 'w'),
        createdDateYear: this._formatDate(value.createdDate, 'yyyy'),
        createdDateMonth: this._formatDate(value.createdDate, 'MM/yyyy'),
        createdDateMonthWeek: this._formatDate(value.createdDate, 'MM/yyyy W'),
        createdDateDay: this._formatDate(value.createdDate, 'dd/MM/yyyy'),
        createdDateHour: this._formatDate(value.createdDate, 'dd/MM/yyyy HH:00 - HH:59'),
        createdDateOriginal: this._formatDate(value.createdDate, 'dd/MM/yyyy HH:mm')
      };
    });

    const weeks = [];
    this._groupByPipe
      .transform(format, 'createdDateYear')
      .forEach(item => {
        const groupWeek = this._groupByPipe.transform(item.groupItems, 'numberWeek');
        groupWeek.forEach(week => {
          week.groupItems = week.groupItems.sort((a, b) => a.createdDate.localeCompare(b.createdDate));
          const groupCode = week.groupItems.length === 1
            ? item.groupItems[0].createdDate
            : `${
            this._formatDate(week.groupItems[0].createdDate, 'dd/MM/yyyy')
            } - ${
            this._formatDate(week.groupItems[week.groupItems.length - 1].createdDate, 'dd/MM/yyyy')
            }`;

          weeks.push({
            ...week,
            groupCode,
            amountSoles: this._twoDecimal(week.groupItems.reduce((a, b) => Number(a) + Number(b.amountSoles), 0)),
            amountDollars: this._twoDecimal(week.groupItems.reduce((a, b) => Number(a) + Number(b.amountDollars), 0))
          });
        });
      });

    return weeks;
  }

  private _loadByYear(response): Array<any> {
    const values = response.sort((a, b) => a.createdDate.localeCompare(b.createdDate));
    const format = values.map(value => {
      return {
        ...value,
        numberWeek: this._formatDate(value.createdDate, 'w'),
        createdDateYear: this._formatDate(value.createdDate, 'yyyy'),
        createdDateMonth: this._formatDate(value.createdDate, 'MM/yyyy'),
        createdDateMonthWeek: this._formatDate(value.createdDate, 'MM/yyyy W'),
        createdDateDay: this._formatDate(value.createdDate, 'dd/MM/yyyy'),
        createdDateHour: this._formatDate(value.createdDate, 'dd/MM/yyyy HH:00 - HH:59'),
        createdDateOriginal: this._formatDate(value.createdDate, 'dd/MM/yyyy HH:mm')
      };
    });

    const months = [];

    const groupMonths = this._groupByPipe.transform(format, 'createdDateMonth');
    groupMonths.forEach(month => {
      months.push({
        ...month,
        amountSoles: this._twoDecimal(month.groupItems.reduce((a, b) => Number(a) + Number(b.amountSoles), 0)),
        amountDollars: this._twoDecimal(month.groupItems.reduce((a, b) => Number(a) + Number(b.amountDollars), 0))
      });
    });

    return months;
  }

  private _loadDrillDay(value: any, solesDrillDownName: string, dollarsDrillDownName: string): void {
    const solesDrillDown = { name: 'Soles', id: solesDrillDownName, data: [] };
    const dollarsDrillDown = { name: this.TEXT.general.dolars, id: dollarsDrillDownName, data: [] };

    value.groupItems.forEach(groupItem => {
      solesDrillDown.data.push({
        name: groupItem.createdDateOriginal,
        y: groupItem.amountSoles
      });
      dollarsDrillDown.data.push({
        name: groupItem.createdDateOriginal,
        y: groupItem.amountDollars
      });
    });

    this.dataDrilldownChart.push(solesDrillDown);
    this.dataDrilldownChart.push(dollarsDrillDown);
  }

  private _loadDrillWeek(value: any, solesDrillDownName: string, dollarsDrillDownName: string): void {
    const solesDrillDown = { name: 'Soles', id: solesDrillDownName, data: [] };
    const dollarsDrillDown = { name: this.TEXT.general.dolars, id: dollarsDrillDownName, data: [] };

    const groupDays = this._groupByPipe.transform(value.groupItems, 'createdDateDay');

    groupDays.forEach(groudDay => {
      const name = groudDay.groupCode;
      const solesDrillDownNameSecond = `${name} - ${solesDrillDown.id}`;
      const dollarsDrillDownNameSecond = `${name} - ${dollarsDrillDown.id}`;

      solesDrillDown.data.push({
        name,
        drilldown: solesDrillDownNameSecond,
        y: this._twoDecimal(groudDay.groupItems.reduce((a, b) => Number(a) + Number(b.amountSoles), 0))
      });
      dollarsDrillDown.data.push({
        name,
        drilldown: dollarsDrillDownNameSecond,
        y: this._twoDecimal(groudDay.groupItems.reduce((a, b) => Number(a) + Number(b.amountDollars), 0))
      });

      const solesDrillDownSecond = { name: 'Soles', id: solesDrillDownNameSecond, data: [] };
      const dollarsDrillDownSecond = { name: this.TEXT.general.dolars, id: dollarsDrillDownNameSecond, data: [] };

      groudDay.groupItems.forEach(itemsDay => {
        const nameDrill = itemsDay.createdDateOriginal;
        solesDrillDownSecond.data.push({
          name: nameDrill,
          id: solesDrillDownNameSecond,
          y: itemsDay.amountSoles
        });
        dollarsDrillDownSecond.data.push({
          name: nameDrill,
          id: dollarsDrillDownNameSecond,
          y: itemsDay.amountDollars
        });
      });

      this.dataDrilldownChart.push(solesDrillDownSecond);
      this.dataDrilldownChart.push(dollarsDrillDownSecond);
    });

    this.dataDrilldownChart.push(solesDrillDown);
    this.dataDrilldownChart.push(dollarsDrillDown);
  }

  private _loadDrillYear(value: any, solesDrillDownName: string, dollarsDrillDownName: string): void {
    const solesDrillDown = { name: 'Soles', id: solesDrillDownName, data: [] };
    const dollarsDrillDown = { name: this.TEXT.general.dolars, id: dollarsDrillDownName, data: [] };

    const groupWeeks = this._groupByPipe.transform(value.groupItems, 'createdDateMonthWeek');

    groupWeeks.forEach(groupWeek => {
      const name = groupWeek.groupItems.length === 1
        ? groupWeek.groupItems[0].createdDate
        : `${
        this._formatDate(groupWeek.groupItems[0].createdDate, 'dd/MM/yyyy')
        } - ${
        this._formatDate(groupWeek.groupItems[groupWeek.groupItems.length - 1].createdDate, 'dd/MM/yyyy')
        }`;

      const solesDrillDownNameSecond = `${name} - ${solesDrillDown.id}`;
      const dollarsDrillDownNameSecond = `${name} - ${dollarsDrillDown.id}`;

      solesDrillDown.data.push({
        name,
        drilldown: solesDrillDownNameSecond,
        y: this._twoDecimal(groupWeek.groupItems.reduce((a, b) => Number(a) + Number(b.amountSoles), 0))
      });
      dollarsDrillDown.data.push({
        name,
        drilldown: dollarsDrillDownNameSecond,
        y: this._twoDecimal(groupWeek.groupItems.reduce((a, b) => Number(a) + Number(b.amountDollars), 0))
      });

      const solesDrillDownSecond = { name: 'Soles', id: solesDrillDownNameSecond, data: [] };
      const dollarsDrillDownSecond = { name: this.TEXT.general.dolars, id: dollarsDrillDownNameSecond, data: [] };

      const groudDays = this._groupByPipe.transform(groupWeek.groupItems, 'createdDateDay');
      groudDays.forEach(groupDay => {
        const nameDrill = groupDay.groupCode;
        solesDrillDownSecond.data.push({
          name: nameDrill,
          id: solesDrillDownNameSecond,
          y: this._twoDecimal(groupDay.groupItems.reduce((a, b) => Number(a) + Number(b.amountDollars), 0))
        });
        dollarsDrillDownSecond.data.push({
          name: nameDrill,
          id: dollarsDrillDownNameSecond,
          y: this._twoDecimal(groupDay.groupItems.reduce((a, b) => Number(a) + Number(b.amountDollars), 0))
        });
      });

      this.dataDrilldownChart.push(solesDrillDownSecond);
      this.dataDrilldownChart.push(dollarsDrillDownSecond);
    });

    this.dataDrilldownChart.push(solesDrillDown);
    this.dataDrilldownChart.push(dollarsDrillDown);
  }

  private _formatDate(value: string, format: string): string {
    return value.indexOf('T') === -1
      ? value
      : this._datePipe.transform(new Date(value), format);
  }

  private _twoDecimal(number: Number): Number {
    return Number(number.toFixed(2));
  }

  private _loadChart(): void {
    this.showLoader = false;
    const filename = `${StringSlugify(this.TEXT.general.marketSummary)}-${getCurrentDateISO()}`;
    const zoomRange = 100;
    Highcharts.setOptions({
      chart: {
        events: {
          render: () => {
            const button: HTMLElement = document.querySelector('.highcharts-button');
            button.onclick(null);
            button.style.display = 'none';
          }
        }
      },
      colors: ['#2b908f', '#90ee7e'],
      lang: {
        decimalPoint: '.',
        thousandsSep: ','
      }
    });
    const chart = Highcharts.chart('resume-chart', {
      chart: {
        type: 'column',
        backgroundColor: '#353535',
        styledMode: true
      },
      title: { text: '' },
      subtitle: { text: '' },
      legend: {
        enabled: true,
        itemStyle: {
          color: 'rgba(255, 255, 255, .5)'
        },
        itemHoverStyle: {
          color: '#FFF'
        },
        itemHiddenStyle: {
          color: '#444'
        }
      },
      lang: {
        drillUpText: `◁ ${this.TEXT.general.goBackCapitalize}`
      },
      xAxis: {
        type: 'category',
        labels: {
          style: { color: 'rgba(255, 255, 255, 1)' },
          enabled: true
        }
      },
      yAxis: {
        gridLineColor: 'rgba(120, 120, 120, 1)',
        title: { text: '' },
        labels: {
          style: {
            color: 'rgba(255, 255, 255, 1)'
          }
        }
      },
      plotOptions: {
        series: {
          allowPointSelect: true,
          showInLegend: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false,
            format: '{y}'
          }
        },
        column: {
          // pointWidth: 40,
          borderWidth: 0
        }
      },
      tooltip: {
        headerFormat: `
          <span>${this.TEXT.general.date}: </span>
          <strong>{point.key}</strong><br>
        `,
        borderRadius: 10,
        borderWidth: 0,
        backgroundColor: '#353535',
        style: { color: '#dcdcdc' }
      },
      series: this.dataChart,
      drilldown: {
        activeAxisLabelStyle: {
          textDecoration: 'none',
          fontStyle: 'regular',
          fontWeight: 'normal',
          color: 'rgba(255, 255, 255, 1)',
          fill: 'rgba(255, 255, 255, 1)'
        },
        drillUpButton: {
          relativeTo: 'spacingBox',
          position: {
            y: 0,
            x: 0
          },
          theme: {
            fill: '#ff0100',
            stroke: '#ff5c5c',
            r: 5,
            states: {
              hover: {
                fill: '#cc0100',
                stroke: '#ff2a29'
              },
              select: {
                stroke: '#039',
                fill: '#a4edba'
              }
            }
          }
        },
        series: this.dataDrilldownChart
      },
      credits: { enabled: false },
      exporting: {
        buttons: {
          contextButton: {
            menuItems: [
              {
                text: '<i class="icon-add"></i>',
                onclick: () => {
                  this.addEvent(this.chartOption, { chartOption: 'Aumentar' });
                  const max = (chart.yAxis[0] as any).max;
                  const min = (chart.yAxis[0] as any).min;
                  if (max - zoomRange !== min) {
                    chart.yAxis[0].setExtremes(
                      +min + zoomRange,
                      max - zoomRange
                    );
                  }
                }
              },
              {
                text: '<i class="icon-minus-circle"></i>',
                onclick: () => {
                  this.addEvent(this.chartOption, { chartOption: 'Disminuir' });
                  const max = (chart.yAxis[0] as any).max;
                  const min = (chart.yAxis[0] as any).min;
                  if (max !== min && min > 0) {
                    chart.yAxis[0].setExtremes(
                      min - zoomRange,
                      +max + zoomRange
                    );
                  }
                }
              },
              {
                text: '<i class="icon-camera"></i>',
                onclick: () => {
                  this.addEvent(this.chartOption, { chartOption: 'Descargar imagen' });
                  chart.exportChart(
                    {
                      type: 'image/png',
                      filename
                    },
                    {
                      chart: {
                        width: 1680,
                        height: 768
                      }
                    }
                  );
                }
              },
              {
                text: 'PDF',
                onclick: () => {
                  this.addEvent(this.chartOption, { chartOption: 'Descargar PDF' });
                  chart.exportChart(
                    {
                      type: 'application/pdf',
                      filename
                    },
                    {
                      chart: {
                        width: 1680,
                        height: 768
                      }
                    }
                  );
                }
              }
            ]
          }
        }
      }
    } as any);
  }
}
