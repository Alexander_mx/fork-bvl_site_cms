import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { ApiModule, DomChangeDirectiveModule, LoaderModule, PipesModule } from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ResumeChartComponent } from './resume-chart.component';

@NgModule({
  declarations: [
    ResumeChartComponent
  ],
  imports: [
    ApiModule,
    BrowserModule,
    DomChangeDirectiveModule,
    FormsModule,
    PipesModule,
    LoaderModule
  ],
  providers: [TableServices],
  entryComponents: [ResumeChartComponent]
})
export class ResumeChartModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'bvl-resume-chart'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ResumeChartComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
