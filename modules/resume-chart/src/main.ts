import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ResumeChartModule } from './app/resume-chart.module';

platformBrowserDynamic().bootstrapModule(ResumeChartModule)
  .catch(err => console.error(err));
