import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';

import {
  ApiModule,
  CarouselModule,
  EntryModule,
  FiltersModule,
  PipesModule,
  SectionHeaderModule,
  WatchListService,
  WithoutDataModule
} from '@bvl/library';
import { BagPlayComponent } from './bag-play.component';

@NgModule({
  declarations: [
    BagPlayComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    PipesModule,
    SectionHeaderModule,
    FiltersModule,
    CarouselModule,
    EntryModule,
    FlexLayoutModule,
    ApiModule,
    WithoutDataModule
  ],
  providers: [
    WatchListService
  ],
  entryComponents: [BagPlayComponent]
})
export class BagPlayModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-bag-play'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(BagPlayComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
