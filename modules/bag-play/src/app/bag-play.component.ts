import { Component, ElementRef, HostBinding, Input, NgZone, OnDestroy, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';

import {
  coerceBooleanProp,
  CommonComponentBase,
  ConfigurationService,
  getStorageLanguage,
  IComponentPropertiesCode,
  IComponentValueResponse,
  IFilter,
  IUserProfile,
  LANGUAGES,
  WatchListService,
  WITHOUT_DATA_TYPE
} from '@bvl/library';
import { environment } from '../environments/environment';
import { ComponentService } from '../providers/component.service';

@Component({
  selector: 'bvl-bag-play',
  templateUrl: './bag-play.component.html',
  styleUrls: ['./bag-play.component.scss']
})
export class BagPlayComponent extends CommonComponentBase implements OnInit, OnDestroy {

  WITHOUT_DATA_TYPE = WITHOUT_DATA_TYPE;

  @HostBinding('attr.class') attrClass = 'g-transparent';

  private _userProfile: IUserProfile;

  private _privateSite: boolean;
  @Input()
  get privateSite(): boolean {
    return this._privateSite;
  }
  set privateSite(value: boolean) {
    this._privateSite = coerceBooleanProp(value);
  }

  bagPlay: IComponentPropertiesCode;
  activeFilter: IFilter;

  TEXT = LANGUAGES[
    getStorageLanguage()
      .substring(0, 2)
  ];
  env = environment.API_URL;

  constructor(
    protected _elementRef: ElementRef,
    private _componentService: ComponentService,
    private _watchListService: WatchListService,
    private _configService: ConfigurationService,
    private _ngZone: NgZone
  ) {
    super(_elementRef);
    this._userProfile = this._configService.getUserProfile();
    this._privateSite = false;
  }

  ngOnInit(): void {
    this._watchListService.setEnviroment(this.env);
    this._componentValue('bvl-bag-play');
  }

  private _componentValue(componentCode: string): void {
    this._componentService.componentValue(this.env, componentCode)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IComponentValueResponse) => {
        const componentValue = this.componentValueByLanguage(response, this.language);

        this._ngZone.run(() => {
          this.bagPlay = this.getComponentValuePropertiesCode(componentValue);
        });

        if (this._userProfile) {
          this._watchListService.getPlayList(this._userProfile.cognitoUsername)
            .pipe(
              takeUntil(this.unsubscribeDestroy$)
            )
            .subscribe(playList => {
              this.bagPlay.videos = this.bagPlay.videos.filter(item => {
                const findVideo = playList.videos.find(video => video.url === item.url);
                item.saved = !!findVideo;

                return (this.privateSite)
                  ? item.saved
                  : !!item;
              });
            });
        }
      });
  }

  changeEntry(video: any): void {
    if (this.privateSite) {
      this.bagPlay.videos.splice(this.bagPlay.videos.indexOf(video), 1);
    }
  }

}
