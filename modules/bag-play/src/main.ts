import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BagPlayModule } from './app/bag-play.module';

platformBrowserDynamic().bootstrapModule(BagPlayModule)
  .catch(err => console.error(err));
