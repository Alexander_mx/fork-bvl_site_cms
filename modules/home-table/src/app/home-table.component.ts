import * as animations from '@angular/animations';
import { Component, ElementRef, EventEmitter, NgZone, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import * as Highcharts from 'highcharts';
import { takeUntil } from 'rxjs/operators';

import { IFancyTableSortedBy } from '@bvl-core/shared/helpers/models/fancy-table.model';
import { pagination } from '@bvl-core/shared/helpers/util';
import { setFilterRequest } from '@bvl-core/shared/helpers/util/body-request';
import { getCurrentDateISO } from '@bvl-core/shared/helpers/util/date';
import { getFancyTableSortBy, sortFancyTable } from '@bvl-core/shared/helpers/util/sort';
import { STATIC_PAGES } from '@bvl-site/settings/constants/general.constant';
import {
  AngularUtil,
  ConfigurationService,
  GaUnsubscribeBase,
  getStorageLanguage,
  IFancyTableColumn,
  IStock,
  LANGUAGES,
  WatchListService
} from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';

import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { INITIAL_BODY } from 'modules/daily-table/src/app/tables';
import {
  IFancyTableData,
  IFancyTableEntry,
  IStockQuote,
  IStockQuoteRequest,
  IUpdateWatchListRequest,
  setLinks
} from '../../../fancy-table/src/app/fancy-table';
import { FancyTableDataSource } from '../../../fancy-table/src/app/fancy-table-data-source';
import { environment } from '../environments/environment';
import { HOME_REQUEST, TABLE_COLUMN } from './data';
import { IEntriesHomeTableSource, IHomeTableSource, IStockQuoteView } from './home-table';

let nextId = 0;

@Component({
  selector: 'bvl-home-table',
  templateUrl: './home-table.component.html',
  styleUrls: ['./home-table.component.scss'],
  animations: [
    animations.trigger('detailExpand', [
      animations.state('collapsed, hidden', animations.style({ height: '0px', minHeight: '0' })),
      animations.state('expanded, shown', animations.style({ height: '*' })),
      animations.transition(
        'expanded <=> collapsed',
        animations.animate('.2s cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class HomeTableComponent extends GaUnsubscribeBase implements OnInit {
  id = `bvl-table-${nextId++}`;
  tablesData: FancyTableDataSource<IHomeTableSource> = {} as FancyTableDataSource<IHomeTableSource>;
  private _userProfile = this._configService.getUserProfile();

  @Output() tableEvents = new EventEmitter<any>();

  tableColumns: Array<IFancyTableColumn> = TABLE_COLUMN;

  tablesTitle: string;
  url: string;
  headerTitle: string;
  chartOptions: any;
  columnsName: Array<string>;
  entriesNumber: number;

  tablesFitInContainer = true;
  viewLoaded = false;
  selectedTable = undefined;
  expandedElement: { tableId: IFancyTableData['id']; element: IStock };

  scrollingTable = false;
  headerCellsWidth: Array<number> = [];

  completeData: Array<IStockQuoteView>;
  maxRows = 6;
  currentPage = 1;
  lang = getStorageLanguage();
  TEXT = LANGUAGES[getStorageLanguage()];
  STATIC_PAGES = STATIC_PAGES;

  constructor(
    private exchange: ExchangeProvider,
    public media: ObservableMedia,
    protected _element: ElementRef,
    private _tableServices: TableServices,
    private _graphicOptions: GraphicOptionsProvider,
    private _configService: ConfigurationService,
    private _watchListService: WatchListService,
    private _ngZone: NgZone
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this._watchListService.setEnviroment(environment.API_URL);
    this._getHomeTableData();
    this.chartOptions = this._graphicOptions.getGraphicOptions();
  }

  onFilterChange(activeFilters: any): void {
    this._getHomeTableData(setFilterRequest(INITIAL_BODY, activeFilters), true);
  }

  getAnimationState(element: IFancyTableEntry, table: IFancyTableData): string {
    const areDetailsShown = table.metadata && table.metadata.expandedElement === element;
    const validGtSm = this.media.isActive('gt-sm') && this.tablesData.data.length === 1;

    return areDetailsShown ? (validGtSm ? 'expanded' : 'shown') : (validGtSm ? 'collapsed' : 'hidden');
  }

  expandElement(element: IEntriesHomeTableSource, table: IFancyTableData): void {
    if (element.code !== 'XXX') {
      if (element === table.metadata.expandedElement) {
        table.metadata.expandedElement = null;

        return;
      }

      table.metadata.expandedElement = element;
      element.detailsShowed = true;
      if (!this._isChartLoaded(element, table)) {
        this._loadChart(element);
        table.metadata.loadedCharts = [
          ...table.metadata.loadedCharts,
          Number(element.id)
        ];
      }
    }
  }

  sortBy(column: IFancyTableColumn, table: IFancyTableData): void {
    const orderedData = sortFancyTable<IStockQuoteView>(column, table, this.completeData);
    const sortBy = getFancyTableSortBy(column, table.metadata);
    this._setData(orderedData, sortBy);
  }

  toggleInWatchList(element: IFancyTableEntry): void {
    if (this._userProfile !== null) {
      const value = !element.inWatchlist;
      const body: IUpdateWatchListRequest = {
        idUser: this._userProfile.cognitoUsername,
        nemonico: element.mnemonic,
        add: value
      };

      this._watchListService.updateStockWatchList(body)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => element.inWatchlist = !element.inWatchlist);

    } else {
      location.href = `${location.origin}/login`;
    }
  }

  private _loadChart(element: IEntriesHomeTableSource): void {
    this._tableServices.getGraph(element.name, getCurrentDateISO())
      .subscribe(response => {
        this.chartOptions.series = [];
        const values = response.sort((a, b) => a.lastDate.localeCompare(b.lastDate));

        this.chartOptions.series.push({
          data: values.map((item: any) => ({
            name: (new Date(`${item.lastDate}+0500`)).toISOString()
            .replace('T', ' ')
            .substring(0, 19),
            y: item.lastValue || 0 }))
        });
        Highcharts.chart(`home-graphic-${element.id}`, this.chartOptions);
      });
  }

  private _isChartLoaded(element: IEntriesHomeTableSource, table: IFancyTableData): boolean {
    return table.metadata.loadedCharts.indexOf(Number(element.id)) !== -1;
  }

  private _setHomeTableData(data: Array<IStockQuoteView>, sortedBy: IFancyTableSortedBy): FancyTableDataSource<IHomeTableSource> {
    data = (data || []);
    const formatedId = (data.length)
      ? data[0].name
        .split(' ')
        .join('_')
        .toLowerCase()
      : '';

    const formatData = {
      id: formatedId,
      entries: data.map((item, index) => {
        return {
          id: index,
          name: item.name,
          price: AngularUtil.formatNumberToString(item.price, 2),
          variation: item.variation,
          negotiatedAmount: AngularUtil.formatNumberToString(item.negotiatedAmount, 2),
          operations: item.operations,
          inWatchlist: item.inWatchlist,
          previousDate: item.previousDate,
          mnemonic: item.mnemonic,
          currency: item.currency,
          links: item.links,
          code: item.code
        } as IEntriesHomeTableSource;
      }),
      metadata: {
        sortedBy,
        expandedElement: null,
        loadedCharts: []
      }
    } as IHomeTableSource;

    return new FancyTableDataSource([formatData], this.id, this.exchange);
  }

  private _getHomeTableData(filters?: IStockQuoteRequest, isWatchList = false): void {
    this._tableServices.getHome(filters || HOME_REQUEST)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this._ngZone.run(() => {
          const completeData = this._getTransformData(response);
          if (this._userProfile !== null) {
            this._watchListService.getStockWatchList(this._userProfile.cognitoUsername)
              .pipe(takeUntil(this.unsubscribeDestroy$))
              .subscribe(data => {
                const stocks = data.stocks.filter(stock => !!stock);
                const dataWatchList = completeData.map(item => {
                  item.inWatchlist = !!stocks.find(el => el.nemonico === item.mnemonic);

                  return item;
                });
                this._setData(
                  (isWatchList)
                    ? dataWatchList.sort((a: IStockQuoteView, b: IStockQuoteView) => +b.inWatchlist - +a.inWatchlist)
                    : dataWatchList
                );
              });
          } else {
            this._setData(completeData);
          }
        });
      });
  }

  private _getTransformData(data: Array<IStockQuote>): Array<IStockQuoteView> {
    return data.map(stock => ({
      name: stock.nemonico,
      price: Number(stock.last),
      variation: Number(stock.percentageChange),
      negotiatedAmount: Number(stock.negotiatedAmount),
      operations: stock.operationsNumber,
      inWatchlist: false,
      previousDate: stock.previousDate,
      mnemonic: stock.nemonico,
      currency: {
        symbol: stock.currency === 'US$' ? '$' : stock.currency
      },
      links: setLinks(stock.companyCode),
      code: stock.companyCode
    }));
  }

  emitChangePage(currentPage: number): void {
    this.currentPage = currentPage;
    this.tablesData = this._setHomeTableData(pagination(this.completeData, this.currentPage, this.maxRows), this.getSortBy());
  }

  getSortBy(): any {
    return this.tablesData.data[0].metadata.sortedBy;
  }

  getTotalPages(): number {
    return Math.ceil((this.completeData || []).length / this.maxRows);
  }

  private _setData(data: Array<IStockQuoteView>, sortedBy: IFancyTableSortedBy = { column: null, ascendingOrder: true }): void {
    this.tablesData = {} as FancyTableDataSource<IHomeTableSource>;
    this.completeData = (data || []);
    this.tablesData = this._setHomeTableData(pagination(this.completeData, this.currentPage, this.maxRows), sortedBy);
  }

  getLabelWatchList(element: IFancyTableEntry): any {
    const action = this._userProfile ? (!element.inWatchlist ? 'Agregar' : 'Quitar') : 'Ir a Login';

    return { ...element, action };
  }

  getUrlViewAll(): string {
    return this.STATIC_PAGES[this.lang]['marketDailyMovement'];
  }

}
