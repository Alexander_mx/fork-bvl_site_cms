export type IFancyTableCapability = 'sort' | 'sector' | 'inWatchList' | '';

export type IFancyTableByInitialFilter = Array<string>;

export interface IFancyTableByTypeFilter {
    label: string;
    options: Array<{ id: number, label: string, code: string }>;
}

export interface IFancyTableByCategoryFilter {
    filterId: number;
    title?: string;
    label?: string;
    defaultOption: number;
    options: Array<{ id: number, label: string }>;
}
