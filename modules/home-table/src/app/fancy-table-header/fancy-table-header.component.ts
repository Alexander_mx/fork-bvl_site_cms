import {
  Component,
  ElementRef,
  EventEmitter,
  NgZone,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { takeUntil } from 'rxjs/operators';

import { ConfigurationService, GaUnsubscribeBase, getStorageLanguage, LANGUAGES } from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import {
  IFancyTableByTypeFilter,
  IFancyTableCapability
} from './fancy-table-header';

@Component({
  selector: 'bvl-home-table-header',
  templateUrl: './fancy-table-header.component.html',
  styleUrls: ['./fancy-table-header.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class HomeTableHeaderComponent extends GaUnsubscribeBase implements OnInit {
  @Output() activeFilters = new EventEmitter<any>();
  sector:  Array<{ id: number, label: string, code: string }>;
  byTypeFilter: IFancyTableByTypeFilter;
  currentFilter: { type: IFancyTableCapability, value: any, filterId?: number };
  selectedType: string | number | null = null;

  private _userProfile = this._configService.getUserProfile();

  TEXT = LANGUAGES[getStorageLanguage()];

  constructor(
    public media: ObservableMedia,
    private _tableServices: TableServices,
    private _configService: ConfigurationService,
    protected _element: ElementRef,
    private _ngZone: NgZone
  ) {
    super(_element);
  }

  ngOnInit(): void {
    this._getSectors();
  }

  onTypeChange(typeValue: IFancyTableCapability, value: any): void {
    this.selectedType = typeValue;
    this._addActiveFilter(typeValue, value);
  }
  isNumber(val: any): boolean {
    return typeof val === 'number';
  }
  getSelectedLabel(code, object): string {
    return object.find(item => item.code === code).label;
  }
  private _addActiveFilter(
    type: IFancyTableCapability,
    value: string | number | boolean,
    filterId = null
  ): void {
    this.currentFilter = { type, value };
    if (filterId) { this.currentFilter.filterId = filterId; }
    this.activeFilters.emit(this.currentFilter);
  }

  private _getSectors(): void {
    this._tableServices.getSectors()
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe(response => {
      this._ngZone.run(() => {
        this.byTypeFilter = {
          label: 'SECTOR',
          options: response.map((item, index) => ({ id: index, label: item.description, code: item.sectorCode }))
        };
      });
    });
  }

  getAction(): string {
    return this._userProfile ? 'Listar movimiento' : 'Ir a Login';
  }
}
