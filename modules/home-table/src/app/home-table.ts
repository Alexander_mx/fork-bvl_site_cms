import { IFancyTableEntry, IFancyTableSortedBy } from '@bvl-core/shared/helpers/models/fancy-table.model';
import { ILink } from '@bvl/library';

export interface IHomeTableSource {
  id: string;
  theme?: string;
  title?: string;
  iconTitle?: string;
  entries: Array<IEntriesHomeTableSource>;
  metadata: {
    sortedBy: IFancyTableSortedBy;
    expandedElement: any;
    loadedCharts: Array<number>;
  };
}

export interface IEntriesHomeTableSource extends IFancyTableEntry {
  id?: string | number;
  name: string;
  price: string;
  variation: number;
  negotiatedAmount: string;
  operations: string;
  inWatchlist: boolean;
  previousDate: string;
  mnemonic: string;
  currency: {
    code?: string;
    symbol: string;
  };
  links: Array<ILink>;
  code: string;
}

export interface IStockQuoteView {
  name: string;
  price: number;
  variation: number;
  negotiatedAmount: number;
  operations: string;
  inWatchlist: boolean;
  previousDate: string;
  mnemonic: string;
  currency: {
    code?: string;
    symbol: string;
  };
  links: Array<ILink>;
  code: string;
}
