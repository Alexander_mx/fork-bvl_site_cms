import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  AmountCellModule,
  ApiModule,
  DomChangeDirectiveModule,
  LabelModule,
  LinksListModule,
  PaginationModule,
  PipesModule,
  ToggleByPositionModule,
  WatchListService
} from '@bvl/library';
import { TableServices } from '@bvl/table-services';
import { ExchangeProvider } from 'core/providers/exchange.provider';
import { GraphicOptionsProvider } from 'core/providers/graphic-options.provider';
import { HomeTableHeaderComponent } from './fancy-table-header/fancy-table-header.component';
import { HomeTableComponent } from './home-table.component';

@NgModule({
  declarations: [
    HomeTableComponent,
    HomeTableHeaderComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AmountCellModule,
    LabelModule,
    LinksListModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    ApiModule,
    PaginationModule,
    PipesModule,
    ToggleByPositionModule,
    DomChangeDirectiveModule
  ],
  entryComponents: [
    HomeTableComponent,
    HomeTableHeaderComponent
  ],
  exports: [
    HomeTableComponent
  ],
  providers: [
    ExchangeProvider,
    GraphicOptionsProvider,
    TableServices,
    WatchListService
  ]
})
export class HomeTableModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'bvl-home-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(HomeTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
