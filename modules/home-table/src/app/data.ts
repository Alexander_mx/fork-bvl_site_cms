import { getStorageLanguage, LANGUAGES } from '@bvl/library';
import { IStockQuoteRequest } from 'modules/fancy-table/src/app/fancy-table';

const TEXT = LANGUAGES[
  getStorageLanguage()
  .substring(0, 2)
];

export let TABLE_COLUMN = [
  {
    name: 'mnemonic',
    label: TEXT.codification.nemonic,
    valueType: 'string'
  },
  {
    name: 'price',
    label: TEXT.tables.last,
    valueType: 'price'
  },
  {
    name: 'variation',
    label: `${TEXT.tables.var}%`,
    valueType: 'percent'
  },
  {
    name: 'negotiatedAmount',
    label: TEXT.general.negotiatedAmount,
    valueType: 'amount'
  },
  {
    name: 'operations',
    label: TEXT.tables.noOp,
    valueType: 'number'
  },
  {
    name: 'inWatchlist',
    label: 'Watchlist',
    valueType: 'boolean'
  }
];

export let HOME_REQUEST: IStockQuoteRequest = {
  sector: '',
  isToday: true,
  companyCode: '',
  inputCompany: ''
};
