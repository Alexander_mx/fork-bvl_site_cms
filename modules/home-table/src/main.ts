import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { HomeTableModule } from './app/home-table.module';
platformBrowserDynamic().bootstrapModule(HomeTableModule)
  .catch(err => console.error(err));
