import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bvl-simple-table',
  templateUrl: './simple-table.component.html',
  styleUrls: ['./simple-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SimpleTableComponent {
  private _data: any;
  @Input()
  get data(): any {
    return this._data;
  }
  set data(value: any) {
    this._data = typeof value === 'string'
      ? JSON.parse(value)
      : value;
  }

  isObject(value: any): boolean {
    return typeof value === 'object' && !Array.isArray(value);
  }
}
