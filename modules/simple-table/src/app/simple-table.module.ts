import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { SimpleTableComponent } from './simple-table.component';

@NgModule({
  declarations: [
    SimpleTableComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  exports: [SimpleTableComponent],
  entryComponents: [SimpleTableComponent]
})
export class SimpleTableModule {
  constructor(private injector: Injector) { }
  ngDoBootstrap(): void {
    const name = 'bvl-simple-table'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(SimpleTableComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
