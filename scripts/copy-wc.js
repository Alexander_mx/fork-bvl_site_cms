const envs = require('./../core/shared/src/helpers/environment/environment.js'),
      copy = require('copy'),
      fs = require('fs'),
      apps = ['bvladmin', 'bvlsite'],
      comp = process.env.npm_config_comp;
      env = process.env.npm_config_env || '';

const _copyWC = (comp, app, env) => {
  return new Promise ((resolve, reject) => {
    const fileToCopy = `dist/${comp}/main.js`,
          pathDes = `apps/${app}/src/${env.WC_BUNDLE_PATH}/${comp}`,
          copiedFile = `${pathDes}/main.js`;

    copy(fileToCopy, pathDes, { flatten: true }, (copyError, file) => {
      if (copyError) {
        reject(copyError);
        return;
      }

      fs.readFile(copiedFile, 'utf8', (rfError, data) => {
        if (rfError) {
          reject(rfError);
          return;
        }

        const newData = data.replace(new RegExp(envs.prod.API_URL, 'g'), env.API_URL);
        fs.writeFile(copiedFile, newData, 'utf8', wfError => {
          if (wfError) {
            reject(rfError);
            return;
          }
          resolve(true);
        });
      });
    });
  }).then(() => {
    console.log('\x1b[36m%s\x1b[0m', `COPIED TO`, env.code);
  }, error => {
    console.log('\x1b[31m%s\x1b[0m', `${error} \n`);
  });
};

const copyWC = async (comp, env) => {
  for (let app of apps) {
    console.log(`COPYING BUNDLE IN... ${app}`);
    if (env) {
      await _copyWC(comp, app, envs[env]);
    } else {
      for (let e in envs) {
        await _copyWC(comp, app, envs[e]);
      }
    }
  }
};

copyWC(comp, env);
