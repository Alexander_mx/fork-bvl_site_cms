const tslintHtmlReport = require('tslint-html-report');
const configSite = {
  tslint: 'apps/bvlsite/tslint.json', // path to tslint.json
  srcFiles: "apps/bvlsite/src/**/*.ts",
  outDir: 'reports/bvlsite', // output folder to write the report to
  html: 'tslint-report.html', // name of the html report generated
  breakOnError: false, // Should it throw an error in tslint errors are found
  typeCheck: true, // enable type checking. requires tsconfig.json
  tsconfig: 'apps/bvlsite/tsconfig.app.json' // path to tsconfig.json
}

const configAdmin = {
  tslint: 'apps/bvladmin/tslint.json', // path to tslint.json
  srcFiles: "apps/bvladmin/src/**/*.ts",
  outDir: 'reports/bvladmin', // output folder to write the report to
  html: 'tslint-report.html', // name of the html report generated
  breakOnError: false, // Should it throw an error in tslint errors are found
  typeCheck: true, // enable type checking. requires tsconfig.json
  tsconfig: 'apps/bvladmin/tsconfig.app.json' // path to tsconfig.json
}

tslintHtmlReport(configSite);
tslintHtmlReport(configAdmin);

