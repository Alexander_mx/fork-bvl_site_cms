const exec = require('child_process').exec,
      comp = process.env.npm_config_comp || 'all';
      env = process.env.npm_config_env || '';

const commandLine = (compName) => {
  return `ng build --project ${compName} --prod --extraWebpackConfig ../../webpack.externals.js --output-hashing none --single-bundle true && npm run copy:wc --comp=${compName} --env=${env}`;
}

const execute = (commandLine, compName, compNumber) => {
  return new Promise ((resolve, reject) => {
    console.log(`\x1b[37m%s\x1b[47m\x1b[30m%s\x1b[0m`, `BUILDING... `, ` ${compName} `, `\n`);
    exec(commandLine, { maxBuffer: 1024 * 500 }, (error, stdout) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(stdout.trim());
    });
  }).then((response) => {
    console.log(response);
    console.log('\x1b[32m%s\x1b[0m', `SUCCESSFUL BUILD`, compName);
    console.log(`---------- ${compNumber} ---------- \n`);
  }, error => {
    console.log('\x1b[31m%s\x1b[0m', `${error} \n`);
  });
};


const buildWC = async (comp) => {
  const components = (comp === 'all')
                        ? [
                            'accordion-resources',
                            'agent-details',
                            'agents-list',
                            'article',
                            'bag-play',
                            'banner-background',
                            'banner-featured',
                            'banner-full',
                            'banner-movil',
                            'banner-side-img',
                            'banner-slider',
                            'banner',
                            'brands',
                            'codification-table',
                            'collapsible-info',
                            'commodities',
                            'daily-table',
                            'details-chart',
                            'entries-table',
                            'fancy-table',
                            'feed',
                            'home-table',
                            'img-info',
                            'index-details',
                            'index-table',
                            'indices',
                            'issuer-details',
                            'issuers-list-table',
                            'issuers-news',
                            'items-grid',
                            'lib-index',
                            'links',
                            'market-table',
                            'news',
                            'offices',
                            'plans',
                            'popup-img',
                            'popup-video',
                            'publications',
                            'rates',
                            'real-time',
                            'resources',
                            'resume-chart',
                            'simple-table',
                            'tab-fidecomisos',
                            'testimonial',
                            'text-block',
                            'today',
                            'contact-us'
                          ]
                        : comp.split(',');

  for (let index = 0; index < components.length; index++) {
    const compNumber = `${(index + 1)}/${components.length}`;
    await execute(commandLine(components[index]), components[index], compNumber);
  }
}

buildWC(comp);
