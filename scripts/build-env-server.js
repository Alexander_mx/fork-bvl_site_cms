const fs = require('fs');

const env = process.env.npm_config_env || 'dev';

fs.writeFile('.env', `ENV=${env}`, errorEnv => {
  if (errorEnv) {
    console.log('\x1b[31m%s\x1b[0m', `${errorEnv} \n`);
    return;
  }

  console.log('\x1b[32m%s\x1b[0m', `SUCCESSFUL ENV`, env);
});
