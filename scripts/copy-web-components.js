const copy = require('copy'),
      comp = process.env.npm_config_comp,
      env = process.env.npm_config_env || 'dev',
      apps = ['bvlsite', 'bvladmin'],
      webComponentsDir = (env === 'prod')
                            ? `src/assets/web-components`
                            : `src/assets/web-components/${env}`;

for (let app of apps) {
  copy(`dist/${comp}/main.*`, `apps/${app}/${webComponentsDir}/${comp}`, {}, (error, file) => {
    if (error) {
      console.log('\x1b[31m%s\x1b[0m', `COPIED ERROR ${error}`);
      return;
    }
    console.log('\x1b[36m%s\x1b[0m', `(${app}) COPIED BUNDLE`, comp);
  });
}
