const fs = require('fs');
const apps = ['bvladmin', 'bvlsite'];
const bundles = require('./bundles.json');

console.log('COPYING UMD BUNDLE...');

for (let app of apps) {
  const pathDes = `apps/${app}/src/assets/bundles`;
  bundles.forEach(bundle => {
    const output = `${pathDes}/${bundle.filename}`;
    fs.copyFileSync(bundle.input, output);
    console.log('\x1b[36m%s\x1b[0m', 'COPY SUCCESS', output);
  })
}
