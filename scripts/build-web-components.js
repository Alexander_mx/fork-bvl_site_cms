const exec = require('child_process').exec,
      comp = process.env.npm_config_comp || 'all',
      env = process.env.npm_config_env || '';

let compNumber = '1/1';

const commandLine = (compName) => {
  const ngBuild = (env === 'prod')
                    ? `ng build --project ${compName} --prod`
                    : `ng build --project ${compName} -c ${env} --optimization=true --sourceMap=false --extractCss=true --namedChunks=false --aot=true --extractLicenses=true --vendorChunk=false --buildOptimizer=true`;

  return `${ngBuild} --extraWebpackConfig ../../webpack.externals.js --output-hashing none --single-bundle true && npm run copy:comp --comp=${compName} --env=${env}`;
}

const execute = (commandLine, compName, compNumber) => {
  return new Promise ((resolve, reject) => {
    console.log(`\x1b[37m%s\x1b[47m\x1b[30m%s\x1b[0m`, `BUILDING... `, ` ${compName} `, `\n`);
    exec(commandLine, { maxBuffer: 1024 * 500 }, (error, stdout) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(stdout.trim());
    });
  }).then((response) => {
    console.log(response);
    console.log('\x1b[32m%s\x1b[0m', `SUCCESSFUL BUILD`, compName);
    console.log(`---------- ${compNumber} ---------- \n`);
  }, error => {
    console.log('\x1b[31m%s\x1b[0m', `${error} \n`);
  });
};


const buildWC = async (comp) => {
  if (comp === 'all') {
    const components = [
      'banner',
      'accordion-resources',
      'items-grid',
      'news',
      'bag-play',
      'indices',
      'feed',
      'links',
      'issuer-details',
      'resources',
      'today',
      'offices',
      'agents-list',
      'home-table',
      'market-table',
      'daily-table',
      'issuers-list-table',
      'entries-table',
      'banner-slider',
      'index-table',
      'banner-movil',
      'resume-chart',
      'details-chart',
      'banner-featured',
      'banner-background',
      'brands',
      'rates',
      'banner-full',
      'text-block',
      'testimonial',
      'article',
      'issuers-news',
      'collapsible-info',
      'agent-details',
      'codification-table',
      'simple-table',
      'banner-side-img',
      'img-info',
      'index-details',
      'lib-index',
      'publications',
      'plans'
    ];

    for (let index = 0; index < components.length; index++) {
      compNumber = `${(index + 1)}/${components.length}`
      await execute(commandLine(components[index]), components[index], compNumber);
    }
  } else {
    await execute(commandLine(comp), comp, compNumber);
  }
}

buildWC(comp);

